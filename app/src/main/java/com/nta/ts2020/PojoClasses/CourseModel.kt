package com.nta.ts2020.PojoClasses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CourseModel (@SerializedName("courses")
                       @Expose
                       var courseModelsLists: Array<CourseModel>,
                       @SerializedName("course_id")
                       @Expose
                       var courseId: String? = null,

                       @SerializedName("course_name")
                       @Expose
                       var courseName: String? = null,

                       @SerializedName("course_image")
                       @Expose
                       var courseImage: String? = null,

                       @SerializedName("course_description")
                       @Expose
                       var courseDescription: String? = null,

                       @SerializedName("status")
                       @Expose
                       var status: String? = null,

                       @SerializedName("visiblity")
                       @Expose
                       var visiblity: String? = null,

                       @SerializedName("android_visibility")
                       @Expose
                       var androidVisibility: String? = null,

                       @SerializedName("institution_id")
                       @Expose
                       var institutionId: String? = null,

                       @SerializedName("batch_id")
                       @Expose
                       var batchid: List<BatchID>? = null
)
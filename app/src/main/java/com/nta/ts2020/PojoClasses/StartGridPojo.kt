package com.nta.ts2020.PojoClasses

/**
 * Created by SiteGalleria on 24-Mar-17.
 */
class StartGridPojo {
    var icon = 0
    var name: String? = null

    constructor() {}
    constructor(icon: Int, name: String?) {
        this.icon = icon
        this.name = name
    }

}
package com.nta.ts2020.PojoClasses

import java.util.*

class QuizSubject(val subId: Int, val subName: String) {
    var isChecked // For checking the quizSubject checkbox is checked or not
            = false

    companion object {
        @JvmField
        var subjectIds = ArrayList<String>() //For returning all quizSubject ids to the server
    }

}
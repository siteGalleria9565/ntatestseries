package com.nta.ts2020.PojoClasses.ParcelPojoClass

import android.os.Parcel
import android.os.Parcelable
import com.nta.ts2020.PojoClasses.NotificationRelatedPostsPojo
import java.util.*

/**
 * Created by suvajit on 3/6/17.
 */
class NotificationDetailsPojo : Parcelable {
    var title: String?
    var description: String?
    var link: String?
    var tag: String?
    var courseId: String?
    var time: String?
    var image: String?
    var file1: String?
    var file1Name: String?
    var file2: String?
    var file2Name: String?
    var file3: String?
    var file3Name: String?
    var notificationDetailsPojos: ArrayList<NotificationRelatedPostsPojo?>? = null

    constructor(title: String?, description: String?, link: String?, tag: String?, courseId: String?, time: String?, image: String?,
                file1: String?, file1Name: String?, file2: String?, file2Name: String?, file3: String?, file3Name: String?, notificationDetailsPojos: ArrayList<NotificationRelatedPostsPojo?>?) {
        this.title = title
        this.description = description
        this.link = link
        this.tag = tag
        this.courseId = courseId
        this.time = time
        this.image = image
        this.file1 = file1
        this.file1Name = file1Name
        this.file2 = file2
        this.file2Name = file2Name
        this.file3 = file3
        this.file3Name = file3Name
        this.notificationDetailsPojos = notificationDetailsPojos
    }

    private constructor(`in`: Parcel) {
        title = `in`.readString()
        description = `in`.readString()
        link = `in`.readString()
        tag = `in`.readString()
        courseId = `in`.readString()
        time = `in`.readString()
        image = `in`.readString()
        file1 = `in`.readString()
        file1Name = `in`.readString()
        file2 = `in`.readString()
        file2Name = `in`.readString()
        file3 = `in`.readString()
        file3Name = `in`.readString()
        if (`in`.readByte().toInt() == 0x01) {
            notificationDetailsPojos = ArrayList()
            `in`.readList(notificationDetailsPojos as List<*>, NotificationDetailsPojo::class.java.classLoader)
        } else {
            notificationDetailsPojos = null
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(title)
        dest.writeString(description)
        dest.writeString(link)
        dest.writeString(tag)
        dest.writeString(courseId)
        dest.writeString(time)
        dest.writeString(image)
        dest.writeString(file1)
        dest.writeString(file1Name)
        dest.writeString(file2)
        dest.writeString(file2Name)
        dest.writeString(file3)
        dest.writeString(file3Name)
        if (notificationDetailsPojos == null) {
            dest.writeByte(0x00.toByte())
        } else {
            dest.writeByte(0x01.toByte())
            dest.writeList(notificationDetailsPojos as List<*>?)
        }
    }

    companion object {
        val CREATOR: Parcelable.Creator<NotificationDetailsPojo?> = object : Parcelable.Creator<NotificationDetailsPojo?> {
            override fun createFromParcel(`in`: Parcel): NotificationDetailsPojo? {
                return NotificationDetailsPojo(`in`)
            }

            override fun newArray(size: Int): Array<NotificationDetailsPojo?> {
                return arrayOfNulls(size)
            }
        }
    }
}
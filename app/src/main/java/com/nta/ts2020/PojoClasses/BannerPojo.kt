package com.nta.ts2020.PojoClasses

data class BannerPojo(var id: String? = null,
                      var background: String? = null,
                      var logo: String? = null,
                      var title: String? = null,
                      var description: String? = null)

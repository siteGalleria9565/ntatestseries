package com.nta.ts2020.PojoClasses
data class ExamAllExaminationDataPojo(var examId : Int= 0 ,
                                      var subject : Int= 0 ,
                                      var examName: String? = null)

package com.nta.ts2020.PojoClasses

class ShowSubjectWiseDataInTablePojo(val subjectName: String, val subjectScore: String, val subjectPercentage: String)
package com.nta.ts2020.PojoClasses

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by suvajit on 23/6/17.
 */
data class SupportPojo(var queryId: String? = null,
                       var queryToken: String? = null,
                       var title: String? = null,
                       var query: String? = null,
                       var email: String? = null,
                       var answer: String? = null,
                       var date: String? = null)
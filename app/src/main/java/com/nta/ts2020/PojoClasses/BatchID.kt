package com.nta.ts2020.PojoClasses

data class BatchID(var batch_id: String? = null,
                   var batch_name: String? = null)

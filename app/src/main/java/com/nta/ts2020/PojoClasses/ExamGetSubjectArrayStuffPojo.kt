package com.nta.ts2020.PojoClasses

data class ExamGetSubjectArrayStuffPojo (var examId: String? = null,
                                         var subjectId: String? = null,
                                         var numberOfQuestions: String? = null,
                                         var subjectNames: String? = null,
                                         var numberOfMarks: String? = null)

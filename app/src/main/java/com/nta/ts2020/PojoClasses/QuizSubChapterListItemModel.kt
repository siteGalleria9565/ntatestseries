package com.nta.ts2020.PojoClasses

import android.animation.TimeInterpolator

class QuizSubChapterListItemModel(val quizSubjectChapterCombined: QuizSubjectChapterCombined, val colorId1: Int, val colorId2: Int, val interpolator: TimeInterpolator)
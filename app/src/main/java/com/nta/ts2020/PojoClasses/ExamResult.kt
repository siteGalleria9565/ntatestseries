package com.nta.ts2020.PojoClasses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ExamResult : Serializable {
    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("subject")
    @Expose
    var subject: String? = null

    @SerializedName("video_id")
    @Expose
    var video_id: String? = null

    @SerializedName("icon")
    @Expose
    var icon: String? = null

}
package com.nta.ts2020.PojoClasses

import java.util.*

class ResultListPojo {
    var examName: String? = null
    var id: String? = null
    var examType: String? = null
    var date: String? = null
    var totalQues = 0
    var totalMarks = 0f
    var attemptedQues = 0
    var correctQues = 0
    var totalObtainedMarks = 0f
    var subjectIds: ArrayList<String>? = null
    var subjectNames: ArrayList<String>? = null

}
package com.nta.ts2020.PojoClasses

import java.util.*

class QuizChapter(val chapId: Int, val chapName: String) {
    var isChecked // For checking the chapter checkbox is checked or not
            = false

    companion object {
        @JvmField
        var chapterIds = ArrayList<String>() //For returning all chapter ids to the server
    }

}
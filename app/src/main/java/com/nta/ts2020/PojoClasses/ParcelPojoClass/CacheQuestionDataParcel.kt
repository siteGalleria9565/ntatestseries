package com.nta.ts2020.PojoClasses.ParcelPojoClass

import android.os.Parcel
import android.os.Parcelable
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import java.util.*

class CacheQuestionDataParcel : Parcelable {
    var localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo?>?

    /**
     * Constructor is used for var Initialization ....
     * @param localQuestionDataBasedOnExamIdList
     */
    constructor(localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo?>?) {
        this.localQuestionDataBasedOnExamIdList = localQuestionDataBasedOnExamIdList
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeList(localQuestionDataBasedOnExamIdList as List<*>?)
    }

    constructor(`in`: Parcel) {
        localQuestionDataBasedOnExamIdList = `in`.readArrayList(ClassLoader.getSystemClassLoader()) as ArrayList<ExamAllQuestionsSubjectWisePojo?>?
    }

    companion object {
        const val TAG = "CacheQuestionDataParcel"
        val CREATOR: Parcelable.Creator<CacheQuestionDataParcel?> = object : Parcelable.Creator<CacheQuestionDataParcel?> {
            override fun createFromParcel(`in`: Parcel): CacheQuestionDataParcel? {
                return CacheQuestionDataParcel(`in`)
            }

            override fun newArray(size: Int): Array<CacheQuestionDataParcel?> {
                return arrayOfNulls(size)
            }
        }
    }
}
package com.nta.ts2020.PojoClasses

data class EBookPojo(var eBookId: String? = null,
                     var eBookPdfName: String? = null,
                     var thumbnail: String? = null,
                     var type: String? = null,
                     var eBookName: String? = null,
                     var eBookAuthor: String? = null,
                     var eBookPublication: String? = null,
                     var eBookLanguage: String? = null,
                     var eBookSubjectId: String? = null,
                     var downloadUrl: String? = null)
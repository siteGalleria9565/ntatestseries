package com.nta.ts2020.PojoClasses

data class EBookSubjectsPojo(var id: String? = null,
                             var name: String? = null)
package com.nta.ts2020.PojoClasses

/**
 * Created by SiteGalleria on 16-Mar-17.
 */
class UserDataPojo {
    var userId: String? = null
    var fName: String? = null
    var lName: String? = null
    var email: String? = null
    var phNo: String? = null
    var profPic: String? = null
    var courseId: String? = null
    var courseName: String? = null
    var classId: String? = null
    var className: String? = null
    var isUserLoggedIn = false
        private set

    fun setUserLoginStatus(userLoginStatus: Boolean) {
        isUserLoggedIn = userLoginStatus
    }
}
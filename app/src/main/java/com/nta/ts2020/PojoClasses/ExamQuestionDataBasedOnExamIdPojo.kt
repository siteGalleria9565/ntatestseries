package com.nta.ts2020.PojoClasses

class ExamQuestionDataBasedOnExamIdPojo {
    var examId: String? = null
    var examName: String? = null
    var totalQuestions: String? = null
    var totalMarks: String? = null
    var timeDurationInSeconds: String? = null
    var instructions: String? = null

    //subject_names with subject id
    lateinit var subjectId //Array
            : Array<String?>
    lateinit var numberOfQuestions //Array
            : Array<String?>
    lateinit var subjectNames //Array
            : Array<String?>
    lateinit var numberOfMarks //Array
            : Array<String?>

}
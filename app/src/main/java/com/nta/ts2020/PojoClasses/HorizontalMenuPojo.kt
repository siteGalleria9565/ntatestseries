package com.nta.ts2020.PojoClasses

class HorizontalMenuPojo {
    var id: String? = null
    var background: String? = null
    var title: String? = null
    var description: String? = null
    var tag: String? = null
    var examType: String? = null

}
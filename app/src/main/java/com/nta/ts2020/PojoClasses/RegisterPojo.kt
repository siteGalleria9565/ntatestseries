package com.nta.ts2020.PojoClasses

class RegisterPojo {
    var fName: String? = null
    var lName: String? = null
    var phNumber: String? = null
    var email: String? = null
    var password: String? = null
    var course: String? = null
    var classe: String? = null
    var city: String? = null
    var state: String? = null
    var currentAddress: String? = null
    var dOB: String? = null
    var referralcode: String? = null

    fun getfName(): String? {
        return fName
    }

    fun setfName(fName: String?) {
        this.fName = fName
    }

    fun getlName(): String? {
        return lName
    }

    fun setlName(lName: String?) {
        this.lName = lName
    }

}
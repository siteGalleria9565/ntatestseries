
package com.nta.ts2020.PojoClasses;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class Clas {

    @SerializedName("class")
    private String _class;
    @SerializedName("id")
    private String id;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

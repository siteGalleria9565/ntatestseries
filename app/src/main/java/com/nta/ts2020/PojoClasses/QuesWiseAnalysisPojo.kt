package com.nta.ts2020.PojoClasses

class QuesWiseAnalysisPojo {
    var quesNo: String? = null
    var status: String? = null
    var yourAns: String? = null
    var correctAns: String? = null
    var yourScore: String? = null
    var yourTime: String? = null
    var topperTime: String? = null

}
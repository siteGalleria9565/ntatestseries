package com.nta.ts2020.PojoClasses

/**
 * Created by    on 8/3/17.
 */
class ShowSubjectWiseTimeAnalysisDataInTablePojo(val subjectName: String, val subjectPercentage: String, val subjectScore: String, val subjectTopperScoreInTime: String)
package com.nta.ts2020.PojoClasses

import com.google.gson.annotations.SerializedName
import java.util.*

class FinalSubmissionPojo {
    /**
     * User id of the student
     *
     * @return Integer user id
     */
    /**
     * User id of the student
     *
     * @param userId Integer user id
     */
    @SerializedName("userId")
    var userId = 0

    /**
     * Exam id of the current exam
     *
     * @return Integer Exam id
     */
    /**
     * Sets the exam id of the current exam
     *
     * @param examId Integer Exam id
     */
    @SerializedName("examId")
    var examId: String? = null

    /**
     * Exam type of the current exam
     *
     * @return String Exam type
     */
    /**
     * Sets exam type of the current exam
     *
     * @param examType String exam type
     */
    @SerializedName("examType")
    var examType: String? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("allottedTime")
    var allottedTime = 0

    @SerializedName("totalQues")
    var totalQues = 0

    @SerializedName("totalMarks")
    var totalMarks = 0

    @SerializedName("subjectIds")
    var subjectIds: ArrayList<String>? = null

    @SerializedName("nosOfQuesSubWise")
    var nosOfQuesSubWise: ArrayList<String>? = null

    @SerializedName("subWiseMaxMarks")
    var subWiseMaxMarks: ArrayList<String>? = null

    @SerializedName("quesIds")
    var quesIds: ArrayList<String>? = null

    @SerializedName("responses")
    var responses: ArrayList<String>? = null

    @SerializedName("subWiseMarks")
    var subWiseMarks: ArrayList<String>? = null

    /**
     * ArrayList of question data with space separated values as <br></br>
     * 1) No. of correct questions, <br></br>
     * 2) No. of Attempted questions, <br></br>
     * 3) No. of skipped questions, <br></br>
     * 4) No. of correct marks, <br></br>
     * 5) No. of negative marks
     *
     * @return Total questions array list with space separated values
     */
    /**
     * ArrayList of question data with space separated values as <br></br>
     * 1) No. of correct questions, <br></br>
     * 2) No. of Attempted questions, <br></br>
     * 3) No. of skipped questions, <br></br>
     * 4) No. of correct marks, <br></br>
     * 5) No. of negative marks
     *
     * @param subWiseQuesData Total questions array list with space separated values
     */
    @SerializedName("subWiseQuesData")
    var subWiseQuesData: ArrayList<String>? = null

    @SerializedName("totalObtainedMarks")
    var totalObtainedMarks = 0f

    /**
     * All question data with space separated values as <br></br>
     * 1) No. of correct questions, <br></br>
     * 2) No. of Attempted questions, <br></br>
     * 3) No. of skipped questions, <br></br>
     * 4) No. of correct marks, <br></br>
     * 5) No. of negative marks
     *
     * @return Total questions data with space separated values
     */
    /**
     * All question data with space separated values as <br></br>
     * 1) No. of correct questions, <br></br>
     * 2) No. of Attempted questions, <br></br>
     * 3) No. of skipped questions, <br></br>
     * 4) No. of correct marks, <br></br>
     * 5) No. of negative marks
     *
     * @param totalQuesData Total questions data with space separated values
     */
    @SerializedName("totalQuesData")
    var totalQuesData: String? = null

    @SerializedName("subWiseTime")
    var subWiseTime: ArrayList<String>? = null

    @SerializedName("quesWiseTime")
    var quesWiseTime: ArrayList<String>? = null

    @SerializedName("overAllTime")
    var overAllTime: Long = 0

    @Transient
    var isSync = false
        private set

    fun setSyncStatus(syncStatus: Boolean) {
        isSync = syncStatus
    }
}
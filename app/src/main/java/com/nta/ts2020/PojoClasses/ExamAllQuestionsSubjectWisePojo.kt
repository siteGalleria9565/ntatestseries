package com.nta.ts2020.PojoClasses

data class ExamAllQuestionsSubjectWisePojo (var subjectId: String? = null,
                                            var questionType: String? = null,
                                            var questionId: String? = null,
                                            var questionText: String? = null,
                                            var optionA: String? = null,
                                            var optionB: String? = null,
                                            var optionC: String? = null,
                                            var optionD: String? = null,
                                            var optionE: String? = null,
                                            var rightAns: String? = null,
                                            var rightMarks: String? = null,
                                            var wrongMarks: String? = null,
                                            var noOptions: String? = null,
                                            var essayId: String? = null,
                                            var essayData: String? = null)
package com.nta.ts2020.PojoClasses

class QuizSetCurrentExamSessionPojo {
    var submitExamID: String? = null
    var examStatus: String? = null
    var currentPosition: String? = null
    var remainingTime: String? = null

}
package com.nta.ts2020.PojoClasses

class NotificationRelatedPostsPojo {
    var id: String? = null
    var title: String? = null
    var date: String? = null
    var image: String? = null
}
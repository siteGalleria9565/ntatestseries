package com.nta.ts2020.PojoClasses

data class ExamGetCurrentExamSessionPojo(var questionId: String? = null,
                                         var response: String? = null,
                                         var timeDuration: String? = null,
                                         var colorStatus: String? = null,

        //added Later -- Suvajit
                                         var rightAns: String? = null)

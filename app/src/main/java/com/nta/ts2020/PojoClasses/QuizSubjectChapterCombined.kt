package com.nta.ts2020.PojoClasses

import java.util.*

class QuizSubjectChapterCombined(var quizSubject: QuizSubject, var quizChapters: ArrayList<QuizChapter>)
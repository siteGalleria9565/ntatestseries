package com.nta.ts2020.PojoClasses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class QuestionPaperPDF protected constructor(`in`: Parcel) : Parcelable {

    @SerializedName("year")
    var year: String? = `in`.readString()

    @SerializedName("paper_title")
    var paper_title: String? = `in`.readString()

    @SerializedName("file_link")
    var file_link: String? = `in`.readString()
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(year)
        dest.writeString(paper_title)
        dest.writeString(file_link)
    }

    companion object {
        val CREATOR: Parcelable.Creator<QuestionPaperPDF?> = object : Parcelable.Creator<QuestionPaperPDF?> {
            override fun createFromParcel(`in`: Parcel): QuestionPaperPDF? {
                return QuestionPaperPDF(`in`)
            }

            override fun newArray(size: Int): Array<QuestionPaperPDF?> {
                return arrayOfNulls(size)
            }
        }
    }

}
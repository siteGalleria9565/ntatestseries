package com.nta.ts2020.PojoClasses

import java.util.*

/**
 * Created by SiteGalleria on 11-Mar-17.
 */

data class CoursePojo( var id: String? = null,
                       var name: String? = null,
                       var classes: ArrayList<ClassPojo>? = null)

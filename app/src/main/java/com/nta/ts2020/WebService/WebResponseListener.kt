package com.nta.ts2020.WebService

interface WebResponseListener {
    fun onError(message: String?)
    fun onResponse(response: Any?, webCallType: WebCallType?)
}
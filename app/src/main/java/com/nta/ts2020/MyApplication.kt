package com.nta.ts2020

import android.app.Application
import com.nta.ts2020.UtilsHelper.FontsOverride

class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        FontsOverride.setDefaultFont(applicationContext, "DEFAULT", "fonts/gilroy_light.otf")
        FontsOverride.setDefaultFont(applicationContext, "MONOSPACE", "fonts/gilroy_bold.otf")
        FontsOverride.setDefaultFont(applicationContext, "SERIF", "fonts/gilroy_light.otf")
        FontsOverride.setDefaultFont(applicationContext, "SANS", "fonts/gilroy_light.otf")
    }
}
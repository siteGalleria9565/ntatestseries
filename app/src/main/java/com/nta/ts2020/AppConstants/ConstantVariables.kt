package com.nta.ts2020.AppConstants

import android.content.Context
import android.content.Intent
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputLayout
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.LocalStorage.CommonAssetToLocalDatabase
import com.nta.ts2020.LocalStorage.ExamAssetToLocalDatabase
import com.nta.ts2020.LocalStorage.QuizAssetToLocalDatabase
import com.nta.ts2020.UtilsHelper.SharedPrefManager

object ConstantVariables {
    //Project constant variables
    const val TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss" //24-hr format date-time
    const val READ_TIMEOUT_VALUE = 15000
    const val CONNECT_TIMEOUT_VALUE = 15000
    const val EXTERNAL_FOLDER_PATH = "/Android/data/com.nta.ts2020/"
    const val E_BOOK = "ebook"
    const val FORMULA_BOOK = "fbook"
    const val DATABASE_VERSION = 1

    //Google SignIn
    const val GOOGLE_REQUEST_TOKEN = "812372678801-n1mg4iang04ipcc38bnrs3ilu1lm0cps.apps.googleusercontent.com" //Presently not in use but can be used if needed in google sign in (Web client Id for OAuth2)

    //Network Activity
    const val SUCCESS = 1
    const val NO_DATA = 2
    const val NETWORK_ERROR = 3
    const val DATABASE_ERROR = 4

    //pref values
    const val USER_DATA = "userdata"

    //Activity type
    const val PERSONAL_DETAILS_ACTIVITY = 100
    const val ACADEMIC_DETAILS_ACTIVITY = 101
    const val SECURITY_ACTIVITY = 102
    const val ADDRESS_ACTIVITY = 103
    const val SELECT_ADDRESS_ACTIVITY = 108
    const val OTHER_ACTIVITY = 104
    const val REGISTER_ACTIVITY = 105
    const val POST_REGISTRATION_ACTIVITY = 106
    const val RESULT_DATA_LIST_ACTIVITY = 107

    //Question download type
    const val EXAM = 0
    const val QUIZ = 1
    const val QUES_ANS_KEY = 2

    //Horizontal Menu Tag
    const val UPCOMING = "Upcoming"
    const val LATEST = "Latest"
    const val RESULT = "ExamResult"
    const val RESUME = "Resume"

    //Policies
    const val PRIVACY_POLICY = "privacy_policy"
    const val TERMS_CONDITIONS = "terms_conditions"
    const val ABOUT_US = "about_us"

    //Server Connection Constant Variables
    const val URL_RESET_PASSWORD = "https://new-examin.examin.co.in/api/reset-password?token=05552f7cc31e136e7d5e063fdc17a7da5f4395cc09d69"
    const val URL_FORGET_PASSWORD = "https://new-examin.examin.co.in/api/forget_pasword"
    const val URL_LOAD_QUESTION_BASED_ON_EXAM_ID_FROM_SERVER = "https://www.examin.co.in/myaccount/dashboard2/exam-api/exam_for_android3.php"
    const val URL_QUIZ_ANS_KEY_LOAD_QUES = "https://www.examin.co.in/myaccount/dashboard/exams/answer_key.php"
    const val URL_OF_All_EXAMINATION_DATA_FROM_SERVER = "https://www.examin.co.in/myaccount/dashboard/android/android_user_exam_list_req.php"
    const val URL_GET_RESULT_LIST = "https://www.examin.co.in/myaccount/dashboard/exam_list_for_android.php"
    const val URL_GET_RESULT_DATA = "https://www.examin.co.in/myaccount/dashboard/result_for_android.php"
    const val URL_GET_COURSE_CLASS_LIST = "https://www.examin.co.in/myaccount/dashboard/courses_for_android_v2.php"
    const val URL_EXAM_FINAL_SUBMISSION = "https://www.examin.co.in/myaccount/dashboard/save_android_user_response.php"
    const val URL_UPDATE_PROFILE = "https://new-examin.examin.co.in/api/update_profile"

    const val URL_LOAD_PROFILE = "https://www.examin.co.in/myaccount/dashboard/get_android_user_profile.php"
    const val URL_SYNC_CONTACTS = "https://www.examin.co.in/myaccount/dashboard/sync_android_user_contacts.php"
    const val URL_UPDATE_FCM_TOKEN = "https://www.examin.co.in/myaccount/dashboard/save_android_token.php"
    const val URL_UPLOAD_PROF_IMAGE = "https://www.examin.co.in/myaccount/dashboard/upload_android_image.php"
    const val URL_SOCIAL_LOGIN = "https://www.examin.co.in/myaccount/dashboard/google-signin.php"
    const val URL_SOCIAL_SAVE_COURSES = "https://www.examin.co.in/myaccount/dashboard/save_course_for_android.php"
    const val URL_E_BOOK_SUBJECTS_LIST = "https://www.examin.co.in/myaccount/dashboard/users_subject_list_for_android.php"
    const val URL_E_BOOK_LIST = "https://www.examin.co.in/myaccount/dashboard/fetch_ebook.php"
    const val URL_PDF_PREVIOUS_YEAR_QUESTION_PAPER = "https://www.examin.co.in/myaccount/dashboard/exams/previous_year_papers.php"
    const val URL_TOPPER = "https://www.examin.co.in/myaccount/dashboard/top_std_results_for_android.php"
    const val URL_COURSE = "https://www.examin.co.in/myaccount/dashboard/exams/course_for_android.php"
    const val URL_LOAD_SCHOLARSHIPS = " https://www.examin.co.in/myaccount/dashboard/exams/Android_scholarships.php"
    const val URL_ADD_NEW_ADDRESS = "https://www.examin.co.in/myaccount/dashboard/exams/Android_user_shipping_addr.php"
    const val URL_LOAD_QUIZ_QUESTION_BASED_ON_EXAM_ID_FROM_SERVER = "https://www.examin.co.in/myaccount/dashboard/exams/questions_for_quiz.php"
    const val URL_LOAD_QUIZ_QUESTION_BASED_ON_LANGUAGE_FROM_SERVER = "https://www.examin.co.in/myaccount/dashboard/exams/questions_for_quiz_language_based.php"
    const val URL_LOAD_QUIZ_SUBJECT_CHAPTER_FROM_SERVER = "https://www.examin.co.in/examin-admin/include/android_quiz_sub_list_req.php"
    const val URL_REGISTRATION = "https://new-examin.examin.co.in/api/auth/signup"
    const val URL_LOGIN = "https://new-examin.examin.co.in/api/auth/login"
    const val URL_NOTIFICATIONS = "https://www.examin.co.in/examin-admin/include/all_new_notification.php"
    const val URL_NOTIFICATIONS_DETAILS = "https://www.examin.co.in/examin-admin/include/notifications_data.php"
    const val URL_SUPPORT_LIST = "https://www.examin.co.in/myaccount/dashboard/query_list_for_android.php"
    const val URL_SUPPORT_INSERT = "https://www.examin.co.in/myaccount/dashboard/insert_query_for_android.php"
    const val URL_BANNER = "https://www.examin.co.in/myaccount/dashboard/banners.php"
    const val URL_HORIZONTAL_MENU = "https://www.examin.co.in/myaccount/dashboard/android/upcomming_latest_exams_for_android.php"
    const val URL_LOGOUT = "https://new-examin.examin.co.in/logout"
    const val URL_OTP_VERIFICATION = "https://new-examin.examin.co.in/api/auth/verify_account"
    const val URL_GET_COURSE_CLASS = "https://www.examin.co.in/myaccount/dashboard/courses_for_android_v2.php"

    //User Profile Database table names for jason
    const val EX_STUDENT_USER = "ex_student_user"
    const val STUDENT_PERSONAL_DETAILS = "student_persnal_details"
    const val URL_GET_NEW_ADDRESS = "https://www.examin.co.in/myaccount/dashboard/exams/Android_get_user_shipping_addr.php"

    const val OTP = "otp"
    const val IS_VERIFIED = "is_verified"
    const val COURSES_CLASSES = "courses_classes"

    //Logout user
    @JvmStatic
    fun logout(context: Context): Boolean {
        //Clear all cached data
        CommonAssetToLocalDatabase.clearAllTables(context)
        ExamAssetToLocalDatabase.clearAllTables(context)
        QuizAssetToLocalDatabase.clearAllTables(context)

        //To send fcm token again after login
        val sharedPrefManager = SharedPrefManager(context)
        sharedPrefManager.isFcmTokenSynced = false
        context.startActivity(Intent(context, ExamLoginScreenActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        return true
    }

    //Global validator for password -- Start
    private const val MIN_LENGTH = 8
    private const val INVALID_PASS_MSG = "Password must contain at least $MIN_LENGTH characters"

    //Validation logic
    private fun validator(password: String): Boolean {
        return password.length >= MIN_LENGTH
    }
    @JvmStatic
    fun isValidPassword(password: String, passwordContainer: TextInputLayout): Boolean {
        val isValid = validator(password)
        if (!isValid) passwordContainer.error = INVALID_PASS_MSG
        return isValid
    }

    @JvmStatic
    fun isValidPassword(password: String): Boolean {
        return validator(password)
    }

    @JvmStatic
    fun isValidPassword(tvNewPass: EditText): Boolean {
        val isValid = validator(tvNewPass.text.toString().trim { it <= ' ' })
        if (!isValid) tvNewPass.error = INVALID_PASS_MSG
        return isValid
    } //Global validator for password -- End

}
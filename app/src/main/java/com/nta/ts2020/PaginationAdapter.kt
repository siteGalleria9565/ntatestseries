package com.nta.ts2020

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.nta.ts2020.PojoClasses.ExamResult
import com.nta.ts2020.Activities.ExamSubjectVideoListActivity
import com.nta.ts2020.Activities.ExamYoutubePlay
import org.json.JSONException
import org.json.JSONObject
import java.util.*

open class PaginationAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val movieResults: MutableList<ExamResult>?
    private var isLoadingAdded = false
    private var retryPageLoad = false
    private var errorMsg: String? = null
    var titleURL: String? = null
    var fortitleURL: String? = null
    var title: String? = null
    var videotitle: String? = null
    var authorname: String? = null
    var Position = 0
    var requestQueue: RequestQueue? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            ITEM -> {
                val viewItem = inflater.inflate(R.layout.recycleradaptermain, parent, false)
                viewHolder = MovieViewHolder(viewItem)
            }
            LOADING -> viewHolder = if (viewType == ExamSubjectVideoListActivity.TotalPages) {
                val viewLoading = inflater.inflate(R.layout.item_progress_none, parent, false)
                LoadingViewHolder(viewLoading)
            } else {
                val viewLoading = inflater.inflate(R.layout.item_progress, parent, false)
                LoadingViewHolder(viewLoading)
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val result = movieResults!![position] // Movie
        when (getItemViewType(position)) {
            ITEM -> {
                val movieViewHolder = holder as MovieViewHolder
                Position = position
                fortitleURL = "https://www.youtube.com/watch?v=" + result.video_id
                titleURL = "http://www.youtube.com/oembed?url=$fortitleURL"
                requestQueue = Volley.newRequestQueue(context)
                val objectRequest: StringRequest = object : StringRequest(Method.GET, titleURL,
                        Response.Listener { response ->
                            try {
                                val jsonObject = JSONObject(response)
                                title = jsonObject.getString("title")
                                title = title?.substring(0, 1)?.toUpperCase() + title?.substring(1)?.toLowerCase()
                                videotitle = title
                                /*
                                                    movieViewHolder.titlename.setText(result.getTitle());


                                                    Glide.with(context)
                                                            .load("http://examin.co.in/admin-panel/youtube_videos_icon/"+result.getIcon())
                                                            .into(movieViewHolder.thumnil);
                */movieViewHolder.titlename.text = title
                                title = jsonObject.getString("thumbnail_url")
                                Glide.with(context)
                                        .load(title)
                                        .into(movieViewHolder.thumnil)
                            } catch (e: JSONException) {
                                Log.e("json parse pro", "error")
                                e.printStackTrace()
                            }
                        },
                        Response.ErrorListener { error -> Log.e("erorr::", error.toString()) }) {
                    override fun getParams(): Map<String, String> {
                        return HashMap()
                    }
                }
                requestQueue?.add(objectRequest)
                /*
                movieViewHolder.titlename.setText(result.getTitle());


                Glide.with(context)
                        .load("http://examin.co.in/admin-panel/youtube_videos_icon/"+result.getIcon())
                        .into(movieViewHolder.thumnil);*/movieViewHolder.thumnil.setOnClickListener {
                    val i = Intent(context, ExamYoutubePlay::class.java)
                    i.putExtra("id", result.video_id)
                    context.startActivity(i)
                }
            }
            LOADING -> {
                val loadingViewHolder = holder as LoadingViewHolder
                if (retryPageLoad) {
                    loadingViewHolder.linearLayout.visibility = View.VISIBLE
                    loadingViewHolder.progressBar.visibility = View.GONE
                    loadingViewHolder.textView.text = if (errorMsg != null) errorMsg else context.getString(R.string.error_msg_unknown)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return movieResults?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == movieResults!!.size - 1 && isLoadingAdded) LOADING else ITEM
    }

    fun add(r: ExamResult) {
        movieResults!!.add(r)
        notifyItemInserted(movieResults.size - 1)
    }

    fun remove(r: ExamResult?) {
        val position = movieResults!!.indexOf(r)
        if (position > -1) {
            movieResults.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        isLoadingAdded = false
        while (itemCount > 0) {
            remove(getItem(0))
        }
    }

    val isEmpty: Boolean
        get() = itemCount == 0

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(ExamResult())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = movieResults!!.size - 1
        val result = getItem(position)
        movieResults.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getItem(position: Int): ExamResult {
        return movieResults!![position]
    }

    fun showRetry(show: Boolean, errorMsg: String?) {
        retryPageLoad = show
        notifyItemChanged(movieResults!!.size - 1)
        if (errorMsg != null) this.errorMsg = errorMsg
    }

    fun addAll(results: List<ExamResult>) {
        for (result in results) {
            add(result)
        }
    }

    protected class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var RecyclercardView: CardView = itemView.findViewById(R.id.RecyclercardView)
        var thumnil: ImageView = itemView.findViewById(R.id.thumnil)
        var titlename: TextView = itemView.findViewById(R.id.titlename)

    }

    protected inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val progressBar: ProgressBar = itemView.findViewById<View>(R.id.loadmore_progress) as ProgressBar
        private val imageButton: ImageButton = itemView.findViewById<View>(R.id.loadmore_retry) as ImageButton
        val textView: TextView = itemView.findViewById<View>(R.id.loadmore_errortxt) as TextView
        val linearLayout: LinearLayout = itemView.findViewById<View>(R.id.loadmore_errorlayout) as LinearLayout
        override fun onClick(view: View) {
            when (view.id) {
                R.id.loadmore_retry, R.id.loadmore_errorlayout -> showRetry(false, null)
            }
        }

    }

    companion object {
        private const val ITEM = 0
        private const val LOADING = 1
        private const val BASE_URL_IMG = ""
    }

    init {
        movieResults = ArrayList()
    }
}
package com.nta.ts2020.Fragments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.nta.ts2020.Adapters.MyResultListDataRecyclerAdapter
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.QUIZ
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.LocalStorage.CacheResultList
import com.nta.ts2020.PojoClasses.ResultListPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.SaveResultListFromServerToCacheHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import java.util.*

class QuizResultListFragment : Fragment() {
    //List View Object
    private var recyclerView: RecyclerView? = null

    //for Set Content In recyclerView
    var customList: MyResultListDataRecyclerAdapter? = null

    // Collections ....
    var resultListPojoArrayList: ArrayList<ResultListPojo>? = null

    //toolbar
    var setToolbar: Toolbar? = null

    //Log
    private val TAG = "ExamResultFragment"

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private val extras: Bundle? = null
    private val resumeExamId = 0
    var examStatus: String? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var linLayoutNoData: LinearLayout? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var imageViewError: ImageView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_quiz_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = getView()!!.findViewById<View>(R.id.rvQuizResultList) as RecyclerView
        //Message Layout
        linLayoutNoData = getView()!!.findViewById<View>(R.id.llQuizResultListNoData) as LinearLayout
        textViewMessage = getView()!!.findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = getView()!!.findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        imageViewError = getView()!!.findViewById<View>(R.id.ivExamResultListError) as ImageView
        swipeRefreshLayout = getView()!!.findViewById<View>(R.id.swQuizRefreshResultList) as SwipeRefreshLayout
        swipeRefreshLayout!!.setColorSchemeResources(
                R.color.material_blue_700,
                R.color.material_green_700,
                R.color.material_yellow_700,
                R.color.material_red_700)
        val saveResultListFromServerToCacheHandler = SaveResultListFromServerToCacheHandler(this, QUIZ)
        //        if (isNetworkConnected()) {
//            swipeRefreshLayout.setRefreshing(true);
//            new ThreadPoolService().saveResultListFromServerToCache(context, saveResultListFromServerToCacheHandler);
//        }
        swipeRefreshLayout!!.setOnRefreshListener {
            if (isNetworkConnected) {
                ThreadPoolService().saveResultListFromServerToCache(context, saveResultListFromServerToCacheHandler)
                //                    new ThreadPoolService().saveResultDataFromServerToCache(ExamResultDataListActivity.this, null);
            } else {
                loadData(SUCCESS)
            }
        }
        if (!isNetworkConnected) loadData(SUCCESS)
    }

    fun loadData(status: Int) {


        //fetch data from cache
        var status = status
        resultListPojoArrayList = CacheResultList(context!!).getResultListFromCache(QUIZ)
        if (resultListPojoArrayList!!.isEmpty() && status != NETWORK_ERROR) status = NO_DATA
        if (status == SUCCESS) {
            linLayoutNoData!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE
            customList = examStatus?.let { MyResultListDataRecyclerAdapter(context!!, resultListPojoArrayList!!, it, resumeExamId) }
            recyclerView!!.layoutManager = LinearLayoutManager(context)
            //        recyclerView.addItemDecoration(new QuizDividerItemDecoration(this));
            recyclerView!!.adapter = customList
        } else if (status == NO_DATA) {
            linLayoutNoData!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
            textViewMessage!!.setText(R.string.result_list_message_no_data)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_no_data)
            imageViewError!!.setImageResource(R.drawable.ic_no_data)
        } else if (status == NETWORK_ERROR) {
            linLayoutNoData!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
            textViewMessage!!.setText(R.string.result_list_message_network_error)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_network_error)
            imageViewError!!.setImageResource(R.drawable.ic_no_network)
        }
        if (swipeRefreshLayout!!.isRefreshing) {
            swipeRefreshLayout!!.isRefreshing = false
        }
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }
}
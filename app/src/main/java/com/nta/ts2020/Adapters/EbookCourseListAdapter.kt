package com.nta.ts2020.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.R

class EbookCourseListAdapter(var context: Context, var onClickPaper: OnClickPaper, var mListName: List<String>?, var mListYear: List<String>?, var names: String) : RecyclerView.Adapter<EbookCourseListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.ebook_subjects_list_row, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        if (mListName != null) {
            val name = mListName!![i]
            viewHolder.name.text = name
            viewHolder.name.setOnClickListener { onClickPaper.onSelectPaper(name, viewHolder.adapterPosition) }
        }
        if (mListYear != null) {
            val year = mListYear!![i]
            viewHolder.name.text = year
            viewHolder.name.setOnClickListener { onClickPaper.onSelectYear(year, names) }
        }
    }

    override fun getItemCount(): Int {
        if (mListName != null) return mListName!!.size
        return if (mListYear != null) mListYear!!.size else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById<View>(R.id.tvEBookName) as TextView

    }

    interface OnClickPaper {
        fun onSelectPaper(name: String?, position: Int)
        fun onSelectYear(year: String?, name: String?)
    }

}
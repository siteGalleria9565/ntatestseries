package com.nta.ts2020.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.PojoClasses.SupportPojo
import com.nta.ts2020.R
import java.util.*

class SupportRecyclerAdapter(private val supportPojoArrayList: ArrayList<SupportPojo>) : RecyclerView.Adapter<SupportRecyclerAdapter.ViewHolder>() {
    interface OnItemClickListener {
        fun onClick(supportPojo: SupportPojo?)
    }

    private var onItemClickListener: OnItemClickListener? = null
    private var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.activity_support_row, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val supportPojo = supportPojoArrayList[position]
        holder.title.text = supportPojo.title
        holder.queryToken.text = supportPojo.queryToken
        holder.query.text = supportPojo.query
        holder.timeStamp.text = supportPojo.date

        //Set status color to denote the query is resolved or not
        if (supportPojo.answer == null || supportPojo.answer == "") holder.replyIndicator.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_500)) else holder.replyIndicator.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_500))
        holder.rowHolder.setOnClickListener { onItemClickListener!!.onClick(supportPojo) }
    }

    override fun getItemCount(): Int {
        return supportPojoArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById<View>(R.id.tvSupportTittle) as TextView
        var queryToken: TextView = itemView.findViewById<View>(R.id.tvSupportQueryToken) as TextView
        var query: TextView = itemView.findViewById<View>(R.id.tvSupportQuery) as TextView
        var timeStamp: TextView = itemView.findViewById<View>(R.id.tvSupportTimeStamp) as TextView
        var replyIndicator: View = itemView.findViewById(R.id.supportIndicator)
        var rowHolder: RelativeLayout = itemView.findViewById<View>(R.id.supportHolder) as RelativeLayout

    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

}
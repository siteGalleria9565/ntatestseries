package com.nta.ts2020.Adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.PojoClasses.ShowSubjectWiseTimeAnalysisDataInTablePojo
import de.codecrafters.tableview.TableDataAdapter
import java.util.*

/**
 * Created by suvajit on 3/7/17.
 */
class ResultTimeAnalysisAdapter(context: AppCompatActivity?, productList: ArrayList<ShowSubjectWiseTimeAnalysisDataInTablePojo>?) : TableDataAdapter<ShowSubjectWiseTimeAnalysisDataInTablePojo?>(context, productList) {
    override fun getCellView(rowIndex: Int, columnIndex: Int, parentView: ViewGroup): View {
        val rowData = getRowData(rowIndex)
        var view: View? = null
        when (columnIndex) {
            0 -> view = renderView(rowData!!.subjectName)
            1 -> view = renderView(rowData!!.subjectPercentage)
            2 -> view = renderView(rowData!!.subjectScore)
            3 -> view = renderView(rowData!!.subjectTopperScoreInTime)
        }
        return view!!
    }

    private fun renderView(data: String): View {
        val textView = TextView(context)
        textView.text = data
        textView.setPadding(20, 10, 20, 10)
        textView.textSize = 14f
        return textView
    }
}
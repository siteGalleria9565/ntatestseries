package com.nta.ts2020.Adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.nta.ts2020.PojoClasses.ShowSubjectWiseDataInTablePojo
import com.nta.ts2020.R
import java.util.*

class ShowTableSubjectWiseDataAdapter(var activity: Activity, var productList: ArrayList<ShowSubjectWiseDataInTablePojo>) : BaseAdapter() {
    override fun getCount(): Int {
        return productList.size
    }

    override fun getItem(position: Int): Any {
        return productList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private inner class ViewHolder {
        var subjectName: TextView? = null
        var subjectScore: TextView? = null
        var subjectPercentage: TextView? = null
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        val convertView = convertView
        val holder: ViewHolder
        val inflater = activity.layoutInflater
        holder = convertView.tag as ViewHolder
        val item = productList[position]
        holder.subjectName!!.text = item.subjectName
        holder.subjectScore!!.text = item.subjectScore
        holder.subjectPercentage!!.text = item.subjectPercentage
        return convertView
    }

}
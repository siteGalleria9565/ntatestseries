package com.nta.ts2020.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.nta.ts2020.PojoClasses.EBookPojo
import com.nta.ts2020.R
import com.squareup.picasso.Picasso
import java.util.*

class EBookListRecyclerAdapter(private val eBookPojoArrayList: ArrayList<EBookPojo>, private val onButtonClickListener: OnButtonClickListener) : BaseAdapter() {
    interface OnButtonClickListener {
        fun onDownloadButtonClick(eBookPojo: EBookPojo?, viewHolder: ViewHolder?)
        fun onOpenButtonClick(eBookPojo: EBookPojo?)
        fun onViewInfoButtonClick(eBookPojo: EBookPojo?)
    }

    override fun getCount(): Int {
        return eBookPojoArrayList.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val context = parent.context
        var viewHolder: ViewHolder
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.context).inflate(R.layout.ebook_list_row, parent, false)
            viewHolder = ViewHolder()
            viewHolder.thumbNail = convertView.findViewById<View>(R.id.ivEBookThumbNail) as ImageView
            viewHolder.name = convertView.findViewById<View>(R.id.tvEBookName) as TextView
            viewHolder.language = convertView.findViewById<View>(R.id.tvEBookLanguage) as TextView
            // viewHolder.download = (Button) convertView.findViewById(R.id.btEBookDownload);
            viewHolder.open = convertView.findViewById<View>(R.id.btEBookOpen) as Button
            viewHolder.viewInfo = convertView.findViewById<View>(R.id.btEBookViewInfo) as Button
            convertView.tag = viewHolder
        }
        viewHolder = convertView?.tag as ViewHolder
        val eBookPojo = eBookPojoArrayList[position]
        val thumbnail = eBookPojo.thumbnail
        if (thumbnail != null && thumbnail != "") Picasso.with(parent.context).load(thumbnail)
        if (eBookPojo.eBookName != "null") {
            viewHolder.name!!.text = eBookPojo.eBookName
        } else {
            viewHolder.name!!.text = eBookPojo.eBookPdfName
        }
        viewHolder.language!!.text = eBookPojo.eBookLanguage
        val finalViewHolder = viewHolder
        //final File pdf = new File(Environment.getExternalStorageDirectory() + EXTERNAL_FOLDER_PATH + ".pdf/" + eBookPojo.getEBookId());
//        if (pdf.exists()) {
//            viewHolder.open.setVisibility(View.VISIBLE);
//            viewHolder.download.setVisibility(View.GONE);
//        }
//        viewHolder.download.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onButtonClickListener.onDownloadButtonClick(eBookPojo, finalViewHolder);
//            }
//        });
        viewHolder.open!!.setOnClickListener { onButtonClickListener.onOpenButtonClick(eBookPojo) }
        viewHolder.viewInfo!!.setOnClickListener { onButtonClickListener.onViewInfoButtonClick(eBookPojo) }
        return convertView
    }

    class ViewHolder {
        var thumbNail: ImageView? = null
        var name: TextView? = null
        var language: TextView? = null
        var open: Button? = null
        var viewInfo: Button? = null
    }

}
package com.nta.ts2020.Adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nta.ts2020.PojoClasses.ExamGetCurrentExamSessionPojo
import de.codecrafters.tableview.TableDataAdapter

class ResultQuesWiseAnalysisAdapter(context: Context?, data: List<ExamGetCurrentExamSessionPojo?>?) : TableDataAdapter<ExamGetCurrentExamSessionPojo?>(context, data) {
    override fun getCellView(rowIndex: Int, columnIndex: Int, parentView: ViewGroup): View {
        val tempExamSessionPojo = getRowData(rowIndex)
        var renderedView: View? = null
        when (columnIndex) {
            0 -> renderedView = renderQuestionNo(rowIndex + 1)
            1 -> renderedView = renderStatus(tempExamSessionPojo)
            2 -> renderedView = renderYourAns(tempExamSessionPojo)
            3 -> renderedView = renderRightAns(tempExamSessionPojo)
            4 -> renderedView = renderYourScore(tempExamSessionPojo)
            5 -> renderedView = renderYourTime(tempExamSessionPojo)
            6 -> renderedView = renderTopperTime(tempExamSessionPojo)
        }
        return renderedView!!
    }

    //TODO:Remove Static data with dynamic data
    private fun renderTopperTime(tempExamSessionPojo: ExamGetCurrentExamSessionPojo?): View {
        val tempTextView = TextView(context)
        tempTextView.text = "0"
        return tempTextView
    }

    private fun renderYourTime(tempExamSessionPojo: ExamGetCurrentExamSessionPojo?): View {
        val tempTextView = TextView(context)
        tempTextView.text = tempExamSessionPojo!!.timeDuration
        return tempTextView
    }

    private fun renderYourScore(tempExamSessionPojo: ExamGetCurrentExamSessionPojo?): View {
        val tempTextView = TextView(context)
        tempTextView.text = "0"
        return tempTextView
    }

    private fun renderRightAns(tempExamSessionPojo: ExamGetCurrentExamSessionPojo?): View {
        val tempTextView = TextView(context)
        tempTextView.text = tempExamSessionPojo!!.rightAns
        return tempTextView
    }

    private fun renderYourAns(tempExamSessionPojo: ExamGetCurrentExamSessionPojo?): View {
        val tempTextView = TextView(context)
        tempTextView.text = tempExamSessionPojo!!.response
        return tempTextView
    }

    private fun renderStatus(tempExamSessionPojo: ExamGetCurrentExamSessionPojo?): View {
        val tempTextView = TextView(context)
        //Todo: fix correct and incorrect
        if (tempExamSessionPojo!!.response == "") {
            tempTextView.text = "Not Attempted"
        } else {
            tempTextView.text = "incorrect"
        }
        return tempTextView
    }

    private fun renderQuestionNo(rowIndex: Int): View {
        val tempTextView = TextView(context)
        tempTextView.text = rowIndex.toString()
        return tempTextView
    }
}
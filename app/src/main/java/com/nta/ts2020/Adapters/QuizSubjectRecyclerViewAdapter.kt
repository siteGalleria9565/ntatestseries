package com.nta.ts2020.Adapters

import android.animation.ObjectAnimator
import android.content.Context
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.aakira.expandablelayout.ExpandableLayout
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter
import com.github.aakira.expandablelayout.ExpandableLinearLayout
import com.github.aakira.expandablelayout.Utils
import com.nta.ts2020.PojoClasses.QuizSubChapterListItemModel
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.QuizDividerItemDecoration

class QuizSubjectRecyclerViewAdapter(private val data: List<QuizSubChapterListItemModel>) : RecyclerView.Adapter<QuizSubjectRecyclerViewAdapter.ViewHolder>() {
    private var context: Context? = null
    private val expandState = SparseBooleanArray()
    private var chapterListViewAdapter: QuizChapterRecyclerViewAdapter? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.quiz_selection_list_row, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.setIsRecyclable(false)
        holder.subNameCheckBox.text = item.quizSubjectChapterCombined.quizSubject.subName
        chapterListViewAdapter = QuizChapterRecyclerViewAdapter(item.quizSubjectChapterCombined,
                holder.headingLayout, item.colorId1) // Also passing itemView,quizSubject and color to set color to perform operation on checked chapter cases
        holder.chaptersList.layoutManager = LinearLayoutManager(context)
        holder.chaptersList.addItemDecoration(QuizDividerItemDecoration(context!!))
        holder.chaptersList.adapter = chapterListViewAdapter
        holder.subNameCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
            item.quizSubjectChapterCombined.quizSubject.isChecked = isChecked
            for (i in 0 until holder.chaptersList.childCount) {
                val cbChapter = holder.chaptersList.getChildAt(i).findViewById<View>(R.id.checkBoxChapName) as CheckBox
                cbChapter.isChecked = item.quizSubjectChapterCombined.quizSubject.isChecked
            }
        }
        holder.subNameCheckBox.isChecked = item.quizSubjectChapterCombined.quizSubject.isChecked
        holder.headingLayout.setBackgroundColor(ContextCompat.getColor(context!!, item.colorId1))
        holder.expandableLayout.setInRecyclerView(true)
        holder.expandableLayout.setBackgroundColor(ContextCompat.getColor(context!!, item.colorId2))
        holder.expandableLayout.setInterpolator(item.interpolator)
        holder.expandableLayout.isExpanded = expandState[position]
        holder.expandableLayout.setListener(object : ExpandableLayoutListenerAdapter() {
            override fun onPreOpen() {
                createRotateAnimator(holder.buttonLayout, 0f, 180f).start()
                expandState.put(position, true)
            }

            override fun onPreClose() {
                createRotateAnimator(holder.buttonLayout, 180f, 0f).start()
                expandState.put(position, false)
            }
        })
        holder.buttonLayout.rotation = if (expandState[position]) 180f else 0f
        holder.menuRowLayout.setOnClickListener { onClickButton(holder.expandableLayout) }
    }

    private fun onClickButton(expandableLayout: ExpandableLayout) {
        expandableLayout.toggle()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var subNameCheckBox: CheckBox = v.findViewById<View>(R.id.checkBoxSubject) as CheckBox
        var buttonLayout: RelativeLayout = v.findViewById<View>(R.id.relativeLayoutButtonHolder) as RelativeLayout
        var headingLayout: RelativeLayout = v.findViewById<View>(R.id.relativeLayoutParagraphHeading) as RelativeLayout
        var menuRowLayout: RelativeLayout = v.findViewById<View>(R.id.relativeLayoutMenuRow) as RelativeLayout
        var chaptersList: RecyclerView = v.findViewById<View>(R.id.recyclerViewChapters) as RecyclerView

        /**
         * You must use the ExpandableLinearLayout in the recycler view.
         * The ExpandableRelativeLayout doesn't work.
         */
        var expandableLayout: ExpandableLinearLayout = v.findViewById<View>(R.id.expandableLayout) as ExpandableLinearLayout
        override fun onClick(v: View) {}

    }

    fun createRotateAnimator(target: View?, from: Float, to: Float): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(target, "rotation", from, to)
        animator.duration = 300
        animator.interpolator = Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR)
        return animator
    }

    init {
        for (i in data.indices) {
            expandState.append(i, false)
        }
    }
}
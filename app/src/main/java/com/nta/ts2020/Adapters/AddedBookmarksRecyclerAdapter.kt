package com.nta.ts2020.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.NestedScrollWebView
import java.util.*

class AddedBookmarksRecyclerAdapter(private val eBookSubjectsPojoArrayList: ArrayList<ExamAllQuestionsSubjectWisePojo>) : RecyclerView.Adapter<AddedBookmarksRecyclerAdapter.ViewHolder>() {
    interface OnItemClickListener {
        fun onClick(examAllQuestionsSubjectWisePojo: ExamAllQuestionsSubjectWisePojo?)
        fun onDeleteButtonClick(examAllQuestionsSubjectWisePojo: ExamAllQuestionsSubjectWisePojo?)
    }

    private var onItemClickListener: OnItemClickListener? = null
    private var context: Context? = null
    private var count = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.bookmarks_list_row, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val examAllQuestionsSubjectWisePojo = eBookSubjectsPojoArrayList[position]
        holder.questionText.loadData(Html.fromHtml(examAllQuestionsSubjectWisePojo.questionText!!.trim { it <= ' ' }).toString(), "text/html", "UTF-8")
        holder.questionText.setOnTouchListener { view, motionEvent ->
            var isSingleTap = true
            if (motionEvent.action == 2) {
                count++
            }
            if (count > 3) isSingleTap = false
            if (view.id == R.id.wvBookmarkName && motionEvent.action == MotionEvent.ACTION_UP) count = 0
            if (view.id == R.id.wvBookmarkName && motionEvent.action == MotionEvent.ACTION_UP && isSingleTap) {
                onItemClickListener!!.onClick(examAllQuestionsSubjectWisePojo)
            }
            false
        }
        holder.llDelButtonContainer.setOnClickListener { onItemClickListener!!.onDeleteButtonClick(examAllQuestionsSubjectWisePojo) }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemCount(): Int {
        return eBookSubjectsPojoArrayList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var questionText: NestedScrollWebView = itemView.findViewById<View>(R.id.wvBookmarkName) as NestedScrollWebView
        var llDelButtonContainer: LinearLayout = itemView.findViewById<View>(R.id.llBookmarksRowDelContainer) as LinearLayout

    }

}
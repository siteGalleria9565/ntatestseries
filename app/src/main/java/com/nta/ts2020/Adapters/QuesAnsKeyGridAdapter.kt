package com.nta.ts2020.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.nta.ts2020.R
import java.util.*

class QuesAnsKeyGridAdapter(private val mContext: Context, private val responses: ArrayList<String>) : BaseAdapter() {
    override fun getCount(): Int {
        return responses.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        val convertView = convertView
        val holder: ViewHolder
        holder = convertView.tag as ViewHolder
        holder.title!!.text = (position + 1).toString()
        if (responses[position] != "0") {
            holder.title!!.setBackgroundResource(R.drawable.gridview_round_teal)
        } else {
            holder.title!!.setBackgroundResource(R.drawable.gridview_round_blue)
        }
        return convertView
    }

    private inner class ViewHolder {
        var title: TextView? = null
    }

}
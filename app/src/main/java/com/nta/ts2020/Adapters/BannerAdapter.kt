package com.nta.ts2020.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.PojoClasses.BannerPojo
import com.nta.ts2020.R
import com.squareup.picasso.Picasso
import java.util.*

class BannerAdapter(private val bannerPojoArrayList: ArrayList<BannerPojo>) : RecyclerView.Adapter<BannerAdapter.ViewHolder>() {
    private var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.activity_dashboard_banner_row, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (_, background, logo, title, description) = bannerPojoArrayList[position]
        Picasso.with(context).load(logo).into(holder.logo)
        Picasso.with(context).load(background).into(holder.bankground)
        holder.tittle.text = title
        holder.description.text = description
    }

    override fun getItemCount(): Int {
        return bannerPojoArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val bankground: ImageView = itemView.findViewById<View>(R.id.ivBannerBanner) as ImageView
        val logo: ImageView = itemView.findViewById<View>(R.id.tvBannerLogo) as ImageView
        val tittle: TextView = itemView.findViewById<View>(R.id.tvBannerTittle) as TextView
        val description: TextView = itemView.findViewById<View>(R.id.tvBannerDescription) as TextView

    }

}
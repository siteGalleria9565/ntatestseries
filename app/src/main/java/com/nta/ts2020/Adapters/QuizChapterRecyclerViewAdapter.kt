package com.nta.ts2020.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.PojoClasses.QuizChapter
import com.nta.ts2020.PojoClasses.QuizSubject
import com.nta.ts2020.PojoClasses.QuizSubjectChapterCombined
import com.nta.ts2020.R

class QuizChapterRecyclerViewAdapter(var quizSubjectChapterCombined: QuizSubjectChapterCombined, private val parentExpandableLayout: View,
                                     private val parentExpandableLayoutDefaultColor: Int) : RecyclerView.Adapter<QuizChapterRecyclerViewAdapter.ViewHolder>() {
    private var context: Context? = null
    private var chapterCheckedCount = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val itemView = LayoutInflater.from(context)
                .inflate(R.layout.quiz_select_chapter_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val quizChapter = quizSubjectChapterCombined.quizChapters[position]
        holder.checkBoxViewChapterName.text = quizChapter.chapName
        holder.txtViewChapterId.text = quizChapter.chapId.toString()
        holder.checkBoxViewChapterName.setOnCheckedChangeListener(null)

        //On checked listener to for returning quizChapter ids
        holder.checkBoxViewChapterName.setOnCheckedChangeListener { buttonView, isChecked ->
            val tempChapterId = holder.txtViewChapterId.text as String
            val tempSubjectId = quizSubjectChapterCombined.quizSubject.subId.toString()
            if (isChecked) {
                if (!QuizChapter.chapterIds.contains(tempChapterId)) QuizChapter.chapterIds.add(tempChapterId)
                if (!QuizSubject.subjectIds.contains(tempSubjectId)) QuizSubject.subjectIds.add(tempSubjectId)
                chapterCheckedCount++
                parentExpandableLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.expandableLayoutHeadingSelected))
            } else {
                QuizChapter.chapterIds.remove(tempChapterId)
                chapterCheckedCount--
                if (chapterCheckedCount == 0) {
                    parentExpandableLayout.setBackgroundColor(ContextCompat.getColor(context!!, parentExpandableLayoutDefaultColor))
                    QuizSubject.subjectIds.remove(tempSubjectId)
                }
            }
            quizChapter.isChecked = isChecked
        }
        holder.checkBoxViewChapterName.isChecked = quizChapter.isChecked
    }

    override fun getItemCount(): Int {
        return quizSubjectChapterCombined.quizChapters.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtViewChapterId: TextView = view.findViewById<View>(R.id.textViewChapId) as TextView
        var checkBoxViewChapterName: CheckBox = view.findViewById<View>(R.id.checkBoxChapName) as CheckBox

    }

}
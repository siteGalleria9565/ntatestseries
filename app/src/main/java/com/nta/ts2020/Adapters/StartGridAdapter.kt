package com.nta.ts2020.Adapters
//package in.co.examin.adapter;
//
//import android.content.Intent;
//import androidx.recyclerview.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import in.co.examin.R;
//import in.co.examin.activity.DashboardActivity;
//import in.co.examin.activity.EBookSubjectsListActivity;
//import in.co.examin.activity.QuizSelectionActivity;
//import in.co.examin.activity.ExamResultDataListActivity;
//import in.co.examin.pojo.StartGridPojo;
//
//import static in.co.examin.constants.ConstantVariables.E_BOOK;
//import static in.co.examin.constants.ConstantVariables.FORMULA_BOOK;
//
///**
// * Created by SiteGalleria on 24-Mar-17.
// */
//
//public class StartGridAdapter extends RecyclerView.Adapter<StartGridAdapter.VIewHolder> {
//    private final ArrayList<StartGridPojo> data;
//    private DashboardActivity dashboardActivity;
//    private int width;
//
//    public StartGridAdapter(ArrayList<StartGridPojo> data, DashboardActivity startUpPage) {
//        this.data = data;
//        this.dashboardActivity = startUpPage;
//    }
//
//    @Override
//    public VIewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        width = parent.getMeasuredWidth();
//        View itemView = LayoutInflater.from(dashboardActivity).inflate(R.layout.deprecated_start_grid_cell, parent, false);
//        return new VIewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(VIewHolder holder, int position) {
//        final int tempPos = position;
//        StartGridPojo tempData = data.get(position);
//        for (int i = 0; i < data.size(); i++) {
//            holder.imageView.setImageResource(tempData.getIcon());
//            holder.textView.setText(tempData.getName());
//
//        }
//
//        holder.container.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (tempPos) {
//                    case 0:
//                        dashboardActivity.examModule();
//                        break;
//                    case 1:
//                        dashboardActivity.startActivity(new Intent(dashboardActivity, QuizSelectionActivity.class));
//                        break;
//                    case 2:
//                        dashboardActivity.startActivity(new Intent(dashboardActivity, ExamResultDataListActivity.class));
//                        break;
//                    case 3:
//                        dashboardActivity.startActivity(new Intent(dashboardActivity, EBookSubjectsListActivity.class).putExtra("E_BOOK_TYPE", E_BOOK));
//                        break;
//                    case 4:
//                        dashboardActivity.startActivity(new Intent(dashboardActivity, EBookSubjectsListActivity.class).putExtra("E_BOOK_TYPE", FORMULA_BOOK));
//                        break;
//                    default:
//                        break;
//
//                }
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return data.size();
//    }
//
//    class VIewHolder extends RecyclerView.ViewHolder {
//        ImageView imageView;
//        TextView textView;
//        LinearLayout container;
//
//        VIewHolder(View itemView) {
//            super(itemView);
//            imageView = (ImageView) itemView.findViewById(R.id.imageViewStartGridIcon);
//            textView = (TextView) itemView.findViewById(R.id.textViewStartGridIconName);
//            container = (LinearLayout) itemView.findViewById(R.id.gridContainer);
//        }
//    }
//}

package com.nta.ts2020.Adapters

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.R
import java.util.*

class QuizGridAdapter(context: Context, questionContainer: ArrayList<ExamAllQuestionsSubjectWisePojo>, reviewLaterQuestions: Array<String>) : BaseAdapter() {
    private val mContext: Context = context
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    var questionContainer: ArrayList<ExamAllQuestionsSubjectWisePojo> = questionContainer
    private val reviewLaterQuestions: Array<String> = reviewLaterQuestions
    override fun getCount(): Int {
        return questionContainer.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        var convertView = convertView
        convertView = mInflater.inflate(R.layout.quiz_gridview_numberseries, null)
        val title = convertView.findViewById<View>(R.id.categoryText) as TextView
        title.text = (position + 1).toString()
        Log.d("QuizGrid color Value", reviewLaterQuestions[position])
        if (reviewLaterQuestions[position] == "0") {
            title.setBackgroundColor(Color.BLUE)
        } else if (reviewLaterQuestions[position] == "1") {
            title.setBackgroundColor(mContext.resources.getColor(R.color.orange))
        } else if (reviewLaterQuestions[position] == "2") {
            title.setBackgroundColor(Color.GREEN)
        } else if (reviewLaterQuestions[position] == "3") {
            title.setBackgroundColor(Color.RED)
        }
        return convertView
    }

}
package com.nta.ts2020.Adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.Activities.ExamNotificationsDetailsActivity
import com.nta.ts2020.PojoClasses.NotificationRelatedPostsPojo
import com.nta.ts2020.R
import com.squareup.picasso.Picasso
import java.util.*

class NotificationRelatedRecyclerAdapter(private val context: ExamNotificationsDetailsActivity, private val notificationRelatedPostsPojos: ArrayList<NotificationRelatedPostsPojo?>?) : RecyclerView.Adapter<NotificationRelatedRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.notification_related_posts_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = notificationRelatedPostsPojos?.get(position)?.title
        holder.date.text = notificationRelatedPostsPojos?.get(position)?.date
        Picasso.with(context).load(notificationRelatedPostsPojos?.get(position)?.image).into(holder.image)
        holder.notifyHolder.setOnClickListener {
            context.finish()
            context.startActivity(Intent(context, ExamNotificationsDetailsActivity::class.java)
                    .putExtra("notification_heading", notificationRelatedPostsPojos?.get(position)?.title)
                    .putExtra("notification_id", notificationRelatedPostsPojos?.get(position)?.id.toString()))
        }
    }

    override fun getItemCount(): Int {
        return notificationRelatedPostsPojos?.size!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById<View>(R.id.tvNotifyRowTitle) as TextView
        var date: TextView = itemView.findViewById<View>(R.id.tvNotifyRowDate) as TextView
        var image: ImageView = itemView.findViewById<View>(R.id.ivNotifyRowImage) as ImageView
        var notifyHolder: CardView = itemView.findViewById<View>(R.id.notifyRelatedHolder) as CardView

    }

}
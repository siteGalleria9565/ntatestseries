package com.nta.ts2020.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.Activities.ExamNotificationsDetailsActivity
import com.nta.ts2020.PojoClasses.NotificationsPojo
import com.nta.ts2020.R
import java.util.*

class NotificationRecyclerAdapter(private val context: Context, private val notificationArrayList: ArrayList<NotificationsPojo>) : RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.notifications_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textViewNotification.text = notificationArrayList[position].notificationHeading
        holder.textViewDate.text = notificationArrayList[position].notificationDate
        holder.relativeLayoutNotificationHolder.setOnClickListener {
            context.startActivity(Intent(context, ExamNotificationsDetailsActivity::class.java)
                    .putExtra("notification_heading", notificationArrayList[position].notificationHeading)
                    .putExtra("notification_id", notificationArrayList[position].notificationId.toString()))
        }
    }

    override fun getItemCount(): Int {
        return notificationArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewNotification: TextView = itemView.findViewById<View>(R.id.textViewNotification) as TextView
        var textViewDate: TextView = itemView.findViewById<View>(R.id.textViewNotificationDate) as TextView
        var relativeLayoutNotificationHolder: RelativeLayout = itemView.findViewById<View>(R.id.relativeLayoutNotificationHolder) as RelativeLayout

    }

}
package com.nta.ts2020.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.Activities.EBooklistActivity
import com.nta.ts2020.PojoClasses.EBookSubjectsPojo
import com.nta.ts2020.R
import java.util.*

class EBookSubjectsListRecyclerAdapter(private val eBookSubjectsPojoArrayList: ArrayList<EBookSubjectsPojo>, private val eBookType: String?) : RecyclerView.Adapter<EBookSubjectsListRecyclerAdapter.ViewHolder>() {
    private var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.ebook_subjects_list_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (id, name) = eBookSubjectsPojoArrayList[position]
        holder.name.text = name
        holder.name.setOnClickListener {
            context!!.startActivity(Intent(context, EBooklistActivity::class.java)
                    .putExtra("E_BOOK_TYPE", eBookType)
                    .putExtra("SUBJECT_NAME", name)
                    .putExtra("SUBJECT_ID", id))
        }
    }

    override fun getItemCount(): Int {
        return eBookSubjectsPojoArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById<View>(R.id.tvEBookName) as TextView

    }

}
package com.nta.ts2020.Adapters
//package in.co.examin.adapter;
//
//import android.content.Context;
//import android.content.Intent;
//import androidx.recyclerview.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import in.co.examin.R;
//import in.co.examin.activity.QuizShowQuestionInstructionsActivity;
//import in.co.examin.pojo.QuizAllExaminationDataPojo;
//
///**
// * Created by Abhimnaoj on 6/2/17.
// */
//
//public class QuizSetAllExamListDataAdapter extends RecyclerView.Adapter<QuizSetAllExamListDataAdapter.ViewHolder> {
//
//
//    private final ArrayList<QuizAllExaminationDataPojo> localAllExaminationDataList;
//    private Context context;
//
//    public QuizSetAllExamListDataAdapter(ArrayList<QuizAllExaminationDataPojo> localAllExaminationDataList) {
//        this.localAllExaminationDataList=localAllExaminationDataList;
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//
//        context=parent.getContext();
//        View view = LayoutInflater.from(context).inflate(R.layout.quiz_all_examination_list_layout,parent,false);
//
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//
//        final String examID=String.valueOf(localAllExaminationDataList.get(position).getExamId());
//        holder.idData.setText(examID);
//        holder.textViewName.setText(localAllExaminationDataList.get(position).getExamName());
//
//        holder.textViewName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//               Intent intent = new Intent(context, QuizShowQuestionInstructionsActivity.class);
//               intent.putExtra("ExamID",examID);
//               context.startActivity(intent);
//
//            }
//        });
//
//    }
//
//
//    @Override
//    public int getItemCount() {
//        return localAllExaminationDataList.size();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//
//        TextView textViewName;
//        TextView idData;
//        public ViewHolder(View itemView) {
//            super(itemView);
//
//            textViewName = (TextView) itemView.findViewById(R.id.MyResultExamName);
//            idData = (TextView) itemView.findViewById(R.id.idData);
//
//
//        }
//    }
//}

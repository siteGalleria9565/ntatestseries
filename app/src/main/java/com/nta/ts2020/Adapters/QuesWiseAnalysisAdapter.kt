package com.nta.ts2020.Adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nta.ts2020.PojoClasses.QuesWiseAnalysisPojo
import de.codecrafters.tableview.TableDataAdapter

class QuesWiseAnalysisAdapter(context: Context?, data: List<QuesWiseAnalysisPojo?>?) : TableDataAdapter<QuesWiseAnalysisPojo?>(context, data) {
    override fun getCellView(rowIndex: Int, columnIndex: Int, parentView: ViewGroup): View {
        val quesWiseAnalysisPojo = getRowData(rowIndex)
        var view: View? = null
        when (columnIndex) {
            0 -> view = renderView(quesWiseAnalysisPojo!!.quesNo)
            1 -> view = renderView(quesWiseAnalysisPojo!!.status)
            2 -> view = renderView(quesWiseAnalysisPojo!!.yourAns)
            3 -> view = renderView(quesWiseAnalysisPojo!!.correctAns)
            4 -> view = renderView(quesWiseAnalysisPojo!!.yourScore)
            5 -> view = renderView(quesWiseAnalysisPojo!!.yourTime)
            6 -> view = renderView(quesWiseAnalysisPojo!!.topperTime)
        }
        return view!!
    }

    private fun renderView(data: String?): View {
        val textView = TextView(context)
        textView.text = data
        textView.setPadding(20, 10, 20, 10)
        textView.textSize = 14f
        return textView
    }
}
package com.nta.ts2020.Adapters

import android.app.AlertDialog
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.Activities.ExamShowQuestionInstructionActivity
import com.nta.ts2020.PojoClasses.ExamAllExaminationDataPojo
import com.nta.ts2020.R
import java.util.*

class ExamSetAllExamListDataRecyclerAdapter(private val activity: AppCompatActivity, private val localAllExaminationDataList: ArrayList<ExamAllExaminationDataPojo>, private val examStatusVariable: String, private val resumeExamId: Int) : RecyclerView.Adapter<ExamSetAllExamListDataRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        //  activity=parent.getContext();
        val view = LayoutInflater.from(activity).inflate(R.layout.exam_all_examination_list_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val examID = localAllExaminationDataList[position].examId.toString()
        holder.idData.text = examID
        holder.textViewName.text = localAllExaminationDataList[position].examName
        holder.textViewName.setOnClickListener(object : View.OnClickListener {
            var buildAlertDial: AlertDialog? = null
            override fun onClick(view: View) {
                val intent = Intent(activity, ExamShowQuestionInstructionActivity::class.java)
                if (resumeExamId != 0 && resumeExamId != examID.toInt()) {
                    val builder1 = AlertDialog.Builder(activity)
                    builder1.setTitle("Abort Test")
                    builder1.setMessage("Do you really want to abort the previous ongoing test ?")
                    builder1.setCancelable(false)
                    builder1.setPositiveButton(
                            "Yes"
                    ) { dialogs, id ->
                        intent.putExtra("Status", "Initial")
                        intent.putExtra("ExamID", examID)
                        intent.putExtra("TYPE", "EXM")
                        activity.startActivity(intent)
                        dialogs.cancel()
                        activity.finish()
                    }
                    builder1.setNegativeButton(
                            "No"
                    ) { dialogs, id -> dialogs.cancel() }
                    buildAlertDial = builder1.create()
                    buildAlertDial?.show()
                } else {
                    if (resumeExamId == examID.toInt()) {
                        intent.putExtra("Status", examStatusVariable)
                    } else {
                        intent.putExtra("Status", "Initial")
                    }
                    intent.putExtra("ExamID", examID)
                    intent.putExtra("TYPE", "EXM")
                    activity.startActivity(intent)
                    activity.finish()
                }
            }
        })
    }

    override fun getItemCount(): Int {
        return localAllExaminationDataList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewName: TextView = itemView.findViewById<View>(R.id.MyResultExamName) as TextView
        var idData: TextView = itemView.findViewById<View>(R.id.idData) as TextView

    }

}
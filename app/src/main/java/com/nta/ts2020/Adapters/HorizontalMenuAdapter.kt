package com.nta.ts2020.Adapters

import android.content.Context
import android.content.Intent
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.Activities.ExamResultGridViewActivity
import com.nta.ts2020.Activities.ExamShowQuestionInstructionActivity
import com.nta.ts2020.AppConstants.ConstantVariables.LATEST
import com.nta.ts2020.AppConstants.ConstantVariables.RESULT
import com.nta.ts2020.AppConstants.ConstantVariables.RESUME
import com.nta.ts2020.AppConstants.ConstantVariables.UPCOMING
import com.nta.ts2020.PojoClasses.HorizontalMenuPojo
import com.nta.ts2020.R
import java.util.*

class HorizontalMenuAdapter(private val horizontalMenuPojoArrayList: ArrayList<HorizontalMenuPojo>) : RecyclerView.Adapter<HorizontalMenuAdapter.ViewHolder>() {
    private var backgrounds: TypedArray? = null
    private var backgrounds_caard: TypedArray? = null
    private var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        backgrounds = parent.resources.obtainTypedArray(R.array.round_borders)
        backgrounds_caard = parent.resources.obtainTypedArray(R.array.round_colors)
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.activity_dashboard_horizontal_menu_row, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val horizontalMenuPojo = horizontalMenuPojoArrayList[position]
        if (horizontalMenuPojo.background == null || horizontalMenuPojo.background == "") {
            holder.background.setImageResource(backgrounds!!.getResourceId(position % 4, 0))
            holder.tag.setBackgroundResource(backgrounds_caard!!.getResourceId(position % 4, 0))
            holder.tag.setPadding(convertDpToPixel(8f, context).toInt(), 0, convertDpToPixel(8f, context).toInt(), 0)
        }
        holder.tittle.text = horizontalMenuPojo.title
        holder.description.text = horizontalMenuPojo.description
        if (horizontalMenuPojo.tag.equals("Resume", ignoreCase = true)) holder.description.setTypeface(null, Typeface.BOLD)
        if (horizontalMenuPojo.tag != "" || horizontalMenuPojo.tag != null) {
            holder.tag.visibility = View.VISIBLE
            holder.tag.text = horizontalMenuPojo.tag
        } else {
            holder.tag.visibility = View.GONE
        }
        holder.container.setOnClickListener {
            when (horizontalMenuPojo.tag) {
                UPCOMING -> Toast.makeText(context, "You cannot start this exam now.", Toast.LENGTH_LONG).show()
                LATEST -> context!!.startActivity(Intent(context, ExamShowQuestionInstructionActivity::class.java)
                        .putExtra("TYPE", "EXM")
                        .putExtra("ExamID", horizontalMenuPojo.id)
                        .putExtra("Status", "initial"))
                RESULT -> context!!.startActivity(Intent(context, ExamResultGridViewActivity::class.java)
                        .putExtra("IS_HORIZONTAL", true)
                        .putExtra("ID", horizontalMenuPojo.id)
                        .putExtra("EXAM_NAME", horizontalMenuPojo.title))
                RESUME -> {
                    val intent = Intent(context, ExamShowQuestionInstructionActivity::class.java)
                    intent.putExtra("Status", RESUME)
                    intent.putExtra("ExamID", horizontalMenuPojo.id)
                    intent.putExtra("TYPE", horizontalMenuPojo.examType)
                    context!!.startActivity(intent)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return horizontalMenuPojoArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tittle: TextView = itemView.findViewById<View>(R.id.tvHorizontalMenuTittle) as TextView
        val description: TextView = itemView.findViewById<View>(R.id.tvHorizontalMenuDescription) as TextView
        val tag: TextView = itemView.findViewById<View>(R.id.tvHorizontalMenuTag) as TextView
        val background: ImageView = itemView.findViewById<View>(R.id.ivHorizontalMenuBackground) as ImageView
        val container: CardView = itemView.findViewById<View>(R.id.cvHorizontalMenu) as CardView
        private val rl_background: RelativeLayout = itemView.findViewById(R.id.rl_background)

    }

    companion object {
        private const val TAG = "HorizontalMenuAdapter"
        fun convertDpToPixel(dp: Float, context: Context?): Float {
            return dp * (context!!.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }
    }

}
package com.nta.ts2020.Adapters

import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.nta.ts2020.PojoClasses.ExamGetCurrentExamSessionPojo
import com.nta.ts2020.R
import java.util.*

class ExamGridAdapter(private val mContext: Context, var questionContainer: ArrayList<ExamGetCurrentExamSessionPojo>, private val reviewLaterQuestions: Array<String?>) : BaseAdapter() {
    override fun getCount(): Int {
        return questionContainer.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        val convertView = convertView
        val holder: ViewHolder
        holder = convertView.tag as ViewHolder
        holder.title!!.text = (position + 1).toString()
        Log.d("ExamGridAdapt", reviewLaterQuestions[position])
        when (reviewLaterQuestions[position]) {
            "0" -> holder.title!!.setBackgroundResource(R.drawable.gridview_round_blue)
            "1" -> holder.title!!.setBackgroundResource(R.drawable.gridview_round_orange)
            "2" -> holder.title!!.setBackgroundResource(R.drawable.gridview_round_green)
            "3" -> holder.title!!.setBackgroundResource(R.drawable.gridview_round_red)
        }
        return convertView
    }

    private inner class ViewHolder {
        var title: TextView? = null
    }

}
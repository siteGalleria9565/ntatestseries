package com.nta.ts2020.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.Adapters.ScholarShipAdapter.MyViewHolder
import com.nta.ts2020.Interfaces.OnScholarshipApplyClick
import com.nta.ts2020.PojoClasses.ScholerShipPojo
import com.nta.ts2020.R

class ScholarShipAdapter(var scholerShipPojoList: MutableList<ScholerShipPojo?>, var context: Context, var onScholarshipApplyClick: OnScholarshipApplyClick) : RecyclerView.Adapter<MyViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.scholership_row_layout, viewGroup, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(myViewHolder: MyViewHolder, i: Int) {
        val scholerShipPojoOB = scholerShipPojoList[i]
        myViewHolder.tv_title.text = scholerShipPojoOB?.title
        myViewHolder.tv_description.text = scholerShipPojoOB?.description
        myViewHolder.ll_itemview.setOnClickListener { onScholarshipApplyClick.onClick(scholerShipPojoOB, "linear_layout") }
    }

    fun updateList(list: MutableList<ScholerShipPojo?>) {
        scholerShipPojoList = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return scholerShipPojoList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_title: TextView = itemView.findViewById(R.id.scholarship_title)
        var tv_description: TextView = itemView.findViewById(R.id.scholarship_description)
        var ll_itemview: LinearLayout = itemView.findViewById(R.id.ll_itemview)

    }

}
package com.nta.ts2020.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.Activities.ExamResultGridViewActivity
import com.nta.ts2020.PojoClasses.ResultListPojo
import com.nta.ts2020.R
import java.util.*

class MyResultListDataRecyclerAdapter(private val context: Context, private val resultListPojoArrayList: ArrayList<ResultListPojo>, private val examStatusVariable: String, private val resumeExamId: Int) : RecyclerView.Adapter<MyResultListDataRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        //  context=parent.getContext();
        val view = LayoutInflater.from(context).inflate(R.layout.my_result_list_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tempResultListPojo = resultListPojoArrayList[position]
        holder.myResultDate.text = tempResultListPojo.date
        holder.totalAttempt.setText("${tempResultListPojo.attemptedQues} / ${tempResultListPojo.totalQues}")
        holder.totalMarks.setText("${tempResultListPojo.totalObtainedMarks} / ${tempResultListPojo.totalMarks}")
        holder.myResultExamName.text = tempResultListPojo.examName
        holder.myResultListBaseLayout.setOnClickListener {
            val intent = Intent(context, ExamResultGridViewActivity::class.java)
            intent.putExtra("ID", tempResultListPojo.id)
            intent.putExtra("EXAM_NAME", tempResultListPojo.examName)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return resultListPojoArrayList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var myResultExamName: TextView = itemView.findViewById<View>(R.id.MyResultExamName) as TextView
        var myResultDate: TextView = itemView.findViewById<View>(R.id.MyResultDate) as TextView
        var totalAttempt: TextView = itemView.findViewById<View>(R.id.TotalAttempt) as TextView
        var totalMarks: TextView = itemView.findViewById<View>(R.id.TotalMarks) as TextView
        var myResultListBaseLayout: RelativeLayout = itemView.findViewById<View>(R.id.MyResultListBaseLayout) as RelativeLayout

    }

}
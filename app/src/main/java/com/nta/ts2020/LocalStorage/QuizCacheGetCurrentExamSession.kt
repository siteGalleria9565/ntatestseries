package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.LocalStorage.QuizAssetToLocalDatabase.Companion.getHelper
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.PojoClasses.ExamGetCurrentExamSessionPojo
import java.util.*

class QuizCacheGetCurrentExamSession //        Log.i(TAG, "Constructor QuizCacheGetCurrentExamSession() invoked.");
(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var TABLE_CACHE_SUBMIT_ANSWER_STUFF = "submit_answer_stuff"

    // Examinations_Data Table Columns names ....
    //Store Exam Current Session
    var KEY_QUESTION_ANSWER_ID = "question_answer_id"
    var KEY_RESPONSE_DATA = "response_data"
    var KEY_TIME_DURATION = "time_duration"
    var KEY_COLOR_STATUS = "color_status"
    var projection: Array<String?>? = null
    var selection: String? = null
    var selectionArgs: Array<String?>? = null
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    var sqliteContentValuesInstance: ContentValues? = null

    // POJOs ....
    var localQuizGetCurrentExamSessionPojoInstance: ExamGetCurrentExamSessionPojo? = null

    // Collections ....
    var localGetCurrentExamSessionList: ArrayList<ExamGetCurrentExamSessionPojo>? = null

    /**
     * Setting All Data of Examination in Cache ....
     */
    fun setCurrentExamSession(localQuizAllQuestionsSubjectWisePojoList: ArrayList<ExamAllQuestionsSubjectWisePojo>): String {
//        Log.i(TAG, "setCurrentExamSession() called Data Size: ");
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        sqliteContentValuesInstance = ContentValues()
        for (j in localQuizAllQuestionsSubjectWisePojoList.indices) {
            localQuizGetCurrentExamSessionPojoInstance = ExamGetCurrentExamSessionPojo()
            sqliteContentValuesInstance!!.put(KEY_QUESTION_ANSWER_ID, localQuizAllQuestionsSubjectWisePojoList[j].questionId)
            sqliteContentValuesInstance!!.put(KEY_RESPONSE_DATA, "")
            sqliteContentValuesInstance!!.put(KEY_TIME_DURATION, 0)
            sqliteContentValuesInstance!!.put(KEY_COLOR_STATUS, 0)

//            Log.i(TAG, "Inserting current session data into the Cache Table. With QuestionId= " + localQuizAllQuestionsSubjectWisePojoList.get(j).getQuestionId());
            // Inserting Row
            mSQLiteInstance.insert(TABLE_CACHE_SUBMIT_ANSWER_STUFF, null, sqliteContentValuesInstance)
        }


        // mSQLiteInstance.close(); // Closing database connection

//        Log.i(TAG, "Examination Data inserted successfully into the Cache Table.");
        return "succes"
    }//        Log.i(TAG, "getCurrentExamSession() called: ");

    // Obtained instance of Readable SQLite Database.

    // Executing the Query.

    // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.

    // return Contacts list

    /**
     * Getting Profile of a Student ....
     */
    val currentExamSession: ArrayList<ExamGetCurrentExamSessionPojo>
        get() {
//        Log.i(TAG, "getCurrentExamSession() called: ");
            projection = arrayOfNulls(1)
            selectionArgs = arrayOfNulls(1)
            localGetCurrentExamSessionList = ArrayList()
            projection = null
            selection = null
            selectionArgs = null

            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase

            // Executing the Query.
            cursor = mSQLiteInstance.query(TABLE_CACHE_SUBMIT_ANSWER_STUFF,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)

            // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
            if (cursor.moveToFirst()) {
                do {
                    localQuizGetCurrentExamSessionPojoInstance = ExamGetCurrentExamSessionPojo()
                    localQuizGetCurrentExamSessionPojoInstance!!.questionId = cursor.getInt(0).toString()
                    localQuizGetCurrentExamSessionPojoInstance!!.response = cursor.getString(1)
                    localQuizGetCurrentExamSessionPojoInstance!!.timeDuration = cursor.getInt(2).toString()
                    localQuizGetCurrentExamSessionPojoInstance!!.colorStatus = cursor.getInt(3).toString()
                    localGetCurrentExamSessionList!!.add(localQuizGetCurrentExamSessionPojoInstance!!)
                } while (cursor.moveToNext())
            }

            // return Contacts list
            return localGetCurrentExamSessionList!!
        }

    /**
     * Updating All Data of a Examination ....
     */
    fun updateCurrentExamSession(QuestionID: String, updatedQuizGetCurrentExamSessionPojo: ExamGetCurrentExamSessionPojo): String {
//        Log.i(TAG, "updateExaminationData() called for QuestionId: " + QuestionID);
        projection = arrayOfNulls(1)
        selectionArgs = arrayOfNulls(1)
        projection = null
        selection = "$KEY_QUESTION_ANSWER_ID = $QuestionID"
        selectionArgs = null

        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        sqliteContentValuesInstance = ContentValues()

//        Log.i(TAG, "KEY_QUESTION_ANSWER_ID: " + updatedQuizGetCurrentExamSessionPojo.getQuestionId() + " Responce " + updatedQuizGetCurrentExamSessionPojo.getResponse() + " TIme " + updatedQuizGetCurrentExamSessionPojo.getTimeDuration() + " color " + updatedQuizGetCurrentExamSessionPojo.getColorStatus());

        /* sqliteContentValuesInstance.put(KEY_COLOR_STATUS, Integer.parseInt(updatedQuizGetCurrentExamSessionPojo.getColorStatus()));
        sqliteContentValuesInstance.put(KEY_TIME_DURATION,KEY_TIME_DURATION+" + "+Integer.parseInt(updatedQuizGetCurrentExamSessionPojo.getTimeDuration()));
        sqliteContentValuesInstance.put(KEY_RESPONSE_DATA, updatedQuizGetCurrentExamSessionPojo.getResponse());
*/

        // Updating Row
        //mSQLiteInstance.update(TABLE_CACHE_SUBMIT_ANSWER_STUFF, sqliteContentValuesInstance, selection, selectionArgs);
        mSQLiteInstance.execSQL("UPDATE " + TABLE_CACHE_SUBMIT_ANSWER_STUFF + " SET " + KEY_COLOR_STATUS + "=" + updatedQuizGetCurrentExamSessionPojo.colorStatus!!.toInt() + ", " + KEY_TIME_DURATION + " = " + "time_duration + " + updatedQuizGetCurrentExamSessionPojo.timeDuration!!.toInt() + ", " + KEY_RESPONSE_DATA + " = '" + updatedQuizGetCurrentExamSessionPojo.response + "' WHERE " + selection)

        // mSQLiteInstance.close();

        // return Contacts list
        return "Success"
    }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    // Obtained instance of Readable SQLite Database.
    val quesIds: ArrayList<String>
        get() {
            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase
            val tempArrayList = ArrayList<String>()
            cursor = mSQLiteInstance.query(TABLE_CACHE_SUBMIT_ANSWER_STUFF, arrayOf(KEY_QUESTION_ANSWER_ID), null, null, null, null, null)
            if (cursor.moveToFirst()) {
                do {
                    tempArrayList.add(cursor.getString(0))
                } while (cursor.moveToNext())
            }
            return tempArrayList
        }

    // Obtained instance of Readable SQLite Database.
    val responses: ArrayList<String>
        get() {

            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase
            val tempArrayList = ArrayList<String>()
            cursor = mSQLiteInstance.query(TABLE_CACHE_SUBMIT_ANSWER_STUFF, arrayOf(KEY_RESPONSE_DATA), null, null, null, null, null)
            if (cursor.moveToFirst()) {
                do {
                    tempArrayList.add(cursor.getString(0))
                } while (cursor.moveToNext())
            }
            return tempArrayList
        }

    // Obtained instance of Readable SQLite Database.
    val quesWiseTime: ArrayList<String>
        get() {
            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase
            val tempArrayList = ArrayList<String>()
            cursor = mSQLiteInstance.query(TABLE_CACHE_SUBMIT_ANSWER_STUFF, arrayOf(KEY_TIME_DURATION), null, null, null, null, null)
            if (cursor.moveToFirst()) {
                do {
                    tempArrayList.add(cursor.getString(0))
                } while (cursor.moveToNext())
            }
            return tempArrayList
        }

    companion object {
        private const val TAG = "CacheExamSession"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "QuizLocalDatabase"
    }

}
package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.LocalStorage.QuizAssetToLocalDatabase.Companion.getHelper
import com.nta.ts2020.PojoClasses.ExamGetSubjectArrayStuffPojo
import com.nta.ts2020.PojoClasses.ExamQuestionDataBasedOnExamIdPojo
import java.util.*

class QuizCacheAllQuestionRelatedStuff(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var TABLE_CACHE_QUESTION_RELATED_STUFF = "questions_related_stuff"
    var TABLE_CACHE_QUESTION_SUBJECT_STUFF = "questions_subject_stuff"

    // Examinations_Data Table Columns names ....
    var KEY_QUESTION_STUFF_EXAM_ID = "question_stuff_exam_id"
    var KEY_QUESTION_EXAM_NAME = "question_exam_name"
    var KEY_TOTAL_QUESTION = "total_questions"
    var KEY_TOTAL_MARKS = "total_marks"
    var KEY_TIME_DURATION_IN_SECONDS = "duration"
    var KEY_INSTRUCTIONS = "instructions"

    //subject_names with quizSubject id
    var KEY_SUBJECT_EXAM_ID = "subject_exam_id"
    var KEY_NUMBER_OF_MARKS = "number_of_marks"
    var KEY_SUBJECT_ID = "subject_id"
    var KEY_NUMBER_OF_QUESTIONS = "number_of_questions"
    var KEY_SUBJECT_NAMES = "subject_names"
    var projection: Array<String?>? = null
    var selection: String? = null
    lateinit var selectionArgs: Array<String?>
    var myDataBase: SQLiteDatabase? = null
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    var sqliteContentValuesInstance: ContentValues? = null

    // POJOs ....
    var localQuestionDataBasedOnExamIdPojoInstance: ExamQuestionDataBasedOnExamIdPojo? = null
    var quizGetSubjectArrayStuffPojoInstance: ExamGetSubjectArrayStuffPojo? = null

    // Collections ....
    var localQuestionRelatedDataList: ArrayList<ExamQuestionDataBasedOnExamIdPojo>? = null
    var quizGetSubjectArrayStuffPojoList: ArrayList<ExamGetSubjectArrayStuffPojo>? = null

    /**
     * Setting All Data of Examination in Cache ....
     */
    fun setCacheAllQuestionRelatedStuff(localQuestionDataBasedOnExamIdPojo: ExamQuestionDataBasedOnExamIdPojo): String {
        Log.i(TAG, "setAllExaminationData() called Data Size: ")
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        sqliteContentValuesInstance = ContentValues()
        localQuestionDataBasedOnExamIdPojoInstance = ExamQuestionDataBasedOnExamIdPojo()
        localQuestionDataBasedOnExamIdPojoInstance = localQuestionDataBasedOnExamIdPojo
        sqliteContentValuesInstance!!.put(KEY_QUESTION_STUFF_EXAM_ID, localQuestionDataBasedOnExamIdPojoInstance!!.examId)
        sqliteContentValuesInstance!!.put(KEY_QUESTION_EXAM_NAME, localQuestionDataBasedOnExamIdPojoInstance!!.examName)
        sqliteContentValuesInstance!!.put(KEY_TOTAL_QUESTION, localQuestionDataBasedOnExamIdPojoInstance!!.totalQuestions)
        sqliteContentValuesInstance!!.put(KEY_TOTAL_MARKS, localQuestionDataBasedOnExamIdPojoInstance!!.totalMarks)
        sqliteContentValuesInstance!!.put(KEY_TIME_DURATION_IN_SECONDS, localQuestionDataBasedOnExamIdPojoInstance!!.timeDurationInSeconds)
        sqliteContentValuesInstance!!.put(KEY_INSTRUCTIONS, localQuestionDataBasedOnExamIdPojoInstance!!.instructions)
        Log.i(TAG, "Inserting All Examination data into the Cache Table. With ExamId= " + localQuestionDataBasedOnExamIdPojoInstance!!.examId)
        // Inserting Row
        mSQLiteInstance.insert(TABLE_CACHE_QUESTION_RELATED_STUFF, null, sqliteContentValuesInstance)

        //fetch array values
        val getNumberOfMarks = localQuestionDataBasedOnExamIdPojoInstance!!.numberOfMarks
        val getSubjectId = localQuestionDataBasedOnExamIdPojoInstance!!.subjectId
        val getNumberOfQuestions = localQuestionDataBasedOnExamIdPojoInstance!!.numberOfQuestions
        val getSubjectNames = localQuestionDataBasedOnExamIdPojoInstance!!.subjectNames
        Log.i(TAG, "QuizSubject Id Data.$getSubjectNames getNumberOfQuestions $getNumberOfQuestions getSubjectId $getSubjectId getNumberOfMarks $getNumberOfMarks")
        sqliteContentValuesInstance = ContentValues()
        for (i in getSubjectId.indices) {
            sqliteContentValuesInstance!!.put(KEY_SUBJECT_EXAM_ID, localQuestionDataBasedOnExamIdPojoInstance!!.examId)
            sqliteContentValuesInstance!!.put(KEY_NUMBER_OF_MARKS, getNumberOfMarks[i])
            sqliteContentValuesInstance!!.put(KEY_SUBJECT_ID, getSubjectId[i])
            sqliteContentValuesInstance!!.put(KEY_NUMBER_OF_QUESTIONS, getNumberOfQuestions[i])
            sqliteContentValuesInstance!!.put(KEY_SUBJECT_NAMES, getSubjectNames[i])

            // Inserting Row
            mSQLiteInstance.insert(TABLE_CACHE_QUESTION_SUBJECT_STUFF, null, sqliteContentValuesInstance)
        }

        // mSQLiteInstance.close(); // Closing database connection
        Log.i(TAG, "Examination Data inserted successfully into the Cache Table.")
        return "succes"
    }// Obtained instance of Readable SQLite Database.


    // Executing the Query.

    // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.

    // return Contacts list

    /**
     * Getting Profile of a Student ....
     */
    val cacheAllQuestionRelatedStuff: ArrayList<ExamQuestionDataBasedOnExamIdPojo>
        get() {
            Log.i(TAG, "getAllExaminationData() called: ")
            projection = arrayOfNulls(1)
            selectionArgs = arrayOfNulls(1)
            localQuestionRelatedDataList = ArrayList()
            projection = null
            selection = "$KEY_QUESTION_STUFF_EXAM_ID LIKE ? "
            selectionArgs[0] = null


            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase


            // Executing the Query.
            cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_RELATED_STUFF,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)

            // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
            if (cursor.moveToFirst()) {
                do {
                    localQuestionDataBasedOnExamIdPojoInstance = ExamQuestionDataBasedOnExamIdPojo()
                    localQuestionDataBasedOnExamIdPojoInstance!!.examId = cursor.getString(0)
                    localQuestionDataBasedOnExamIdPojoInstance!!.examName = cursor.getString(1)
                    localQuestionDataBasedOnExamIdPojoInstance!!.totalQuestions = cursor.getString(2)
                    localQuestionDataBasedOnExamIdPojoInstance!!.totalMarks = cursor.getString(3)
                    localQuestionDataBasedOnExamIdPojoInstance!!.timeDurationInSeconds = cursor.getString(4)
                    localQuestionDataBasedOnExamIdPojoInstance!!.instructions = cursor.getString(5)
                    localQuestionRelatedDataList!!.add(localQuestionDataBasedOnExamIdPojoInstance!!)
                    Log.i(TAG, "Data: " + cursor.getInt(0))
                } while (cursor.moveToNext())
            }

            // return Contacts list
            return localQuestionRelatedDataList!!
        }// Obtained instance of Readable SQLite Database.


    // Executing the Query.

    // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.

    // return Contacts list

    /**
     * Getting Profile of a Student ....
     */
    val cacheAllQuestionSubjectRelatedStuff: ArrayList<ExamGetSubjectArrayStuffPojo>
        get() {
            Log.i(TAG, "getQuestionSubjectRelatedStuff() called: ")
            projection = arrayOfNulls(1)
            selectionArgs = arrayOfNulls(1)
            quizGetSubjectArrayStuffPojoList = ArrayList()
            projection = null
            selection = "$KEY_SUBJECT_EXAM_ID LIKE ? "
            selectionArgs[0] = null


            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase


            // Executing the Query.
            cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)

            // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
            if (cursor.moveToFirst()) {
                do {
                    quizGetSubjectArrayStuffPojoInstance = ExamGetSubjectArrayStuffPojo()
                    quizGetSubjectArrayStuffPojoInstance!!.examId = cursor.getString(0)
                    quizGetSubjectArrayStuffPojoInstance!!.numberOfMarks = cursor.getString(1)
                    quizGetSubjectArrayStuffPojoInstance!!.subjectId = cursor.getString(2)
                    quizGetSubjectArrayStuffPojoInstance!!.numberOfQuestions = cursor.getString(3)
                    quizGetSubjectArrayStuffPojoInstance!!.subjectNames = cursor.getString(4)
                    quizGetSubjectArrayStuffPojoList!!.add(quizGetSubjectArrayStuffPojoInstance!!)
                } while (cursor.moveToNext())
            }

            // return Contacts list
            return quizGetSubjectArrayStuffPojoList!!
        }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}
    fun getSubjectIds(examID: Int): ArrayList<String> {
        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        val selectionQuery = "$KEY_SUBJECT_EXAM_ID = $examID"
        val tempArrayList = ArrayList<String>()
        cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF, arrayOf(KEY_SUBJECT_ID), selectionQuery, null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                tempArrayList.add(cursor.getString(0))
            } while (cursor.moveToNext())
        }
        return tempArrayList
    }

    fun getNosOfQuesSubWise(examID: Int): ArrayList<String> {
        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        val selectionQuery = "$KEY_SUBJECT_EXAM_ID = $examID"
        val tempArrayList = ArrayList<String>()
        cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF, arrayOf(KEY_NUMBER_OF_QUESTIONS), selectionQuery, null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                tempArrayList.add(cursor.getString(0))
            } while (cursor.moveToNext())
        }
        return tempArrayList
    }

    fun getSubWiseMaxMarks(examID: Int): ArrayList<String> {
        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        val selectionQuery = "$KEY_SUBJECT_EXAM_ID = $examID"
        val tempArrayList = ArrayList<String>()
        cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF, arrayOf(KEY_NUMBER_OF_MARKS), selectionQuery, null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                tempArrayList.add(cursor.getString(0))
            } while (cursor.moveToNext())
        }
        return tempArrayList
    }

    companion object {
        private const val TAG = "CacheAllExamData"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "QuizLocalDatabase"
    }

    init {
        Log.i(TAG, "Constructor CacheClassStudentsMapping() invoked.")
    }
}
package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteException
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.PojoClasses.BannerPojo
import java.util.*

class CacheBanner(private val context: Context) {
    fun saveBanner(bannerPojoArrayList: ArrayList<BannerPojo>): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        for (i in bannerPojoArrayList.indices) {
            val (id, background, logo, title, description) = bannerPojoArrayList[i]
            val contentValues = ContentValues()
            contentValues.put(KEY_BANNER_ID, id)
            contentValues.put(KEY_BANNER_TITLE, title)
            contentValues.put(KEY_BANNER_DESCRIPTION, description)
            contentValues.put(KEY_BANNER_LOGO, logo)
            contentValues.put(KEY_BANNER_BACKGROUND, background)
            try {
                sqLiteDatabase?.insertOrThrow(TABLE_BANNER, null, contentValues)
            } catch (e: SQLiteException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                return false
            }
        }
        return true
    }

    val banner: ArrayList<BannerPojo>
        get() {
            val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
            val cursor = sqLiteDatabase?.query(TABLE_BANNER, null, null, null, null, null, null)
            val bannerPojoArrayList = ArrayList<BannerPojo>()
            if (cursor!!.moveToFirst()) {
                do {
                    val bannerPojo = BannerPojo()
                    bannerPojo.id = cursor?.getString(cursor.getColumnIndex(KEY_BANNER_ID))
                    bannerPojo.title = cursor?.getString(cursor.getColumnIndex(KEY_BANNER_TITLE))
                    bannerPojo.description = cursor?.getString(cursor.getColumnIndex(KEY_BANNER_DESCRIPTION))
                    bannerPojo.logo = cursor?.getString(cursor.getColumnIndex(KEY_BANNER_LOGO))
                    bannerPojo.background = cursor?.getString(cursor.getColumnIndex(KEY_BANNER_BACKGROUND))
                    bannerPojoArrayList.add(bannerPojo)
                } while (cursor.moveToNext())
            }
            return bannerPojoArrayList
        }

    fun clearRecords() {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        //Clear all old records
        sqLiteDatabase?.execSQL("DELETE FROM $TABLE_BANNER")
    }

    companion object {
        private const val TABLE_BANNER = "banner"
        private const val KEY_BANNER_ID = "id"
        private const val KEY_BANNER_TITLE = "title"
        private const val KEY_BANNER_DESCRIPTION = "description"
        private const val KEY_BANNER_LOGO = "logo"
        private const val KEY_BANNER_BACKGROUND = "background"
    }

}
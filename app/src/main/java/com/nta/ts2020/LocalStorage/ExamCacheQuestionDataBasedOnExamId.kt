package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.LocalStorage.ExamAssetToLocalDatabase.Companion.getHelper
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.PojoClasses.ExamQuestionDataBasedOnExamIdPojo
import java.util.*

class ExamCacheQuestionDataBasedOnExamId //        Log.i(TAG, "Constructor CacheClassStudentsMapping() invoked.");
(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var TABLE_CACHE_QUESTION_DATA = "questions_data"

    // Students_Profile Table Columns names ....
    var KEY_QUESTION_SUBJECT_ID = "subject_id"
    var KEY_QUESTION_ID = "question_id"
    var KEY_EASSY_ID = "eassy_id"
    var KEY_QUESTION_TYPE = "question_type"
    var KEY_QUESTION_TEXT = "question_ext"
    var KEY_EASSY_TEXT = "eassy_ext"
    var KEY_OPTION_A = "option_a"
    var KEY_OPTION_B = "option_b"
    var KEY_OPTION_C = "option_c"
    var KEY_OPTION_D = "option_d"
    var KEY_OPTION_E = "option_e"
    var KEY_RIGHT_MARKS = "right_marks"
    var KEY_WRONG_MARKS = "wrong_marks"
    var KEY_NO_OPTIONS = "no_options"
    var KEY_RIGHT_ANS = "right_ANS"
    var projection: Array<String?>? = null
    var selection: String? = null
    var selectionArgs: Array<String?>? = null
    var myDataBase: SQLiteDatabase? = null
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    var sqliteContentValuesInstance: ContentValues? = null

    // POJOs ....
    var localExamQuestionDataBasedOnExamIdPojoInstance: ExamQuestionDataBasedOnExamIdPojo? = null
    var examAllQuestionsSubjectWisePojoInstance: ExamAllQuestionsSubjectWisePojo? = null

    // Collections ....
    var localAllExaminationDataList: ArrayList<ExamAllQuestionsSubjectWisePojo>? = null

    /**
     * Setting All Data of Examination in Cache ....
     */
    fun setQuestionDataBasedOnExamId(localExamAllQuestionsSubjectWisePojoList: ArrayList<ExamAllQuestionsSubjectWisePojo>): String {
//        Log.i(TAG, "setQuestionDataBasedOnExamId() called Data Size: " );

        //  this.localAllExaminationDataList = localAllExaminationDataList;
        mSQLiteInstance = getHelper(context)!!.writableDatabase
        sqliteContentValuesInstance = ContentValues()
        for (j in localExamAllQuestionsSubjectWisePojoList.indices) {
            localExamQuestionDataBasedOnExamIdPojoInstance = ExamQuestionDataBasedOnExamIdPojo()
            examAllQuestionsSubjectWisePojoInstance = localExamAllQuestionsSubjectWisePojoList[j]
            sqliteContentValuesInstance!!.put(KEY_QUESTION_ID, examAllQuestionsSubjectWisePojoInstance!!.questionId)
            sqliteContentValuesInstance!!.put(KEY_QUESTION_SUBJECT_ID, examAllQuestionsSubjectWisePojoInstance!!.subjectId)
            sqliteContentValuesInstance!!.put(KEY_EASSY_ID, examAllQuestionsSubjectWisePojoInstance!!.essayId)
            sqliteContentValuesInstance!!.put(KEY_QUESTION_TYPE, examAllQuestionsSubjectWisePojoInstance!!.questionType)
            sqliteContentValuesInstance!!.put(KEY_QUESTION_TEXT, examAllQuestionsSubjectWisePojoInstance!!.questionText)
            sqliteContentValuesInstance!!.put(KEY_EASSY_TEXT, examAllQuestionsSubjectWisePojoInstance!!.essayData)
            sqliteContentValuesInstance!!.put(KEY_OPTION_A, examAllQuestionsSubjectWisePojoInstance!!.optionA)
            sqliteContentValuesInstance!!.put(KEY_OPTION_B, examAllQuestionsSubjectWisePojoInstance!!.optionB)
            sqliteContentValuesInstance!!.put(KEY_OPTION_C, examAllQuestionsSubjectWisePojoInstance!!.optionC)
            sqliteContentValuesInstance!!.put(KEY_OPTION_D, examAllQuestionsSubjectWisePojoInstance!!.optionD)
            sqliteContentValuesInstance!!.put(KEY_OPTION_E, examAllQuestionsSubjectWisePojoInstance!!.optionE)
            sqliteContentValuesInstance!!.put(KEY_RIGHT_MARKS, examAllQuestionsSubjectWisePojoInstance!!.rightMarks)
            sqliteContentValuesInstance!!.put(KEY_WRONG_MARKS, examAllQuestionsSubjectWisePojoInstance!!.wrongMarks)
            sqliteContentValuesInstance!!.put(KEY_NO_OPTIONS, examAllQuestionsSubjectWisePojoInstance!!.noOptions)
            sqliteContentValuesInstance!!.put(KEY_RIGHT_ANS, examAllQuestionsSubjectWisePojoInstance!!.rightAns)

//            Log.i(TAG, "Inserting All Examination data into the Cache Table. With SubjectName= "+ examAllQuestionsSubjectWisePojoInstance.getEBookAuthor());
            // Inserting Row
            mSQLiteInstance.insert(TABLE_CACHE_QUESTION_DATA, null, sqliteContentValuesInstance)
        }


        //mSQLiteInstance.close(); // Closing database connection

//        Log.i(TAG, "Examination Data inserted successfully into the Cache Table.");
        return "succes"
    }//        Log.i(TAG, "getQuestionDataBasedOnExamId() called: ");
    //selection     = KEY_QUESTION_SUBJECT_ID + " LIKE ? ";
    //selectionArgs[0] = SubjectId;

    // Obtained instance of Readable SQLite Database.


    // Executing the Query.

    // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.

    // return Contacts list

    /**
     * Getting Profile of a Student ....
     */
    val questionDataBasedOnExamId: ArrayList<ExamAllQuestionsSubjectWisePojo>
        get() {
//        Log.i(TAG, "getQuestionDataBasedOnExamId() called: ");
            projection = arrayOfNulls(1)
            selectionArgs = arrayOfNulls(1)
            localAllExaminationDataList = ArrayList()
            projection = null
            selection = null
            //selection     = KEY_QUESTION_SUBJECT_ID + " LIKE ? ";
            //selectionArgs[0] = SubjectId;
            selectionArgs = null

            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.writableDatabase


            // Executing the Query.
            cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_DATA,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)

            // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
            if (cursor.moveToFirst()) {
                do {
                    examAllQuestionsSubjectWisePojoInstance = ExamAllQuestionsSubjectWisePojo()
                    examAllQuestionsSubjectWisePojoInstance!!.questionId = cursor.getString(0)
                    examAllQuestionsSubjectWisePojoInstance!!.subjectId = cursor.getString(1)
                    examAllQuestionsSubjectWisePojoInstance!!.essayId = cursor.getString(2)
                    examAllQuestionsSubjectWisePojoInstance!!.questionType = cursor.getString(3)
                    examAllQuestionsSubjectWisePojoInstance!!.questionText = cursor.getString(4)
                    examAllQuestionsSubjectWisePojoInstance!!.essayData = cursor.getString(5)
                    examAllQuestionsSubjectWisePojoInstance!!.optionA = cursor.getString(6)
                    examAllQuestionsSubjectWisePojoInstance!!.optionB = cursor.getString(7)
                    examAllQuestionsSubjectWisePojoInstance!!.optionC = cursor.getString(8)
                    examAllQuestionsSubjectWisePojoInstance!!.optionD = cursor.getString(9)
                    examAllQuestionsSubjectWisePojoInstance!!.optionE = cursor.getString(10)
                    examAllQuestionsSubjectWisePojoInstance!!.rightMarks = cursor.getString(11)
                    examAllQuestionsSubjectWisePojoInstance!!.wrongMarks = cursor.getString(12)
                    examAllQuestionsSubjectWisePojoInstance!!.noOptions = cursor.getString(13)
                    examAllQuestionsSubjectWisePojoInstance!!.rightAns = cursor.getString(14)
                    localAllExaminationDataList!!.add(examAllQuestionsSubjectWisePojoInstance!!)
                } while (cursor.moveToNext())
            }
            cursor.close()

            // return Contacts list
            return localAllExaminationDataList!!
        }

    //Added Later -- Suvajit
    fun getQuestionDataBasedOnQuesId(questionId: String): ExamAllQuestionsSubjectWisePojo? {
        mSQLiteInstance = getHelper(context)!!.writableDatabase
        cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_DATA, null, "$KEY_QUESTION_ID=$questionId", null, null, null, null)
        if (cursor.getCount() == 1) {
            cursor.moveToFirst()
            examAllQuestionsSubjectWisePojoInstance = ExamAllQuestionsSubjectWisePojo()
            examAllQuestionsSubjectWisePojoInstance!!.questionId = cursor.getString(0)
            examAllQuestionsSubjectWisePojoInstance!!.subjectId = cursor.getString(1)
            examAllQuestionsSubjectWisePojoInstance!!.essayId = cursor.getString(2)
            examAllQuestionsSubjectWisePojoInstance!!.questionType = cursor.getString(3)
            examAllQuestionsSubjectWisePojoInstance!!.questionText = cursor.getString(4)
            examAllQuestionsSubjectWisePojoInstance!!.essayData = cursor.getString(5)
            examAllQuestionsSubjectWisePojoInstance!!.optionA = cursor.getString(6)
            examAllQuestionsSubjectWisePojoInstance!!.optionB = cursor.getString(7)
            examAllQuestionsSubjectWisePojoInstance!!.optionC = cursor.getString(8)
            examAllQuestionsSubjectWisePojoInstance!!.optionD = cursor.getString(9)
            examAllQuestionsSubjectWisePojoInstance!!.optionE = cursor.getString(10)
            examAllQuestionsSubjectWisePojoInstance!!.rightMarks = cursor.getString(11)
            examAllQuestionsSubjectWisePojoInstance!!.wrongMarks = cursor.getString(12)
            examAllQuestionsSubjectWisePojoInstance!!.noOptions = cursor.getString(13)
            examAllQuestionsSubjectWisePojoInstance!!.rightAns = cursor.getString(14)
        }
        cursor.close()
        return examAllQuestionsSubjectWisePojoInstance
    }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {
        private const val TAG = "CacheQuestionExamData"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "ExaminationLocalDatabase"
    }

}
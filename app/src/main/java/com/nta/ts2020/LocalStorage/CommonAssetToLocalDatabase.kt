package com.nta.ts2020.LocalStorage

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import java.util.*

class CommonAssetToLocalDatabase private constructor(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        Log.d(TAG, "#Method onCreate() called of CommonAssetToLocalDatabase")
        val CREATE_RESULT_LIST_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_CACHE_RESULT_LIST
                + "(" + KEY_EXAM_NAME + " TEXT ," +
                KEY_ID + " TEXT," +
                KEY_EXAM_TYPE + " TEXT," +
                KEY_DATE + " TEXT," +
                KEY_TOTAL_QUESTIONS + " INTEGER," +
                KEY_TOTAL_MARKS + " REAL," +
                KEY_ATTEMPTED_QUESTIONS + " INTEGER," +
                KEY_CORRECT_QUESTIONS + " INTEGER," +
                KEY_TOTAL_OBTAINED_MARKS + " REAL," +
                KEY_SUBJECT_IDS + " TEXT," +
                KEY_SUBJECT_NAMES + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_RESULT_LIST query:$CREATE_RESULT_LIST_TABLE")
        db.execSQL(CREATE_RESULT_LIST_TABLE)
        Log.d(TAG, "$TABLE_CACHE_RESULT_LIST successfully created")
        val CREATE_FINAL_SUBMISSION_DATA_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_CACHE_FINAL_DATA
                + "(" + KEY_FINAL_USER_ID + " INTEGER ," +
                KEY_FINAL_ID + " TEXT," +
                KEY_FINAL_EXAM_TYPE + " TEXT," +
                KEY_FINAL_DATE + " TEXT," +
                KEY_FINAL_ALLOTTED_TIME + " INTEGER," +
                KEY_FINAL_TOTAL_QUESTIONS + " INTEGER," +
                KEY_FINAL_TOTAL_MARKS + " INTEGER," +
                KEY_FINAL_SUBJECT_ID + " TEXT," +
                KEY_FINAL_NO_OF_QUES_SUB_WISE + " TEXT," +
                KEY_FINAL_SUB_WISE_MAX_MARKS + " TEXT," +
                KEY_FINAL_QUES_ID + " TEXT," +
                KEY_FINAL_RESPONSE + " TEXT," +
                KEY_FINAL_SUB_WISE_MARKS + " TEXT," +
                KEY_FINAL_SUB_WISE_QUESTION_DATA + " TEXT," +
                KEY_FINAL_TOTAL_OBTAINED_MARKS + " INTEGER," +
                KEY_FINAL_TOTAL_QUESTION_DATA + " TEXT," +
                KEY_FINAL_SUB_WISE_TIME + " TEXT," +
                KEY_FINAL_QUES_WISE_TIME + " TEXT," +
                KEY_FINAL_OVERALL_TIME + " INTEGER," +
                KEY_SYNC_STATUS + " TEXT" + ")")
        Log.d(TAG, "CREATE_FINAL_SUBMISSION_DATA_TABLE: $CREATE_FINAL_SUBMISSION_DATA_TABLE")
        // Now we're finally Executing SQL Commands which Creates these tables inside the Local Database ....
        db.execSQL(CREATE_FINAL_SUBMISSION_DATA_TABLE)
        Log.d(TAG, "Tables Schema created successfully ....$db")
        val CREATE_USER_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_USER
                + "(" + KEY_USER_ID + " TEXT ," +
                KEY_EMAIL + " TEXT," +
                KEY_F_NAME + " TEXT," +
                KEY_L_NAME + " TEXT," +
                KEY_PH_NO + " TEXT," +
                KEY_PROF_PIC + " TEXT," +
                KEY_COURSE_ID + " TEXT," +
                KEY_COURSE_NAME + " TEXT," +
                KEY_CLASS_ID + " TEXT," +
                KEY_CLASS_NAME + " TEXT," +
                KEY_USER_LOGIN_STATUS + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_USER query:$CREATE_USER_TABLE")
        db.execSQL(CREATE_USER_TABLE)
        Log.d(TAG, "$TABLE_USER successfully created")
        val CREATE_NOTIFICATIONS_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_NOTIFICATIONS
                + "(" + KEY_NOTIFICATION_ID + " TEXT ," +
                KEY_NOTIFICATION_HEADING + " TEXT," +
                KEY_NOTIFICATION_DATE + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_USER query:$CREATE_NOTIFICATIONS_TABLE")
        db.execSQL(CREATE_NOTIFICATIONS_TABLE)
        Log.d(TAG, "$TABLE_NOTIFICATIONS successfully created")
        val CREATE_E_BOOK_SUBJECTS_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_E_BOOK_SUBJECTS
                + "(" + KEY_SUBJECT_ID + " TEXT ," +
                KEY_SUBJECT_NAME + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_USER query:$CREATE_E_BOOK_SUBJECTS_TABLE")
        db.execSQL(CREATE_E_BOOK_SUBJECTS_TABLE)
        Log.d(TAG, "$TABLE_E_BOOK_SUBJECTS successfully created")
        val CREATE_E_BOOK_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_E_BOOK
                + "(" + KEY_E_BOOK_ID + " TEXT ," +
                KEY_E_BOOK_NAME + " TEXT," +
                KEY_E_BOOK_THUMBNAIL + " TEXT," +
                KEY_TYPE + " TEXT," +
                KEY_E_BOOK_AUTHOR + " TEXT," +
                KEY_E_BOOK_PUBLICATION + " TEXT," +
                KEY_E_BOOK_LANGUAGE + " TEXT," +
                KEY_E_BOOK_SUBJECT_ID + " TEXT," +
                KEY_DOWNLOAD_URL + " TEXT," +
                KEY_E_BOOK_PDF_NAME + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_USER query:$CREATE_E_BOOK_TABLE")
        db.execSQL(CREATE_E_BOOK_TABLE)
        Log.d(TAG, "$TABLE_E_BOOK successfully created")
        val CREATE_SUPPORT_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_SUPPORT
                + "(" + KEY_QUERY_ID + " TEXT ," +
                KEY_QUERY_TOKEN + " TEXT," +
                KEY_TITLE + " TEXT," +
                KEY_QUERY + " TEXT," +
                KEY_QUERY_EMAIL + " TEXT," +
                KEY_ANSWER + " TEXT," +
                KEY_QUERY_TIME_STAMP + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_USER query:$CREATE_SUPPORT_TABLE")
        db.execSQL(CREATE_SUPPORT_TABLE)
        Log.d(TAG, "$TABLE_SUPPORT successfully created")
        val CREATE_BANNER_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_BANNER
                + "(" + KEY_BANNER_ID + " TEXT ," +
                KEY_BANNER_TITLE + " TEXT," +
                KEY_BANNER_DESCRIPTION + " TEXT," +
                KEY_BANNER_LOGO + " TEXT," +
                KEY_BANNER_BACKGROUND + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_USER query:$CREATE_BANNER_TABLE")
        db.execSQL(CREATE_BANNER_TABLE)
        Log.d(TAG, "$TABLE_BANNER successfully created")
        val CREATE_HORIZONTAL_MENU_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_HORIZONTAL_MENU
                + "(" + KEY_HORIZONTAL_MENU_ID + " TEXT ," +
                KEY_HORIZONTAL_BACKGROUND + " TEXT," +
                KEY_HORIZONTAL_MENU_TITTLE + " TEXT," +
                KEY_HORIZONTAL_MENU_DESCRIPTION + " TEXT," +
                KEY_HORIZONTAL_MENU_TAG + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_USER query:$CREATE_HORIZONTAL_MENU_TABLE")
        db.execSQL(CREATE_HORIZONTAL_MENU_TABLE)
        Log.d(TAG, "$TABLE_HORIZONTAL_MENU successfully created")
        val CREATE_BOOKMARK_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_BOOKMARK
                + "(" + KEY_QUESTION_ID + " INTEGER ," +
                KEY_QUESTION_SUBJECT_ID + " TEXT," +
                KEY_EASSY_ID + " INTEGER," +
                KEY_QUESTION_TYPE + " TEXT," +
                KEY_QUESTION_TEXT + " TEXT," +
                KEY_EASSY_TEXT + " TEXT," +
                KEY_OPTION_A + " TEXT," +
                KEY_OPTION_B + " TEXT," +
                KEY_OPTION_C + " TEXT," +
                KEY_OPTION_D + " TEXT," +
                KEY_OPTION_E + " TEXT," +
                KEY_RIGHT_MARKS + " INTEGER," +
                KEY_WRONG_MARKS + " INTEGER," +
                KEY_NO_OPTIONS + " INTEGER," +
                KEY_RIGHT_ANS + " TEXT" + ")")
        Log.d(TAG, "TABLE_CACHE_USER query:$CREATE_BOOKMARK_TABLE")
        db.execSQL(CREATE_BOOKMARK_TABLE)
        Log.d(TAG, "$TABLE_HORIZONTAL_MENU successfully created")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        //Delete all tables --start
        // query to obtain the names of all tables in your database
        val c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null)
        val tables: MutableList<String> = ArrayList()

        // iterate over the result set, adding every table name to a list
        while (c.moveToNext()) {
            tables.add(c.getString(0))
        }

        //  Drop on every table name
        for (table in tables) {
            val dropQuery = "DROP TABLE IF EXISTS $table"
            db.execSQL(dropQuery)
        }
        c.close()
        //Delete all tables --End

        //Create new tables again
        onCreate(db)
    }

    companion object {
        private const val TAG = "CommonAssetCache"

        //Database
        private const val DATABASE_NAME = "common.db"
        private var instance: CommonAssetToLocalDatabase? = null

        //Table(s)
        //ExamResult List Table column names
        private const val TABLE_CACHE_RESULT_LIST = "result_list"
        private const val KEY_EXAM_NAME = "exam_name"
        private const val KEY_ID = "id"
        private const val KEY_EXAM_TYPE = "exam_type"
        private const val KEY_DATE = "date"
        private const val KEY_TOTAL_QUESTIONS = "total_questions"
        private const val KEY_TOTAL_MARKS = "total_marks"
        private const val KEY_ATTEMPTED_QUESTIONS = "attempted_questions"
        private const val KEY_CORRECT_QUESTIONS = "correct_questions"
        private const val KEY_TOTAL_OBTAINED_MARKS = "obtained_marks"
        private const val KEY_SUBJECT_IDS = "subject_ids"
        private const val KEY_SUBJECT_NAMES = "subject_names"

        //final_data column names
        private const val TABLE_CACHE_FINAL_DATA = "final_data"
        private const val KEY_FINAL_USER_ID = "final_user_id"
        private const val KEY_FINAL_ID = "final_id"
        private const val KEY_FINAL_EXAM_TYPE = "final_exam_type"
        private const val KEY_FINAL_DATE = "final_date"
        private const val KEY_FINAL_ALLOTTED_TIME = "final_allotted_time"
        private const val KEY_FINAL_TOTAL_QUESTIONS = "final_total_questions"
        private const val KEY_FINAL_TOTAL_MARKS = "final_total_marks"
        private const val KEY_FINAL_SUBJECT_ID = "final_subject_id"
        private const val KEY_FINAL_NO_OF_QUES_SUB_WISE = "final_no_of_ques_sub_wise"
        private const val KEY_FINAL_SUB_WISE_MAX_MARKS = "final_sub_wise_max_marks"
        private const val KEY_FINAL_QUES_ID = "final_ques_id"
        private const val KEY_FINAL_RESPONSE = "final_response"
        private const val KEY_FINAL_SUB_WISE_MARKS = "final_sub_wise_marks"
        private const val KEY_FINAL_SUB_WISE_QUESTION_DATA = "final_sub_wise_question_data"
        private const val KEY_FINAL_TOTAL_OBTAINED_MARKS = "final_total_obtained_marks"
        private const val KEY_FINAL_TOTAL_QUESTION_DATA = "final_total_question_data"
        private const val KEY_FINAL_SUB_WISE_TIME = "final_sub_wise_time"
        private const val KEY_FINAL_QUES_WISE_TIME = "final_ques_wise_time"
        private const val KEY_FINAL_OVERALL_TIME = "final_overall_time"
        private const val KEY_SYNC_STATUS = "sync_status"

        //User Table and Columns
        private const val TABLE_USER = "user"
        private const val KEY_USER_ID = "user_id"
        private const val KEY_EMAIL = "email"
        private const val KEY_F_NAME = "f_name"
        private const val KEY_L_NAME = "l_name"
        private const val KEY_PH_NO = "ph_no"
        private const val KEY_PROF_PIC = "prof_pic"
        private const val KEY_COURSE_ID = "course_id"
        private const val KEY_COURSE_NAME = "course_name"
        private const val KEY_CLASS_ID = "class_id"
        private const val KEY_CLASS_NAME = "class_name"
        private const val KEY_USER_LOGIN_STATUS = "user_login_status"

        //Notification Table and Columns
        private const val TABLE_NOTIFICATIONS = "notifications"
        private const val KEY_NOTIFICATION_ID = "notification_id"
        private const val KEY_NOTIFICATION_HEADING = "notification_heading"
        private const val KEY_NOTIFICATION_DATE = "notification_date"

        //EBook Subjects
        private const val TABLE_E_BOOK_SUBJECTS = "e_book_subjects"
        private const val KEY_SUBJECT_ID = "subject_id"
        private const val KEY_SUBJECT_NAME = "subject_name"

        //EBook
        private const val TABLE_E_BOOK = "e_book"
        private const val KEY_E_BOOK_ID = "e_book_id"
        private const val KEY_E_BOOK_PDF_NAME = "e_book_pdf_name"
        private const val KEY_E_BOOK_THUMBNAIL = "e_book_thumbnail"
        private const val KEY_TYPE = "type"
        private const val KEY_E_BOOK_NAME = "e_book_name"
        private const val KEY_E_BOOK_AUTHOR = "e_book_author"
        private const val KEY_E_BOOK_PUBLICATION = "e_book_publication"
        private const val KEY_E_BOOK_LANGUAGE = "e_book_language"
        private const val KEY_E_BOOK_SUBJECT_ID = "e_book_subject_id"
        private const val KEY_DOWNLOAD_URL = "download_url"

        //Support
        private const val TABLE_SUPPORT = "support"
        private const val KEY_QUERY_ID = "id"
        private const val KEY_QUERY_TOKEN = "query_token"
        private const val KEY_TITLE = "title"
        private const val KEY_QUERY = "query"
        private const val KEY_QUERY_EMAIL = "email"
        private const val KEY_ANSWER = "answer"
        private const val KEY_QUERY_TIME_STAMP = "time_stamp"

        //Banner
        private const val TABLE_BANNER = "banner"
        private const val KEY_BANNER_ID = "id"
        private const val KEY_BANNER_TITLE = "title"
        private const val KEY_BANNER_DESCRIPTION = "description"
        private const val KEY_BANNER_LOGO = "logo"
        private const val KEY_BANNER_BACKGROUND = "background"

        //HorizontalMenu
        private const val TABLE_HORIZONTAL_MENU = "horizontal_menu"
        private const val KEY_HORIZONTAL_MENU_ID = "id"
        private const val KEY_HORIZONTAL_BACKGROUND = "BACKGROUND"
        private const val KEY_HORIZONTAL_MENU_TITTLE = "tittle"
        private const val KEY_HORIZONTAL_MENU_DESCRIPTION = "description"
        private const val KEY_HORIZONTAL_MENU_TAG = "tag"

        //Bookmark table
        private const val TABLE_BOOKMARK = "bookmark"
        private const val KEY_QUESTION_SUBJECT_ID = "subject_id"
        private const val KEY_QUESTION_ID = "question_id"
        private const val KEY_EASSY_ID = "eassy_id"
        private const val KEY_QUESTION_TYPE = "question_type"
        private const val KEY_QUESTION_TEXT = "question_ext"
        private const val KEY_EASSY_TEXT = "eassy_ext"
        private const val KEY_OPTION_A = "option_a"
        private const val KEY_OPTION_B = "option_b"
        private const val KEY_OPTION_C = "option_c"
        private const val KEY_OPTION_D = "option_d"
        private const val KEY_OPTION_E = "option_e"
        private const val KEY_RIGHT_MARKS = "right_marks"
        private const val KEY_WRONG_MARKS = "wrong_marks"
        private const val KEY_NO_OPTIONS = "no_options"
        private const val KEY_RIGHT_ANS = "right_ANS"

        /**
         * Call CommonAssetToLocalDatabase getHelper for accessing the common.db database
         *
         * @param context Context of the calling class
         * @return Returns the instance of the CommonAssetToLocalDatabase
         */
        fun getHelper(context: Context): CommonAssetToLocalDatabase? {
            if (instance == null) {
                instance = CommonAssetToLocalDatabase(context)
            }
            return instance
        }

        fun clearAllTables(context: Context) {
            val db = getHelper(context)!!.writableDatabase
            // query to obtain the names of all tables in your database
            val c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null)
            val tables: MutableList<String> = ArrayList()

            // iterate over the result set, adding every table name to a list
            while (c.moveToNext()) {
                tables.add(c.getString(0))
            }

            //  Delete data from every table name
            for (table in tables) {
                val dropQuery = "DELETE FROM  $table"
                db.execSQL(dropQuery)
            }
            c.close()
        }
    }

    init {
        Log.d(TAG, "Constructor CommonAssetToLocalDatabase() invoked")
    }
}
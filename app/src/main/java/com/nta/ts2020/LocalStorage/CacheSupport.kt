package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteException
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.PojoClasses.SupportPojo
import java.util.*

/**
 * Created by suvajit on 23/6/17.
 */
class CacheSupport(private val context: Context) {
    fun saveQueries(supportPojoArrayList: ArrayList<SupportPojo>): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        for (i in supportPojoArrayList.indices) {
            val supportPojo = supportPojoArrayList[i]
            val contentValues = ContentValues()
            contentValues.put(KEY_QUERY_ID, supportPojo.queryId)
            contentValues.put(KEY_QUERY_TOKEN, supportPojo.queryToken)
            contentValues.put(KEY_TITLE, supportPojo.title)
            contentValues.put(KEY_QUERY, supportPojo.query)
            contentValues.put(KEY_QUERY_EMAIL, supportPojo.email)
            contentValues.put(KEY_ANSWER, supportPojo.answer)
            contentValues.put(KEY_QUERY_TIME_STAMP, supportPojo.date)
            try {
                sqLiteDatabase?.insertOrThrow(TABLE_SUPPORT, null, contentValues)
            } catch (e: SQLiteException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                return false
            }
        }
        return true
    }

    val queries: ArrayList<SupportPojo>
        get() {
            val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
            val supportPojoArrayList = ArrayList<SupportPojo>()
            val cursor = sqLiteDatabase?.query(TABLE_SUPPORT, null, null, null, null, null, null)
            if (cursor?.moveToFirst()!!) {
                do {
                    val supportPojo = SupportPojo()
                    supportPojo.queryId = cursor.getString(cursor.getColumnIndex(KEY_QUERY_ID))
                    supportPojo.queryToken = cursor.getString(cursor.getColumnIndex(KEY_QUERY_TOKEN))
                    supportPojo.title = cursor.getString(cursor.getColumnIndex(KEY_TITLE))
                    supportPojo.query = cursor.getString(cursor.getColumnIndex(KEY_QUERY))
                    supportPojo.email = cursor.getString(cursor.getColumnIndex(KEY_QUERY_EMAIL))
                    supportPojo.answer = cursor.getString(cursor.getColumnIndex(KEY_ANSWER))
                    supportPojo.date = cursor.getString(cursor.getColumnIndex(KEY_QUERY_TIME_STAMP))
                    supportPojoArrayList.add(supportPojo)
                } while (cursor.moveToNext())
            }
            cursor.close()
            return supportPojoArrayList
        }

    fun clearRecords() {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        //Clear all old records
        sqLiteDatabase?.execSQL("DELETE FROM $TABLE_SUPPORT")
    }

    companion object {
        private const val TAG = "CacheSupport"
        private const val TABLE_SUPPORT = "support"
        private const val KEY_QUERY_ID = "id"
        private const val KEY_QUERY_TOKEN = "query_token"
        private const val KEY_TITLE = "title"
        private const val KEY_QUERY = "query"
        private const val KEY_QUERY_EMAIL = "email"
        private const val KEY_ANSWER = "answer"
        private const val KEY_QUERY_TIME_STAMP = "time_stamp"
    }

}
package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteException
import android.util.Log
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.PojoClasses.FinalSubmissionPojo
import java.util.*

class ExamCacheFinalSubmission(private val context: Context) {
    fun setFinalSubmissionData(finalSubmissionDataArrayList: ArrayList<FinalSubmissionPojo>): Boolean {
        Log.i(TAG, "setFinalSubmissionData() called")
        val mSQLiteInstance = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        for (i in finalSubmissionDataArrayList.indices) {
            val sqLiteContentValuesInstance = ContentValues()
            val finalSubmissionData = finalSubmissionDataArrayList[i]
            val cursor = mSQLiteInstance?.query(TABLE_CACHE_FINAL_DATA, arrayOf(KEY_FINAL_DATE),
                    KEY_FINAL_EXAM_ID + " = '" + finalSubmissionData.examId + "'", null, null, null, null)
            if (cursor?.count!! <= 0) {
                //preparing content values for insertion when there is new data
                sqLiteContentValuesInstance.put(KEY_FINAL_USER_ID, finalSubmissionData.userId)
                sqLiteContentValuesInstance.put(KEY_FINAL_EXAM_ID, finalSubmissionData.examId)
                sqLiteContentValuesInstance.put(KEY_FINAL_EXAM_TYPE, finalSubmissionData.examType)
                sqLiteContentValuesInstance.put(KEY_FINAL_DATE, finalSubmissionData.date)
                sqLiteContentValuesInstance.put(KEY_FINAL_ALLOTTED_TIME, finalSubmissionData.allottedTime)
                sqLiteContentValuesInstance.put(KEY_FINAL_TOTAL_QUESTIONS, finalSubmissionData.totalQues)
                sqLiteContentValuesInstance.put(KEY_FINAL_TOTAL_MARKS, finalSubmissionData.totalMarks)
                sqLiteContentValuesInstance.put(KEY_FINAL_SUBJECT_ID, finalSubmissionData.subjectIds.toString().replace("[", "").replace("]", ""))
                sqLiteContentValuesInstance.put(KEY_FINAL_NO_OF_QUES_SUB_WISE, finalSubmissionData.nosOfQuesSubWise.toString().replace("[", "").replace("]", ""))
                sqLiteContentValuesInstance.put(KEY_FINAL_SUB_WISE_MAX_MARKS, finalSubmissionData.subWiseMarks.toString().replace("[", "").replace("]", ""))
                sqLiteContentValuesInstance.put(KEY_FINAL_QUES_ID, finalSubmissionData.quesIds.toString().replace("[", "").replace("]", ""))
                //Add 0 for blank responses
                val tempResponses = finalSubmissionData.responses
                val finalResponses = ArrayList<String>()
                for (j in tempResponses!!.indices) {
                    if (tempResponses[j] == "") {
                        finalResponses.add("0")
                    } else {
                        finalResponses.add(tempResponses[j])
                    }
                }
                sqLiteContentValuesInstance.put(KEY_FINAL_RESPONSE, finalResponses.toString().replace("[", "").replace("]", ""))
                sqLiteContentValuesInstance.put(KEY_FINAL_SUB_WISE_MARKS, finalSubmissionData.subWiseMarks.toString().replace("[", "").replace("]", ""))
                sqLiteContentValuesInstance.put(KEY_FINAL_SUB_WISE_QUESTION_DATA, finalSubmissionData.subWiseQuesData.toString().replace("[", "").replace("]", ""))
                sqLiteContentValuesInstance.put(KEY_FINAL_TOTAL_OBTAINED_MARKS, finalSubmissionData.totalObtainedMarks)
                sqLiteContentValuesInstance.put(KEY_FINAL_TOTAL_QUESTION_DATA, finalSubmissionData.totalQuesData)
                sqLiteContentValuesInstance.put(KEY_FINAL_SUB_WISE_TIME, finalSubmissionData.subWiseTime.toString().replace("[", "").replace("]", ""))
                sqLiteContentValuesInstance.put(KEY_FINAL_QUES_WISE_TIME, finalSubmissionData.quesWiseTime.toString().replace("[", "").replace("]", ""))
                sqLiteContentValuesInstance.put(KEY_FINAL_OVERALL_TIME, finalSubmissionData.overAllTime)
                sqLiteContentValuesInstance.put(KEY_SYNC_STATUS, finalSubmissionData.isSync)
                Log.i(TAG, "Inserting Data to " + TABLE_CACHE_FINAL_DATA + " table with exam id = " + finalSubmissionData.examId)
                try {
                    mSQLiteInstance.insertOrThrow(TABLE_CACHE_FINAL_DATA, null, sqLiteContentValuesInstance)
                } catch (e: SQLiteException) {
                    if (BuildConfig.DEBUG) e.printStackTrace()
                    return false
                }
                Log.i(TAG, "Examination Data inserted successfully into the $TABLE_CACHE_FINAL_DATA table")
            }
            cursor.close()
        }
        return true
    }

    /**
     * This is  the overloaded function for [getFinalSubmissionData][.getFinalSubmissionData]
     *
     * @return [ArrayList] of [FinalSubmissionPojo]
     */
    val finalSubmissionData: ArrayList<FinalSubmissionPojo>
        get() = getFinalSubmissionData(null)

    /**
     * Returns records based on the arguments it receives <br></br>
     * 1) Null : Returns all records of the final_data table <br></br>
     * 2) Exam_id or Quiz_id : Returns records based on the id <br></br>
     * 3) SYNC : By sending 'sync' parameter it will only return records which are needed to sync with server
     *
     * @param arg null, examId , quizId or 'sync'
     * @return [ArrayList] of [FinalSubmissionPojo]
     */
    fun getFinalSubmissionData(arg: String?): ArrayList<FinalSubmissionPojo> {
        //Object for returning result
        val finalSubmissionData = ArrayList<FinalSubmissionPojo>()

        // Obtained instance of Readable SQLite Database.
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase

        //Preparing query to execute
        var cursor: Cursor? = null
        cursor = if (arg == null || arg == "") {
            sqLiteDatabase?.query(TABLE_CACHE_FINAL_DATA, null, null, null, null, null, null)
        } else {
            if (arg.equals("sync", ignoreCase = true)) {
                sqLiteDatabase?.query(TABLE_CACHE_FINAL_DATA, null, "$KEY_SYNC_STATUS = 0", null, null, null, null)
            } else {
                sqLiteDatabase?.query(TABLE_CACHE_FINAL_DATA, null, "$KEY_FINAL_EXAM_ID = '$arg'", null, null, null, null)
            }
        }
        if (cursor?.moveToFirst()!!) {
            do {
                val finalSubmissionPojo = FinalSubmissionPojo()
                //TODO: Change user id to dynamic values
                finalSubmissionPojo.userId = cursor.getInt(cursor.getColumnIndex(KEY_FINAL_USER_ID))
                finalSubmissionPojo.examId = cursor.getString(cursor.getColumnIndex(KEY_FINAL_EXAM_ID))
                finalSubmissionPojo.examType = cursor.getString(cursor.getColumnIndex(KEY_FINAL_EXAM_TYPE))
                finalSubmissionPojo.date = cursor.getString(cursor.getColumnIndex(KEY_FINAL_DATE))
                finalSubmissionPojo.allottedTime = cursor.getInt(cursor.getColumnIndex(KEY_FINAL_ALLOTTED_TIME))
                finalSubmissionPojo.totalQues = cursor.getInt(cursor.getColumnIndex(KEY_FINAL_TOTAL_QUESTIONS))
                finalSubmissionPojo.totalMarks = cursor.getInt(cursor.getColumnIndex(KEY_FINAL_TOTAL_MARKS))
                finalSubmissionPojo.subjectIds = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_SUBJECT_ID)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.nosOfQuesSubWise = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_NO_OF_QUES_SUB_WISE)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.subWiseMaxMarks = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_SUB_WISE_MAX_MARKS)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.quesIds = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_QUES_ID)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.responses = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_RESPONSE)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.subWiseMarks = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_SUB_WISE_MARKS)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.subWiseQuesData = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_SUB_WISE_QUESTION_DATA)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.totalObtainedMarks = cursor.getInt(cursor.getColumnIndex(KEY_FINAL_TOTAL_OBTAINED_MARKS)).toFloat()
                finalSubmissionPojo.totalQuesData = cursor.getString(cursor.getColumnIndex(KEY_FINAL_TOTAL_QUESTION_DATA))
                finalSubmissionPojo.subWiseTime = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_SUB_WISE_TIME)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.quesWiseTime = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_FINAL_QUES_WISE_TIME)).split(", ".toRegex()).toTypedArray()))
                finalSubmissionPojo.overAllTime = cursor.getInt(cursor.getColumnIndex(KEY_FINAL_OVERALL_TIME)).toLong()
                finalSubmissionPojo.setSyncStatus(cursor.getInt(cursor.getColumnIndex(KEY_SYNC_STATUS)) == 1)
                finalSubmissionData.add(finalSubmissionPojo)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return finalSubmissionData
    }

    /**
     * Updates the sync status of the records provided
     *
     * @param finalSubmissionDataArrayList The list of records need to be updated
     * @param syncStatus                   Status : true/false
     */
    fun updateSyncStatus(finalSubmissionDataArrayList: ArrayList<FinalSubmissionPojo>, syncStatus: Boolean?) {
        Log.d(TAG, "updateSyncStatus() invoked")
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_SYNC_STATUS, syncStatus)
        for (i in finalSubmissionDataArrayList.indices) {
            val id = finalSubmissionDataArrayList[i].examId
            Log.d(TAG, "Preparing to update record with exam id:$id")
            sqLiteDatabase?.update(TABLE_CACHE_FINAL_DATA, contentValues, "$KEY_FINAL_EXAM_ID = '$id'", null)
        }
    }

    fun clearRecords() {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        //Clear all old records
        sqLiteDatabase?.execSQL("DELETE FROM $TABLE_CACHE_FINAL_DATA")
    }

    companion object {
        private const val TAG = "CacheFinalSubmission"

        //Table & Columns names
        private const val TABLE_CACHE_FINAL_DATA = "final_data"
        private const val KEY_FINAL_USER_ID = "final_user_id"
        private const val KEY_FINAL_EXAM_ID = "final_id"
        private const val KEY_FINAL_EXAM_TYPE = "final_exam_type"
        private const val KEY_FINAL_DATE = "final_date"
        private const val KEY_FINAL_ALLOTTED_TIME = "final_allotted_time"
        private const val KEY_FINAL_TOTAL_QUESTIONS = "final_total_questions"
        private const val KEY_FINAL_TOTAL_MARKS = "final_total_marks"
        private const val KEY_FINAL_SUBJECT_ID = "final_subject_id"
        private const val KEY_FINAL_NO_OF_QUES_SUB_WISE = "final_no_of_ques_sub_wise"
        private const val KEY_FINAL_SUB_WISE_MAX_MARKS = "final_sub_wise_max_marks"
        private const val KEY_FINAL_QUES_ID = "final_ques_id"
        private const val KEY_FINAL_RESPONSE = "final_response"
        private const val KEY_FINAL_SUB_WISE_MARKS = "final_sub_wise_marks"
        private const val KEY_FINAL_SUB_WISE_QUESTION_DATA = "final_sub_wise_question_data"
        private const val KEY_FINAL_TOTAL_OBTAINED_MARKS = "final_total_obtained_marks"
        private const val KEY_FINAL_TOTAL_QUESTION_DATA = "final_total_question_data"
        private const val KEY_FINAL_SUB_WISE_TIME = "final_sub_wise_time"
        private const val KEY_FINAL_QUES_WISE_TIME = "final_ques_wise_time"
        private const val KEY_FINAL_OVERALL_TIME = "final_overall_time"
        private const val KEY_SYNC_STATUS = "sync_status"
    }

    init {
        Log.i(TAG, "Constructor ExamCacheFinalSubmission() invoked.")
    }
}
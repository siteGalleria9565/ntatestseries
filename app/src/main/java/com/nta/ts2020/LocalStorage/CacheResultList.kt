package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.AppConstants.ConstantVariables.QUIZ
import com.nta.ts2020.PojoClasses.ResultListPojo
import java.util.*

class CacheResultList(//context
        private val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    fun setResultListToCache(resultListPojoArrayList: ArrayList<ResultListPojo>): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        val contentValues = ContentValues()
        for (i in resultListPojoArrayList.indices) {
            val cursor = sqLiteDatabase?.query(TABLE_CACHE_RESULT_LIST, null, KEY_ID + "='" + resultListPojoArrayList[i].id + "'", null, null, null, null)

//            Log.d(TAG, String.valueOf(cursor.getCount()) + "  id == " + resultListPojoArrayList.get(i).getId());
            if (cursor?.count!! <= 0) {
                val tempResultListPojo = resultListPojoArrayList[i]
                contentValues.put(KEY_EXAM_NAME, tempResultListPojo.examName)
                contentValues.put(KEY_ID, tempResultListPojo.id)
                contentValues.put(KEY_EXAM_TYPE, tempResultListPojo.examType)
                contentValues.put(KEY_DATE, tempResultListPojo.date)
                contentValues.put(KEY_TOTAL_QUESTIONS, tempResultListPojo.totalQues)
                contentValues.put(KEY_TOTAL_MARKS, tempResultListPojo.totalMarks)
                contentValues.put(KEY_ATTEMPTED_QUESTIONS, tempResultListPojo.attemptedQues)
                contentValues.put(KEY_CORRECT_QUESTIONS, tempResultListPojo.correctQues)
                contentValues.put(KEY_TOTAL_OBTAINED_MARKS, tempResultListPojo.totalObtainedMarks)
                contentValues.put(KEY_SUBJECT_IDS, tempResultListPojo.subjectIds.toString().replace("[", "").replace("]", ""))
                contentValues.put(KEY_SUBJECT_NAMES, tempResultListPojo.subjectNames.toString().replace("[", "").replace("]", ""))
                try {
                    sqLiteDatabase.insertOrThrow(TABLE_CACHE_RESULT_LIST, null, contentValues)
                } catch (e: SQLiteException) {
                    e.printStackTrace()
                    return false
                }

//                Log.d(TAG, i + ") Data inserted to '" + TABLE_CACHE_RESULT_LIST + "' table");
            }
            cursor.close()
        }
        return true
    }

    /**
     * OverLoaded function for [getResultListFromCache][.getResultListFromCache]
     *
     * @return [ArrayList] of [ResultListPojo]
     */
    val resultListFromCache: ArrayList<ResultListPojo>
        get() = getResultListFromCache(null)

    fun getResultListFromCache(id: String?): ArrayList<ResultListPojo> {

//        Log.d(TAG, "getResultListFromCache invoked");
        val resultListPojoArrayList = ArrayList<ResultListPojo>()
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
        var cursor: Cursor? = null
        cursor = if (id == null || id == "") {
            sqLiteDatabase?.query(TABLE_CACHE_RESULT_LIST, null, null, null, null, null, null)
        } else {
            sqLiteDatabase?.query(TABLE_CACHE_RESULT_LIST, null, "$KEY_ID = '$id'", null, null, null, null)
        }
        if (cursor?.moveToFirst()!!) {
            do {
                val resultListPojo = ResultListPojo()
                resultListPojo.examName = cursor.getString(cursor.getColumnIndex(KEY_EXAM_NAME))
                resultListPojo.id = cursor.getString(cursor.getColumnIndex(KEY_ID))
                resultListPojo.examType = cursor.getString(cursor.getColumnIndex(KEY_EXAM_TYPE))
                resultListPojo.date = cursor.getString(cursor.getColumnIndex(KEY_DATE))
                resultListPojo.totalQues = cursor.getInt(cursor.getColumnIndex(KEY_TOTAL_QUESTIONS))
                resultListPojo.totalMarks = cursor.getFloat(cursor.getColumnIndex(KEY_TOTAL_MARKS))
                resultListPojo.attemptedQues = cursor.getInt(cursor.getColumnIndex(KEY_ATTEMPTED_QUESTIONS))
                resultListPojo.correctQues = cursor.getInt(cursor.getColumnIndex(KEY_CORRECT_QUESTIONS))
                resultListPojo.totalObtainedMarks = cursor.getFloat(cursor.getColumnIndex(KEY_TOTAL_OBTAINED_MARKS))
                resultListPojo.subjectIds = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_SUBJECT_IDS)).split(", ".toRegex()).toTypedArray()))
                resultListPojo.subjectNames = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_SUBJECT_NAMES)).split(", ".toRegex()).toTypedArray()))
                resultListPojoArrayList.add(resultListPojo)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return resultListPojoArrayList
    }

    /**
     * @param type Type of the result which is EXAM = 0  or QUIZ = 1
     * @return [ArrayList] of [ResultListPojo] for Exam or Quiz
     */
    fun getResultListFromCache(type: Int): ArrayList<ResultListPojo> {
//        Log.d(TAG, "getResultListFromCache invoked");
        val resultListPojoArrayList = ArrayList<ResultListPojo>()
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
        var cursor: Cursor? = null
        var examType = "EXM"
        if (type == QUIZ) examType = "QUIZ"
        cursor = sqLiteDatabase?.query(TABLE_CACHE_RESULT_LIST, null, "$KEY_EXAM_TYPE = '$examType'", null, null, null, null)
        if (cursor?.moveToFirst()!!) {
            do {
                val resultListPojo = ResultListPojo()
                resultListPojo.examName = cursor.getString(cursor.getColumnIndex(KEY_EXAM_NAME))
                resultListPojo.id = cursor.getString(cursor.getColumnIndex(KEY_ID))
                resultListPojo.examType = cursor.getString(cursor.getColumnIndex(KEY_EXAM_TYPE))
                resultListPojo.date = cursor.getString(cursor.getColumnIndex(KEY_DATE))
                resultListPojo.totalQues = cursor.getInt(cursor.getColumnIndex(KEY_TOTAL_QUESTIONS))
                resultListPojo.totalMarks = cursor.getFloat(cursor.getColumnIndex(KEY_TOTAL_MARKS))
                resultListPojo.attemptedQues = cursor.getInt(cursor.getColumnIndex(KEY_ATTEMPTED_QUESTIONS))
                resultListPojo.correctQues = cursor.getInt(cursor.getColumnIndex(KEY_CORRECT_QUESTIONS))
                resultListPojo.totalObtainedMarks = cursor.getFloat(cursor.getColumnIndex(KEY_TOTAL_OBTAINED_MARKS))
                resultListPojo.subjectIds = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_SUBJECT_IDS)).split(", ".toRegex()).toTypedArray()))
                resultListPojo.subjectNames = ArrayList(Arrays.asList(*cursor.getString(cursor.getColumnIndex(KEY_SUBJECT_NAMES)).split(", ".toRegex()).toTypedArray()))
                resultListPojoArrayList.add(resultListPojo)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return resultListPojoArrayList
    }

    /**
     * Clear all records from the table
     */
    fun clearAllRecords() {
        this.writableDatabase.execSQL("delete from $TABLE_CACHE_RESULT_LIST")
    }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {
        private const val TAG = "CacheResultList"

        //Database
        private const val DATABASE_NAME = "common.db"

        //Table(s)
        //ExamResult List Table column names
        private const val TABLE_CACHE_RESULT_LIST = "result_list"
        private const val KEY_EXAM_NAME = "exam_name"
        private const val KEY_ID = "id"
        private const val KEY_EXAM_TYPE = "exam_type"
        private const val KEY_DATE = "date"
        private const val KEY_TOTAL_QUESTIONS = "total_questions"
        private const val KEY_TOTAL_MARKS = "total_marks"
        private const val KEY_ATTEMPTED_QUESTIONS = "attempted_questions"
        private const val KEY_CORRECT_QUESTIONS = "correct_questions"
        private const val KEY_TOTAL_OBTAINED_MARKS = "obtained_marks"
        private const val KEY_SUBJECT_IDS = "subject_ids"
        private const val KEY_SUBJECT_NAMES = "subject_names"
    }

}
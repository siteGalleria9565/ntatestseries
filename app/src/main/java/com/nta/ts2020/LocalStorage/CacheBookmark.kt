package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteException
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import java.util.*

class CacheBookmark(private val context: Context) {
    fun saveBookmarkedQuestion(examAllQuestionsSubjectWisePojoInstance: ExamAllQuestionsSubjectWisePojo): Boolean {
        val mSQLiteInstance = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        val cursor = mSQLiteInstance?.query(TABLE_BOOKMARK, null,
                KEY_QUESTION_ID + " = '" + examAllQuestionsSubjectWisePojoInstance.questionId + "'", null, null, null, null)
        if (cursor?.count!! <= 0) {
            val contentValues = ContentValues()
            contentValues.put(KEY_QUESTION_ID, examAllQuestionsSubjectWisePojoInstance.questionId)
            contentValues.put(KEY_QUESTION_SUBJECT_ID, examAllQuestionsSubjectWisePojoInstance.subjectId)
            contentValues.put(KEY_EASSY_ID, examAllQuestionsSubjectWisePojoInstance.essayId)
            contentValues.put(KEY_QUESTION_TYPE, examAllQuestionsSubjectWisePojoInstance.questionType)
            contentValues.put(KEY_QUESTION_TEXT, examAllQuestionsSubjectWisePojoInstance.questionText)
            contentValues.put(KEY_EASSY_TEXT, examAllQuestionsSubjectWisePojoInstance.essayData)
            contentValues.put(KEY_OPTION_A, examAllQuestionsSubjectWisePojoInstance.optionA)
            contentValues.put(KEY_OPTION_B, examAllQuestionsSubjectWisePojoInstance.optionB)
            contentValues.put(KEY_OPTION_C, examAllQuestionsSubjectWisePojoInstance.optionC)
            contentValues.put(KEY_OPTION_D, examAllQuestionsSubjectWisePojoInstance.optionD)
            contentValues.put(KEY_OPTION_E, examAllQuestionsSubjectWisePojoInstance.optionE)
            contentValues.put(KEY_RIGHT_MARKS, examAllQuestionsSubjectWisePojoInstance.rightMarks)
            contentValues.put(KEY_WRONG_MARKS, examAllQuestionsSubjectWisePojoInstance.wrongMarks)
            contentValues.put(KEY_NO_OPTIONS, examAllQuestionsSubjectWisePojoInstance.noOptions)
            contentValues.put(KEY_RIGHT_ANS, examAllQuestionsSubjectWisePojoInstance.rightAns)

            // Inserting Row
            try {
                mSQLiteInstance?.insertOrThrow(TABLE_BOOKMARK, null, contentValues)
            } catch (e: SQLiteException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                cursor.close()
                return false
            }
        }
        cursor.close()
        return true
    }

    fun isBookmarked(questionId: String): Boolean {
        val mSQLiteInstance = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
        val cursor = mSQLiteInstance?.query(TABLE_BOOKMARK, null,
                "$KEY_QUESTION_ID = '$questionId'", null, null, null, null)
        val count = cursor?.count
        cursor?.close()
        if (count != null) {
            return count > 0
        }
        return false
    }

    fun removeBookmark(questionId: String): Boolean {
        val mSQLiteInstance = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        val count = mSQLiteInstance?.delete(TABLE_BOOKMARK, "$KEY_QUESTION_ID = '$questionId'", null)
        if (count != null) {
            return count > 0
        }
        return false
    }

    // Obtained instance of Readable SQLite Database.
    val bookmarkedQuestions: ArrayList<ExamAllQuestionsSubjectWisePojo>
        get() {
            val localAllExaminationDataList = ArrayList<ExamAllQuestionsSubjectWisePojo>()

            // Obtained instance of Readable SQLite Database.
            val mSQLiteInstance = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase


            // Executing the Query.
            val cursor = mSQLiteInstance?.query(TABLE_BOOKMARK, null, null, null, null, null, null)
            if (cursor?.moveToFirst()!!) {
                do {
                    val examAllQuestionsSubjectWisePojoInstance = ExamAllQuestionsSubjectWisePojo()
                    examAllQuestionsSubjectWisePojoInstance.questionId = cursor?.getString(0)
                    examAllQuestionsSubjectWisePojoInstance.subjectId = cursor?.getString(1)
                    examAllQuestionsSubjectWisePojoInstance.essayId = cursor?.getString(2)
                    examAllQuestionsSubjectWisePojoInstance.questionType = cursor?.getString(3)
                    examAllQuestionsSubjectWisePojoInstance.questionText = cursor?.getString(4)
                    examAllQuestionsSubjectWisePojoInstance.essayData = cursor?.getString(5)
                    examAllQuestionsSubjectWisePojoInstance.optionA = cursor?.getString(6)
                    examAllQuestionsSubjectWisePojoInstance.optionB = cursor?.getString(7)
                    examAllQuestionsSubjectWisePojoInstance.optionC = cursor?.getString(8)
                    examAllQuestionsSubjectWisePojoInstance.optionD = cursor?.getString(9)
                    examAllQuestionsSubjectWisePojoInstance.optionE = cursor?.getString(10)
                    examAllQuestionsSubjectWisePojoInstance.rightMarks = cursor?.getString(11)
                    examAllQuestionsSubjectWisePojoInstance.wrongMarks = cursor?.getString(12)
                    examAllQuestionsSubjectWisePojoInstance.noOptions = cursor?.getString(13)
                    examAllQuestionsSubjectWisePojoInstance.rightAns = cursor?.getString(14)
                    localAllExaminationDataList.add(examAllQuestionsSubjectWisePojoInstance)
                } while (cursor?.moveToNext()!!)
            }
            cursor?.close()

            // return Contacts list
            return localAllExaminationDataList
        }

    companion object {
        private const val TABLE_BOOKMARK = "bookmark"
        private const val KEY_QUESTION_SUBJECT_ID = "subject_id"
        private const val KEY_QUESTION_ID = "question_id"
        private const val KEY_EASSY_ID = "eassy_id"
        private const val KEY_QUESTION_TYPE = "question_type"
        private const val KEY_QUESTION_TEXT = "question_ext"
        private const val KEY_EASSY_TEXT = "eassy_ext"
        private const val KEY_OPTION_A = "option_a"
        private const val KEY_OPTION_B = "option_b"
        private const val KEY_OPTION_C = "option_c"
        private const val KEY_OPTION_D = "option_d"
        private const val KEY_OPTION_E = "option_e"
        private const val KEY_RIGHT_MARKS = "right_marks"
        private const val KEY_WRONG_MARKS = "wrong_marks"
        private const val KEY_NO_OPTIONS = "no_options"
        private const val KEY_RIGHT_ANS = "right_ANS"
    }

}
package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.SQLException
import com.nta.ts2020.PojoClasses.NotificationsPojo
import java.util.*

class CacheNotifications(//Context
        private val context: Context) {
    fun saveNotificationsToCache(notificationsPojoArrayList: ArrayList<NotificationsPojo>): Boolean {
        //Get writable instance
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase

        //Prepare query
        val contentValues = ContentValues()
        for (i in notificationsPojoArrayList.indices) {
            contentValues.put(KEY_NOTIFICATION_ID, notificationsPojoArrayList[i].notificationId)
            contentValues.put(KEY_NOTIFICATION_HEADING, notificationsPojoArrayList[i].notificationHeading)
            contentValues.put(KEY_NOTIFICATION_DATE, notificationsPojoArrayList[i].notificationDate)
            try {
                sqLiteDatabase?.insertOrThrow(TABLE_NOTIFICATIONS, null, contentValues)
            } catch (e: SQLException) {
                e.printStackTrace()
                return false
            }
            //            Log.d(TAG, "Notifications Saved with id: " + notificationsPojoArrayList.get(i).getNotificationId());
        }

//        Log.d(TAG, "All Notifications Saved");
        return true
    }

    //ArrayList to return data
    val notificationsFromCache: ArrayList<NotificationsPojo>
        get() {
            //ArrayList to return data
            val notificationsPojoArrayList = ArrayList<NotificationsPojo>()

            //Get readable instance
            val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase

            //Cursor for getting data
            val cursor = sqLiteDatabase?.query(TABLE_NOTIFICATIONS, null, null, null, null, null, null, null)
            if (cursor?.moveToFirst()!!) {
                do {
                    notificationsPojoArrayList.add(NotificationsPojo(
                            cursor.getInt(cursor.getColumnIndex(KEY_NOTIFICATION_ID)),
                            cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_HEADING)),
                            cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATION_DATE))
                    ))
                } while (cursor.moveToNext())
            }
            cursor.close()
            return notificationsPojoArrayList
        }

    fun clearRecords() {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        //Clear all old records
        sqLiteDatabase?.execSQL("DELETE FROM $TABLE_NOTIFICATIONS")
    }

    companion object {
        private const val TAG = "CacheNotifications"

        //Table and Columns
        private const val TABLE_NOTIFICATIONS = "notifications"
        private const val KEY_NOTIFICATION_ID = "notification_id"
        private const val KEY_NOTIFICATION_HEADING = "notification_heading"
        private const val KEY_NOTIFICATION_DATE = "notification_date"
    }

}
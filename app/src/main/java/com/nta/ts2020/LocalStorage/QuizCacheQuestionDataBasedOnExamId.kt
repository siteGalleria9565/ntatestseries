package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.LocalStorage.QuizAssetToLocalDatabase.Companion.getHelper
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import java.util.*

class QuizCacheQuestionDataBasedOnExamId(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var TABLE_CACHE_QUESTION_DATA = "questions_data"

    // Students_Profile Table Columns names ....
    var KEY_QUESTION_SUBJECT_ID = "subject_id"
    var KEY_QUESTION_ID = "question_id"
    var KEY_EASSY_ID = "eassy_id"
    var KEY_QUESTION_TYPE = "question_type"
    var KEY_QUESTION_TEXT = "question_ext"
    var KEY_EASSY_TEXT = "eassy_ext"
    var KEY_OPTION_A = "option_a"
    var KEY_OPTION_B = "option_b"
    var KEY_OPTION_C = "option_c"
    var KEY_OPTION_D = "option_d"
    var KEY_OPTION_E = "option_e"
    var KEY_RIGHT_MARKS = "right_marks"
    var KEY_WRONG_MARKS = "wrong_marks"
    var KEY_NO_OPTIONS = "no_options"
    var KEY_RIGHT_ANS = "right_ANS"
    var projection: Array<String?>? = null
    var selection: String? = null
    var selectionArgs: Array<String?>? = null
    var myDataBase: SQLiteDatabase? = null
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    var sqliteContentValuesInstance: ContentValues? = null

    // POJOs ....
    var localQuestionDataBasedOnExamIdPojoInstance: ExamAllQuestionsSubjectWisePojo? = null
    var quizAllQuestionsSubjectWisePojoInstance: ExamAllQuestionsSubjectWisePojo? = null

    // Collections ....
    var localAllExaminationDataList: ArrayList<ExamAllQuestionsSubjectWisePojo>? = null

    /**
     * Setting All Data of Examination in Cache ....
     */
    fun setQuestionDataBasedOnExamId(localQuizAllQuestionsSubjectWisePojoList: ArrayList<ExamAllQuestionsSubjectWisePojo>): String {
        Log.i(TAG, "setQuestionDataBasedOnExamId() called Data Size: ")
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        sqliteContentValuesInstance = ContentValues()
        for (j in localQuizAllQuestionsSubjectWisePojoList.indices) {
            localQuestionDataBasedOnExamIdPojoInstance = ExamAllQuestionsSubjectWisePojo()
            quizAllQuestionsSubjectWisePojoInstance = localQuizAllQuestionsSubjectWisePojoList[j]
            sqliteContentValuesInstance!!.put(KEY_QUESTION_ID, quizAllQuestionsSubjectWisePojoInstance!!.questionId)
            sqliteContentValuesInstance!!.put(KEY_QUESTION_SUBJECT_ID, quizAllQuestionsSubjectWisePojoInstance!!.subjectId)
            sqliteContentValuesInstance!!.put(KEY_EASSY_ID, quizAllQuestionsSubjectWisePojoInstance!!.essayId)
            sqliteContentValuesInstance!!.put(KEY_QUESTION_TYPE, quizAllQuestionsSubjectWisePojoInstance!!.questionType)
            sqliteContentValuesInstance!!.put(KEY_QUESTION_TEXT, quizAllQuestionsSubjectWisePojoInstance!!.questionText)
            sqliteContentValuesInstance!!.put(KEY_EASSY_TEXT, quizAllQuestionsSubjectWisePojoInstance!!.essayData)
            sqliteContentValuesInstance!!.put(KEY_OPTION_A, quizAllQuestionsSubjectWisePojoInstance!!.optionA)
            sqliteContentValuesInstance!!.put(KEY_OPTION_B, quizAllQuestionsSubjectWisePojoInstance!!.optionB)
            sqliteContentValuesInstance!!.put(KEY_OPTION_C, quizAllQuestionsSubjectWisePojoInstance!!.optionC)
            sqliteContentValuesInstance!!.put(KEY_OPTION_D, quizAllQuestionsSubjectWisePojoInstance!!.optionD)
            sqliteContentValuesInstance!!.put(KEY_OPTION_E, quizAllQuestionsSubjectWisePojoInstance!!.optionE)
            sqliteContentValuesInstance!!.put(KEY_RIGHT_MARKS, quizAllQuestionsSubjectWisePojoInstance!!.rightMarks)
            sqliteContentValuesInstance!!.put(KEY_WRONG_MARKS, quizAllQuestionsSubjectWisePojoInstance!!.wrongMarks)
            sqliteContentValuesInstance!!.put(KEY_NO_OPTIONS, quizAllQuestionsSubjectWisePojoInstance!!.noOptions)
            sqliteContentValuesInstance!!.put(KEY_RIGHT_ANS, quizAllQuestionsSubjectWisePojoInstance!!.rightAns)
            Log.i(TAG, "Inserting All Examination data into the Cache Table. With SubjectName= " + quizAllQuestionsSubjectWisePojoInstance!!.subjectId)
            // Inserting Row
            mSQLiteInstance.insert(TABLE_CACHE_QUESTION_DATA, null, sqliteContentValuesInstance)
        }


        //mSQLiteInstance.close(); // Closing database connection
        Log.i(TAG, "Examination Data inserted successfully into the Cache Table.")
        return "succes"
    }//selection     = KEY_QUESTION_SUBJECT_ID + " LIKE ? ";
    //selectionArgs[0] = SubjectId;

    // Obtained instance of Readable SQLite Database.

    // Executing the Query.

    // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.

    // return Contacts list

    /**
     * Getting Profile of a Student ....
     */
    val questionDataBasedOnExamId: ArrayList<ExamAllQuestionsSubjectWisePojo>
        get() {
            Log.i(TAG, "getQuestionDataBasedOnExamId() called: ")
            projection = arrayOfNulls(1)
            selectionArgs = arrayOfNulls(1)
            localAllExaminationDataList = ArrayList()
            projection = null
            selection = null
            //selection     = KEY_QUESTION_SUBJECT_ID + " LIKE ? ";
            //selectionArgs[0] = SubjectId;
            selectionArgs = null

            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase

            // Executing the Query.
            cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_DATA,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)

            // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
            if (cursor.moveToFirst()) {
                do {
                    quizAllQuestionsSubjectWisePojoInstance = ExamAllQuestionsSubjectWisePojo()
                    quizAllQuestionsSubjectWisePojoInstance!!.questionId = cursor.getString(0)
                    quizAllQuestionsSubjectWisePojoInstance!!.subjectId = cursor.getString(1)
                    quizAllQuestionsSubjectWisePojoInstance!!.essayId = cursor.getString(2)
                    quizAllQuestionsSubjectWisePojoInstance!!.questionType = cursor.getString(3)
                    quizAllQuestionsSubjectWisePojoInstance!!.questionText = cursor.getString(4)
                    quizAllQuestionsSubjectWisePojoInstance!!.essayData = cursor.getString(5)
                    quizAllQuestionsSubjectWisePojoInstance!!.optionA = cursor.getString(6)
                    quizAllQuestionsSubjectWisePojoInstance!!.optionB = cursor.getString(7)
                    quizAllQuestionsSubjectWisePojoInstance!!.optionC = cursor.getString(8)
                    quizAllQuestionsSubjectWisePojoInstance!!.optionD = cursor.getString(9)
                    quizAllQuestionsSubjectWisePojoInstance!!.optionE = cursor.getString(10)
                    quizAllQuestionsSubjectWisePojoInstance!!.rightMarks = cursor.getString(11)
                    quizAllQuestionsSubjectWisePojoInstance!!.wrongMarks = cursor.getString(12)
                    quizAllQuestionsSubjectWisePojoInstance!!.noOptions = cursor.getString(13)
                    quizAllQuestionsSubjectWisePojoInstance!!.rightAns = cursor.getString(14)
                    localAllExaminationDataList!!.add(quizAllQuestionsSubjectWisePojoInstance!!)
                    Log.i(TAG, "Data: " + cursor.getInt(0))
                } while (cursor.moveToNext())
            }

            // return Contacts list
            return localAllExaminationDataList!!
        }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {
        private const val TAG = "CacheQuestionExamData"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "QuizLocalDatabase"
    }

    init {
        Log.i(TAG, "Constructor CacheClassStudentsMapping() invoked.")
    }
}
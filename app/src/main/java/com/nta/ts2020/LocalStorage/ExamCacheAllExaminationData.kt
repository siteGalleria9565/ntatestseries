package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.LocalStorage.ExamAssetToLocalDatabase.Companion.getHelper
import com.nta.ts2020.PojoClasses.ExamAllExaminationDataPojo
import java.util.*

class ExamCacheAllExaminationData(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var TABLE_CACHE_EXAMINATION_DATA = "examination_data"

    // Examinations_Data Table Columns names ....
    var KEY_EXAM_ID = "exam_id"
    var KEY_EXAM_SUBJECTS = "subjects"
    var KEY_EXAM_NAME = "exam_name"
    var projection: Array<String?>? = null
    var selection: String? = null
    var selectionArgs: Array<String?>? = null
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    var sqliteContentValuesInstance: ContentValues? = null

    // POJOs ....
    var localExamAllExaminationDataPojoInstance: ExamAllExaminationDataPojo? = null

    // Collections ....
    var localAllExaminationDataList: ArrayList<ExamAllExaminationDataPojo>? = null

    /**
     * Setting All Data of Examination in Cache ....
     */
    fun setAllExaminationData(allExaminationDataList: ArrayList<ExamAllExaminationDataPojo>): String {
        mSQLiteInstance = getHelper(context)!!.writableDatabase
        sqliteContentValuesInstance = ContentValues()
        for (j in allExaminationDataList.indices) {
            localExamAllExaminationDataPojoInstance = ExamAllExaminationDataPojo()
            localExamAllExaminationDataPojoInstance = allExaminationDataList[j]
            sqliteContentValuesInstance!!.put(KEY_EXAM_ID, localExamAllExaminationDataPojoInstance!!.examId)
            sqliteContentValuesInstance!!.put(KEY_EXAM_SUBJECTS, localExamAllExaminationDataPojoInstance!!.subject)
            sqliteContentValuesInstance!!.put(KEY_EXAM_NAME, localExamAllExaminationDataPojoInstance!!.examName)
            Log.i(TAG, "Inserting All Examination data into the Cache Table. With ExamId= " + localExamAllExaminationDataPojoInstance!!.examId)
            // Inserting Row
            mSQLiteInstance.insert(TABLE_CACHE_EXAMINATION_DATA, null, sqliteContentValuesInstance)
        }


        //mSQLiteInstance.close(); // Closing database connection
        Log.i(TAG, "Examination Data inserted successfully into the Cache Table.")
        return "succes"
    }// Obtained instance of Readable SQLite Database.

    // Executing the Query.

    // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.

    // return Contacts list

    /**
     * Getting Profile of a Student ....
     */
    val allExaminationData: ArrayList<ExamAllExaminationDataPojo>
        get() {
            Log.i(TAG, "getAllExaminationData() called: ")
            projection = arrayOfNulls(1)
            selectionArgs = arrayOfNulls(1)
            localAllExaminationDataList = ArrayList()
            projection = null
            selection = null
            selectionArgs = null

            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase

            // Executing the Query.
            cursor = mSQLiteInstance.query(TABLE_CACHE_EXAMINATION_DATA,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)

            // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
            if (cursor.moveToFirst()) {
                do {
                    localExamAllExaminationDataPojoInstance = ExamAllExaminationDataPojo()
                    localExamAllExaminationDataPojoInstance!!.examId = cursor.getInt(0)
                    localExamAllExaminationDataPojoInstance!!.subject = cursor.getInt(1)
                    localExamAllExaminationDataPojoInstance!!.examName = cursor.getString(2)
                    localAllExaminationDataList!!.add(localExamAllExaminationDataPojoInstance!!)
                } while (cursor.moveToNext())
            }

            // return Contacts list
            return localAllExaminationDataList!!
        }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {
        private const val TAG = "CacheAllExamData"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "ExaminationLocalDatabase"
    }

    init {
        Log.i(TAG, "Constructor CacheClassStudentsMapping() invoked.")
    }
}
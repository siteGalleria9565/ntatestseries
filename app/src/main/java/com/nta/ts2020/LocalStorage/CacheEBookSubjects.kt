package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.SQLException
import com.nta.ts2020.PojoClasses.EBookSubjectsPojo
import java.util.*

class CacheEBookSubjects(private val context: Context) {
    fun saveEBookSubjectsList(eBookSubjectsPojoArrayList: ArrayList<EBookSubjectsPojo>): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        for ((id, name) in eBookSubjectsPojoArrayList) {
            val contentValues = ContentValues()
            contentValues.put(KEY_SUBJECT_ID, id)
            contentValues.put(KEY_SUBJECT_NAME, name)
            try {
                sqLiteDatabase?.insertOrThrow(TABLE_E_BOOK_SUBJECTS, null, contentValues)
            } catch (e: SQLException) {
                e.printStackTrace()
                return false
            }
        }
        return true
    }

    val eBookSubjectsList: ArrayList<EBookSubjectsPojo>
        get() {
            val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
            val eBookSubjectsPojoArrayList = ArrayList<EBookSubjectsPojo>()
            val cursor = sqLiteDatabase?.query(TABLE_E_BOOK_SUBJECTS, null, null, null, null, null, null)
            if (cursor?.moveToFirst()!!) {
                do {
                    val eBookSubjectsPojo = EBookSubjectsPojo()
                    eBookSubjectsPojo.id = cursor.getString(cursor.getColumnIndex(KEY_SUBJECT_ID))
                    eBookSubjectsPojo.name = cursor.getString(cursor.getColumnIndex(KEY_SUBJECT_NAME))
                    eBookSubjectsPojoArrayList.add(eBookSubjectsPojo)
                } while (cursor.moveToNext())
            }
            cursor.close()
            return eBookSubjectsPojoArrayList
        }

    fun clearRecords() {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        //Clear all old records
        sqLiteDatabase?.execSQL("DELETE FROM $TABLE_E_BOOK_SUBJECTS")
    }

    companion object {
        private const val TAG = "CacheEBookSubjects"
        private const val TABLE_E_BOOK_SUBJECTS = "e_book_subjects"
        private const val KEY_SUBJECT_ID = "subject_id"
        private const val KEY_SUBJECT_NAME = "subject_name"
    }

}
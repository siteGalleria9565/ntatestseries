package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.LocalStorage.QuizAssetToLocalDatabase.Companion.getHelper
import com.nta.ts2020.PojoClasses.QuizChapter
import java.util.*

class QuizCacheAllChapterData(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var projection: Array<String?>? = null
    var selection: String? = null
    lateinit var selectionArgs: Array<String?>
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    var sqliteContentValuesInstance: ContentValues? = null
    var quizChapterArrayList: ArrayList<QuizChapter>? = null
    var quizChapter: QuizChapter? = null

    /**
     * Setting All Data of Chapter in Cache ....
     */
    fun insertChap(subId: Int, chapId: Int, chapName: String?): String {
        Log.i(TAG, "insertChap() called ")
        mSQLiteInstance = getHelper(context)!!.writableDatabase
        sqliteContentValuesInstance = ContentValues()
        sqliteContentValuesInstance!!.put(KEY_QUIZ_SUBJECT_ID, subId)
        sqliteContentValuesInstance!!.put(KEY_CHAPTER_ID, chapId)
        sqliteContentValuesInstance!!.put(KEY_CHAPTER_NAME, chapName)

        // Inserting Row
        mSQLiteInstance.insert(TABLE_CACHE_ALL_CHAPTER, null, sqliteContentValuesInstance)
        Log.i(TAG, "Inserting Chapter data into the Cache Table. With chapId= $chapId")
        return "succes"
    }

    fun clearAllChaps() {
        mSQLiteInstance = getHelper(context)!!.writableDatabase
        val QuizChapterData = "delete from all_chapter"
        mSQLiteInstance.execSQL(QuizChapterData)
    }

    /**
     * Getting Profile of a Student ....
     */
    fun getChapters(subId: Int): ArrayList<QuizChapter> {
        Log.i(TAG, "getChapters() called: ")
        projection = arrayOfNulls(1)
        selectionArgs = arrayOfNulls(1)
        quizChapterArrayList = ArrayList()
        projection = null
        selection = "$KEY_QUIZ_SUBJECT_ID LIKE ? "
        selectionArgs[0] = subId.toString()


        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase

        // Executing the Query.
        cursor = mSQLiteInstance.query(TABLE_CACHE_ALL_CHAPTER,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null)

        // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
        if (cursor.moveToFirst()) {
            do {
                quizChapter = QuizChapter(cursor.getString(1).toInt(), cursor.getString(2))
                quizChapterArrayList!!.add(quizChapter!!)
            } while (cursor.moveToNext())
        }

        // return Contacts list
        return quizChapterArrayList!!
    }

    fun getChapterName(chapId: Int): String {
        var chapName = ""
        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase


        // Executing the Query.
        cursor = mSQLiteInstance.query(TABLE_CACHE_ALL_CHAPTER, arrayOf(KEY_CHAPTER_NAME),
                "$KEY_CHAPTER_ID = $chapId",
                null,
                null,
                null,
                null)
        if (cursor.getCount() > 0) {
            cursor.moveToFirst()
            chapName = cursor.getString(0)
        }
        return chapName
    }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {
        private const val TAG = "CacheAllChapter"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "QuizLocalDatabase"
        private const val TABLE_CACHE_ALL_CHAPTER = "all_chapter"

        //Chapter List
        private const val KEY_QUIZ_SUBJECT_ID = "subject_id"
        private const val KEY_CHAPTER_ID = "chapter_id"
        private const val KEY_CHAPTER_NAME = "chapter_name"
    }

    init {
        Log.i(TAG, "Constructor CacheClassStudentsMapping() invoked.")
    }
}
package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.LocalStorage.ExamAssetToLocalDatabase.Companion.getHelper
import com.nta.ts2020.PojoClasses.ExamGetSubjectArrayStuffPojo
import com.nta.ts2020.PojoClasses.ExamQuestionDataBasedOnExamIdPojo
import java.util.*

class ExamCacheAllQuestionRelatedStuff(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var TABLE_CACHE_QUESTION_RELATED_STUFF = "questions_related_stuff"
    var TABLE_CACHE_QUESTION_SUBJECT_STUFF = "questions_subject_stuff"

    // Examinations_Data Table Columns names ....
    var KEY_QUESTION_STUFF_EXAM_ID = "question_stuff_exam_id"
    var KEY_QUESTION_EXAM_NAME = "question_exam_name"
    var KEY_TOTAL_QUESTION = "total_questions"
    var KEY_TOTAL_MARKS = "total_marks"
    var KEY_TIME_DURATION_IN_SECONDS = "duration"
    var KEY_INSTRUCTIONS = "instructions"

    //subject_names with subject id
    var KEY_SUBJECT_EXAM_ID = "subject_exam_id"
    var KEY_NUMBER_OF_MARKS = "number_of_marks"
    var KEY_SUBJECT_ID = "subject_id"
    var KEY_NUMBER_OF_QUESTIONS = "number_of_questions"
    var KEY_SUBJECT_NAMES = "subject_names"
    var projection: Array<String?>? = null
    var selection: String? = null
    lateinit var selectionArgs: Array<String?>
    var myDataBase: SQLiteDatabase? = null
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    var sqliteContentValuesInstance: ContentValues? = null

    // POJOs ....
    var localExamQuestionDataBasedOnExamIdPojoInstance: ExamQuestionDataBasedOnExamIdPojo? = null
    var examGetSubjectArrayStuffPojoInstance: ExamGetSubjectArrayStuffPojo? = null

    // Collections ....
    var localQuestionRelatedDataList: ArrayList<ExamQuestionDataBasedOnExamIdPojo>? = null
    var examGetSubjectArrayStuffPojoList: ArrayList<ExamGetSubjectArrayStuffPojo>? = null

    /**
     * Setting All Data of Examination in Cache ....
     */
    fun setCacheAllQuestionRelatedStuff(localExamQuestionDataBasedOnExamIdPojo: ExamQuestionDataBasedOnExamIdPojo): String {
        Log.i(TAG, "setAllExaminationData() called Data Size: ")
        mSQLiteInstance = getHelper(context)!!.writableDatabase
        sqliteContentValuesInstance = ContentValues()
        localExamQuestionDataBasedOnExamIdPojoInstance = ExamQuestionDataBasedOnExamIdPojo()
        localExamQuestionDataBasedOnExamIdPojoInstance = localExamQuestionDataBasedOnExamIdPojo
        sqliteContentValuesInstance!!.put(KEY_QUESTION_STUFF_EXAM_ID, localExamQuestionDataBasedOnExamIdPojoInstance!!.examId)
        sqliteContentValuesInstance!!.put(KEY_QUESTION_EXAM_NAME, localExamQuestionDataBasedOnExamIdPojoInstance!!.examName)
        sqliteContentValuesInstance!!.put(KEY_TOTAL_QUESTION, localExamQuestionDataBasedOnExamIdPojoInstance!!.totalQuestions)
        sqliteContentValuesInstance!!.put(KEY_TOTAL_MARKS, localExamQuestionDataBasedOnExamIdPojoInstance!!.totalMarks)
        sqliteContentValuesInstance!!.put(KEY_TIME_DURATION_IN_SECONDS, localExamQuestionDataBasedOnExamIdPojoInstance!!.timeDurationInSeconds)
        sqliteContentValuesInstance!!.put(KEY_INSTRUCTIONS, localExamQuestionDataBasedOnExamIdPojoInstance!!.instructions)
        Log.i(TAG, "Inserting All Examination data into the Cache Table. With ExamId= " + localExamQuestionDataBasedOnExamIdPojoInstance!!.examId)

        // Inserting Row
        mSQLiteInstance.insert(TABLE_CACHE_QUESTION_RELATED_STUFF, null, sqliteContentValuesInstance)

        //fetch array values
        val getNumberOfMarks = localExamQuestionDataBasedOnExamIdPojoInstance!!.numberOfMarks
        val getSubjectId = localExamQuestionDataBasedOnExamIdPojoInstance!!.subjectId
        val getNumberOfQuestions = localExamQuestionDataBasedOnExamIdPojoInstance!!.numberOfQuestions
        val getSubjectNames = localExamQuestionDataBasedOnExamIdPojoInstance!!.subjectNames
        Log.i(TAG, "Subject Id Data.$getSubjectNames getNumberOfQuestions $getNumberOfQuestions getSubjectId $getSubjectId getNumberOfMarks $getNumberOfMarks")
        sqliteContentValuesInstance = ContentValues()
        for (i in getSubjectId.indices) {
            sqliteContentValuesInstance!!.put(KEY_SUBJECT_EXAM_ID, localExamQuestionDataBasedOnExamIdPojoInstance!!.examId)
            sqliteContentValuesInstance!!.put(KEY_NUMBER_OF_MARKS, getNumberOfMarks[i])
            sqliteContentValuesInstance!!.put(KEY_SUBJECT_ID, getSubjectId[i])
            sqliteContentValuesInstance!!.put(KEY_NUMBER_OF_QUESTIONS, getNumberOfQuestions[i])
            sqliteContentValuesInstance!!.put(KEY_SUBJECT_NAMES, getSubjectNames[i])

            // Inserting Row
            mSQLiteInstance.insert(TABLE_CACHE_QUESTION_SUBJECT_STUFF, null, sqliteContentValuesInstance)
        }


        //  mSQLiteInstance.close(); // Closing database connection
        Log.i(TAG, "Examination Data inserted successfully into the Cache Table.")
        return "succes"
    }

    /**
     * Getting Profile of a Student ....
     */
    fun getCacheAllQuestionRelatedStuff(ExamID: String?): ArrayList<ExamQuestionDataBasedOnExamIdPojo> {
        Log.i(TAG, "getAllExaminationData() called: ")
        projection = arrayOfNulls(1)
        selectionArgs = arrayOfNulls(1)
        localQuestionRelatedDataList = ArrayList()
        projection = null
        selection = "$KEY_QUESTION_STUFF_EXAM_ID LIKE ? "
        selectionArgs[0] = ExamID


        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase

        // Executing the Query.
        cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_RELATED_STUFF,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null)

        // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
        if (cursor.moveToFirst()) {
            do {
                localExamQuestionDataBasedOnExamIdPojoInstance = ExamQuestionDataBasedOnExamIdPojo()
                localExamQuestionDataBasedOnExamIdPojoInstance!!.examId = cursor.getString(0)
                localExamQuestionDataBasedOnExamIdPojoInstance!!.examName = cursor.getString(1)
                localExamQuestionDataBasedOnExamIdPojoInstance!!.totalQuestions = cursor.getString(2)
                localExamQuestionDataBasedOnExamIdPojoInstance!!.totalMarks = cursor.getString(3)
                localExamQuestionDataBasedOnExamIdPojoInstance!!.timeDurationInSeconds = cursor.getString(4)
                localExamQuestionDataBasedOnExamIdPojoInstance!!.instructions = cursor.getString(5)
                localQuestionRelatedDataList!!.add(localExamQuestionDataBasedOnExamIdPojoInstance!!)
            } while (cursor.moveToNext())
        }

        // return Contacts list
        return localQuestionRelatedDataList!!
    }

    /**
     * Getting Profile of a Student ....
     */
    fun getCacheAllQuestionSubjectRelatedStuff(ExamID: String?): ArrayList<ExamGetSubjectArrayStuffPojo> {
        Log.i(TAG, "getQuestionSubjectRelatedStuff() called: ")
        projection = arrayOfNulls(1)
        selectionArgs = arrayOfNulls(1)
        examGetSubjectArrayStuffPojoList = ArrayList()
        projection = null
        selection = "$KEY_SUBJECT_EXAM_ID LIKE ? "
        selectionArgs[0] = ExamID


        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        cursor = if (ExamID == null) {
            mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)
        } else {

            // Executing the Query.
            mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    null)
        }

        // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
        if (cursor.moveToFirst()) {
            do {
                examGetSubjectArrayStuffPojoInstance = ExamGetSubjectArrayStuffPojo()
                examGetSubjectArrayStuffPojoInstance!!.examId = cursor.getString(0)
                examGetSubjectArrayStuffPojoInstance!!.numberOfMarks = cursor.getString(1)
                examGetSubjectArrayStuffPojoInstance!!.subjectId = cursor.getString(2)
                examGetSubjectArrayStuffPojoInstance!!.numberOfQuestions = cursor.getString(3)
                examGetSubjectArrayStuffPojoInstance!!.subjectNames = cursor.getString(4)
                examGetSubjectArrayStuffPojoList!!.add(examGetSubjectArrayStuffPojoInstance!!)
            } while (cursor.moveToNext())
        }

        // return Contacts list
        return examGetSubjectArrayStuffPojoList!!
    }

    //Added Later -- Suvajit
    fun getSubjectIds(examID: Int): ArrayList<String> {
        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        val selectionQuery = "$KEY_SUBJECT_EXAM_ID = $examID"
        val tempArrayList = ArrayList<String>()
        cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF, arrayOf(KEY_SUBJECT_ID), selectionQuery, null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                tempArrayList.add(cursor.getString(0))
            } while (cursor.moveToNext())
        }
        return tempArrayList
    }

    fun getNosOfQuesSubWise(examID: Int): ArrayList<String> {
        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        val selectionQuery = "$KEY_SUBJECT_EXAM_ID = $examID"
        val tempArrayList = ArrayList<String>()
        cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF, arrayOf(KEY_NUMBER_OF_QUESTIONS), selectionQuery, null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                tempArrayList.add(cursor.getString(0))
            } while (cursor.moveToNext())
        }
        return tempArrayList
    }

    fun getSubWiseMaxMarks(examID: Int): ArrayList<String> {
        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = getHelper(context)!!.readableDatabase
        val selectionQuery = "$KEY_SUBJECT_EXAM_ID = $examID"
        val tempArrayList = ArrayList<String>()
        cursor = mSQLiteInstance.query(TABLE_CACHE_QUESTION_SUBJECT_STUFF, arrayOf(KEY_NUMBER_OF_MARKS), selectionQuery, null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                tempArrayList.add(cursor.getString(0))
            } while (cursor.moveToNext())
        }
        return tempArrayList
    }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {
        private const val TAG = "CacheAllExamData"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "ExaminationLocalDatabase"
    }

    init {
        Log.i(TAG, "Constructor CacheClassStudentsMapping() invoked.")
    }
}
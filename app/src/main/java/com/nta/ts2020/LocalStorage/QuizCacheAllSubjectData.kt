package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.LocalStorage.QuizAssetToLocalDatabase.Companion.getHelper
import com.nta.ts2020.PojoClasses.QuizSubject
import java.util.*

/**
 * Created by    on 20/2/17.
 */
class QuizCacheAllSubjectData //        Log.i(TAG, "Constructor CacheClassStudentsMapping() invoked.");
(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var TABLE_CACHE_ALL_SUBJECT = "all_subject"
    var projection: Array<String?>? = null
    var selection: String? = null
    var selectionArgs: Array<String?>? = null
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    var sqliteContentValuesInstance: ContentValues? = null

    /**
     * Setting All Data of Subject in Cache ....
     */
    fun insertSub(id: Int, subName: String?): String {

//        Log.i(TAG, "insertSub() called ");
        mSQLiteInstance = getHelper(context)!!.writableDatabase
        sqliteContentValuesInstance = ContentValues()
        sqliteContentValuesInstance!!.put(KEY_QUIZ_SUBJECT_ID, id)
        sqliteContentValuesInstance!!.put(KEY_QUIZ_SUBJECT_NAME, subName)

        // Inserting Row
        mSQLiteInstance.insert(TABLE_CACHE_ALL_SUBJECT, null, sqliteContentValuesInstance)

//        Log.i(TAG, "Inserting Subject data into the Cache Table. With SubjectID= "+ id);
        return "succes"
    }

    fun clearAllSubs() {
        mSQLiteInstance = getHelper(context)!!.writableDatabase
        val QuizSubjectData = "delete from all_subject"
        mSQLiteInstance.execSQL(QuizSubjectData)
    }//        Log.i(TAG, "getAllSubs() called: ");

    // Obtained instance of Readable SQLite Database.

    // Executing the Query.

    // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.

    // return Subject list

    /**
     * Getting Profile of a Student ....
     */
    val allSubs: ArrayList<QuizSubject>
        get() {
//        Log.i(TAG, "getAllSubs() called: ");
            projection = arrayOfNulls(1)
            selectionArgs = arrayOfNulls(1)
            val allSubs = ArrayList<QuizSubject>()
            projection = null
            selection = null
            selectionArgs = null

            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = getHelper(context)!!.readableDatabase

            // Executing the Query.
            cursor = mSQLiteInstance.query(TABLE_CACHE_ALL_SUBJECT,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)

            // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
            if (cursor.moveToFirst()) {
                do {
                    val quizSubject = QuizSubject(cursor.getString(0).toInt(), cursor.getString(1))
                    allSubs.add(quizSubject)
                } while (cursor.moveToNext())
            }

            // return Subject list
            return allSubs
        }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {
        private const val TAG = "CacheAllSubject"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "QuizLocalDatabase"

        //Subject List
        private const val KEY_QUIZ_SUBJECT_ID = "subject_id"
        private const val KEY_QUIZ_SUBJECT_NAME = "subject_name"
    }

}
package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteException
import android.util.Log
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.PojoClasses.UserDataPojo
import java.util.*

class CacheUserData(private val context: Context) {
    private val userDataPojo: UserDataPojo? = null

    fun saveUserDataToCache(userDataPojo: UserDataPojo): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase

        //Prepare values before writing to database
        val contentValues = ContentValues()
        contentValues.put(KEY_USER_ID, userDataPojo.userId)
        contentValues.put(KEY_EMAIL, userDataPojo.email)
        contentValues.put(KEY_F_NAME, userDataPojo.fName)
        contentValues.put(KEY_L_NAME, userDataPojo.lName)
        contentValues.put(KEY_PH_NO, userDataPojo.phNo)
        contentValues.put(KEY_PROF_PIC, userDataPojo.profPic)
        contentValues.put(KEY_COURSE_ID, userDataPojo.courseId)
        contentValues.put(KEY_COURSE_NAME, userDataPojo.courseName)
        contentValues.put(KEY_CLASS_ID, userDataPojo.classId)
        contentValues.put(KEY_CLASS_NAME, userDataPojo.className)
        contentValues.put(KEY_USER_LOGIN_STATUS, userDataPojo.isUserLoggedIn)

        //Write to database
        try {
            sqLiteDatabase?.insertOrThrow(TABLE_USER, null, contentValues)
        } catch (e: SQLiteException) {
            Log.d("er",e.message)
            if (BuildConfig.DEBUG) e.printStackTrace()
            return false
        }

//        Log.d(TAG, "Data inserted to '" + TABLE_USER + "' table");
        return true
    }

    //        Log.d(TAG, "getUserDataFromCache() invoked");
    val userDataFromCache: UserDataPojo
        get() {
//        Log.d(TAG, "getUserDataFromCache() invoked");
            val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
            val cursor = sqLiteDatabase?.query(TABLE_USER, null, null, null, null, null, null)
            val userDataPojo = UserDataPojo()
            if (cursor?.count == 1) {
                cursor.moveToFirst()
                userDataPojo.userId = cursor.getString(cursor.getColumnIndex(KEY_USER_ID))
                userDataPojo.email = cursor.getString(cursor.getColumnIndex(KEY_EMAIL))
                userDataPojo.fName = cursor.getString(cursor.getColumnIndex(KEY_F_NAME))
                userDataPojo.lName = cursor.getString(cursor.getColumnIndex(KEY_L_NAME))
                userDataPojo.phNo = cursor.getString(cursor.getColumnIndex(KEY_PH_NO))
                userDataPojo.profPic = cursor.getString(cursor.getColumnIndex(KEY_PROF_PIC))
                userDataPojo.courseId = cursor.getString(cursor.getColumnIndex(KEY_COURSE_ID))
                userDataPojo.courseName = cursor.getString(cursor.getColumnIndex(KEY_COURSE_NAME))
                userDataPojo.classId = cursor.getString(cursor.getColumnIndex(KEY_CLASS_ID))
                userDataPojo.className = cursor.getString(cursor.getColumnIndex(KEY_CLASS_NAME))
                userDataPojo.setUserLoginStatus(cursor.getInt(cursor.getColumnIndex(KEY_USER_LOGIN_STATUS)) == 1)
            }
            cursor?.close()
            return userDataPojo
        }

    fun updateUserLoginStatus(status: Boolean): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_USER_LOGIN_STATUS, status)
        sqLiteDatabase?.update(TABLE_USER, contentValues, null, null)
        return true
    }

    /**Update Updates the user details which are provided
     * @param userDataPojo Any member of the userDataPojo
     * @return status in boolean
     */
    fun updateUserDetails(userDataPojo: UserDataPojo): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        val contentValues = ContentValues()
        val updatedValues = ArrayList<String>()
        if (userDataPojo.fName != null) {
            contentValues.put(KEY_F_NAME, userDataPojo.fName)
            updatedValues.add("First Name: " + userDataPojo.fName)
        }
        if (userDataPojo.lName != null) {
            contentValues.put(KEY_L_NAME, userDataPojo.lName)
            updatedValues.add("Last Name: " + userDataPojo.lName)
        }
        if (userDataPojo.phNo != null) {
            contentValues.put(KEY_PH_NO, userDataPojo.phNo)
            updatedValues.add("Mobile No: " + userDataPojo.phNo)
        }
        if (userDataPojo.profPic != null) {
            contentValues.put(KEY_PROF_PIC, userDataPojo.profPic)
            updatedValues.add("Profile Pic: " + userDataPojo.profPic)
        }
        if (userDataPojo.courseId != null) {
            contentValues.put(KEY_COURSE_ID, userDataPojo.courseId)
            updatedValues.add("Course ID: " + userDataPojo.courseId)
        }
        if (userDataPojo.courseName != null) {
            contentValues.put(KEY_COURSE_NAME, userDataPojo.courseName)
            updatedValues.add("Course name: " + userDataPojo.courseName)
        }
        if (userDataPojo.classId != null) {
            contentValues.put(KEY_CLASS_ID, userDataPojo.classId)
            updatedValues.add("Class ID: " + userDataPojo.classId)
        }
        if (userDataPojo.className != null) {
            contentValues.put(KEY_CLASS_NAME, userDataPojo.className)
            updatedValues.add("Class name: " + userDataPojo.className)
        }
        sqLiteDatabase?.update(TABLE_USER, contentValues, null, null)
        //        Log.d(TAG, "User details updated with value: " + updatedValues.toString());
        return true
    }

    fun clearRecords() {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        //Clear all old records
        sqLiteDatabase?.execSQL("DELETE FROM $TABLE_USER")
    }

    companion object {
        private const val TAG = "CacheUserData"
        private const val TABLE_USER = "user"
        private const val KEY_USER_ID = "user_id"
        private const val KEY_EMAIL = "email"
        private const val KEY_F_NAME = "f_name"
        private const val KEY_L_NAME = "l_name"
        private const val KEY_PH_NO = "ph_no"
        private const val KEY_PROF_PIC = "prof_pic"
        private const val KEY_COURSE_ID = "course_id"
        private const val KEY_COURSE_NAME = "course_name"
        private const val KEY_CLASS_ID = "class_id"
        private const val KEY_CLASS_NAME = "class_name"
        private const val KEY_USER_LOGIN_STATUS = "user_login_status"
    }

}
package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import com.nta.ts2020.PojoClasses.ExamSetCurrentExamSessionPojo
import java.util.*

class QuizCacheSetCurrentSessionData(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    var TABLE_CACHE_SUBMIT_SESSION_DATA = "submit_session_data"

    // Examinations_Data Table Columns names ...    //maintain Session
    var KEY_SUBMIT_EXAM_ID = "submit_exam_id"
    var KEY_SUBMIT_EXAM_STATUS = "submit_exam_status"
    var KEY_SUBMIT_EXAM_CURRENT_POSITION = "submit_exam_current_position"
    var KEY_COUNT_RESUME_SESSION = "count_resume"
    var KEY_SUBMIT_EXAM_TIME = "submit_exam_time"
    var projection: Array<String?>? = null
    var selection: String? = null
    var selectionArgs: Array<String?>? = null
    lateinit var cursor: Cursor
    lateinit var mSQLiteInstance: SQLiteDatabase
    lateinit var sqliteContentValuesInstance: ContentValues

    // POJOs ....
    var localQuizSetCurrentExamSessionPojoInstance: ExamSetCurrentExamSessionPojo? = null

    // Collections ....
    var localGetCurrentExamSessionList: ArrayList<ExamSetCurrentExamSessionPojo>? = null

    /**
     * Setting All Data of Examination in Cache ....
     */
    fun setCurrentExamSession(examID: String?, timeInSeconds: String?): String {
        Log.i(TAG, "setCurrentExamSession() called Data Size: ")
        mSQLiteInstance = QuizAssetToLocalDatabase.getHelper(context)!!.readableDatabase
        sqliteContentValuesInstance = ContentValues()
        sqliteContentValuesInstance!!.put(KEY_SUBMIT_EXAM_ID, examID)
        sqliteContentValuesInstance!!.put(KEY_SUBMIT_EXAM_STATUS, "Resume")
        sqliteContentValuesInstance!!.put(KEY_SUBMIT_EXAM_CURRENT_POSITION, 0)
        sqliteContentValuesInstance!!.put(KEY_COUNT_RESUME_SESSION, 0)
        sqliteContentValuesInstance!!.put(KEY_SUBMIT_EXAM_TIME, timeInSeconds)
        Log.i(TAG, "Inserting current session data into the Cache Table. With QuestionId= 80")
        // Inserting Row
        mSQLiteInstance.insert(TABLE_CACHE_SUBMIT_SESSION_DATA, null, sqliteContentValuesInstance)

        //mSQLiteInstance.close(); // Closing database connection
        Log.i(TAG, "Examination Data inserted successfully into the Cache Table.")
        return "succes"
    }// Obtained instance of Readable SQLite Database.

    // Executing the Query.

    // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.

    // return Contacts list

    /**
     * Getting Profile of a Student ....
     */
    val currentExamSession: ArrayList<ExamSetCurrentExamSessionPojo>
        get() {
            Log.i(TAG, "getCurrentExamSession() called: ")
            projection = arrayOfNulls(1)
            selectionArgs = arrayOfNulls(1)
            localGetCurrentExamSessionList = ArrayList()
            projection = null
            selection = null
            selectionArgs = null

            // Obtained instance of Readable SQLite Database.
            mSQLiteInstance = QuizAssetToLocalDatabase.getHelper(context)!!.readableDatabase

            // Executing the Query.
            cursor = mSQLiteInstance.query(TABLE_CACHE_SUBMIT_SESSION_DATA,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null)

            // Looping through all the Fetched Rows and adding each Cell  name into DataList ArrayList.
            if (cursor.moveToFirst()) {
                do {
                    localQuizSetCurrentExamSessionPojoInstance = ExamSetCurrentExamSessionPojo()
                    localQuizSetCurrentExamSessionPojoInstance!!.submitExamID = cursor.getInt(0).toString()
                    localQuizSetCurrentExamSessionPojoInstance!!.examStatus = cursor.getString(1)
                    localQuizSetCurrentExamSessionPojoInstance!!.currentPosition = cursor.getInt(2).toString()
                    localQuizSetCurrentExamSessionPojoInstance!!.countResum = cursor.getInt(3).toString()
                    localQuizSetCurrentExamSessionPojoInstance!!.remainingTime = cursor.getInt(4).toString()
                    localGetCurrentExamSessionList!!.add(localQuizSetCurrentExamSessionPojoInstance!!)
                } while (cursor.moveToNext())
            }

            // return Contacts list
            return localGetCurrentExamSessionList!!
        }

    /**
     * Updating All Data of a Examination ....
     */
    fun updateCurrentExamSession(CurrentExamId: String, updatedQuizSetCurrentExamSessionPojo: ExamSetCurrentExamSessionPojo): String {
        Log.i(TAG, "updateExaminationData() called for QuestionId: $CurrentExamId")
        projection = arrayOfNulls(1)
        selectionArgs = arrayOfNulls(1)
        projection = null
        selection = "$KEY_SUBMIT_EXAM_ID = $CurrentExamId"
        selectionArgs = null

        // Obtained instance of Readable SQLite Database.
        mSQLiteInstance = this.readableDatabase
        sqliteContentValuesInstance = ContentValues()
        Log.i(TAG, "KEY_SUBMIT_EXAM_ID: " + updatedQuizSetCurrentExamSessionPojo.submitExamID + " SaveState " + updatedQuizSetCurrentExamSessionPojo.examStatus + " TIme " + updatedQuizSetCurrentExamSessionPojo.remainingTime + " position " + updatedQuizSetCurrentExamSessionPojo.currentPosition)
        mSQLiteInstance.execSQL("UPDATE " + TABLE_CACHE_SUBMIT_SESSION_DATA + " SET " + KEY_SUBMIT_EXAM_TIME + "=" + updatedQuizSetCurrentExamSessionPojo.remainingTime!!.toInt() + ", " + KEY_COUNT_RESUME_SESSION + " = " + KEY_COUNT_RESUME_SESSION + " + " + 1 + ", " + KEY_SUBMIT_EXAM_CURRENT_POSITION + " = " + updatedQuizSetCurrentExamSessionPojo.currentPosition!!.toInt() + ", " + KEY_SUBMIT_EXAM_STATUS + " = '" + updatedQuizSetCurrentExamSessionPojo.examStatus + "' WHERE " + selection)


        // return Contacts list
        return "Success"
    }

    override fun onCreate(db: SQLiteDatabase) {}
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}
    fun clearRecords() {
        mSQLiteInstance = ExamAssetToLocalDatabase.getHelper(context)!!.writableDatabase
        //Clear all old records
        mSQLiteInstance.execSQL("DELETE FROM $TABLE_CACHE_SUBMIT_SESSION_DATA")
    }

    companion object {
        private const val TAG = "CacheSetSessionData"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "QuizLocalDatabase"
    }

    init {
        Log.i(TAG, "Constructor QuizCacheGetCurrentExamSession() invoked.")
    }
}
package com.nta.ts2020.LocalStorage

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_VERSION
import java.util.*

class QuizAssetToLocalDatabase(var context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    // SQLiteDatabase Instances for ASSET and LOCAL Database ....
    lateinit var lSQLiteInstance: SQLiteDatabase

    // Member Variables ....
    private var CREATE_QUIZ_ALL_SUBJECT_TABLE: String? = null
    private var CREATE_QUIZ_ALL_CHAPTER_TABLE: String? = null
    private var CREATE_QUESTIONS_DATA_TABLE: String? = null
    private var CREATE_QUESTION_RELATED_STUFF_TABLE: String? = null
    private var CREATE_QUESTION_SUBJECT_RELATED_STUFF_TABLE: String? = null
    private var CREATE_QUESTION_ANSWER_EXAM_TIME_TABLE: String? = null
    private var CREATE_SUBMIT_EXAM_SESSION_DATA_TABLE: String? = null

    /**
     * This is called when the database is created for the first time.
     *
     * @param db
     */
    override fun onCreate(db: SQLiteDatabase) {


        //Create quiz all subject table
        CREATE_QUIZ_ALL_SUBJECT_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_CACHE_ALL_SUBJECT +
                " ( "
                + KEY_QUIZ_SUBJECT_ID + " INTEGER, "
                + KEY_QUIZ_SUBJECT_NAME + " TEXT" + ")")
        db.execSQL(CREATE_QUIZ_ALL_SUBJECT_TABLE)


        //Create QuizChapter Table
        CREATE_QUIZ_ALL_CHAPTER_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_CACHE_ALL_CHAPTER +
                " ( "
                + KEY_QUIZ_SUBJECT_ID + " INTEGER, "
                + KEY_CHAPTER_ID + " INTEGER, "
                + KEY_CHAPTER_NAME + " TEXT" + ")")
        db.execSQL(CREATE_QUIZ_ALL_CHAPTER_TABLE)
        CREATE_QUESTIONS_DATA_TABLE = ("CREATE TABLE IF NOT EXISTS " + TABLE_CACHE_QUESTION_DATA
                + "(" + KEY_QUESTION_ID + " INTEGER ," +
                KEY_QUESTION_SUBJECT_ID + " TEXT," +
                KEY_EASSY_ID + " INTEGER," +
                KEY_QUESTION_TYPE + " TEXT," +
                KEY_QUESTION_TEXT + " TEXT," +
                KEY_EASSY_TEXT + " TEXT," +
                KEY_OPTION_A + " TEXT," +
                KEY_OPTION_B + " TEXT," +
                KEY_OPTION_C + " TEXT," +
                KEY_OPTION_D + " TEXT," +
                KEY_OPTION_E + " TEXT," +
                KEY_RIGHT_MARKS + " INTEGER," +
                KEY_WRONG_MARKS + " INTEGER," +
                KEY_NO_OPTIONS + " INTEGER," +
                KEY_RIGHT_ANS + " TEXT" + ")")
        db.execSQL(CREATE_QUESTIONS_DATA_TABLE)
        Log.d(TAG, "Tables Schema created successfully ....$db")
        CREATE_QUESTION_RELATED_STUFF_TABLE = ("CREATE TABLE " + TABLE_CACHE_QUESTION_RELATED_STUFF
                + "(" + KEY_QUESTION_STUFF_EXAM_ID + " INTEGER ," +
                KEY_QUESTION_EXAM_NAME + " TEXT," +
                KEY_TOTAL_QUESTION + " INTEGER," +
                KEY_TOTAL_MARKS + " TEXT," +
                KEY_TIME_DURATION_IN_SECONDS + " INTEGER," +
                KEY_INSTRUCTIONS + " TEXT" + ")")
        db.execSQL(CREATE_QUESTION_RELATED_STUFF_TABLE)
        Log.d(TAG, "Tables Schema created successfully ....$db")
        CREATE_QUESTION_SUBJECT_RELATED_STUFF_TABLE = ("CREATE TABLE " + TABLE_CACHE_QUESTION_SUBJECT_STUFF
                + "(" + KEY_SUBJECT_EXAM_ID + " INTEGER ," +
                KEY_NUMBER_OF_MARKS + " INTEGER," +
                KEY_SUBJECT_ID + " INTEGER," +
                KEY_NUMBER_OF_QUESTIONS + " INTEGER," +
                KEY_SUBJECT_NAMES + " TEXT" + ")")
        db.execSQL(CREATE_QUESTION_SUBJECT_RELATED_STUFF_TABLE)
        Log.d(TAG, "Tables Schema created successfully ....$db")
        CREATE_QUESTION_ANSWER_EXAM_TIME_TABLE = ("CREATE TABLE " + TABLE_CACHE_SUBMIT_ANSWER_STUFF
                + "(" + KEY_QUESTION_ANSWER_ID + " INTEGER ," +
                KEY_RESPONSE_DATA + " TEXT," +
                KEY_TIME_DURATION + " INTEGER," +
                KEY_COLOR_STATUS + " INTEGER" + ")")
        db.execSQL(CREATE_QUESTION_ANSWER_EXAM_TIME_TABLE)
        Log.d(TAG, "Tables Schema created successfully ....$db")
        CREATE_SUBMIT_EXAM_SESSION_DATA_TABLE = ("CREATE TABLE " + TABLE_CACHE_SUBMIT_SESSION_DATA
                + "(" + KEY_SUBMIT_EXAM_ID + " INTEGER ," +
                KEY_SUBMIT_EXAM_STATUS + " TEXT," +
                KEY_SUBMIT_EXAM_CURRENT_POSITION + " INTEGER," +
                KEY_COUNT_RESUME_SESSION + " INTEGER," +
                KEY_SUBMIT_EXAM_TIME + " INTEGER" + ")")
        db.execSQL(CREATE_SUBMIT_EXAM_SESSION_DATA_TABLE)
        Log.d(TAG, "Tables Schema created successfully ....$db")
    }

    /**
     * This method first DELETES Data from the tables then UPDATES the SQLITE_SEQUENCE "seq" to 0 for each table.
     */
    fun deleteData() {
        // Log.d(TAG, "Method deleteData() called.");
        try {
            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val QuestionData = "delete from questions_data"
            lSQLiteInstance.execSQL(QuestionData)

            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val QuestionRelatedStuff = "delete from questions_related_stuff"
            lSQLiteInstance.execSQL(QuestionRelatedStuff)

            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val QuestionSubjectRelatedStuff = "delete from questions_subject_stuff"
            lSQLiteInstance.execSQL(QuestionSubjectRelatedStuff)

            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val QuestionAnswerExamTimeStuff = "delete from submit_answer_stuff"
            lSQLiteInstance.execSQL(QuestionAnswerExamTimeStuff)


            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val SubmitExamSessionData = "delete from submit_session_data"
            lSQLiteInstance.execSQL(SubmitExamSessionData)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun deleteCurrentSessionData() {
        try {
            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val QuestionData = "delete from questions_data"
            lSQLiteInstance.execSQL(QuestionData)

            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val QuestionRelatedStuff = "delete from questions_related_stuff"
            lSQLiteInstance.execSQL(QuestionRelatedStuff)

            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val QuestionSubjectRelatedStuff = "delete from questions_subject_stuff"
            lSQLiteInstance.execSQL(QuestionSubjectRelatedStuff)

            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val QuestionAnswerExamTimeStuff = "delete from submit_answer_stuff"
            lSQLiteInstance.execSQL(QuestionAnswerExamTimeStuff)

            // Getting an Instance of Local Database for Writing into it ....
            lSQLiteInstance = this.writableDatabase
            val SubmitExamSessionData = "delete from submit_session_data"
            lSQLiteInstance.execSQL(SubmitExamSessionData)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        //Delete all tables --start
        // query to obtain the names of all tables in your database
        val c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null)
        val tables: MutableList<String> = ArrayList()

        // iterate over the result set, adding every table name to a list
        while (c.moveToNext()) {
            tables.add(c.getString(0))
        }

        //  Drop on every table name
        for (table in tables) {
            val dropQuery = "DROP TABLE IF EXISTS $table"
            db.execSQL(dropQuery)
        }
        c.close()
        //Delete all tables --End

        //Create new tables again
        onCreate(db)
    }

    companion object {
        private var instance: QuizAssetToLocalDatabase? = null
        private const val TAG = "QuizAssetToDatabase"

        // LOCAL APPLICATION DATABASE Details ....
        const val DATABASE_NAME = "QuizLocalDatabase"

        // TABLE NAMES ....
        private const val TABLE_CACHE_ALL_SUBJECT = "all_subject"
        private const val TABLE_CACHE_ALL_CHAPTER = "all_chapter"
        private const val TABLE_CACHE_QUESTION_DATA = "questions_data"
        private const val TABLE_CACHE_QUESTION_RELATED_STUFF = "questions_related_stuff"
        private const val TABLE_CACHE_QUESTION_SUBJECT_STUFF = "questions_subject_stuff"
        private const val TABLE_CACHE_SUBMIT_ANSWER_STUFF = "submit_answer_stuff"
        private const val TABLE_CACHE_SUBMIT_SESSION_DATA = "submit_session_data"

        //Subject List
        private const val KEY_QUIZ_SUBJECT_ID = "subject_id"
        private const val KEY_QUIZ_SUBJECT_NAME = "subject_name"

        //Chapter List
        private const val KEY_CHAPTER_ID = "chapter_id"
        private const val KEY_CHAPTER_NAME = "chapter_name"

        // Students_Profile Table Columns names ....
        private const val KEY_QUESTION_SUBJECT_ID = "subject_id"
        private const val KEY_QUESTION_ID = "question_id"
        private const val KEY_EASSY_ID = "eassy_id"
        private const val KEY_QUESTION_TYPE = "question_type"
        private const val KEY_QUESTION_TEXT = "question_ext"
        private const val KEY_EASSY_TEXT = "eassy_ext"
        private const val KEY_OPTION_A = "option_a"
        private const val KEY_OPTION_B = "option_b"
        private const val KEY_OPTION_C = "option_c"
        private const val KEY_OPTION_D = "option_d"
        private const val KEY_OPTION_E = "option_e"
        private const val KEY_RIGHT_MARKS = "right_marks"
        private const val KEY_WRONG_MARKS = "wrong_marks"
        private const val KEY_NO_OPTIONS = "no_options"
        private const val KEY_RIGHT_ANS = "right_ANS"

        // Examinations_Data Table Columns names ....
        private const val KEY_QUESTION_STUFF_EXAM_ID = "question_stuff_exam_id"
        private const val KEY_QUESTION_EXAM_NAME = "question_exam_name"
        private const val KEY_TOTAL_QUESTION = "total_questions"
        private const val KEY_TOTAL_MARKS = "total_marks"
        private const val KEY_TIME_DURATION_IN_SECONDS = "duration"
        private const val KEY_INSTRUCTIONS = "instructions"

        //subject_names with quizSubject id
        private const val KEY_SUBJECT_EXAM_ID = "subject_exam_id"
        private const val KEY_NUMBER_OF_MARKS = "number_of_marks"
        private const val KEY_SUBJECT_ID = "subject_id"
        private const val KEY_NUMBER_OF_QUESTIONS = "number_of_questions"
        private const val KEY_SUBJECT_NAMES = "subject_names"

        //Store Exam Current Session
        private const val KEY_QUESTION_ANSWER_ID = "question_answer_id"
        private const val KEY_RESPONSE_DATA = "response_data"
        private const val KEY_TIME_DURATION = "time_duration"
        private const val KEY_COLOR_STATUS = "color_status"

        //maintain Session
        private const val KEY_SUBMIT_EXAM_ID = "submit_exam_id"
        private const val KEY_SUBMIT_EXAM_STATUS = "submit_exam_status"
        private const val KEY_SUBMIT_EXAM_CURRENT_POSITION = "submit_exam_current_position"
        private const val KEY_SUBMIT_EXAM_TIME = "submit_exam_time"
        private const val KEY_COUNT_RESUME_SESSION = "count_resume"

        @JvmStatic
        @Synchronized
        fun getHelper(context: Context?): QuizAssetToLocalDatabase? {
            if (instance == null) instance = QuizAssetToLocalDatabase(context)
            return instance
        }

        fun clearAllTables(context: Context?) {
            val db = getHelper(context)!!.writableDatabase
            // query to obtain the names of all tables in your database
            val c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null)
            val tables: MutableList<String> = ArrayList()

// iterate over the result set, adding every table name to a list
            while (c.moveToNext()) {
                tables.add(c.getString(0))
            }

// call DROP TABLE on every table name
            for (table in tables) {
                val dropQuery = "DELETE FROM  $table"
                db.execSQL(dropQuery)
            }
            c.close()
        }
    }

}
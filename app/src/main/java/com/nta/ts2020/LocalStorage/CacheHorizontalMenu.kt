package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteException
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.PojoClasses.HorizontalMenuPojo
import java.util.*

class CacheHorizontalMenu(private val context: Context) {
    fun saveHorizontalMenu(bannerPojoArrayList: ArrayList<HorizontalMenuPojo>): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        for (i in bannerPojoArrayList.indices) {
            val horizontalMenuPojo = bannerPojoArrayList[i]
            val contentValues = ContentValues()
            contentValues.put(KEY_HORIZONTAL_MENU_ID, horizontalMenuPojo.id)
            contentValues.put(KEY_HORIZONTAL_BACKGROUND, horizontalMenuPojo.background)
            contentValues.put(KEY_HORIZONTAL_MENU_TITTLE, horizontalMenuPojo.title)
            contentValues.put(KEY_HORIZONTAL_MENU_DESCRIPTION, horizontalMenuPojo.description)
            contentValues.put(KEY_HORIZONTAL_MENU_TAG, horizontalMenuPojo.tag)
            try {
                sqLiteDatabase?.insertOrThrow(TABLE_HORIZONTAL_MENU, null, contentValues)
            } catch (e: SQLiteException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                return false
            }
        }
        return true
    }

    val horizontalMenu: ArrayList<HorizontalMenuPojo>
        get() {
            val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
            val cursor = sqLiteDatabase?.query(TABLE_HORIZONTAL_MENU, null, null, null, null, null, null)
            val horizontalMenuPojoArrayList = ArrayList<HorizontalMenuPojo>()
            if (cursor?.moveToFirst()!!) {
                do {
                    val horizontalMenuPojo = HorizontalMenuPojo()
                    horizontalMenuPojo.id = cursor?.getString(cursor.getColumnIndex(KEY_HORIZONTAL_MENU_ID))
                    horizontalMenuPojo.background = cursor?.getString(cursor.getColumnIndex(KEY_HORIZONTAL_BACKGROUND))
                    horizontalMenuPojo.title = cursor?.getString(cursor.getColumnIndex(KEY_HORIZONTAL_MENU_TITTLE))
                    horizontalMenuPojo.description = cursor?.getString(cursor.getColumnIndex(KEY_HORIZONTAL_MENU_DESCRIPTION))
                    horizontalMenuPojo.tag = cursor?.getString(cursor.getColumnIndex(KEY_HORIZONTAL_MENU_TAG))
                    horizontalMenuPojoArrayList.add(horizontalMenuPojo)
                } while (cursor.moveToNext())
            }
            return horizontalMenuPojoArrayList
        }

    fun clearRecords() {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        //Clear all old records
        sqLiteDatabase?.execSQL("DELETE FROM $TABLE_HORIZONTAL_MENU")
    }

    companion object {
        private const val TABLE_HORIZONTAL_MENU = "horizontal_menu"
        private const val KEY_HORIZONTAL_MENU_ID = "id"
        private const val KEY_HORIZONTAL_BACKGROUND = "BACKGROUND"
        private const val KEY_HORIZONTAL_MENU_TITTLE = "tittle"
        private const val KEY_HORIZONTAL_MENU_DESCRIPTION = "description"
        private const val KEY_HORIZONTAL_MENU_TAG = "tag"
    }

}
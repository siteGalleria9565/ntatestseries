package com.nta.ts2020.LocalStorage

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import com.nta.ts2020.PojoClasses.EBookPojo
import java.util.*

class CacheEBook(private val context: Context) {
    fun saveEBookList(eBookPojoArrayList: ArrayList<EBookPojo>): Boolean {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        for ((eBookId, eBookPdfName, thumbnail, type, eBookName, eBookAuthor, eBookPublication, eBookLanguage, eBookSubjectId, downloadUrl) in eBookPojoArrayList) {
            val contentValues = ContentValues()
            contentValues.put(KEY_E_BOOK_ID, eBookId)
            contentValues.put(KEY_E_BOOK_NAME, eBookName)
            contentValues.put(KEY_E_BOOK_PDF_NAME, eBookPdfName)
            contentValues.put(KEY_E_BOOK_THUMBNAIL, thumbnail)
            contentValues.put(KEY_TYPE, type)
            contentValues.put(KEY_E_BOOK_AUTHOR, eBookAuthor)
            contentValues.put(KEY_E_BOOK_PUBLICATION, eBookPublication)
            contentValues.put(KEY_E_BOOK_LANGUAGE, eBookLanguage)
            contentValues.put(KEY_E_BOOK_SUBJECT_ID, eBookSubjectId)
            contentValues.put(KEY_DOWNLOAD_URL, downloadUrl)
            try {
                sqLiteDatabase?.insertOrThrow(TABLE_E_BOOK, null, contentValues)
            } catch (e: SQLException) {
                e.printStackTrace()
                return false
            }
        }
        return true
    }

    fun getEBookListBySubjectId(subjectId: String?, eBookType: String?): ArrayList<EBookPojo> {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.readableDatabase
        val eBookPojoArrayList = ArrayList<EBookPojo>()
        val cursor: Cursor
        cursor = sqLiteDatabase!!.query(TABLE_E_BOOK, null, "$KEY_E_BOOK_SUBJECT_ID='$subjectId' AND $KEY_TYPE ='$eBookType'", null, null, null, null)
        if (cursor.moveToFirst()) {
            do {
                val eBookPojo = EBookPojo()
                eBookPojo.eBookId = cursor.getString(cursor.getColumnIndex(KEY_E_BOOK_ID))
                eBookPojo.eBookName = cursor.getString(cursor.getColumnIndex(KEY_E_BOOK_NAME))
                eBookPojo.thumbnail = cursor.getString(cursor.getColumnIndex(KEY_E_BOOK_THUMBNAIL))
                eBookPojo.eBookPdfName = cursor.getString(cursor.getColumnIndex(KEY_E_BOOK_PDF_NAME))
                eBookPojo.type = cursor.getString(cursor.getColumnIndex(KEY_TYPE))
                eBookPojo.eBookAuthor = cursor.getString(cursor.getColumnIndex(KEY_E_BOOK_AUTHOR))
                eBookPojo.eBookPublication = cursor.getString(cursor.getColumnIndex(KEY_E_BOOK_PUBLICATION))
                eBookPojo.eBookLanguage = cursor.getString(cursor.getColumnIndex(KEY_E_BOOK_LANGUAGE))
                eBookPojo.eBookSubjectId = cursor.getString(cursor.getColumnIndex(KEY_E_BOOK_SUBJECT_ID))
                eBookPojo.downloadUrl = cursor.getString(cursor.getColumnIndex(KEY_DOWNLOAD_URL))
                eBookPojoArrayList.add(eBookPojo)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return eBookPojoArrayList
    }

    fun clearRecords(subjectId: String, eBookType: String) {
        val sqLiteDatabase = CommonAssetToLocalDatabase.getHelper(context)?.writableDatabase
        //Clear all old records
        sqLiteDatabase?.execSQL("DELETE FROM $TABLE_E_BOOK WHERE $KEY_E_BOOK_SUBJECT_ID = '$subjectId' AND $KEY_TYPE ='$eBookType'")
    }

    companion object {
        private const val TAG = "CacheEBook"
        private const val TABLE_E_BOOK = "e_book"
        private const val KEY_E_BOOK_ID = "e_book_id"
        private const val KEY_E_BOOK_PDF_NAME = "e_book_pdf_name"
        private const val KEY_E_BOOK_THUMBNAIL = "e_book_thumbnail"
        private const val KEY_TYPE = "type"
        private const val KEY_E_BOOK_NAME = "e_book_name"
        private const val KEY_E_BOOK_AUTHOR = "e_book_author"
        private const val KEY_E_BOOK_PUBLICATION = "e_book_publication"
        private const val KEY_E_BOOK_LANGUAGE = "e_book_language"
        private const val KEY_E_BOOK_SUBJECT_ID = "e_book_subject_id"
        private const val KEY_DOWNLOAD_URL = "download_url"
    }

}
package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.ExamNotificationsDetailsActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.PojoClasses.ParcelPojoClass.NotificationDetailsPojo
import java.lang.ref.WeakReference

class NotificationDetailsHandler(notificationsDetailsActivity: ExamNotificationsDetailsActivity) : Handler() {
    var notificationsDetailsActivityWeakReference: WeakReference<ExamNotificationsDetailsActivity>
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        val notificationsDetailsActivity = notificationsDetailsActivityWeakReference.get()
        val notificationDetailsPojo: NotificationDetailsPojo? = bundleMessage.getParcelable("notification")
        if ("Success" == bundleMessage.getString("STATUS")) {
            notificationsDetailsActivity!!.loadData(SUCCESS, notificationDetailsPojo)
        } else {
            notificationsDetailsActivity!!.loadData(NETWORK_ERROR, null)
            Toast.makeText(notificationsDetailsActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show()
        }
    }

    init {
        notificationsDetailsActivityWeakReference = WeakReference(notificationsDetailsActivity)
    }
}
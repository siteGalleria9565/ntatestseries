package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.provider.ContactsContract
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_SYNC_CONTACTS
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.UtilsHelper.SharedPrefManager
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

class ContactsSyncThread(private val context: Context) : Runnable {
    override fun run() {
        //Cursor for reading contacts
        val phones = context.contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null)

        //Variables
        var prevPhNO = ""
        val jsonRoot = JSONObject()
        val jsonContacts = JSONArray()

        //Read contacts and build json
        try {
            if (phones != null) {
                while (phones.moveToNext()) {
                    val name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                    val phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replace(" ", "")
                    if (phoneNumber != prevPhNO) {
                        Log.d(TAG, "Name: $name\nNumber: $phoneNumber")
                        val jsonContact = JSONObject()
                        jsonContact.put("name", name)
                        jsonContact.put("ph_no", phoneNumber)
                        jsonContacts.put(jsonContact)
                    }
                    prevPhNO = phoneNumber
                }
                phones.close()
            }
            jsonRoot.put("user_id", CacheUserData(context).userDataFromCache.userId)
            jsonRoot.put("contacts", jsonContacts)
            Log.d(TAG, "jsonContacts: $jsonContacts")
            //Encode json data to utf-8
            val parameterJson = URLEncoder.encode(jsonRoot.toString(), "utf-8")


            //Send contacts to the server
            var os: OutputStreamWriter? = null
            var conn: HttpURLConnection? = null
            val parameter = "contacts=$parameterJson"
            try {
                val url = URL(URL_SYNC_CONTACTS)
                conn = url.openConnection() as HttpURLConnection
                conn.readTimeout = READ_TIMEOUT_VALUE
                conn.connectTimeout = CONNECT_TIMEOUT_VALUE
                conn.requestMethod = "POST"
                conn.doInput = true
                conn.doOutput = true
                conn.doOutput = true

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
                conn.setRequestProperty("App-Id", "nta")

                //open
                conn.connect()

                //setup send
                os = OutputStreamWriter(conn.outputStream)
                os.write(parameter)
                //clean up
                Log.d(TAG, "run: " + "CALLED")
                os.flush()

                // Read Server Response
                val bufferedReader = BufferedReader(InputStreamReader(conn.inputStream))
                var line: String?
                val sb = StringBuilder()
                while (bufferedReader.readLine().also { line = it } != null) {
                    // Append server response in string
                    sb.append(line).append("\n")
                }
                val response = sb.toString()
                Log.d(TAG, "Response from server: $response")

                //update status if success
                val jsonResponse = JSONObject(response)
                if (jsonResponse.getBoolean("status")) {
                    SharedPrefManager(context).isContactsSynced = true
                }
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                //                Log.w(TAG, e.getMessage());
            } finally {
                //clean up
                try {
                    os?.close()
                } catch (e: IOException) {
                    if (BuildConfig.DEBUG) e.printStackTrace()
                }
                conn?.disconnect()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }

    companion object {
        private const val TAG = "ContactsSyncThread"
    }

}
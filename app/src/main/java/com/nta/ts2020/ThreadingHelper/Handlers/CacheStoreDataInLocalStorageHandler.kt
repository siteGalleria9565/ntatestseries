package com.nta.ts2020.ThreadingHelper.Handlers

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import com.nta.ts2020.Activities.ExamShowQuestionInstructionActivity
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import java.lang.ref.WeakReference

class CacheStoreDataInLocalStorageHandler(// Context ....
        var context: Context, localShowQuestionDataBasedOnExamIdActivityInstance: ExamShowQuestionInstructionActivity) : Handler() {

    // Bundle ....
    lateinit var localQuestionsBasedOnExamIDFromServerInstance: Bundle
    var localShowQuestionDataBasedOnExamIdActivityInstance: Handler

    // Threading ....
    var localThreadPoolServiceInstance: ThreadPoolService
    private val referenceShowQuestionDataBasedOnExamIdActivity: WeakReference<ExamShowQuestionInstructionActivity>
    var localShowQuestionDataBasedOnExamIdInstance: ExamShowQuestionInstructionActivity?

    // Strings ....
    private val TAG = "StoreCacheDataIDHnldr"
    override fun handleMessage(inputMessageFromChildThread: Message) {
        // Getting the Examination Bundle object from the Child Thread's Message ....
        localQuestionsBasedOnExamIDFromServerInstance = inputMessageFromChildThread.data
        Log.d(TAG, "Thread for saving CacheStoreDataInLocalStorageHandler executed and returned message: " + localQuestionsBasedOnExamIDFromServerInstance.getString("output"))
        if (localQuestionsBasedOnExamIDFromServerInstance.getString("output") == "Error") {
            Log.d(TAG, "CacheStoreDataInLocalStorageHandler data could not be saved into the Cloud.")
        } else {
            Log.d(TAG, "CacheStoreDataInLocalStorageHandler data Download successfully into the Cloud.")

            // Setting the Examination Mapping List in the Activity ....
            localShowQuestionDataBasedOnExamIdInstance!!.stopWatch()
        }
    }

    init {
        this.localShowQuestionDataBasedOnExamIdActivityInstance = Handler()
        referenceShowQuestionDataBasedOnExamIdActivity = WeakReference(localShowQuestionDataBasedOnExamIdActivityInstance)
        localShowQuestionDataBasedOnExamIdInstance = referenceShowQuestionDataBasedOnExamIdActivity.get()
        localThreadPoolServiceInstance = ThreadPoolService()
    }
}
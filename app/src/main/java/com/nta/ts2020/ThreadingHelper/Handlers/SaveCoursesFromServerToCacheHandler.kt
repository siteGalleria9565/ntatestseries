package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

class SaveCoursesFromServerToCacheHandler(private val activity: AppCompatActivity, private val activityType: Int) : Handler() {
    override fun handleMessage(msg: Message) {
        Log.d(TAG, "SaveCoursesFromServerToCacheHandler invoked")
        val bundleMessage = msg.data

//        switch (activityType) {
//            case REGISTER_ACTIVITY:
//                WeakReference<ExamRegisterActivity> registerActivityWeakReference = new WeakReference<>((ExamRegisterActivity) activity);
//                ExamRegisterActivity registerActivity = registerActivityWeakReference.get();
//
//                if ("Success".equals(bundleMessage.getString("MESSAGE"))) {
//                    ArrayList<CoursePojo> coursePojoArrayList = new Gson().fromJson(bundleMessage.getString("JSON_DATA"), new TypeToken<List<CoursePojo>>() {
//                    }.getType());
//                    registerActivity.loadData(coursePojoArrayList);
//                } else {
//                    String message = bundleMessage.getString("MESSAGE"); //Can be used if needed to show specific error messages
//                    registerActivity.loadData(new ArrayList<CoursePojo>());
//                    Toast.makeText(registerActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
//                }
//
//                break;
//
//            case POST_REGISTRATION_ACTIVITY:
//                WeakReference<ExamPostRegistrationScreenActivity> postRegistrationActivityWeakReference = new WeakReference<>((ExamPostRegistrationScreenActivity) activity);
//                PostRegistrationActivity postRegistrationActivity = postRegistrationActivityWeakReference.get();
//
//                if ("Success".equals(bundleMessage.getString("MESSAGE"))) {
//                    ArrayList<CoursePojo> coursePojoArrayList = new Gson().fromJson(bundleMessage.getString("JSON_DATA"), new TypeToken<List<CoursePojo>>() {
//                    }.getType());
//                    postRegistrationActivity.loadData(coursePojoArrayList);
//                } else {
//                    String message = bundleMessage.getString("MESSAGE"); //Can be used if needed to show specific error messages
//                    postRegistrationActivity.loadData(new ArrayList<CoursePojo>());
//                    Toast.makeText(postRegistrationActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
//                }
//
//                break;
//        }
        super.handleMessage(msg)
    }

    companion object {
        private const val TAG = "SaveCoursesHandler"
    }

}
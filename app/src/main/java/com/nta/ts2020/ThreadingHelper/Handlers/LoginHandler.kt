package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.LocalStorage.CacheUserData
import java.lang.ref.WeakReference

class LoginHandler(loginActivity: ExamLoginScreenActivity) : Handler() {
    private val loginActivityWeakReference: WeakReference<ExamLoginScreenActivity> = WeakReference(loginActivity)
    override fun handleMessage(msg: Message) {
        val bundleMessage = msg.data
        val loginActivity = loginActivityWeakReference.get()
        if ("Success" == bundleMessage.getString("STATUS")) {
            val serverStatus = bundleMessage.getInt("SERVER_STATUS")
            val serverMessage = bundleMessage.getString("MESSAGE")
            val databaseResponse = bundleMessage.getBoolean("DATABASE_RESPONSE")
            if (200 == serverStatus) {
                if (databaseResponse) {
                    //Successfully logged in , goto next activity
//                    Toast.makeText(loginActivity, serverMessage, Toast.LENGTH_SHORT).show();
                    CacheUserData(loginActivity!!).updateUserLoginStatus(true)
                    loginActivity.startNextActivity()
                } else {
                    //Login success but user data not saved to local database
                    Toast.makeText(loginActivity, "Please try again", Toast.LENGTH_SHORT).show()
                }
            } else {
                //Wrong username and password
                Toast.makeText(loginActivity, serverMessage, Toast.LENGTH_SHORT).show()
            }
        } else {
            //Exceptions
            if ("timeout" == bundleMessage.getString("MESSAGE")) {
                Toast.makeText(loginActivity, "Connection timeout, please try again", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(loginActivity, "Network Error: Please check your internet connection ", Toast.LENGTH_LONG).show()
            }
        }
        val progressDialog = loginActivity!!.progressDialog
        if (progressDialog!!.isShowing) {
            progressDialog.dismiss()
        }
        super.handleMessage(msg)
    }

}
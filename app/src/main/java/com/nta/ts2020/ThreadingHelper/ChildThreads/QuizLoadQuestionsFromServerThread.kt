package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import com.google.gson.Gson
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.QuizCacheAllQuestionRelatedStuff
import com.nta.ts2020.LocalStorage.QuizCacheQuestionDataBasedOnExamId
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.PojoClasses.ExamQuestionDataBasedOnExamIdPojo
import com.nta.ts2020.PojoClasses.ParcelPojoClass.CacheQuestionDataParcel
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class QuizLoadQuestionsFromServerThread(context: Context?, localQuestionDataBasedOnExamIdInstance: Handler, chapterIds: String, numOfSubjects: String, selectedTime: String, private val selectedLanguage: String) : Runnable {

    // UI Stuff ....
    var context: Context? = null

    // Handlers ....
    var handlerQuestionDataBasedOnExamId: Handler? = null

    // Message ....
    var localQuestionsBasedOnExamIDFromServerInstance: Message? = null

    // Bundle ....
    var localQuestionsBasedOnExamIDFromServerBundleDataInstance: Bundle? = null

    //Transection
    var localQuizCacheQuestionDataBasedOnExamIdInstance: QuizCacheQuestionDataBasedOnExamId
    var localQuizCacheAllQuestionRelatedStuffInstance: QuizCacheAllQuestionRelatedStuff

    // POJOs ....
    var QuestionDataBasedOnExamIdPojoInstance: ExamQuestionDataBasedOnExamIdPojo? = null
    var quizAllQuestionsSubjectWisePojoInstance: ExamAllQuestionsSubjectWisePojo? = null

    // Collections ....
    var localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo?>? = null

    // GSONS ....
    var localGsonObject: Gson

    // Network Request Stuff ....
    var requestURL: URL? = null
    var requestURLConnection: HttpURLConnection? = null

    // Stream Writers ....
    var requestDataStreamWriterInstance: OutputStreamWriter? = null

    // Stream Readers ....
    var serverResponseStreamReaderInstance: InputStreamReader? = null
    var serverResponseBufferedReaderInstance: BufferedReader? = null

    //parcel
    var cacheQuestionDataParcelInstance: CacheQuestionDataParcel? = null
    var serverResponseCode = 0
    var i = 0
    var chapterIds: String
    var numOfSubjects: String
    var selectedTime: String
    var selectedLangInt = 0

    // Strings ....
    var jsonRequestDataString: String? = null
    var jsonResponseDataString: String? = null
    var serverURL: String? = null
    var updatingDataInCacheStatus: String? = null

    //Array....
    lateinit var SubjectIdArray: Array<String?>
    lateinit var SubjectWiseMarksArray: Array<String?>
    lateinit var NumberOfQuestionsArray: Array<String?>
    lateinit var SubjectNamesArray: Array<String?>
    private val TAG = "LoadQuesFrmSrvrThread"
    private var isEssayAvailable = false
    override fun run() {
        try {
            jsonRequestDataString = "chapter_ids=$chapterIds"
            jsonRequestDataString += "&num_of_subjects=$numOfSubjects"
            jsonRequestDataString += "&selected_time=$selectedTime"
            if (selectedLanguage == "English") {
                jsonRequestDataString += "&" + "selected_language" + "=" + 1
            } else if (selectedLanguage == "Hindi") {
                jsonRequestDataString += "&" + "selected_language" + "=" + 2
            }
            Log.d(TAG, "Quiz request parameter: $jsonRequestDataString")

            // Establishing URL Connection with the Server ....
            serverURL = ConstantVariables.URL_LOAD_QUIZ_QUESTION_BASED_ON_LANGUAGE_FROM_SERVER
            requestURL = URL(serverURL)
            requestURLConnection = requestURL!!.openConnection() as HttpURLConnection
            requestURLConnection!!.doOutput = true
            //            Log.d(TAG, "#requestURLConnection Code: " + requestURLConnection);

            // Sending the data along with the request to the Server ....
            requestDataStreamWriterInstance = OutputStreamWriter(requestURLConnection!!.outputStream)
            requestDataStreamWriterInstance!!.write(jsonRequestDataString)
            requestDataStreamWriterInstance!!.close()

            // Getting Response from the Server ....
            serverResponseCode = requestURLConnection!!.responseCode
            //            Log.d(TAG, "#Server's Response Code: " + serverResponseCode);
            if (serverResponseCode == HttpURLConnection.HTTP_OK) {
                serverResponseStreamReaderInstance = InputStreamReader(requestURLConnection!!.inputStream)
                serverResponseBufferedReaderInstance = BufferedReader(serverResponseStreamReaderInstance)


                // Reading the Response Data ....
                jsonResponseDataString = serverResponseBufferedReaderInstance!!.readLine()
                Log.d(TAG, "Server Response: $jsonResponseDataString")

                // Closing the Stream ....
                serverResponseBufferedReaderInstance!!.close()

                // Retrieving Profile Pojo from JSON ....
//                Log.d(TAG, "###FunctionOutput_isDataFound: " + jsonResponseDataString);

                // Retrieving Profile Pojo from JSON ....
                val jsonRoot = JSONObject(jsonResponseDataString)

                //Get Json String
                QuestionDataBasedOnExamIdPojoInstance!!.examId = jsonRoot.getString("exam_id")
                //                Log.d(TAG, "#Server Response: setExamId Status " + QuestionDataBasedOnExamIdPojoInstance.getExamId());
                QuestionDataBasedOnExamIdPojoInstance!!.examName = jsonRoot.getString("exam_name")
                //                Log.d(TAG, "#Server Response: setExamName Status " + QuestionDataBasedOnExamIdPojoInstance.getExamName());
                QuestionDataBasedOnExamIdPojoInstance!!.totalQuestions = jsonRoot.getString("total_questions")
                //                Log.d(TAG, "#Server Response: setTotalQuestions Status " + QuestionDataBasedOnExamIdPojoInstance.getTotalQuestions());
                QuestionDataBasedOnExamIdPojoInstance!!.totalMarks = jsonRoot.getString("total_marks")
                //                Log.d(TAG, "#Server Response: setTotalMarks Status " + QuestionDataBasedOnExamIdPojoInstance.getTotalMarks());
                QuestionDataBasedOnExamIdPojoInstance!!.timeDurationInSeconds = jsonRoot.getString("duration")
                //                Log.d(TAG, "#Server Response: getTimeDurationInSeconds Status " + QuestionDataBasedOnExamIdPojoInstance.getTimeDurationInSeconds());
                QuestionDataBasedOnExamIdPojoInstance!!.instructions = jsonRoot.getString("instructions")
                //                Log.d(TAG, "#Server Response: setInstructions Status " + QuestionDataBasedOnExamIdPojoInstance.getInstructions());

                // Getting JSON Array node
                val SubjectId = jsonRoot.getJSONArray("subject_ids")
                //                Log.d(TAG, "#Server Response: getJSONArray SubjectId Status " +SubjectId.length());


                //Memmory Allocation Of Array's
                SubjectIdArray = arrayOfNulls(SubjectId.length())
                SubjectWiseMarksArray = arrayOfNulls(SubjectId.length())
                NumberOfQuestionsArray = arrayOfNulls(SubjectId.length())
                SubjectNamesArray = arrayOfNulls(SubjectId.length())
                val NumberOfQuestionsJSONObject = jsonRoot.getJSONObject("number_of_questions")
                val SubjectWiseMarksJSONObject = jsonRoot.getJSONObject("number_of_marks")
                val SubjectNamesJSONObject = jsonRoot.getJSONObject("subject_names")
                var EassyDataJSONObject = JSONObject()
                isEssayAvailable = jsonRoot.getString("eassy") != "false"
                if (isEssayAvailable) EassyDataJSONObject = jsonRoot.getJSONObject("eassy")


                //Store Data From Json To Pojo
                for (k in 0 until SubjectId.length()) {
                    SubjectIdArray[k] = SubjectId[k].toString()
                    NumberOfQuestionsArray[k] = NumberOfQuestionsJSONObject.getString(SubjectIdArray[k])
                    //                    Log.d(TAG, "#ArraySize NumberOfQuestionsArray= " +NumberOfQuestionsArray[k]);
                    SubjectWiseMarksArray[k] = SubjectWiseMarksJSONObject.getString(SubjectIdArray[k])
                    //                    Log.d(TAG, "#ArrayinLoop SubjectWiseMarksArray= " +  SubjectWiseMarksArray[k]);
                    SubjectNamesArray[k] = SubjectNamesJSONObject.getString(SubjectIdArray[k])
                }
                QuestionDataBasedOnExamIdPojoInstance!!.subjectId = SubjectIdArray
                QuestionDataBasedOnExamIdPojoInstance!!.numberOfQuestions = NumberOfQuestionsArray
                QuestionDataBasedOnExamIdPojoInstance!!.numberOfMarks = SubjectWiseMarksArray
                //                Log.d(TAG, "#ArraySize SubjectWiseMarksArray= " +  QuestionDataBasedOnExamIdPojoInstance.getNumberOfMarks());
                QuestionDataBasedOnExamIdPojoInstance!!.subjectNames = SubjectNamesArray
                //                Log.d(TAG, "#ArraySize SubjectNamesArray= " +QuestionDataBasedOnExamIdPojoInstance.getSubjectNames());

//                Log.d(TAG, "#ArraySize = " +SubjectNamesArray.length);
                for (k in SubjectNamesArray.indices) {

                    // Getting JSON Array node
                    val SubjectWiseData = jsonRoot.getJSONArray(SubjectNamesArray[k])

                    // looping through All Contacts
                    //JST SubjectWiseData.length()
                    for (j in 0 until SubjectWiseData.length()) {
                        val SubjectWiseDataJSONObject = SubjectWiseData.getJSONObject(j)
                        quizAllQuestionsSubjectWisePojoInstance = ExamAllQuestionsSubjectWisePojo()
                        quizAllQuestionsSubjectWisePojoInstance!!.subjectId = SubjectNamesArray[k]
                        quizAllQuestionsSubjectWisePojoInstance!!.questionType = SubjectWiseDataJSONObject.getString("question_type")
                        quizAllQuestionsSubjectWisePojoInstance!!.questionId = SubjectWiseDataJSONObject.getString("question_id")
                        quizAllQuestionsSubjectWisePojoInstance!!.questionText = SubjectWiseDataJSONObject.getString("question_text")
                        quizAllQuestionsSubjectWisePojoInstance!!.noOptions = SubjectWiseDataJSONObject.getString("no_options")
                        quizAllQuestionsSubjectWisePojoInstance!!.optionA = SubjectWiseDataJSONObject.getString("option_A")
                        quizAllQuestionsSubjectWisePojoInstance!!.optionB = SubjectWiseDataJSONObject.getString("option_B")
                        quizAllQuestionsSubjectWisePojoInstance!!.optionC = SubjectWiseDataJSONObject.getString("option_C")
                        quizAllQuestionsSubjectWisePojoInstance!!.optionD = SubjectWiseDataJSONObject.getString("option_D")
                        quizAllQuestionsSubjectWisePojoInstance!!.optionE = SubjectWiseDataJSONObject.getString("option_E")
                        quizAllQuestionsSubjectWisePojoInstance!!.rightMarks = SubjectWiseDataJSONObject.getString("right_marks")
                        quizAllQuestionsSubjectWisePojoInstance!!.wrongMarks = SubjectWiseDataJSONObject.getString("wrong_marks")
                        quizAllQuestionsSubjectWisePojoInstance!!.rightAns = SubjectWiseDataJSONObject.getString("correct_ans")
                        quizAllQuestionsSubjectWisePojoInstance!!.essayId = SubjectWiseDataJSONObject.getString("EASSAY_ID")
                        if (isEssayAvailable && quizAllQuestionsSubjectWisePojoInstance!!.essayId != "0" && !EassyDataJSONObject.isNull(quizAllQuestionsSubjectWisePojoInstance!!.essayId)) {
                            quizAllQuestionsSubjectWisePojoInstance!!.essayData = EassyDataJSONObject.getString(quizAllQuestionsSubjectWisePojoInstance!!.essayId)
                        } else {
                            quizAllQuestionsSubjectWisePojoInstance!!.essayData = ""
                        }
                        localQuestionDataBasedOnExamIdList!!.add(quizAllQuestionsSubjectWisePojoInstance!!)
                    }
                    //                    Log.d(TAG, "#ArrayList Values= " +localQuestionDataBasedOnExamIdList.size());
                }
                //                Log.d(TAG, "#ArrayList Values final Data= " +localQuestionDataBasedOnExamIdList.size());

                //Insert  SubjectStuff in cache
                updatingDataInCacheStatus = localQuizCacheAllQuestionRelatedStuffInstance.setCacheAllQuestionRelatedStuff(QuestionDataBasedOnExamIdPojoInstance!!)

//                Log.d(TAG, "#updatingDataInCacheStatus: " + updatingDataInCacheStatus);
                cacheQuestionDataParcelInstance = CacheQuestionDataParcel(localQuestionDataBasedOnExamIdList)
                localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putParcelable("CacheQuestionDataParcel", cacheQuestionDataParcelInstance)


//                Log.d(TAG, "#updatingDataInCacheStatus: " + updatingDataInCacheStatus);
                localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Success")

                // Storing the Bundle object in the Message Object ....
                localQuestionsBasedOnExamIDFromServerInstance!!.data = localQuestionsBasedOnExamIDFromServerBundleDataInstance

                // The "handleMessage()" function from the "CloudClassStudentsMappingHandler" class  automatically gets invoked after this function call ....
                handlerQuestionDataBasedOnExamId!!.sendMessage(localQuestionsBasedOnExamIDFromServerInstance)
            }
        } catch (e1: IOException) {
            if (BuildConfig.DEBUG) e1.printStackTrace()
            localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Error")

            // Storing the Bundle object in the Message Object ....
            localQuestionsBasedOnExamIDFromServerInstance!!.data = localQuestionsBasedOnExamIDFromServerBundleDataInstance

            // The "handleMessage()" function from the "localQuestionsBasedOnExamIDFromServerInstance" class  automatically gets invoked after this function call ....
            handlerQuestionDataBasedOnExamId!!.sendMessage(localQuestionsBasedOnExamIDFromServerInstance)
        } catch (e1: JSONException) {
            if (BuildConfig.DEBUG) e1.printStackTrace()
            localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Error")
            localQuestionsBasedOnExamIDFromServerInstance!!.data = localQuestionsBasedOnExamIDFromServerBundleDataInstance
            handlerQuestionDataBasedOnExamId!!.sendMessage(localQuestionsBasedOnExamIDFromServerInstance)
        }
    }

    fun initializeLocalVariables(context: Context?, handlerQuestionDataBasedOnExamId: Handler) {
        // UI Stuff ....
        this.context = context

        //pojo
        QuestionDataBasedOnExamIdPojoInstance = ExamQuestionDataBasedOnExamIdPojo()

        //ArrayList
        localQuestionDataBasedOnExamIdList = ArrayList()

        // Handlers ....
        this.handlerQuestionDataBasedOnExamId = handlerQuestionDataBasedOnExamId

        // Bundle ....
        localQuestionsBasedOnExamIDFromServerBundleDataInstance = Bundle()

        // Message....
        localQuestionsBasedOnExamIDFromServerInstance = handlerQuestionDataBasedOnExamId.obtainMessage()
    }

    init {
        initializeLocalVariables(context, localQuestionDataBasedOnExamIdInstance)
        localQuizCacheQuestionDataBasedOnExamIdInstance = context?.let { QuizCacheQuestionDataBasedOnExamId(it) }!!
        localQuizCacheAllQuestionRelatedStuffInstance = QuizCacheAllQuestionRelatedStuff(context)
        localGsonObject = Gson()
        this.chapterIds = chapterIds
        this.numOfSubjects = numOfSubjects
        this.selectedTime = selectedTime
    }
}
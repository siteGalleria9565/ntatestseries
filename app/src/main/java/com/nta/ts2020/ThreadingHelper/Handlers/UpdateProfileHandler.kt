package com.nta.ts2020.ThreadingHelper.Handlers

import android.app.ProgressDialog
import android.content.Context
import android.os.Handler
import android.os.Message
import android.widget.Toast

class UpdateProfileHandler(private val context: Context, private val progressDialog: ProgressDialog) : Handler() {
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        if ("Success" == bundleMessage.getString("STATUS")) {
            Toast.makeText(context, "Data updated successfully", Toast.LENGTH_SHORT).show()
        } else {
            //Exceptions
            if ("timeout" == bundleMessage.getString("MESSAGE")) {
                Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show()
            }
        }
        if (progressDialog.isShowing) progressDialog.dismiss()
    }

}
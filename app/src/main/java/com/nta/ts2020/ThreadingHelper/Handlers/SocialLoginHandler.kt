package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class SocialLoginHandler(loginActivity: ExamLoginScreenActivity) : Handler() {
    var weakReference: WeakReference<ExamLoginScreenActivity>
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val loginActivity = weakReference.get()
        val bundleMessage = msg.data
        if ("Success" == bundleMessage.getString("STATUS")) {
            loginActivity!!.loadData(SUCCESS, bundleMessage.getBoolean("isRegistered"), bundleMessage.getString("USER_ID"))
        } else {
            //Exceptions
            if ("timeout" == bundleMessage.getString("MESSAGE")) {
                Toast.makeText(loginActivity, "Please try again", Toast.LENGTH_SHORT).show()
                loginActivity!!.loadData(NETWORK_ERROR, false, null)
            } else {
                Toast.makeText(loginActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show()
                loginActivity!!.loadData(NETWORK_ERROR, false, null)
            }
        }
    }

    init {
        weakReference = WeakReference(loginActivity)
    }
}
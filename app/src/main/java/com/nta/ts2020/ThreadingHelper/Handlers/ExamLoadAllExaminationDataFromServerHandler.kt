package com.nta.ts2020.ThreadingHelper.Handlers

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import com.nta.ts2020.Activities.ExamShowAllExaminationdatlistActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class ExamLoadAllExaminationDataFromServerHandler(// Context ....
        var context: Context, localShowAllExaminationDataActivityInstance: ExamShowAllExaminationdatlistActivity) : Handler() {

    // Bundle ....
    lateinit var localLoadAllExaminationDataFromServerInstance: Bundle
    private val referenceShowAllExaminationDataActivity: WeakReference<ExamShowAllExaminationdatlistActivity>
    var localShowAllExaminationDataInstance: ExamShowAllExaminationdatlistActivity?

    // Strings ....
    private val TAG = "AllExamDataIDHnldr"
    override fun handleMessage(inputMessageFromChildThread: Message) {

        // Getting the Examination Bundle object from the Child Thread's Message ....
        localLoadAllExaminationDataFromServerInstance = inputMessageFromChildThread.data
        Log.d(TAG, "Thread for saving AllExaminationData executed and returned message: " + localLoadAllExaminationDataFromServerInstance.getString("save_all_Examination_Data_output"))
        if (localLoadAllExaminationDataFromServerInstance.getString("save_all_Examination_Data_output") == "Error") {
            Log.d(TAG, "AllExaminationData data could not be saved into the Cloud.")
            localShowAllExaminationDataInstance!!.loadData(NETWORK_ERROR)
        } else {
            Log.d(TAG, "AllExaminationData data Download successfully into the Cloud.")
            localShowAllExaminationDataInstance!!.loadData(SUCCESS)
        }
    }

    init {
        referenceShowAllExaminationDataActivity = WeakReference(localShowAllExaminationDataActivityInstance)
        localShowAllExaminationDataInstance = referenceShowAllExaminationDataActivity.get()
    }
}
package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_GET_RESULT_LIST
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheResultList
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.ResultListPojo
import com.nta.ts2020.ThreadingHelper.Handlers.SaveResultListFromServerToCacheHandler
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.math.BigDecimal
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class SaveResultListFromServerToCacheThread(private val context: Context, private val saveResultListFromServerToCacheHandler: SaveResultListFromServerToCacheHandler) : Runnable, WebResponseListener {
    override fun run() {
        var connection: HttpURLConnection? = null
        var bufferedReader: BufferedReader? = null
        var request: OutputStreamWriter? = null
        val resultListPojoArrayList = ArrayList<ResultListPojo>()
        val bundleMessage = Bundle()
        val message = saveResultListFromServerToCacheHandler.obtainMessage()
        val cacheResultList = CacheResultList(context)
        cacheResultList.clearAllRecords()
        val userId = CacheUserData(context).userDataFromCache.userId
        val parameter = "user_id=$userId"
        WebRequest(context).GET_METHOD(URL_GET_RESULT_LIST + parameter, this, null, true)
        Log.d(TAG, parameter)
        try {
            //Download json from url
            val url = URL(URL_GET_RESULT_LIST)
            connection = url.openConnection() as HttpURLConnection
            connection.doOutput = true
            connection!!.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

            //Write parameters to the stream
            request = OutputStreamWriter(connection.outputStream)
            request.write(parameter)

            //Clean up
            request.flush()
            //Read Response
            bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
            var line: String?
            val stringBuilder = StringBuilder()
            while (bufferedReader.readLine().also { line = it } != null) {
                stringBuilder.append(line).append("\n")
            }
            Log.d(TAG, "Json from server: $stringBuilder")
            val jsonRoot = JSONObject(stringBuilder.toString())
            var statusDatabase = false
            val responseStatus = jsonRoot.getBoolean("status")
            if (responseStatus) {
                if (jsonRoot.has("exams")) {
                    val jsonExamResultList = jsonRoot.getJSONArray("exams")

                    //Parsing json
                    //Exam
                    for (i in 0 until jsonExamResultList.length()) {
                        val resultListPojo = ResultListPojo()
                        var subjectsJson: JSONObject
                        val subjectIdArrayList = ArrayList<String>()
                        val subjectNameArrayList = ArrayList<String>()
                        val tempJsonObject = jsonExamResultList.getJSONObject(i)
                        resultListPojo.examName = tempJsonObject.getString("exam_name")
                        resultListPojo.id = tempJsonObject.getString("id")
                        resultListPojo.examType = tempJsonObject.getString("exam_type")
                        resultListPojo.date = tempJsonObject.getString("date")
                        resultListPojo.totalQues = tempJsonObject.getInt("total_questions")
                        resultListPojo.totalMarks = BigDecimal.valueOf(tempJsonObject.getDouble("total_marks")).toFloat()
                        resultListPojo.attemptedQues = tempJsonObject.getInt("attempted_questions")
                        resultListPojo.correctQues = tempJsonObject.getInt("correct_questions")
                        resultListPojo.totalObtainedMarks = BigDecimal.valueOf(tempJsonObject.getDouble("total_obtained_marks")).toFloat()
                        subjectsJson = tempJsonObject.getJSONObject("subjects")
                        val subjectKeys = subjectsJson.keys()
                        while (subjectKeys.hasNext()) {
                            val subjectId = subjectKeys.next()
                            val subjectName = subjectsJson.getString(subjectId)
                            subjectIdArrayList.add(subjectId)
                            subjectNameArrayList.add(subjectName)
                        }
                        resultListPojo.subjectIds = subjectIdArrayList
                        resultListPojo.subjectNames = subjectNameArrayList
                        resultListPojoArrayList.add(resultListPojo)
                    }
                }
                if (jsonRoot.has("quiz")) {
                    //Quiz
                    val jsonQuizResultList = jsonRoot.getJSONArray("quiz")
                    for (i in 0 until jsonQuizResultList.length()) {
                        val resultListPojo = ResultListPojo()
                        var subjectsJson: JSONObject
                        val subjectIdArrayList = ArrayList<String>()
                        val subjectNameArrayList = ArrayList<String>()
                        val tempJsonObject = jsonQuizResultList.getJSONObject(i)
                        resultListPojo.examName = tempJsonObject.getString("exam_name")
                        resultListPojo.id = tempJsonObject.getString("id")
                        resultListPojo.examType = tempJsonObject.getString("exam_type")
                        resultListPojo.date = tempJsonObject.getString("date")
                        resultListPojo.totalQues = tempJsonObject.getInt("total_questions")
                        resultListPojo.totalMarks = BigDecimal.valueOf(tempJsonObject.getDouble("total_marks")).toFloat()
                        resultListPojo.attemptedQues = tempJsonObject.getInt("attempted_questions")
                        resultListPojo.correctQues = tempJsonObject.getInt("correct_questions")
                        resultListPojo.totalObtainedMarks = BigDecimal.valueOf(tempJsonObject.getDouble("total_obtained_marks")).toFloat()
                        subjectsJson = tempJsonObject.getJSONObject("subjects")
                        val subjectKeys = subjectsJson.keys()
                        while (subjectKeys.hasNext()) {
                            val subjectId = subjectKeys.next()
                            val subjectName = subjectsJson.getString(subjectId)
                            subjectIdArrayList.add(subjectId)
                            subjectNameArrayList.add(subjectName)
                        }
                        resultListPojo.subjectIds = subjectIdArrayList
                        resultListPojo.subjectNames = subjectNameArrayList
                        resultListPojoArrayList.add(resultListPojo)
                    }
                }

                //Save json to cache
                statusDatabase = cacheResultList.setResultListToCache(resultListPojoArrayList)
                Log.d(TAG, "Inserted data with status: $statusDatabase")
            }
            bundleMessage.putString("MESSAGE", "Success")
            bundleMessage.putBoolean("SERVER_RESPONSE", responseStatus)
            bundleMessage.putBoolean("DATABASE_RESPONSE", statusDatabase)
            message.data = bundleMessage
            saveResultListFromServerToCacheHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            saveResultListFromServerToCacheHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            saveResultListFromServerToCacheHandler.sendMessage(message)
        } finally {
            try {
                request?.close()
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                saveResultListFromServerToCacheHandler.sendMessage(message)
            }
            connection?.disconnect()
        }
    }

    override fun onError(message: String?) {}
    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        Log.d(TAG, "onResponse: " + response.toString())
    }

    companion object {
        private const val TAG = "ExamResultThread"
    }

}
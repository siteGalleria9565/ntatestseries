package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_NOTIFICATIONS
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheNotifications
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.NotificationsPojo
import com.nta.ts2020.ThreadingHelper.Handlers.NotificationsHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class NotificationsThread(context: Context, notificationsHandler: NotificationsHandler) : Runnable {
    private val context: Context
    private val notificationsHandler: NotificationsHandler
    override fun run() {
        //Handler Variables
        val messageBundle = Bundle()
        val message = notificationsHandler.obtainMessage()

        //Data Variables
        val parameter = "user_id=" + CacheUserData(context).userDataFromCache.userId

        //Url Connection
        var connection: HttpURLConnection? = null
        var bufferedReader: BufferedReader? = null
        var os: OutputStreamWriter? = null
        val stringBuilder = StringBuilder()
        var line: String?
        val notificationsPojoArrayList = ArrayList<NotificationsPojo>()
        val cacheNotifications = CacheNotifications(context)
        cacheNotifications.clearRecords() //Clear all previous notifications
        try {
            val url = URL(URL_NOTIFICATIONS)
            connection = url.openConnection() as HttpURLConnection
            connection.doOutput = true
            connection.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            os = OutputStreamWriter(connection.outputStream)
            os.write(parameter)
            os.flush()

            //Get json from stream
            bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
            while (bufferedReader.readLine().also { line = it } != null) {
                stringBuilder.append(line).append("\n")
            }

            //Parse JSON
            val jsonRoot = JSONObject(stringBuilder.toString())
            Log.d(TAG, "Notification json from server: $jsonRoot")
            val serverStatus = jsonRoot.getBoolean("result")
            if (serverStatus) {
                val jsonNotifications = jsonRoot.getJSONArray("data")
                for (i in 0 until jsonNotifications.length()) {
                    val tempJsonNotification = jsonNotifications.getJSONObject(i)
                    notificationsPojoArrayList.add(NotificationsPojo(
                            tempJsonNotification.getInt("notify_id"),
                            tempJsonNotification.getString("notify_heading"),
                            tempJsonNotification.getString("notify_time")
                    ))
                }
            }

            //Save json to cache
            val databaseStatus = cacheNotifications.saveNotificationsToCache(notificationsPojoArrayList)
            messageBundle.putString("MESSAGE", "Success")
            messageBundle.putBoolean("SERVER_RESPONSE", serverStatus)
            messageBundle.putBoolean("DATABASE_RESPONSE", databaseStatus)
            message.data = messageBundle
            notificationsHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            messageBundle.putString("MESSAGE", e.message)
            message.data = messageBundle
            notificationsHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            messageBundle.putString("MESSAGE", e.message)
            message.data = messageBundle
            notificationsHandler.sendMessage(message)
        } finally {
            try {
                bufferedReader?.close()
                os?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                messageBundle.putString("MESSAGE", e.message)
                message.data = messageBundle
                notificationsHandler.sendMessage(message)
            }
            connection?.disconnect()
        }
    }

    companion object {
        private const val TAG = "NotificationsThread"
    }

    init {
        Log.d(TAG, "NotificationsThread() invoked")
        this.context = context
        this.notificationsHandler = notificationsHandler
    }
}
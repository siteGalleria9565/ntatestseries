package com.nta.ts2020.ThreadingHelper.Handlers

import android.app.ProgressDialog
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.nta.ts2020.Activities.ExamResultQuesAndAnsKeyActivity
import com.nta.ts2020.Activities.ExamShowQuestionInstructionActivity
import com.nta.ts2020.AppConstants.ConstantVariables.EXAM
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.QUES_ANS_KEY
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.PojoClasses.ExamQuestionDataBasedOnExamIdPojo
import com.nta.ts2020.PojoClasses.ParcelPojoClass.CacheQuestionDataParcel
import com.nta.ts2020.R
import java.lang.ref.WeakReference
import java.util.*

@Suppress("UNCHECKED_CAST")
class ExamQuestionDataBasedOnExamIdHandler(private val activity: AppCompatActivity, nextActivityButton: Button?) : Handler() {
    private lateinit var nextActivityButton: Button


    //ProgressDialog instant
    var progressDialog: ProgressDialog? = null
    private lateinit var localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo?>

    // Bundle ....
    lateinit var localQuestionsBasedOnExamIDFromServerInstance: Bundle
    var localShowQuestionDataBasedOnExamIdActivityInstance: Handler = Handler()

    // Strings ....
    private val TAG = "AllExamDataIDHnldr"
    override fun handleMessage(inputMessageFromChildThread: Message) {


        // Getting the Examination Bundle object from the Child Thread's Message ....
        localQuestionsBasedOnExamIDFromServerInstance = inputMessageFromChildThread.data
        val cacheQuestionDataParcelInstance: CacheQuestionDataParcel? = localQuestionsBasedOnExamIDFromServerInstance.getParcelable("CacheQuestionDataParcel")
        when (localQuestionsBasedOnExamIDFromServerInstance.getInt("TYPE")) {
            EXAM -> {
                val referenceShowQuestionDataBasedOnExamIdActivity = WeakReference(activity as ExamShowQuestionInstructionActivity)
                val localShowQuestionDataBasedOnExamIdInstance = referenceShowQuestionDataBasedOnExamIdActivity.get()
                progressDialog = localShowQuestionDataBasedOnExamIdInstance!!.progressDialog
                Log.d(TAG, "Thread for saving QuestionDataBasedOnExamId executed and returned message: " + localQuestionsBasedOnExamIDFromServerInstance.getString("save_all_Examination_Data_output"))
                if (localQuestionsBasedOnExamIDFromServerInstance.getString("save_all_Examination_Data_output") == "Error") {
                    Log.d(TAG, "QuestionDataBasedOnExamId data could not be saved into the Cloud.")
                    Toast.makeText(activity, "Please check your internet connectivity", Toast.LENGTH_SHORT).show()
                    nextActivityButton.setBackgroundResource(R.drawable.instruction_network_error)
                    nextActivityButton.isEnabled = false
                    progressDialog!!.dismiss()
                } else {
                    Log.d(TAG, "QuestionDataBasedOnExamId data Download successfully into the Cloud.")
                    localQuestionDataBasedOnExamIdList = cacheQuestionDataParcelInstance!!.localQuestionDataBasedOnExamIdList!!

                    // Setting the Examination Mapping List in the Activity ....
                    localShowQuestionDataBasedOnExamIdInstance.LoadData(localQuestionDataBasedOnExamIdList as ArrayList<ExamAllQuestionsSubjectWisePojo>?)

                    // Getting reference of Progress Dialog from the Activity ....
                    progressDialog!!.dismiss()
                }
            }
            QUES_ANS_KEY -> {
                val weakReference = WeakReference(activity as ExamResultQuesAndAnsKeyActivity)
                val resultQuesAndAnsKeyActivity = weakReference.get()
                if (localQuestionsBasedOnExamIDFromServerInstance.getString("save_all_Examination_Data_output") == "Error") {
                    Toast.makeText(activity, "Please check your internet connectivity", Toast.LENGTH_SHORT).show()
                    resultQuesAndAnsKeyActivity!!.loadData(null, null, NETWORK_ERROR)
                } else {
                    localQuestionDataBasedOnExamIdList = cacheQuestionDataParcelInstance!!.localQuestionDataBasedOnExamIdList!!
                    val examQuestionDataBasedOnExamIdPojo = Gson().fromJson(localQuestionsBasedOnExamIDFromServerInstance.getString("SUBJECT"), ExamQuestionDataBasedOnExamIdPojo::class.java)
                    resultQuesAndAnsKeyActivity!!.loadData(localQuestionDataBasedOnExamIdList as ArrayList<ExamAllQuestionsSubjectWisePojo>?, examQuestionDataBasedOnExamIdPojo, SUCCESS)
                }
            }
        }
    }

    init {
        if (nextActivityButton != null) {
            this.nextActivityButton = nextActivityButton
        }
    }
}
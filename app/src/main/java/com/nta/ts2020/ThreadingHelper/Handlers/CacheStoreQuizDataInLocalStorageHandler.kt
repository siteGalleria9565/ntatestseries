package com.nta.ts2020.ThreadingHelper.Handlers
//package com.nta.ts2020.threading.handlers;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//
//import java.lang.ref.WeakReference;
//
//import com.nta.ts2020.activity.QuizShowQuestionInstructionsActivity;
//import com.nta.ts2020.threading.threadPool.ThreadPoolService;
//
///**
// * Created by    on 17/2/17.
// */
//
//public class CacheStoreQuizDataInLocalStorageHandler extends Handler
//{
//    // Context ....
//    Context context;
//
//    // Bundle ....
//    Bundle localQuestionsBasedOnExamIDFromServerInstance;
//
//    Handler    localShowQuestionDataBasedOnExamIdActivityInstance;
//
//
//    // Threading ....
//    ThreadPoolService localThreadPoolServiceInstance;
//
//
//    private final WeakReference<QuizShowQuestionInstructionsActivity> referenceShowQuestionDataBasedOnExamIdActivity;
//    QuizShowQuestionInstructionsActivity localShowQuestionDataBasedOnExamIdInstance;
//
//    // Strings ....
//    private final String TAG = "StoreCacheDataIDHnldr";
//
//    public CacheStoreQuizDataInLocalStorageHandler(Context context , QuizShowQuestionInstructionsActivity localShowQuestionDataBasedOnExamIdActivityInstance)
//    {
//        this.context = context;
//        this.localShowQuestionDataBasedOnExamIdActivityInstance=new Handler();
//        referenceShowQuestionDataBasedOnExamIdActivity = new WeakReference<>(localShowQuestionDataBasedOnExamIdActivityInstance);
//        this.localShowQuestionDataBasedOnExamIdInstance = referenceShowQuestionDataBasedOnExamIdActivity.get();
//
//        localThreadPoolServiceInstance = new ThreadPoolService();
//
//    }
//
//    @Override
//    public void handleMessage(Message inputMessageFromChildThread)
//    {
//        // Getting the Examination Bundle object from the Child Thread's Message ....
//        localQuestionsBasedOnExamIDFromServerInstance = inputMessageFromChildThread.getData();
//        Log.d(TAG, "Thread for saving CacheStoreDataInLocalStorageHandler executed and returned message: " + localQuestionsBasedOnExamIDFromServerInstance.getString("output"));
//
//        if(localQuestionsBasedOnExamIDFromServerInstance.getString("output").equals("Error"))
//        {
//            Log.d(TAG,"CacheStoreDataInLocalStorageHandler data could not be saved into the Cloud.");
//
//        }
//        else
//        {
//            Log.d(TAG, "CacheStoreDataInLocalStorageHandler data Download successfully into the Cloud.");
//
//           // Setting the Examination Mapping List in the Activity ....
//            localShowQuestionDataBasedOnExamIdInstance.stopWatch();
//
//
//        }
//    }
//}

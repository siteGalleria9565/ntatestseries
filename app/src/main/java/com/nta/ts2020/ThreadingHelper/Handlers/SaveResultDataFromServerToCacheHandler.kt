package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.ExamResultGridViewActivity
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class SaveResultDataFromServerToCacheHandler(resultGridViewActivity: ExamResultGridViewActivity) : Handler() {
    private val resultGridViewActivityWeakReference: WeakReference<ExamResultGridViewActivity>
    override fun handleMessage(msg: Message) {
        val resultGridViewActivity = resultGridViewActivityWeakReference.get()
        val message = msg.data
        if ("Success" == message.getString("MESSAGE")) {
            if (message.getBoolean("DATABASE_RESPONSE")) {
                //Success , Load Data
                resultGridViewActivity!!.dismissProgressDialog(SUCCESS)
            } else {
                resultGridViewActivity!!.dismissProgressDialog(DATABASE_ERROR)
                Toast.makeText(resultGridViewActivity, "Something went wrong. Please try again", Toast.LENGTH_LONG).show()
            }
        } else {
            //Exception/Error (e.g: Network, JsonParsing)
            resultGridViewActivity!!.dismissProgressDialog(NETWORK_ERROR)
            //Toast.makeText(resultGridViewActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
        }
        super.handleMessage(msg)
    }

    init {
        resultGridViewActivityWeakReference = WeakReference(resultGridViewActivity)
    }
}
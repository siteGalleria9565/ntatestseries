package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.Activities.ExamPostRegistrationScreenActivity
import com.nta.ts2020.AppConstants.ConstantVariables.POST_REGISTRATION_ACTIVITY
import java.lang.ref.WeakReference

class PostRegistrationHandler(private val activity: AppCompatActivity, private val activityType: Int) : Handler() {
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        when (activityType) {
            POST_REGISTRATION_ACTIVITY -> {
                val postRegistrationActivityWeakReference = WeakReference(activity as ExamPostRegistrationScreenActivity)
                val postRegistrationActivity = postRegistrationActivityWeakReference.get()
                if ("Success" == bundleMessage.getString("STATUS")) {
                    postRegistrationActivity!!.startNextActivity()
                } else {
                    //Exceptions
                    if ("timeout" == bundleMessage.getString("MESSAGE")) {
                        Toast.makeText(postRegistrationActivity, "Please try again", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(postRegistrationActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

}
package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.EBooklistActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class EBookListHandler(context: EBooklistActivity) : Handler() {
    private val eBookActivityWeakReference: WeakReference<EBooklistActivity>
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        val eBookActivity = eBookActivityWeakReference.get()
        if ("Success" == bundleMessage.getString("STATUS")) {
            if (!bundleMessage.getBoolean("DATABASE_RESPONSE")) Toast.makeText(eBookActivity, "Something went wrong. Please try again", Toast.LENGTH_SHORT).show()
            eBookActivity!!.loadData(SUCCESS)
        } else {
            val message = bundleMessage.getString("MESSAGE")
            eBookActivity!!.loadData(NETWORK_ERROR)
            //Toast.makeText(eBookActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    init {
        eBookActivityWeakReference = WeakReference(context)
    }
}
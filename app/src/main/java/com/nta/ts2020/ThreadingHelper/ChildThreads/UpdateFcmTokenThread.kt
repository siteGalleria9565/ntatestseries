package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.net.Uri
import android.provider.Settings
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_UPDATE_FCM_TOKEN
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.UtilsHelper.SharedPrefManager
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class UpdateFcmTokenThread(private val context: Context, private val token: String) : Runnable {
    override fun run() {
        //Build Parameter
        val userDataPojo = CacheUserData(context).userDataFromCache
        val builder = Uri.Builder()
        val deviceId = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        builder.appendQueryParameter("user_id", userDataPojo.userId)
                .appendQueryParameter("course_id", userDataPojo.courseId)
                .appendQueryParameter("device_id", deviceId)
                .appendQueryParameter("token", token)
                .build()
        val parameter = builder.toString().substring(1)
        Log.d(TAG, "Token Parameters: $parameter")

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: OutputStreamWriter? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_UPDATE_FCM_TOKEN)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            os = OutputStreamWriter(conn.outputStream)
            os.write(parameter)
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            Log.d(TAG, "Server response: $sb")
            val jsonRoot = JSONObject(sb.toString())
            if (jsonRoot.getBoolean("status")) {
                SharedPrefManager(context).isFcmTokenSynced = true
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "UpdateFcmTokenThread"
    }

}
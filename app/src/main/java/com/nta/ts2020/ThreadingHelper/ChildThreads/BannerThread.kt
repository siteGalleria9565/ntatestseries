package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_BANNER
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheBanner
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.BannerPojo
import com.nta.ts2020.ThreadingHelper.Handlers.BannerHandler
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class BannerThread(private val context: ExamMainDashboardActivity, private val bannerHandler: BannerHandler) : Runnable {
    override fun run() {
//Variables for handler
        val bundleMessage = Bundle()
        val message = bannerHandler.obtainMessage()

        //Build Parameter
        val builder = Uri.Builder()
        builder.appendQueryParameter("user_id", CacheUserData(context).userDataFromCache.userId)
                .build()
        val parameter = builder.toString().substring(1)

//        Log.d(TAG,"Token Parameters: " + parameter);

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: OutputStreamWriter? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_BANNER)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            os = OutputStreamWriter(conn.outputStream)
            os.write(parameter)
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            Log.d(TAG, "Server response: $sb")
            val jsonRoot = JSONObject(sb.toString())
            if (jsonRoot.getBoolean("status")) {
                val jsonArray = JSONArray(jsonRoot.getString("banners"))
                val bannerPojoArrayList = ArrayList<BannerPojo>()
                for (i in 0 until jsonArray.length()) {
                    val tempJson = jsonArray.getJSONObject(i)
                    val bannerPojo = BannerPojo()
                    bannerPojo.id = tempJson.getString("id")
                    bannerPojo.title = tempJson.getString("title")
                    bannerPojo.description = tempJson.getString("content")
                    bannerPojo.logo = tempJson.getString("logo")
                    bannerPojo.background = tempJson.getString("bg")
                    bannerPojoArrayList.add(bannerPojo)
                }
                val cacheBanner = CacheBanner(context)
                cacheBanner.clearRecords()
                cacheBanner.saveBanner(bannerPojoArrayList)


                //Set message to handler
                bundleMessage.putString("STATUS", "Success")
                message.data = bundleMessage
                bannerHandler.sendMessage(message)
            } else {
                bundleMessage.putString("MESSAGE", "No data")
                message.data = bundleMessage
                bannerHandler.sendMessage(message)
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            bannerHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            bannerHandler.sendMessage(message)
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                bannerHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "BannerThread"
    }

}
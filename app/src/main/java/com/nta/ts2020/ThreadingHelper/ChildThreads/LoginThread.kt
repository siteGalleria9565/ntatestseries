package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.os.Bundle
import android.util.Log
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_LOGIN
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.ThreadingHelper.Handlers.LoginHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class LoginThread(private val context: ExamLoginScreenActivity, private val loginHandler: LoginHandler, private val parameter: String) : Runnable {
    override fun run() {
        //Variables for handler
        val bundleMessage = Bundle()
        val message = loginHandler.obtainMessage()

        //Connection variables
        var connection: HttpURLConnection? = null
        var request: OutputStreamWriter? = null
        var bReader: BufferedReader? = null
        var line: String?
        val sBuilder = StringBuilder()
        try {
            val url = URL(URL_LOGIN)
            connection = url.openConnection() as HttpURLConnection
            connection.doOutput = true
            connection!!.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

            //Write parameters to the stream
            request = OutputStreamWriter(connection.outputStream)
            request.write(parameter)

            //Clean up
            request.flush()

            //Read Response
            bReader = BufferedReader(InputStreamReader(connection.inputStream))
            while (bReader.readLine().also { line = it } != null) {
                // Append server response in string
                sBuilder.append(line).append("\n")
            }
            Log.d(TAG, "Response from server: $sBuilder")
            val cacheUserData = CacheUserData(context)
            cacheUserData.clearRecords() //Clear old records

            //Json response parsing
            val jsonRoot = JSONObject(sBuilder.toString())
            val jsonObject = jsonRoot.getJSONObject("data")
            val responseStatus = jsonRoot.getInt("status")
            val responseMessage = jsonRoot.getString("message")
            var databaseResponse = false
            if (responseStatus == 200) {
//                val nameCombined = jsonRoot.getString("name").trim { it <= ' ' }
//                val name = nameCombined.split("\\s+".toRegex()).toTypedArray()
                val userDataPojo = UserDataPojo()
                userDataPojo.userId = jsonObject.getInt("ID").toString()
                userDataPojo.fName = jsonObject.getString("FIRST_NAME")
                userDataPojo.lName = jsonObject.getString("LAST_NAME")
                userDataPojo.email = jsonObject.getString("EMAIL")
                userDataPojo.phNo = jsonObject.getString("MOBILE_NO")
                userDataPojo.profPic = jsonObject.getString("PROFILE_PIC")
                userDataPojo.courseId = jsonObject.getString("COURSE_ID")
                userDataPojo.courseName = ""
                userDataPojo.classId = jsonObject.getString("BATCH_ID")
                userDataPojo.className = ""
                userDataPojo.setUserLoginStatus(true)
                databaseResponse = cacheUserData.saveUserDataToCache(userDataPojo)
            }
            bundleMessage.putString("STATUS", "Success")
            bundleMessage.putInt("SERVER_STATUS", responseStatus)
            bundleMessage.putString("MESSAGE", responseMessage)
            bundleMessage.putBoolean("DATABASE_RESPONSE", databaseResponse)
            message.data = bundleMessage
            loginHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            loginHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            loginHandler.sendMessage(message)
        } finally {
            try {
                request?.close()
                bReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                loginHandler.sendMessage(message)
            }
            connection?.disconnect()
        }
    }

    companion object {
        private const val TAG = "LoginThread"
    }

}
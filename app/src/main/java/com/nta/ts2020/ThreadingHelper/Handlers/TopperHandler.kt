package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nta.ts2020.Activities.ExamResultCompareWithTopperActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.PojoClasses.TopperPojo
import java.lang.ref.WeakReference
import java.util.*

class TopperHandler(context: ExamResultCompareWithTopperActivity) : Handler() {
    private val weakReference: WeakReference<ExamResultCompareWithTopperActivity> = WeakReference(context)
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val resultCompareWithTopperActivity = weakReference.get()
        val bundleMessage = msg.data
        if ("Success" == bundleMessage.getString("STATUS")) {
            val topperPojoArrayList = Gson().fromJson<ArrayList<TopperPojo>>(bundleMessage.getString("JSON_DATA"), object : TypeToken<List<TopperPojo?>?>() {}.type)
            resultCompareWithTopperActivity!!.loadData(topperPojoArrayList, SUCCESS)
        } else {
            //Exceptions
            if ("timeout" == bundleMessage.getString("MESSAGE")) {
                resultCompareWithTopperActivity!!.loadData(ArrayList(), NETWORK_ERROR)
                Toast.makeText(resultCompareWithTopperActivity, "Please try again", Toast.LENGTH_SHORT).show()
            } else {
                resultCompareWithTopperActivity!!.loadData(ArrayList(), NETWORK_ERROR)
                Toast.makeText(resultCompareWithTopperActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show()
            }
        }
    }

}
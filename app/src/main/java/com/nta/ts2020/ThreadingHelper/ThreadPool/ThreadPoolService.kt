package com.nta.ts2020.ThreadingHelper.ThreadPool

import android.content.Context
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.Activities.*
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.PojoClasses.RegisterPojo
import com.nta.ts2020.ThreadingHelper.ChildThreads.*
import com.nta.ts2020.ThreadingHelper.Handlers.*
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ThreadPoolService {
    lateinit var threadExecutorService: ExecutorService
    lateinit var localExamLoadAllExaminationDataFromServerThreadInstance: ExamLoadAllExaminationDataFromServerThread
    lateinit var localExamLoadQuestionsBasedOnExamIDFromServerThreadInstance: ExamLoadQuestionsBasedOnExamIDFromServerThread
    lateinit var localSaveQuestionDataInCacheThreadInstance: SaveQuestionDataInCacheThread
    lateinit var localSessionStatusInCacheThreadInstance: SaveSessionStatusInCacheThread
    lateinit var saveQuizSessionStatusInCacheThreadInstance: SaveQuizSessionStatusInCacheThread
    lateinit var saveQuizQuestionDataInCacheThreadInstance: SaveQuizQuestionDataInCacheThread

    //Quiz Section
    var localQuizLoadQuestionsFromServerThreadInstance: QuizLoadQuestionsFromServerThread? = null
    private val TAG = "ThreadPoolService"
    fun LoadAllExaminationDataFromServer(context: Context?, threadHandler: Handler?) {
        Log.d(TAG, "LoadAllExaminationDataFromServer invoked.")
        threadExecutorService = Executors.newSingleThreadExecutor()

        // The "Handler" instance is further passed to the class implementing "Runnable".
        localExamLoadAllExaminationDataFromServerThreadInstance = ExamLoadAllExaminationDataFromServerThread(context, threadHandler!!)
        threadExecutorService.execute(localExamLoadAllExaminationDataFromServerThreadInstance)
        threadExecutorService.shutdown() // This closes the Thread Executor Service.
        Log.d(TAG, "Executor Service ends after starting Child Thread to Update Class LoadAllExaminationData.")
    }

    fun LoadQuestionsBasedOnExamIDFromServer(context: Context?, threadHandler: Handler?, ExamID: String?, type: Int) {
        Log.d(TAG, "LoadAllExaminationDataFromServer invoked.")
        threadExecutorService = Executors.newSingleThreadExecutor()

        // The "Handler" instance is further passed to the class implementing "Runnable".
        localExamLoadQuestionsBasedOnExamIDFromServerThreadInstance = ExamLoadQuestionsBasedOnExamIDFromServerThread(context, threadHandler!!, ExamID!!, type)
        threadExecutorService.execute(localExamLoadQuestionsBasedOnExamIDFromServerThreadInstance)
        threadExecutorService.shutdown() // This closes the Thread Executor Service.
        Log.d(TAG, "Executor Service ends after starting Child Thread to Update Class LoadAllExaminationData.")
    }

    fun saveQuestionDataInCache(context: Context?, threadHandler: Handler?, localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo>) {
        Log.d(TAG, "saveQuestionDataInCache invoked.")
        threadExecutorService = Executors.newSingleThreadExecutor()

        // The "Handler" instance is further passed to the class implementing "Runnable".
        localSaveQuestionDataInCacheThreadInstance = SaveQuestionDataInCacheThread(context!!, threadHandler!!, localQuestionDataBasedOnExamIdList)
        threadExecutorService.execute(localSaveQuestionDataInCacheThreadInstance)
        threadExecutorService.shutdown() // This closes the Thread Executor Service.
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun saveSessionStatusInCache(context: Context?, threadHandler: Handler?, localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo>) {
        Log.d(TAG, "saveQuestionDataInCache invoked.")
        threadExecutorService = Executors.newSingleThreadExecutor()

        // The "Handler" instance is further passed to the class implementing "Runnable".
        localSessionStatusInCacheThreadInstance = SaveSessionStatusInCacheThread(context!!, threadHandler!!, localQuestionDataBasedOnExamIdList)
        threadExecutorService.execute(localSessionStatusInCacheThreadInstance)
        threadExecutorService.shutdown() // This closes the Thread Executor Service.
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    //Quiz Section
    fun LoadQuestionsFromServer(context: Context?, threadHandler: Handler?, chapterIds: String?, numOfSubjects: String?, selectedTime: String?, selectedLanguage: String?) {
        Log.d(TAG, "LoadAllExaminationDataFromServer invoked.")
        threadExecutorService = Executors.newSingleThreadExecutor()

        // The "Handler" instance is further passed to the class implementing "Runnable".
        localQuizLoadQuestionsFromServerThreadInstance = QuizLoadQuestionsFromServerThread(context, threadHandler!!, chapterIds!!, numOfSubjects!!, selectedTime!!, selectedLanguage!!)
        threadExecutorService.execute(localQuizLoadQuestionsFromServerThreadInstance)
        threadExecutorService.shutdown() // This closes the Thread Executor Service.
        Log.d(TAG, "Executor Service ends after starting Child Thread to Update Class LoadAllExaminationData.")
    }

    fun saveQuizQuestionDataInCache(context: Context?, threadHandler: Handler?, localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo>) {
        Log.d(TAG, "saveQuestionDataInCache invoked.")
        threadExecutorService = Executors.newSingleThreadExecutor()

        // The "Handler" instance is further passed to the class implementing "Runnable".
        saveQuizQuestionDataInCacheThreadInstance = SaveQuizQuestionDataInCacheThread(context!!, threadHandler!!, localQuestionDataBasedOnExamIdList)
        threadExecutorService.execute(saveQuizQuestionDataInCacheThreadInstance)
        threadExecutorService.shutdown() // This closes the Thread Executor Service.
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun saveQuizSessionStatusInCache(context: Context?, localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo>) {
        Log.d(TAG, "saveQuestionDataInCache invoked.")
        threadExecutorService = Executors.newSingleThreadExecutor()

        // The "Handler" instance is further passed to the class implementing "Runnable".
        saveQuizSessionStatusInCacheThreadInstance = SaveQuizSessionStatusInCacheThread(context!!, localQuestionDataBasedOnExamIdList)
        threadExecutorService.execute(saveQuizSessionStatusInCacheThreadInstance)
        threadExecutorService.shutdown() // This closes the Thread Executor Service.
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun sendFinalSubmissionDataToServer(context: Context?) {
        //TODO:Make Handler for sendFinalSubmissionDataToServer
        Log.d(TAG, "saveQuestionDataInCache invoked.")
        threadExecutorService = Executors.newSingleThreadExecutor()

        // The "Handler" instance is further passed to the class implementing "Runnable".
        val saveFinalSubmissionDataToServerThreadInstance = SaveFinalSubmissionDataToServerThread(context!!)
        threadExecutorService.execute(saveFinalSubmissionDataToServerThreadInstance)
        threadExecutorService.shutdown() // This closes the Thread Executor Service.
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun saveResultListFromServerToCache(context: Context?, saveResultListFromServerToCacheHandler: SaveResultListFromServerToCacheHandler?) {
        Log.d(TAG, "saveResultListFromServerToCache invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val saveResultListFromServerToCacheThread = SaveResultListFromServerToCacheThread(context!!, saveResultListFromServerToCacheHandler!!)
        threadExecutorService.execute(saveResultListFromServerToCacheThread)
        threadExecutorService.shutdown()
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun saveResultDataFromServerToCache(context: Context?, id: String?, isHorizontal: Boolean?, saveResultDataFromServerToCacheHandler: SaveResultDataFromServerToCacheHandler?) {
        //TODO:Make Handler for saveResultDataFromServerToCache
        Log.d(TAG, "saveResultDataFromServerToCache invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val saveResultDataFromServerToCacheThread = SaveResultDataFromServerToCacheThread(context!!, id!!, isHorizontal!!, saveResultDataFromServerToCacheHandler)
        threadExecutorService.execute(saveResultDataFromServerToCacheThread)
        threadExecutorService.shutdown()
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun quizSaveSubChapFromServerToCache(context: Context?, quizSaveSubChapFromServerToCacheHandler: QuizSaveSubChapFromServerToCacheHandler?) {
        Log.d(TAG, "quizSaveSubChapFromServerToCache invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val quizSaveSubChapFromServerToCacheThread = QuizSaveSubChapFromServerToCacheThread(context!!, quizSaveSubChapFromServerToCacheHandler!!)
        threadExecutorService.execute(quizSaveSubChapFromServerToCacheThread)
        threadExecutorService.shutdown()
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun saveCoursesFromServerToCache(appCompatActivity: AppCompatActivity?, saveCoursesFromServerToCacheHandler: SaveCoursesFromServerToCacheHandler?) {
        Log.d(TAG, "saveCoursesFromServerToCacheThread invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val saveCoursesFromServerToCacheThread = SaveCoursesFromServerToCacheThread(appCompatActivity!!, saveCoursesFromServerToCacheHandler!!)
        threadExecutorService.execute(saveCoursesFromServerToCacheThread)
        threadExecutorService.shutdown()
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun register(context: ExamRegisterActivity?, registrationHandler: RegistrationHandler?, registerPojo: RegisterPojo?) {
        Log.d(TAG, "register invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val registrationThread = RegistrationThread(context!!, registrationHandler!!, registerPojo!!)
        threadExecutorService.execute(registrationThread)
        threadExecutorService.shutdown()
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun login(context: ExamLoginScreenActivity?, loginHandler: LoginHandler?, parameter: String?) {
        Log.d(TAG, "login() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val loginThread = LoginThread(context!!, loginHandler!!, parameter!!)
        threadExecutorService.execute(loginThread)
        threadExecutorService.shutdown()
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun saveNotificationsFromServerToCache(dashboardActivity: ExamMainDashboardActivity?, notificationsHandler: NotificationsHandler?) {
        Log.d(TAG, "saveNotificationsFromServerToCache() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val notificationsThread = NotificationsThread(dashboardActivity!!, notificationsHandler!!)
        threadExecutorService.execute(notificationsThread)
        threadExecutorService.shutdown()
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Question Data into the Cache.")
    }

    fun saveProfileToServer(context: Context?, parameterJson: String?, type: Int, updateProfileHandler: UpdateProfileHandler?) {
        Log.d(TAG, "saveProfileToServer() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val updateProfileThread = UpdateProfileThread(context!!, parameterJson!!, type, updateProfileHandler!!)
        threadExecutorService.execute(updateProfileThread)
        threadExecutorService.shutdown()
        Log.d(TAG, "Executor Service ends after starting Child Thread to Save Profile Data into the Cache.")
    }

    fun loadProfileFromServer(context: Context?, parameterJson: String?, loadProfileHandler: LoadProfileHandler?) {
        Log.d(TAG, "loadProfileFromServer() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val loadProfileThread = LoadProfileThread(context!!, parameterJson!!, loadProfileHandler!!)
        threadExecutorService.execute(loadProfileThread)
        threadExecutorService.shutdown()
    }

    fun syncContacts(context: Context?) {
        Log.d(TAG, "loadProfileFromServer() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val contactsSyncThread = ContactsSyncThread(context!!)
        threadExecutorService.execute(contactsSyncThread)
        threadExecutorService.shutdown()
    }

    fun updateFcmToken(context: Context?, token: String?) {
        Log.d(TAG, "updateFcmToken() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val contactsSyncThread = UpdateFcmTokenThread(context!!, token!!)
        threadExecutorService.execute(contactsSyncThread)
        threadExecutorService.shutdown()
    }

    fun loadNotificationDetailsFromServer(context: Context?, id: String?, notificationDetailsHandler: NotificationDetailsHandler?) {
        Log.d(TAG, "loadNotificationDetailsFromServer() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val notificationDetailsThread = NotificationDetailsThread(context!!, id!!, notificationDetailsHandler!!)
        threadExecutorService.execute(notificationDetailsThread)
        threadExecutorService.shutdown()
    }

    fun uploadProfImage(context: Context?, imagePath: String?, uploadProfImageHandler: UploadProfImageHandler?) {
        Log.d(TAG, "uploadProfImage() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val contactsSyncThread = UploadProfImageThread(context!!, imagePath!!, uploadProfImageHandler!!)
        threadExecutorService.execute(contactsSyncThread)
        threadExecutorService.shutdown()
    }

    fun socialLogin(context: ExamLoginScreenActivity?, jsonParameter: String?, socialLoginHandler: SocialLoginHandler?) {
        Log.d(TAG, "socialLogin() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val contactsSyncThread = SocialLoginThread(context!!, jsonParameter!!, socialLoginHandler!!)
        threadExecutorService.execute(contactsSyncThread)
        threadExecutorService.shutdown()
    }

    fun PostRegistrationRequest(context: ExamPostRegistrationScreenActivity?, jsonParameter: String?, postRegistrationHandler: PostRegistrationHandler?) {
        Log.d(TAG, "PostRegistrationRequest() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val contactsSyncThread = PostRegistrationThread(context!!, jsonParameter!!, postRegistrationHandler!!)
        threadExecutorService.execute(contactsSyncThread)
        threadExecutorService.shutdown()
    }

    fun saveEBookSubjectsListFromServerToCache(context: EBookSubjectlistActivity?, eBookSubjectsListHandler: EBookSubjectsListHandler?) {
        Log.d(TAG, "saveEBookSubjectsListFromServerToCache() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val eBookSubjectsListThread = EBookSubjectsListThread(context!!, eBookSubjectsListHandler!!)
        threadExecutorService.execute(eBookSubjectsListThread)
        threadExecutorService.shutdown()
    }

    fun saveEBookListFromServerToCache(context: EBooklistActivity?, subjectId: String?, eBookListHandler: EBookListHandler?, type: String?) {
        Log.d(TAG, "saveEBookListFromServerToCache() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val eBookSubjectsListThread = EBookListThread(context!!, subjectId!!, eBookListHandler!!, type!!)
        threadExecutorService.execute(eBookSubjectsListThread)
        threadExecutorService.shutdown()
    }

    fun getTopperDataFromServer(context: ExamResultCompareWithTopperActivity?, examId: String?, topperHandler: TopperHandler?) {
        Log.d(TAG, "getTopperDataFromServer() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val topperThread = TopperThread(context!!, examId!!, topperHandler!!)
        threadExecutorService.execute(topperThread)
        threadExecutorService.shutdown()
    }

    fun getSupportList(context: Context?, supportListHandler: SupportListHandler?) {
        Log.d(TAG, "getSupportList() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val supportListThread = SupportListThread(context!!, supportListHandler!!)
        threadExecutorService.execute(supportListThread)
        threadExecutorService.shutdown()
    }

    fun insertNewQuert(parameter: String?, supportInsertHandler: SupportInsertHandler?) {
        Log.d(TAG, "insertNewQuert() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val supportInsertThread = SupportInsertThread(parameter!!, supportInsertHandler!!)
        threadExecutorService.execute(supportInsertThread)
        threadExecutorService.shutdown()
    }

    fun loadBanner(context: ExamMainDashboardActivity?, bannerHandler: BannerHandler?) {
        Log.d(TAG, "loadBanner() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val bannerThread = BannerThread(context!!, bannerHandler!!)
        threadExecutorService.execute(bannerThread)
        threadExecutorService.shutdown()
    }

    fun loadHorizontalMenu(context: ExamMainDashboardActivity?, horizontalMenuHandler: HorizontalMenuHandler?) {
        Log.d(TAG, "loadBanner() invoked")
        threadExecutorService = Executors.newSingleThreadExecutor()
        val horizontalMenuThread = HorizontalMenuThread(context!!, horizontalMenuHandler!!)
        threadExecutorService.execute(horizontalMenuThread)
        threadExecutorService.shutdown()
    }
}
package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import com.nta.ts2020.LocalStorage.ExamCacheGetCurrentExamSession
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import java.util.*

class SaveSessionStatusInCacheThread(var context: Context, private val handlerQuestionDataBasedOnExamId: Handler, // Collections ....
                                     var localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo>) : Runnable {

    private val TAG = "SavSessionStatusCachThd"
    private var updatingDataInCacheStatus: String? = null
    private val examCacheGetCurrentExamSessionInstance: ExamCacheGetCurrentExamSession

    // Message ....
    var localQuestionsBasedOnExamIDFromServerInstance: Message? = null

    // Bundle ....
    var localQuestionsBasedOnExamIDFromServerBundleDataInstance: Bundle? = null
    override fun run() {


        //initialzation
        initionalization()
        updatingDataInCacheStatus = examCacheGetCurrentExamSessionInstance.setCurrentExamSession(localQuestionDataBasedOnExamIdList)
        localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putString("output", "Success")
        localQuestionsBasedOnExamIDFromServerInstance!!.data = localQuestionsBasedOnExamIDFromServerBundleDataInstance
        handlerQuestionDataBasedOnExamId.sendMessage(localQuestionsBasedOnExamIDFromServerInstance)
        Log.d(TAG, "Saved   QuestionData downloaded from Cloud to Cache. $updatingDataInCacheStatus")
    }

    fun initionalization() {


        // Bundle ....
        localQuestionsBasedOnExamIDFromServerBundleDataInstance = Bundle()


        // Message....
        localQuestionsBasedOnExamIDFromServerInstance = handlerQuestionDataBasedOnExamId.obtainMessage()
    }

    init {

        // Handlers ....
        examCacheGetCurrentExamSessionInstance = ExamCacheGetCurrentExamSession(context)
    }
}
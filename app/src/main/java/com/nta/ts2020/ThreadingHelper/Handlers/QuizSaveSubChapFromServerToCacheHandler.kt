package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import com.nta.ts2020.Activities.ExamQuizSelectionActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class QuizSaveSubChapFromServerToCacheHandler(quizSelectionActivity: ExamQuizSelectionActivity) : Handler() {
    var quizSelectionActivityWeakReference: WeakReference<ExamQuizSelectionActivity>
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        val quizSelectionActivity = quizSelectionActivityWeakReference.get()
        //ProgressDialog progressDialog = quizSelectionActivity.getProgressDialog();
//        progressDialog.dismiss();
        if ("Success" == bundleMessage.getString("MESSAGE")) {
            quizSelectionActivity!!.loadData(SUCCESS)
        } else {
            val message = bundleMessage.getString("MESSAGE") //Can be used if needed to show specific error messages
            quizSelectionActivity!!.loadData(NETWORK_ERROR)
            //Toast.makeText(quizSelectionActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    init {
        quizSelectionActivityWeakReference = WeakReference(quizSelectionActivity)
    }
}
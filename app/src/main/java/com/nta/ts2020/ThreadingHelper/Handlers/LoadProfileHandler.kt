package com.nta.ts2020.ThreadingHelper.Handlers

import android.app.Activity
import android.app.ProgressDialog
import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.*
import com.nta.ts2020.AppConstants.ConstantVariables.ACADEMIC_DETAILS_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.ADDRESS_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.PERSONAL_DETAILS_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.SECURITY_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.SELECT_ADDRESS_ACTIVITY
import java.lang.ref.WeakReference

class LoadProfileHandler(private val activity: Activity, private val type: Int, private val progressDialog: ProgressDialog) : Handler() {
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        if ("Success" == bundleMessage.getString("STATUS")) {
            if (type == PERSONAL_DETAILS_ACTIVITY) {
                val weakReference = WeakReference(activity as ExamProfPersonalDetailsActivity)
                val personalDetailsActivity = weakReference.get()
                personalDetailsActivity!!.loadData(bundleMessage.getString("JSON_DATA"))
            }
            if (type == ACADEMIC_DETAILS_ACTIVITY) {
                val weakReference = WeakReference(activity as ExamProfAcademicDetailsActivity)
                val academicDetailsActivity = weakReference.get()
                academicDetailsActivity!!.loadData(bundleMessage.getString("JSON_DATA"))
            }
            if (type == SECURITY_ACTIVITY) {
                val weakReference = WeakReference(activity as ExamProfSecurityActivity)
                val securityActivity = weakReference.get()
                securityActivity!!.loadData(bundleMessage.getString("JSON_DATA"))
            }
            if (type == ADDRESS_ACTIVITY) {
                val weakReference = WeakReference(activity as ExamProfAddressActivity)
                val addressActivity = weakReference.get()
                addressActivity!!.loadData(bundleMessage.getString("JSON_DATA"))
            }
            if (type == SELECT_ADDRESS_ACTIVITY) {
                val weakReference = WeakReference(activity as SelectAddressActivity)
                val addressActivity = weakReference.get()
                addressActivity!!.loadData(bundleMessage.getString("JSON_DATA"))
            }
        } else {
            //Exceptions
            when {
                "NO_RESPONSE" == bundleMessage.getString("MESSAGE") -> {
                    Toast.makeText(activity, "No Data found", Toast.LENGTH_SHORT).show()
                }
                "timeout" == bundleMessage.getString("MESSAGE") -> {
                    Toast.makeText(activity, "Please try again", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(activity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show()
                }
            }
        }
        if (progressDialog.isShowing) progressDialog.dismiss()
    }

}
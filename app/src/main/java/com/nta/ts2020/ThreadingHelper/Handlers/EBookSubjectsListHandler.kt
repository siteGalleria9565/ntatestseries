package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.EBookSubjectlistActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class EBookSubjectsListHandler(context: EBookSubjectlistActivity) : Handler() {
    private val eBookSubjectsActivityWeakReference: WeakReference<EBookSubjectlistActivity>
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        val eBookSubjectsActivity = eBookSubjectsActivityWeakReference.get()
        if ("Success" == bundleMessage.getString("STATUS")) {
            if (!bundleMessage.getBoolean("DATABASE_RESPONSE")) Toast.makeText(eBookSubjectsActivity, "Something went wrong. Please try again", Toast.LENGTH_SHORT).show()
            eBookSubjectsActivity!!.loadData(SUCCESS)
        } else {
            val message = bundleMessage.getString("MESSAGE")
            eBookSubjectsActivity!!.loadData(NETWORK_ERROR)
            //Toast.makeText(eBookSubjectsActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    init {
        eBookSubjectsActivityWeakReference = WeakReference(context)
    }
}
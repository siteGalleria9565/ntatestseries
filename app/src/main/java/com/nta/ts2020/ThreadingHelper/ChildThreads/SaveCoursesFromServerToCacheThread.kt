package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_GET_COURSE_CLASS_LIST
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.PojoClasses.ClassPojo
import com.nta.ts2020.PojoClasses.CoursePojo
import com.nta.ts2020.ThreadingHelper.Handlers.SaveCoursesFromServerToCacheHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class SaveCoursesFromServerToCacheThread(appCompatActivity: AppCompatActivity, saveCoursesFromServerToCacheHandler: SaveCoursesFromServerToCacheHandler) : Runnable {
    private val context: Context
    private val saveCoursesFromServerToCacheHandler: SaveCoursesFromServerToCacheHandler
    override fun run() {
        var connection: HttpURLConnection? = null
        var bufferedReader: BufferedReader? = null
        val bundleMessage = Bundle()
        val message = saveCoursesFromServerToCacheHandler.obtainMessage()
        try {
            //Download json from url
            val url = URL(URL_GET_COURSE_CLASS_LIST)
            connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "POST"
            connection!!.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.connect()
            if (connection.responseCode == HttpURLConnection.HTTP_OK) {
                bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
                var line: String?
                val stringBuilder = StringBuilder()
                while (bufferedReader.readLine().also { line = it } != null) {
                    stringBuilder.append(line).append("\n")
                }
                Log.d(TAG, "Json from server: $stringBuilder")
                val jsonRoot = JSONObject(stringBuilder.toString())
                val coursePojoArrayList = ArrayList<CoursePojo>()
                if (jsonRoot.getBoolean("status")) {
                    val jsonCourses = jsonRoot.getJSONArray("courses")
                    for (i in 0 until jsonCourses.length()) {
                        val jsonCourse = jsonCourses.getJSONObject(i)
                        val coursePojo = CoursePojo()
                        coursePojo.id = jsonCourse.getString("id")
                        coursePojo.name = jsonCourse.getString("course")
                        val jsonClasses = jsonCourse.getJSONArray("class")
                        val classPojoArrayList = ArrayList<ClassPojo>()
                        for (j in 0 until jsonClasses.length()) {
                            val jsonClass = jsonClasses.getJSONObject(j)
                            val classPojo = ClassPojo()
                            classPojo.id = jsonClass.getString("id")
                            classPojo.name = jsonClass.getString("class")
                            classPojoArrayList.add(classPojo)
                        }
                        coursePojo.classes = classPojoArrayList
                        coursePojoArrayList.add(coursePojo)
                    }
                }
                val gson = Gson()
                val jsonData = gson.toJson(coursePojoArrayList)
                bundleMessage.putString("MESSAGE", "Success")
                bundleMessage.putString("JSON_DATA", jsonData)
                message.data = bundleMessage
                saveCoursesFromServerToCacheHandler.sendMessage(message)
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            saveCoursesFromServerToCacheHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            saveCoursesFromServerToCacheHandler.sendMessage(message)
        } finally {
            //clean up
            try {
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
            }
            connection?.disconnect()
        }
    }

    companion object {
        private const val TAG = "CoursesFrmSvrToCache"
    }

    init {
        context = appCompatActivity
        this.saveCoursesFromServerToCacheHandler = saveCoursesFromServerToCacheHandler
    }
}
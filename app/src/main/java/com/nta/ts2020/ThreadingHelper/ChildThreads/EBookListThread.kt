package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.os.Bundle
import android.util.Log
import com.nta.ts2020.Activities.EBooklistActivity
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_E_BOOK_LIST
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheEBook
import com.nta.ts2020.PojoClasses.EBookPojo
import com.nta.ts2020.ThreadingHelper.Handlers.EBookListHandler
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class EBookListThread(private val context: EBooklistActivity, private val subjectId: String, private val eBookListHandler: EBookListHandler, private val eBookType: String) : Runnable {
    override fun run() {
        var connection: HttpURLConnection? = null
        var bufferedReader: BufferedReader? = null
        var request: OutputStreamWriter? = null
        val bundleMessage = Bundle()
        val message = eBookListHandler.obtainMessage()
        val eBookPojoArrayList = ArrayList<EBookPojo>()
        var parameter = "type=$eBookType"
        parameter += "&subject_id=$subjectId"
        Log.d(TAG, parameter)
        try {
            //Download json from url
            val url = URL(URL_E_BOOK_LIST)
            connection = url.openConnection() as HttpURLConnection
            connection.doOutput = true
            connection!!.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

            //Write parameters to the stream
            request = OutputStreamWriter(connection.outputStream)
            request.write(parameter)

            //Clean up
            request.flush()

            //Read Response
            bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
            var line: String?
            val stringBuilder = StringBuilder()
            while (bufferedReader.readLine().also { line = it } != null) {
                stringBuilder.append(line).append("\n")
            }
            val jsonRoot = JSONObject(stringBuilder.toString())
            Log.d(TAG, "Json from server: $jsonRoot")
            if (jsonRoot.getBoolean("status")) {
                val jsonEBooks = JSONArray(jsonRoot.getString("PdfData"))
                for (i in 0 until jsonEBooks.length()) {
                    val jsonEBooK = jsonEBooks.getJSONObject(i)
                    val eBookPojo = EBookPojo()
                    eBookPojo.eBookId = jsonEBooK.getString("pdf_id")
                    val tempName = jsonEBooK.getString("Pdf_File_Name")
                    var pdfName: String?
                    pdfName = if (tempName.contains(".")) tempName.substring(0, tempName.lastIndexOf(".")) else tempName
                    eBookPojo.eBookPdfName = pdfName
                    eBookPojo.thumbnail = jsonEBooK.getString("thumnail")
                    eBookPojo.type = jsonEBooK.getString("type")
                    eBookPojo.eBookSubjectId = jsonEBooK.getString("Subject_Id")
                    eBookPojo.eBookName = jsonEBooK.getString("bookname")
                    eBookPojo.eBookAuthor = jsonEBooK.getString("author")
                    eBookPojo.eBookPublication = jsonEBooK.getString("publication")
                    eBookPojo.eBookLanguage = jsonEBooK.getString("language")
                    eBookPojo.downloadUrl = jsonEBooK.getString("download_url")
                    eBookPojoArrayList.add(eBookPojo)
                }
                val cacheEBook = CacheEBook(context)
                cacheEBook.clearRecords(subjectId, eBookType)
                val databaseStatus = cacheEBook.saveEBookList(eBookPojoArrayList)
                bundleMessage.putString("STATUS", "Success")
                bundleMessage.putBoolean("DATABASE_RESPONSE", databaseStatus)
            } else {
                bundleMessage.putString("STATUS", "Success")
                bundleMessage.putBoolean("DATABASE_RESPONSE", true)
            }
            message.data = bundleMessage
            eBookListHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            eBookListHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            eBookListHandler.sendMessage(message)
        } finally {
            try {
                request?.close()
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                eBookListHandler.sendMessage(message)
            }
            connection?.disconnect()
        }
    }

    companion object {
        private const val TAG = "EBookListThread"
    }

}
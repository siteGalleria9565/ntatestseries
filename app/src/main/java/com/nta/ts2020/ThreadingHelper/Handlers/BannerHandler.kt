package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class BannerHandler(private val context: ExamMainDashboardActivity) : Handler() {
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        val weakReference = WeakReference(context)
        val dashboardActivity = weakReference.get()
        dashboardActivity!!.loadBanner(SUCCESS)
    }

}
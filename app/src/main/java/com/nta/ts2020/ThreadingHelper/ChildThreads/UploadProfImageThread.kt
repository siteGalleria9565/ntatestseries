package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.util.Base64
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_UPLOAD_PROF_IMAGE
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.ThreadingHelper.Handlers.UploadProfImageHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class UploadProfImageThread(private val context: Context, private val imagePath: String, private val uploadProfImageHandler: UploadProfImageHandler) : Runnable {
    override fun run() {
        //Convert image to base64
        val bytes: ByteArray
        val buffer = ByteArray(8192)
        var bytesRead: Int
        val output = ByteArrayOutputStream()
        val imageFile = File(imagePath)
        val imageName = imageFile.name
        try {
            val inputStream: InputStream = FileInputStream(imageFile) //You can get an inputStream using any IO API
            while (inputStream.read(buffer).also { bytesRead = it } != -1) {
                output.write(buffer, 0, bytesRead)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        bytes = output.toByteArray()
        val encodedImage = Base64.encodeToString(bytes, Base64.DEFAULT)


        //Handler variables
        val bundleMessage = Bundle()
        val message = Message()

        //Build Parameter
        val builder = Uri.Builder()
        builder.appendQueryParameter("user_id", CacheUserData(context).userDataFromCache.userId)
                .appendQueryParameter("image_name", imageName)
                .appendQueryParameter("encoded_string", encodedImage)
                .build()
        val parameter = builder.toString().substring(1)
        Log.d(TAG, "Token Parameters: $parameter")

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: OutputStreamWriter? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_UPLOAD_PROF_IMAGE)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            os = OutputStreamWriter(conn.outputStream)
            os.write(parameter)
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            Log.d(TAG, "Server response: $sb")
            val jsonRoot = JSONObject(sb.toString())
            if (jsonRoot.getBoolean("status")) {
                val userDataPojo = UserDataPojo()
                userDataPojo.profPic = jsonRoot.getString("image")
                CacheUserData(context).updateUserDetails(userDataPojo)
                bundleMessage.putString("STATUS", "Success")
                bundleMessage.putString("MESSAGE", jsonRoot.getString("msg"))
                message.data = bundleMessage
                uploadProfImageHandler.sendMessage(message)
            } else {
                bundleMessage.putString("MESSAGE", jsonRoot.getString("msg"))
                message.data = bundleMessage
                uploadProfImageHandler.sendMessage(message)
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            uploadProfImageHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            uploadProfImageHandler.sendMessage(message)
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                uploadProfImageHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "UploadProfImageThread"
    }

}
package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.os.Bundle
import android.util.Log
import com.nta.ts2020.Activities.ExamPostRegistrationScreenActivity
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_SOCIAL_SAVE_COURSES
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.ThreadingHelper.Handlers.PostRegistrationHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedOutputStream
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class PostRegistrationThread(private val context: ExamPostRegistrationScreenActivity, private val jsonParameter: String, private val postRegistrationHandler: PostRegistrationHandler) : Runnable {
    override fun run() {
//Variables for handler
        val bundleMessage = Bundle()
        val message = postRegistrationHandler.obtainMessage()
        Log.d(TAG, jsonParameter)

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: BufferedOutputStream? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_SOCIAL_SAVE_COURSES)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setFixedLengthStreamingMode(jsonParameter.toByteArray().size)

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8")
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest")

            //open
            conn.connect()

            //setup send
            os = BufferedOutputStream(conn.outputStream)
            os.write(jsonParameter.toByteArray())
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            Log.d(TAG, "Server response: $sb")
            val jsonRoot = JSONObject(sb.toString())
            if (jsonRoot.getBoolean("status")) {
                val userDataPojo = UserDataPojo()
                userDataPojo.userId = jsonRoot.getString("user_id")
                userDataPojo.email = jsonRoot.getString("email")
                userDataPojo.fName = jsonRoot.getString("name")
                userDataPojo.lName = ""
                userDataPojo.phNo = jsonRoot.getString("mobile_number")
                userDataPojo.profPic = jsonRoot.getString("profile_pic")
                userDataPojo.courseId = jsonRoot.getString("course_id")
                userDataPojo.courseName = jsonRoot.getString("course_name")
                userDataPojo.classId = jsonRoot.getString("class_id")
                userDataPojo.className = jsonRoot.getString("class_name")
                userDataPojo.setUserLoginStatus(true)
                val databaseResponse = CacheUserData(context).saveUserDataToCache(userDataPojo)

                //Set message to handler
                bundleMessage.putString("STATUS", "Success")
                bundleMessage.putBoolean("DATABASE_STATUS", databaseResponse)
                message.data = bundleMessage
                postRegistrationHandler.sendMessage(message)
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            //Set message to handler
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            postRegistrationHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            postRegistrationHandler.sendMessage(message)
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                //Set message to handler
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                postRegistrationHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "PostRegistrationThread"
    }

}
package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class UploadProfImageHandler(private val activity: AppCompatActivity) : Handler() {
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        val weakReference = WeakReference(activity as ExamMainDashboardActivity)
        val dashboardActivity = weakReference.get()
        if ("Success" == bundleMessage.getString("STATUS")) {
            dashboardActivity!!.loadProfImage(SUCCESS)
            Toast.makeText(dashboardActivity, bundleMessage.getString("MESSAGE"), Toast.LENGTH_SHORT).show()
        } else {
            dashboardActivity!!.loadProfImage(NETWORK_ERROR)
            Toast.makeText(dashboardActivity, bundleMessage.getString("MESSAGE"), Toast.LENGTH_SHORT).show()
        }
    }

}
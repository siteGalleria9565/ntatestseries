package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.net.Uri
import android.os.Bundle
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_SUPPORT_LIST
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheSupport
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.SupportPojo
import com.nta.ts2020.ThreadingHelper.Handlers.SupportListHandler
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class SupportListThread(private val context: Context, private val supportListHandler: SupportListHandler) : Runnable {
    override fun run() {
        //Variables for handler
        val bundleMessage = Bundle()
        val message = supportListHandler.obtainMessage()

        //Build Parameter
        val builder = Uri.Builder()
        builder.appendQueryParameter("user_id", CacheUserData(context).userDataFromCache.userId)
                .build()
        val parameter = builder.toString().substring(1)

//        Log.d(TAG,"Token Parameters: " + parameter);

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: OutputStreamWriter? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_SUPPORT_LIST)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            os = OutputStreamWriter(conn.outputStream)
            os.write(parameter)
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }

//            Log.d(TAG, "Server response: " + sb.toString());
            val jsonRoot = JSONObject(sb.toString())
            if (jsonRoot.getBoolean("status")) {
                val jsonArray = JSONArray(jsonRoot.getString("queries"))
                val supportPojoArrayList = ArrayList<SupportPojo>()
                for (i in 0 until jsonArray.length()) {
                    val tempJson = JSONObject(jsonArray.getString(i))
                    val supportPojo = SupportPojo()
                    supportPojo.queryId = tempJson.getString("id")
                    supportPojo.queryToken = tempJson.getString("token")
                    supportPojo.email = tempJson.getString("support_email")
                    supportPojo.title = tempJson.getString("title")
                    supportPojo.query = tempJson.getString("query")
                    supportPojo.answer = tempJson.getString("reply")
                    supportPojo.date = tempJson.getString("date")
                    supportPojoArrayList.add(supportPojo)
                }
                val cacheSupport = CacheSupport(context)
                cacheSupport.clearRecords()
                cacheSupport.saveQueries(supportPojoArrayList)
            }

            //Set message to handler
            bundleMessage.putString("STATUS", "Success")
            message.data = bundleMessage
            supportListHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            supportListHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            supportListHandler.sendMessage(message)
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                supportListHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "SupportListThread"
    }

}
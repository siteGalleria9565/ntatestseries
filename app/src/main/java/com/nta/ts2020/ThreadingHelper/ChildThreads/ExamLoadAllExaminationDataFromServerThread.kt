package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import com.google.gson.Gson
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.LocalStorage.ExamAssetToLocalDatabase
import com.nta.ts2020.LocalStorage.ExamCacheAllExaminationData
import com.nta.ts2020.PojoClasses.ExamAllExaminationDataPojo
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class ExamLoadAllExaminationDataFromServerThread(context: Context?, localAllExaminationDataInstance: Handler) : Runnable {
    // UI Stuff ....
    var context: Context? = null

    // Handlers ....
    var handlerAllExaminationData: Handler? = null

    // Message ....
    var localCloudAllExaminationDataHandlerInstance: Message? = null

    // Bundle ....
    var localCloudAllExaminationDataHandlerBundleDataInstance: Bundle? = null

    //Transection
    var localExamCacheAllExaminationDataInstance: ExamCacheAllExaminationData

    // POJOs ....
    var examAllExaminationDataPojoInstance: ExamAllExaminationDataPojo? = null

    // Collections ....
    var localAllExaminationDataList: ArrayList<ExamAllExaminationDataPojo>? = null

    // GSONS ....
    var localGsonObject: Gson

    // Network Request Stuff ....
    var requestURL: URL? = null
    var requestURLConnection: HttpURLConnection? = null

    // Stream Readers ....
    var serverResponseStreamReaderInstance: InputStreamReader? = null
    var serverResponseBufferedReaderInstance: BufferedReader? = null
    var serverResponseCode = 0

    // Strings ....
    var jsonResponseDataString: String? = null
    var serverURL: String? = null
    var updatingDataInCacheStatus: String? = null
    private val TAG = "AllExmDataFrmSrvrThread"
    override fun run() {
        var request: OutputStreamWriter? = null
        val bufferedReader: BufferedReader? = null
        try {
            // Establishing URL Connection with the Server ....
            serverURL = ConstantVariables.URL_OF_All_EXAMINATION_DATA_FROM_SERVER
            requestURL = URL(serverURL)
            var parameters = "user_id=" + CacheUserData(context!!).userDataFromCache.userId
            parameters += "&type=EXM"
            requestURLConnection = requestURL!!.openConnection() as HttpURLConnection
            requestURLConnection!!.doOutput = true
            requestURLConnection!!.readTimeout = READ_TIMEOUT_VALUE
            requestURLConnection!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            //            requestURLConnection.setRequestProperty("Content-Type", "application/json");
            requestURLConnection!!.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            Log.d(TAG, "parameters :$parameters")
            Log.d(TAG, "#requestURLConnection Code: $requestURLConnection")

            //Write parameters to stream
            request = OutputStreamWriter(requestURLConnection!!.outputStream)
            request.write(parameters)

            //Clean up
            request.flush()

//            bufferedReader = new BufferedReader(new InputStreamReader(requestURLConnection.getInputStream()));

            // Getting Response from the Server ....
            serverResponseCode = requestURLConnection!!.responseCode
            Log.d(TAG, "#Server's Response Code: $serverResponseCode")
            if (serverResponseCode == HttpURLConnection.HTTP_OK) {
                serverResponseStreamReaderInstance = InputStreamReader(requestURLConnection!!.inputStream)
                serverResponseBufferedReaderInstance = BufferedReader(serverResponseStreamReaderInstance)


                // Reading the Response Data ....
                if (serverResponseBufferedReaderInstance != null) jsonResponseDataString = serverResponseBufferedReaderInstance!!.readLine()
                Log.d(TAG, "#Server Response: $jsonResponseDataString")

                // Closing the Stream ....
                serverResponseBufferedReaderInstance!!.close()


                // Retrieving Profile Pojo from JSON ....
                val data = JSONObject(jsonResponseDataString)
                if (data.getBoolean("result")) {
                    // Getting JSON Array node
                    val contacts = data.getJSONArray("data")
                    Log.d(TAG, "#JsonData: $contacts")

                    // looping through All Contacts
                    for (j in 0 until contacts.length()) {
                        val c = contacts.getJSONObject(j)
                        examAllExaminationDataPojoInstance = ExamAllExaminationDataPojo()
                        examAllExaminationDataPojoInstance!!.examId = Integer.valueOf(c.getString("exam_id"))
                        examAllExaminationDataPojoInstance!!.subject = Integer.valueOf(c.getString("subjects"))
                        examAllExaminationDataPojoInstance!!.examName = c.getString("exam_name")
                        localAllExaminationDataList!!.add(examAllExaminationDataPojoInstance!!)
                    }
                    ExamAssetToLocalDatabase(context).deleteList()
                    //Insert  ExaminationData in cache
                    updatingDataInCacheStatus = localAllExaminationDataList?.let { localExamCacheAllExaminationDataInstance.setAllExaminationData(it) }
                    Log.d(TAG, "#updatingDataInCacheStatus: $updatingDataInCacheStatus")
                } else {
                    ExamAssetToLocalDatabase(context).deleteList()
                }
                localCloudAllExaminationDataHandlerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Success")

                // Storing the Bundle object in the Message Object ....
                localCloudAllExaminationDataHandlerInstance!!.data = localCloudAllExaminationDataHandlerBundleDataInstance

                // The "handleMessage()" function from the "CloudClassStudentsMappingHandler" class  automatically gets invoked after this function call ....
                handlerAllExaminationData!!.sendMessage(localCloudAllExaminationDataHandlerInstance)
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            localCloudAllExaminationDataHandlerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Error")

            // Storing the Bundle object in the Message Object ....
            localCloudAllExaminationDataHandlerInstance!!.data = localCloudAllExaminationDataHandlerBundleDataInstance

            // The "handleMessage()" function from the "localCloudAllExaminationDataHandlerInstance" class  automatically gets invoked after this function call ....
            handlerAllExaminationData!!.sendMessage(localCloudAllExaminationDataHandlerInstance)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            localCloudAllExaminationDataHandlerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Error")
            localCloudAllExaminationDataHandlerInstance!!.data = localCloudAllExaminationDataHandlerBundleDataInstance
            handlerAllExaminationData!!.sendMessage(localCloudAllExaminationDataHandlerInstance)
        } finally {
            try {
                request?.close()
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
            }
            if (requestURLConnection != null) {
                requestURLConnection!!.disconnect()
            }
        }
    }

    fun initializeLocalVariables(context: Context?, handlerAllExaminationData: Handler) {
        // UI Stuff ....
        this.context = context

        //ArrayList
        localAllExaminationDataList = ArrayList()

        // Handlers ....
        this.handlerAllExaminationData = handlerAllExaminationData

        // Bundle ....
        localCloudAllExaminationDataHandlerBundleDataInstance = Bundle()

        // Message....
        localCloudAllExaminationDataHandlerInstance = handlerAllExaminationData.obtainMessage()
    }

    init {
        initializeLocalVariables(context, localAllExaminationDataInstance)
        localExamCacheAllExaminationDataInstance = context?.let { ExamCacheAllExaminationData(it) }!!
        localGsonObject = Gson()
    }
}
package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import com.nta.ts2020.Activities.ExamResultCompareWithTopperActivity
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_TOPPER
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.TopperPojo
import com.nta.ts2020.ThreadingHelper.Handlers.TopperHandler
import org.json.JSONArray
import org.json.JSONException
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class TopperThread(private val context: ExamResultCompareWithTopperActivity, private val examId: String, private val topperHandler: TopperHandler) : Runnable {
    override fun run() {
        //Variables for handler
        val bundleMessage = Bundle()
        val message = topperHandler.obtainMessage()

        //Build Parameter
        val builder = Uri.Builder()
        builder.appendQueryParameter("user_id", CacheUserData(context).userDataFromCache.userId)
                .appendQueryParameter("exam_id", examId)
                .build()
        val parameter = builder.toString().substring(1)

//        Log.d(TAG,"Token Parameters: " + parameter);

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: OutputStreamWriter? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_TOPPER)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            os = OutputStreamWriter(conn.outputStream)
            os.write(parameter)
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            Log.d(TAG, "Server response: $sb")
            val jsonArray = JSONArray(sb.toString())
            val topperPojoArrayList = ArrayList<TopperPojo>()
            for (i in 0 until jsonArray.length()) {
                val tempJson = jsonArray.getJSONObject(i)
                val topperPojo = TopperPojo()
                topperPojo.name = tempJson.getString("name")
                topperPojo.marks = tempJson.getString("marks")
                topperPojo.isMe = tempJson.getBoolean("me")
                topperPojoArrayList.add(topperPojo)
            }

            //Set message to handler
            bundleMessage.putString("STATUS", "Success")
            val gson = Gson()
            bundleMessage.putString("JSON_DATA", gson.toJson(topperPojoArrayList))
            message.data = bundleMessage
            topperHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            topperHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            topperHandler.sendMessage(message)
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                topperHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "TopperThread"
    }

}
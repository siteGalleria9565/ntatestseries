package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.os.Message
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_GET_RESULT_DATA
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheResultList
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.LocalStorage.ExamCacheFinalSubmission
import com.nta.ts2020.PojoClasses.FinalSubmissionPojo
import com.nta.ts2020.PojoClasses.ResultListPojo
import com.nta.ts2020.ThreadingHelper.Handlers.SaveResultDataFromServerToCacheHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.math.BigDecimal
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class SaveResultDataFromServerToCacheThread(private val context: Context, private val id: String, private val isHorizontal: Boolean, private val saveResultDataFromServerToCacheHandler: SaveResultDataFromServerToCacheHandler?) : Runnable {
    override fun run() {
        //Handler declaration and initialization
        val bundleMessage = Bundle()
        var message: Message? = null
        if (saveResultDataFromServerToCacheHandler != null) {
            message = saveResultDataFromServerToCacheHandler.obtainMessage()
        }
        var connection: HttpURLConnection? = null
        var bufferedReader: BufferedReader? = null
        var request: OutputStreamWriter? = null
        val finalSubmissionPojoArrayList = ArrayList<FinalSubmissionPojo>()
        val userId = CacheUserData(context).userDataFromCache.userId
        val parameter = "user_id=$userId&exam_id=$id&is_horizontal=$isHorizontal"
        try {
            Log.d(TAG, "Parameter: $parameter")
            //Download json from url
            val url = URL(URL_GET_RESULT_DATA)
            connection = url.openConnection() as HttpURLConnection
            connection.doOutput = true
            connection!!.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

            //Write parameters to the stream
            request = OutputStreamWriter(connection.outputStream)
            request.write(parameter)

            //Clean up
            request.flush()

            //Read Response
            //TODO: Do this with JsonReader (Ref: http://stackoverflow.com/questions/6173396/parse-data-of-size-about-3mb-using-json-in-android)
            bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
            var line: String?
            val stringBuilder = StringBuilder()
            while (bufferedReader.readLine().also { line = it } != null) {
                stringBuilder.append(line).append("\n")
            }
            val jsonRoot = JSONObject(stringBuilder.toString())
            Log.d(TAG, "Json from server: $jsonRoot")
            val responseStatus = jsonRoot.getBoolean("status")
            if (responseStatus) {
                val jsonResultData = jsonRoot.getJSONObject("exams")
                val keys = jsonResultData.keys()
                while (keys.hasNext()) {
                    val finalSubmissionPojo = FinalSubmissionPojo()
                    val key = keys.next()
                    val jsonSingleResultData = jsonResultData.getJSONObject(key)
                    finalSubmissionPojo.userId = jsonSingleResultData.getInt("USER_ID")
                    finalSubmissionPojo.examId = jsonSingleResultData.getString("EXAM_ID")
                    finalSubmissionPojo.examType = jsonSingleResultData.getString("EXAM_TYPE")
                    finalSubmissionPojo.date = jsonSingleResultData.getString("DATE")
                    finalSubmissionPojo.allottedTime = jsonSingleResultData.getInt("ALLOTED_TIME")
                    finalSubmissionPojo.totalQues = jsonSingleResultData.getInt("TOTAL_QUESTIONS")
                    finalSubmissionPojo.totalMarks = jsonSingleResultData.getInt("TOTAL_MARKS")
                    finalSubmissionPojo.subjectIds = ArrayList(Arrays.asList(*jsonSingleResultData.getString("SUBJECT_ID").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.nosOfQuesSubWise = ArrayList(Arrays.asList(*jsonSingleResultData.getString("SUB_WISE_QUESTIONS").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.subWiseMaxMarks = ArrayList(Arrays.asList(*jsonSingleResultData.getString("SUB_WISE_MAX_MARKS").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.quesIds = ArrayList(Arrays.asList(*jsonSingleResultData.getString("QN_ID").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.responses = ArrayList(Arrays.asList(*jsonSingleResultData.getString("RESPONSE").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.subWiseMarks = ArrayList(Arrays.asList(*jsonSingleResultData.getString("SUB_WISE_MARKS").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.subWiseQuesData = ArrayList(Arrays.asList(*jsonSingleResultData.getString("SUB_WISE_QUESTION_ANALYSIS").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.totalObtainedMarks = jsonSingleResultData.getInt("TOTAL_OBTAINED_MARKS").toFloat()
                    finalSubmissionPojo.totalQuesData = jsonSingleResultData.getString("TOTAL_QUESTION_ANALYSIS")
                    finalSubmissionPojo.subWiseTime = ArrayList(Arrays.asList(*jsonSingleResultData.getString("SUB_WISE_TIME").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.quesWiseTime = ArrayList(Arrays.asList(*jsonSingleResultData.getString("QN_WISE_TIME").split(",".toRegex()).toTypedArray()))
                    finalSubmissionPojo.overAllTime = jsonSingleResultData.getInt("OVERALL_TIME").toLong()
                    finalSubmissionPojo.setSyncStatus(true)
                    finalSubmissionPojoArrayList.add(finalSubmissionPojo)
                }
                val examCacheFinalSubmission = ExamCacheFinalSubmission(context)
                val statusDatabase = examCacheFinalSubmission.setFinalSubmissionData(finalSubmissionPojoArrayList)
                if (isHorizontal) {
                    //Parsing json for result if available
                    val cacheResultList = CacheResultList(context)
                    val resultListPojoArrayList = ArrayList<ResultListPojo>()
                    val jsonExamResultList = jsonRoot.getJSONObject("result")
                    val resultListPojo = ResultListPojo()
                    val subjectsJson: JSONObject
                    val subjectIdArrayList = ArrayList<String>()
                    val subjectNameArrayList = ArrayList<String>()
                    resultListPojo.examName = jsonExamResultList.getString("exam_name")
                    resultListPojo.id = jsonExamResultList.getString("id")
                    resultListPojo.examType = jsonExamResultList.getString("exam_type")
                    resultListPojo.date = jsonExamResultList.getString("date")
                    resultListPojo.totalQues = jsonExamResultList.getInt("total_questions")
                    resultListPojo.totalMarks = BigDecimal.valueOf(jsonExamResultList.getDouble("total_marks")).toFloat()
                    resultListPojo.attemptedQues = jsonExamResultList.getInt("attempted_questions")
                    resultListPojo.correctQues = jsonExamResultList.getInt("correct_questions")
                    resultListPojo.totalObtainedMarks = BigDecimal.valueOf(jsonExamResultList.getDouble("total_obtained_marks")).toFloat()
                    subjectsJson = jsonExamResultList.getJSONObject("subjects")
                    val subjectKeys = subjectsJson.keys()
                    while (subjectKeys.hasNext()) {
                        val subjectId = subjectKeys.next()
                        val subjectName = subjectsJson.getString(subjectId)
                        subjectIdArrayList.add(subjectId)
                        subjectNameArrayList.add(subjectName)
                    }
                    resultListPojo.subjectIds = subjectIdArrayList
                    resultListPojo.subjectNames = subjectNameArrayList
                    resultListPojoArrayList.add(resultListPojo)
                    cacheResultList.clearAllRecords()
                    cacheResultList.setResultListToCache(resultListPojoArrayList)
                }
                if (saveResultDataFromServerToCacheHandler != null && message != null) {
                    bundleMessage.putString("MESSAGE", "Success")
                    bundleMessage.putBoolean("DATABASE_RESPONSE", statusDatabase)
                    message.data = bundleMessage
                    saveResultDataFromServerToCacheHandler.sendMessage(message)
                }
            } else {
                bundleMessage.putString("MESSAGE", "Success")
                bundleMessage.putBoolean("DATABASE_RESPONSE", false)
                message!!.data = bundleMessage
                saveResultDataFromServerToCacheHandler!!.sendMessage(message)
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            if (saveResultDataFromServerToCacheHandler != null && message != null) {
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                saveResultDataFromServerToCacheHandler.sendMessage(message)
            }
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            if (saveResultDataFromServerToCacheHandler != null && message != null) {
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                saveResultDataFromServerToCacheHandler.sendMessage(message)
            }
        } finally {
            try {
                request?.close()
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
            }
            connection?.disconnect()
        }
    }

    companion object {
        private const val TAG = "ExamResultThread"
    }

}
package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.LATEST
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.RESULT
import com.nta.ts2020.AppConstants.ConstantVariables.UPCOMING
import com.nta.ts2020.AppConstants.ConstantVariables.URL_HORIZONTAL_MENU
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheHorizontalMenu
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.HorizontalMenuPojo
import com.nta.ts2020.ThreadingHelper.Handlers.HorizontalMenuHandler
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class HorizontalMenuThread(private val context: ExamMainDashboardActivity, private val horizontalMenuHandler: HorizontalMenuHandler) : Runnable {
    override fun run() {
//Variables for handler
        val bundleMessage = Bundle()
        val message = horizontalMenuHandler.obtainMessage()

        //Build Parameter
        val builder = Uri.Builder()
        builder.appendQueryParameter("user_id", CacheUserData(context).userDataFromCache.userId)
                .build()
        val parameter = builder.toString().substring(1)

//        Log.d(TAG,"Token Parameters: " + parameter);

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: OutputStreamWriter? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_HORIZONTAL_MENU)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            os = OutputStreamWriter(conn.outputStream)
            os.write(parameter)
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            Log.d(TAG, "Server response: $sb")
            val jsonRoot = JSONObject(sb.toString())
            val horizontalMenuPojoArrayList = ArrayList<HorizontalMenuPojo>()
            val jsonUpcoming = JSONObject(jsonRoot.getString("upcomming"))
            if (jsonUpcoming.getBoolean("result")) {
                val jsonArray = JSONArray(jsonUpcoming.getString("data"))
                for (i in 0 until jsonArray.length()) {
                    val tempJson = jsonArray.getJSONObject(i)
                    val horizontalMenuPojo = HorizontalMenuPojo()
                    horizontalMenuPojo.id = tempJson.getString("exam_id")
                    horizontalMenuPojo.title = tempJson.getString("exam_name")
                    horizontalMenuPojo.description = """
                        Start Date
                        ${tempJson.getString("description")}
                        """.trimIndent()
                    horizontalMenuPojo.background = tempJson.getString("background")
                    horizontalMenuPojo.tag = UPCOMING
                    horizontalMenuPojoArrayList.add(horizontalMenuPojo)
                }
            }
            val jsonLatest = JSONObject(jsonRoot.getString("latest"))
            if (jsonLatest.getBoolean("result")) {
                val jsonArray = JSONArray(jsonLatest.getString("data"))
                for (i in 0 until jsonArray.length()) {
                    val tempJson = jsonArray.getJSONObject(i)
                    val horizontalMenuPojo = HorizontalMenuPojo()
                    horizontalMenuPojo.id = tempJson.getString("exam_id")
                    horizontalMenuPojo.title = tempJson.getString("exam_name")
                    horizontalMenuPojo.description = """
                        End Date
                        ${tempJson.getString("description")}
                        """.trimIndent()
                    horizontalMenuPojo.background = tempJson.getString("background")
                    horizontalMenuPojo.tag = LATEST
                    horizontalMenuPojoArrayList.add(horizontalMenuPojo)
                }
            }
            val jsonResult = JSONObject(jsonRoot.getString("result"))
            if (jsonResult.getBoolean("result")) {
                val jsonArray = JSONArray(jsonResult.getString("data"))
                for (i in 0 until jsonArray.length()) {
                    val tempJson = jsonArray.getJSONObject(i)
                    val horizontalMenuPojo = HorizontalMenuPojo()
                    horizontalMenuPojo.id = tempJson.getString("exam_id")
                    horizontalMenuPojo.title = tempJson.getString("exam_name")
                    horizontalMenuPojo.description = """
                        Attempted On
                        ${tempJson.getString("description")}
                        """.trimIndent()
                    horizontalMenuPojo.background = tempJson.getString("background")
                    horizontalMenuPojo.tag = RESULT
                    horizontalMenuPojoArrayList.add(horizontalMenuPojo)
                }
            }
            val horizontalMenu = CacheHorizontalMenu(context)
            horizontalMenu.clearRecords()
            horizontalMenu.saveHorizontalMenu(horizontalMenuPojoArrayList)


            //Set message to handler
            bundleMessage.putString("STATUS", "Success")
            message.data = bundleMessage
            horizontalMenuHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            horizontalMenuHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            horizontalMenuHandler.sendMessage(message)
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                horizontalMenuHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "HorizontalMenuThread"
    }

}
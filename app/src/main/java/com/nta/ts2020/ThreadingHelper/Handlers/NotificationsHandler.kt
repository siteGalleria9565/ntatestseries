package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class NotificationsHandler(dashboardActivity: ExamMainDashboardActivity) : Handler() {
    private val startUpPageWeakReference: WeakReference<ExamMainDashboardActivity>
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val dashboardActivity = startUpPageWeakReference.get()
        val bundleMessage = msg.data

        //TODO:Check login and set layout for each status
        if ("Success" == bundleMessage.getString("MESSAGE")) {
            if (bundleMessage.getBoolean("SERVER_RESPONSE")) {
                if (bundleMessage.getBoolean("DATABASE_RESPONSE")) dashboardActivity!!.loadNotifications(SUCCESS)
            } else {
                //No ExamResult List Data
                dashboardActivity!!.loadNotifications(NO_DATA)
            }
        } else {
            val message = bundleMessage.getString("MESSAGE")
            dashboardActivity!!.loadNotifications(NETWORK_ERROR)
            //            Toast.makeText(dashboardActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    init {
        startUpPageWeakReference = WeakReference(dashboardActivity)
    }
}
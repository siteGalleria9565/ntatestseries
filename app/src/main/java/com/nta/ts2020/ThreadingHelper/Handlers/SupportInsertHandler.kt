package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.ExamSupportDetailsActivity
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import java.lang.ref.WeakReference

class SupportInsertHandler(private val context: ExamSupportDetailsActivity) : Handler() {
    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        val bundleMessage = msg.data
        val weakReference = WeakReference(context)
        val supportActivity = weakReference.get()
        if ("Success" == bundleMessage.getString("STATUS")) {
            supportActivity!!.postQueryInsert(SUCCESS)
        } else if ("Failed" == bundleMessage.getString("STATUS")) {
            Toast.makeText(context, "Oops!! Something went wrong.", Toast.LENGTH_SHORT).show()
            supportActivity!!.postQueryInsert(NETWORK_ERROR)
        } else {
            //Exceptions
            if ("timeout" == bundleMessage.getString("MESSAGE")) {
                Toast.makeText(context, "Please try again", Toast.LENGTH_SHORT).show()
                supportActivity!!.postQueryInsert(NETWORK_ERROR)
            } else {
                Toast.makeText(context, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show()
                supportActivity!!.postQueryInsert(NETWORK_ERROR)
            }
        }
    }

}
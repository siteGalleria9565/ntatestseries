package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import android.widget.Toast
import com.nta.ts2020.Activities.ExamRegisterActivity
import java.lang.ref.WeakReference

class RegistrationHandler(registerActivity: ExamRegisterActivity) : Handler() {
    private val registerActivityWeakReference: WeakReference<ExamRegisterActivity> = WeakReference(registerActivity)
    override fun handleMessage(msg: Message) {
        val registerActivity = registerActivityWeakReference.get()
        val bundleMessage = msg.data
        if ("Success" == bundleMessage.getString("STATUS")) {
            val serverStatus = bundleMessage.getInt("SERVER_STATUS")
            val serverMessage = bundleMessage.getString("MESSAGE")
            val databaseResponse = bundleMessage.getBoolean("DATABASE_RESPONSE")
            if (200 == serverStatus) {
                if (databaseResponse) {
                    //Successfully registered and data saved to local database
                   // Toast.makeText(registerActivity, serverMessage, Toast.LENGTH_SHORT).show()
                    registerActivity!!.startNextActivity()
                } else {
                    //Successfully registered but user data has not been saved to local database
                    Toast.makeText(registerActivity, "Successfully Registered. Please Sign In", Toast.LENGTH_SHORT).show()
                    registerActivity!!.startLoginIntent()
                }
            } else {
                //User already exist please login
                Toast.makeText(registerActivity, serverMessage, Toast.LENGTH_SHORT).show()
            }
        } else {
            //Exceptions
            if ("timeout" == bundleMessage.getString("MESSAGE")) {
                Toast.makeText(registerActivity, "Please try again", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(registerActivity, bundleMessage.getString("MESSAGE"), Toast.LENGTH_LONG).show()
            }
        }
        val progressDialog = registerActivity!!.regProgressDialog
        if (progressDialog!!.isShowing) {
            progressDialog.dismiss()
        }
        super.handleMessage(msg)
    }

}
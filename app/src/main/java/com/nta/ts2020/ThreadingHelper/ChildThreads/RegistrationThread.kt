package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.nta.ts2020.Activities.ExamRegisterActivity
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_REGISTRATION
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.RegisterPojo
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.ThreadingHelper.Handlers.RegistrationHandler
import com.nta.ts2020.UtilsHelper.MyPref
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class RegistrationThread(private val context: ExamRegisterActivity, private val registrationHandler: RegistrationHandler, private val registerPojo: RegisterPojo) : Runnable {
    private lateinit var myPref: MyPref
    override fun run() {
        //Variables for handler
        myPref = MyPref(context)
        val bundleMessage = Bundle()
        val message = registrationHandler.obtainMessage()
        //Prepare parameters
        val builderParameters = Uri.Builder()
        builderParameters.appendQueryParameter("FIRST_NAME", registerPojo.getfName())
                .appendQueryParameter("LAST_NAME", registerPojo.getlName())
                .appendQueryParameter("PASSWORD", registerPojo.password)
                .appendQueryParameter("EMAIL", registerPojo.email)
                .appendQueryParameter("mobileNo", registerPojo.phNumber)
                .appendQueryParameter("selected_stream", registerPojo.course)
                .appendQueryParameter("selected_class", registerPojo.classe)
                .appendQueryParameter("city", registerPojo.city)
                .appendQueryParameter("state", registerPojo.state)
                .appendQueryParameter("current_address", registerPojo.currentAddress)
                .appendQueryParameter("dob", registerPojo.dOB)
                .appendQueryParameter("ref_code", registerPojo.referralcode)
                .build()
        val parameters = builderParameters.toString().substring(1)
        Log.d(TAG, parameters)

        //Declare connection variables
        var connection: HttpURLConnection? = null
        var request: OutputStreamWriter? = null
        var bufferedReader: BufferedReader? = null
        val sb = StringBuilder()
        var line: String?
        try {
            //Setup connection
            val url = URL(URL_REGISTRATION)
            connection = url.openConnection() as HttpURLConnection
            connection.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.doOutput = true
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

            //Write parameters to stream
            request = OutputStreamWriter(connection.outputStream)
            request.write(parameters)

            //Clean up
            request.flush()
            bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))

            // Read Server Response
            while (bufferedReader.readLine().also { line = it } != null) {
                // Append server response in string
                sb.append(line).append("\n")
            }
            Log.d(TAG, "Response from server: $sb")
            val jsonRoot = JSONObject(sb.toString())
            val responseStatus = jsonRoot.getInt("status")
            val responseMessage = jsonRoot.getString("message")
            val userId: String
            var databaseResponse = false
            val cacheUserData = CacheUserData(context)
            cacheUserData.clearRecords() //Clear previous user data
            if (responseStatus == 200) {
                val jsonObject = jsonRoot.getJSONObject("data")
                //userId = jsonRoot.getString("user_id")
                val userDataPojo = UserDataPojo()
                myPref.setPref(ConstantVariables.OTP, jsonObject.getInt("REGISTRATION_OTP"))
                userDataPojo.userId = jsonObject.getString("ID")
                userDataPojo.email = jsonObject.getString("EMAIL")
                userDataPojo.fName = jsonObject.getString("FIRST_NAME")
                userDataPojo.lName = jsonObject.getString("LAST_NAME")
                userDataPojo.phNo = registerPojo.phNumber
                userDataPojo.profPic = "0"
                userDataPojo.courseId = ""
                userDataPojo.courseName = ""
                userDataPojo.classId = ""
                userDataPojo.className = ""
                userDataPojo.setUserLoginStatus(false)
                databaseResponse = cacheUserData.saveUserDataToCache(userDataPojo)
            }

            //Set message to handler
            bundleMessage.putString("STATUS", "Success")
            bundleMessage.putInt("SERVER_STATUS", responseStatus)
            bundleMessage.putString("MESSAGE", responseMessage)
            bundleMessage.putBoolean("DATABASE_RESPONSE", databaseResponse)
            message.data = bundleMessage
            registrationHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            //Set message to handler
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            registrationHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            registrationHandler.sendMessage(message)
        } finally {
            try {
                request?.close()
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                registrationHandler.sendMessage(message)
            }
            connection?.disconnect()
        }
    }

    companion object {
        private const val TAG = "RegistrationThread"
    }

}
package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.PERSONAL_DETAILS_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_UPDATE_PROFILE
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.ThreadingHelper.Handlers.UpdateProfileHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class UpdateProfileThread(private val context: Context, private val parameterJson: String, private val type: Int, private val updateProfileHandler: UpdateProfileHandler) : Runnable {
    override fun run() {
        //Variables for handler
        val bundleMessage = Bundle()
        val message = updateProfileHandler.obtainMessage()
        val parameter = "profile=$parameterJson"

        //Declare connection variables
        var conn: HttpURLConnection? = null
        var bufferedReader: BufferedReader? = null
        var os: OutputStreamWriter? = null
        val sb = StringBuilder()
        var line: String?
        try {
            val url = URL(URL_UPDATE_PROFILE)
            conn = url.openConnection() as HttpURLConnection
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.requestMethod = "POST"
            conn.doInput = true
            conn.doOutput = true
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

            //open
            conn.connect()

            //setup send
            os = OutputStreamWriter(conn.outputStream)
            os.write(parameter)
            //clean up
            os.flush()

            //do something with response if needed
            bufferedReader = BufferedReader(InputStreamReader(conn.inputStream))

            // Read Server Response
            while (bufferedReader.readLine().also { line = it } != null) {
                // Append server response in string
                sb.append(line).append("\n")
            }
            val response = sb.toString()
            //            Log.d(TAG, "Response from server: " + response);

            //update status if success
//            if (response.equals("success"))
//                examCacheFinalSubmission.updateSyncStatus(finalSubmissionData, true);
            if (type == PERSONAL_DETAILS_ACTIVITY) {
                val userDataPojo = UserDataPojo()
                try {
                    val json = JSONObject(parameterJson)
                    userDataPojo.fName = json.getString("FIRST_NAME")
                    userDataPojo.lName = json.getString("LAST_NAME")
                    userDataPojo.phNo = json.getString("MOBILE_NO")
                    CacheUserData(context).updateUserDetails(userDataPojo)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            //Set message to handler
            bundleMessage.putString("STATUS", "Success")
            message.data = bundleMessage
            updateProfileHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            //            Log.w(TAG, e.getMessage());
            //Set message to handler
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            updateProfileHandler.sendMessage(message)
        } finally {
            //clean up
            try {
                os?.close()
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                //                Log.w(TAG, e.getMessage());
                //Set message to handler
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                updateProfileHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "UpdateProfileThread"
    }

}
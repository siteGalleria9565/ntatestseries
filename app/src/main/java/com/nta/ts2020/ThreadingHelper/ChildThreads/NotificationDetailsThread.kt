package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_NOTIFICATIONS_DETAILS
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.PojoClasses.NotificationRelatedPostsPojo
import com.nta.ts2020.PojoClasses.ParcelPojoClass.NotificationDetailsPojo
import com.nta.ts2020.ThreadingHelper.Handlers.NotificationDetailsHandler
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class NotificationDetailsThread(private val context: Context, private val id: String, private val notificationDetailsHandler: NotificationDetailsHandler) : Runnable {
    override fun run() {
        //Handler Variables
        val messageBundle = Bundle()
        val message = notificationDetailsHandler.obtainMessage()

        //Data Variables
        val parameter = "id=$id"

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: OutputStreamWriter? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_NOTIFICATIONS_DETAILS)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            os = OutputStreamWriter(conn.outputStream)
            os.write(parameter)
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            Log.d(TAG, "Server response: $sb")
            val jsonRoot = JSONObject(sb.toString())
            val jsonArrayNotificationRelated = JSONArray(jsonRoot.getString("related_post"))
            val notificationRelatedPostsPojos = ArrayList<NotificationRelatedPostsPojo?>()
            for (i in 0 until jsonArrayNotificationRelated.length()) {
                val tempJson = jsonArrayNotificationRelated.getJSONObject(i)
                val notificationRelatedPostsPojo = NotificationRelatedPostsPojo()
                notificationRelatedPostsPojo.id = tempJson.getString("id")
                notificationRelatedPostsPojo.title = tempJson.getString("heading")
                notificationRelatedPostsPojo.date = tempJson.getString("time")
                notificationRelatedPostsPojo.image = tempJson.getString("image")
                notificationRelatedPostsPojos.add(notificationRelatedPostsPojo)
            }
            val notificationDetailsPojo = NotificationDetailsPojo(jsonRoot.getString("notify_heading"),
                    jsonRoot.getString("notify_description"),
                    jsonRoot.getString("notify_link"),
                    jsonRoot.getString("notify_tag"),
                    jsonRoot.getString("notify_courses_id"),
                    jsonRoot.getString("notify_time"),
                    jsonRoot.getString("notify_image"),
                    jsonRoot.getString("notify_file1"),
                    jsonRoot.getString("notify_file1_name"),
                    jsonRoot.getString("notify_file2"),
                    jsonRoot.getString("notify_file2_name"),
                    jsonRoot.getString("notify_file3"),
                    jsonRoot.getString("notify_file3_name"),
                    notificationRelatedPostsPojos)
            //            notificationDetailsPojo.setTitle();
//            notificationDetailsPojo.setDescription("");
//            notificationDetailsPojo.setLink("");
//            notificationDetailsPojo.setTag("");
//            notificationDetailsPojo.setCourseId("");
//            notificationDetailsPojo.setTime("");
//            notificationDetailsPojo.setImage("");
//            notificationDetailsPojo.setFile1("");
//            notificationDetailsPojo.setFile1Name("");
//            notificationDetailsPojo.setFile2("");
//            notificationDetailsPojo.setFile2Name("");
//            notificationDetailsPojo.setFile3("");
//            notificationDetailsPojo.setFile3Name("");
            messageBundle.putString("STATUS", "Success")
            messageBundle.putParcelable("notification", notificationDetailsPojo)
            message.data = messageBundle
            notificationDetailsHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            messageBundle.putString("MESSAGE", e.message)
            message.data = messageBundle
            notificationDetailsHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            messageBundle.putString("MESSAGE", e.message)
            message.data = messageBundle
            notificationDetailsHandler.sendMessage(message)
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                messageBundle.putString("MESSAGE", e.message)
                message.data = messageBundle
                notificationDetailsHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "NotificationDtlsThread"
    }

}
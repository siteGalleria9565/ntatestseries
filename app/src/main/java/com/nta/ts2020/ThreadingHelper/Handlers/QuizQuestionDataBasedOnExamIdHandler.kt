package com.nta.ts2020.ThreadingHelper.Handlers
//package com.nta.ts2020.threading.handlers;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//import android.widget.Button;
//import android.widget.Toast;
//
//import java.lang.ref.WeakReference;
//import java.util.ArrayList;
//
//import com.nta.ts2020.activity.QuizShowQuestionInstructionsActivity;
//import com.nta.ts2020.pojo.ExamAllQuestionsSubjectWisePojo;
//import com.nta.ts2020.pojo.parcel.CacheQuestionDataParcel;
//
///**
// * Created by    on 28/1/17.
// */
//
//public class QuizQuestionDataBasedOnExamIdHandler extends Handler
//{
//    private final Button nextActivityButton;
//    // Context ....
//    Context context;
//
//    //ProgressDialog instant
//    ProgressDialog progressDialog;
//
//    // Bundle ....
//    Bundle localQuestionsBasedOnExamIDFromServerInstance;
//
//    int ExamId;
//
//    private final WeakReference<QuizShowQuestionInstructionsActivity> referenceShowQuestionDataBasedOnExamIdActivity;
//    QuizShowQuestionInstructionsActivity localShowQuestionDataBasedOnExamIdInstance;
//
//    // Strings ....
//    private final String TAG = "AllExamDataIDHnldr";
//    private CacheQuestionDataParcel cacheQuestionDataParcelInstance;
//    private ArrayList<ExamAllQuestionsSubjectWisePojo> localQuestionDataBasedOnExamIdList;
//
//    public QuizQuestionDataBasedOnExamIdHandler(Context context, QuizShowQuestionInstructionsActivity localShowQuestionDataBasedOnExamIdActivityInstance, Button nextActivityButton)
//    {
//        this.context = context;
//        //this.ExamId=ExamId;
//        referenceShowQuestionDataBasedOnExamIdActivity = new WeakReference<>(localShowQuestionDataBasedOnExamIdActivityInstance);
//        this.localShowQuestionDataBasedOnExamIdInstance = referenceShowQuestionDataBasedOnExamIdActivity.get();
//        this.nextActivityButton = nextActivityButton;
//    }
//
//    @Override
//    public void handleMessage(Message inputMessageFromChildThread)
//    {
//        // Getting reference of Progress Dialog from the Activity ....
//        progressDialog = localShowQuestionDataBasedOnExamIdInstance.getProgressDialog();
//
//        // Getting the Examination Bundle object from the Child Thread's Message ....
//        localQuestionsBasedOnExamIDFromServerInstance = inputMessageFromChildThread.getData();
//        Log.d(TAG, "Thread for saving QuestionDataBasedOnExamId executed and returned message: " + localQuestionsBasedOnExamIDFromServerInstance.getString("save_all_Examination_Data_output"));
//
//        if(localQuestionsBasedOnExamIDFromServerInstance.getString("save_all_Examination_Data_output").equals("Error"))
//        {
//            Log.d(TAG,"QuestionDataBasedOnExamId data could not be saved into the Cloud.");
//
//            progressDialog.dismiss();
//            Toast.makeText(context, "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
//            nextActivityButton.setText("Network Error");
//            nextActivityButton.setEnabled(false);
//        }
//        else
//        {
//            Log.d(TAG, "QuestionDataBasedOnExamId data Download successfully into the Cloud.");
//
//            cacheQuestionDataParcelInstance = localQuestionsBasedOnExamIDFromServerInstance.getParcelable("CacheQuestionDataParcel");
//
//            localQuestionDataBasedOnExamIdList=cacheQuestionDataParcelInstance.localQuestionDataBasedOnExamIdList;
//
//            // Setting the Examination Mapping List in the Activity ....
//            localShowQuestionDataBasedOnExamIdInstance.LoadData(localQuestionDataBasedOnExamIdList);
//
//            progressDialog.dismiss();
//
//        }
//    }
//}

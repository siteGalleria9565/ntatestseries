package com.nta.ts2020.ThreadingHelper.Handlers

import android.os.Handler
import android.os.Message
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.nta.ts2020.Activities.ExamResultDataListActivity
import com.nta.ts2020.AppConstants.ConstantVariables.EXAM
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.QUIZ
import com.nta.ts2020.AppConstants.ConstantVariables.RESULT_DATA_LIST_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.Fragments.ExamResultListFragment
import com.nta.ts2020.Fragments.QuizResultListFragment
import java.lang.ref.WeakReference

class SaveResultListFromServerToCacheHandler : Handler {
    private var fragment: Fragment? = null
    private var activity: AppCompatActivity? = null
    private var type: Int

    constructor(fragment: Fragment?, type: Int) {
        this.fragment = fragment
        this.type = type
    }

    constructor(activity: AppCompatActivity?, type: Int) {
        this.activity = activity
        this.type = type
    }

    override fun handleMessage(msg: Message) {
        val bundleMessage = msg.data
        if (type == RESULT_DATA_LIST_ACTIVITY) {
            val resultDataListActivityWeakReference = WeakReference(activity as ExamResultDataListActivity?)
            val resultDataListActivity = resultDataListActivityWeakReference.get()
            if ("Success" == bundleMessage.getString("MESSAGE")) {
                if (bundleMessage.getBoolean("SERVER_RESPONSE")) {
                    if (bundleMessage.getBoolean("DATABASE_RESPONSE")) resultDataListActivity!!.loadData(SUCCESS)
                } else {
                    //No ExamResult List Data
                    resultDataListActivity!!.loadData(NO_DATA)
                }
            } else {
                val message = bundleMessage.getString("MESSAGE")
                resultDataListActivity!!.loadData(NETWORK_ERROR)
                //Toast.makeText(resultDataListActivity, "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        }
        if (type == EXAM) {
            val examResultListFragmentWeakReference = WeakReference(fragment as ExamResultListFragment?)
            val examResultListFragment = examResultListFragmentWeakReference.get()
            if ("Success" == bundleMessage.getString("MESSAGE")) {
                if (bundleMessage.getBoolean("SERVER_RESPONSE")) {
                    if (bundleMessage.getBoolean("DATABASE_RESPONSE")) examResultListFragment!!.loadData(SUCCESS)
                } else {
                    //No ExamResult List Data
                    examResultListFragment!!.loadData(NO_DATA)
                }
            } else {
                val message = bundleMessage.getString("MESSAGE")
                examResultListFragment!!.loadData(NETWORK_ERROR)
                // Toast.makeText(examResultListFragment.getContext(), "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        }
        if (type == QUIZ) {
            val quizResultListFragmentWeakReference = WeakReference(fragment as QuizResultListFragment?)
            val quizResultListFragment = quizResultListFragmentWeakReference.get()
            if ("Success" == bundleMessage.getString("MESSAGE")) {
                if (bundleMessage.getBoolean("SERVER_RESPONSE")) {
                    if (bundleMessage.getBoolean("DATABASE_RESPONSE")) quizResultListFragment!!.loadData(SUCCESS)
                } else {
                    //No ExamResult List Data
                    quizResultListFragment!!.loadData(NO_DATA)
                }
            } else {
                val message = bundleMessage.getString("MESSAGE")
                quizResultListFragment!!.loadData(NETWORK_ERROR)
                // Toast.makeText(quizResultListFragment.getContext(), "Network Error: Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        }
    }
}
package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.os.Message
import android.util.Log
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_LOAD_QUIZ_SUBJECT_CHAPTER_FROM_SERVER
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.LocalStorage.QuizCacheAllChapterData
import com.nta.ts2020.LocalStorage.QuizCacheAllSubjectData
import com.nta.ts2020.PojoClasses.QuizChapter
import com.nta.ts2020.PojoClasses.QuizSubject
import com.nta.ts2020.PojoClasses.QuizSubjectChapterCombined
import com.nta.ts2020.ThreadingHelper.Handlers.QuizSaveSubChapFromServerToCacheHandler
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class QuizSaveSubChapFromServerToCacheThread(private val context: Context, private val quizSaveSubChapFromServerToCacheHandler: QuizSaveSubChapFromServerToCacheHandler) : Runnable {
    private val quizCacheAllChapterDataInstance: QuizCacheAllChapterData
    private val quizCacheAllSubjectDataInstance: QuizCacheAllSubjectData
    override fun run() {
        val quizSubjectChapterCombinedArrayList = ArrayList<QuizSubjectChapterCombined>()
        val messageBundle = Bundle()
        val message = Message()
        var request: OutputStreamWriter? = null
        var bReader: BufferedReader? = null
        var parameters = "user_id=" + CacheUserData(context).userDataFromCache.userId
        parameters += "&type=QUIZ"
        try {
            val url = URL(URL_LOAD_QUIZ_SUBJECT_CHAPTER_FROM_SERVER)
            //HttpURLConnection for creating connection to the url
            val connection = url.openConnection() as HttpURLConnection
            connection.doOutput = true
            connection.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")


            //Write parameters to stream
            request = OutputStreamWriter(connection.outputStream)
            request.write(parameters)

            //Clean up
            request.flush()

            //BufferedReader to store data stream
            bReader = BufferedReader(InputStreamReader(connection.inputStream))
            var line: String? = ""
            val sBuilder = StringBuilder()

            //Read Line by line data
            while (bReader.readLine().also { line = it } != null) {
                sBuilder.append(line).append('\n')
            }
            Log.d(TAG, "Server Response: $sBuilder")

            //Json Parsing -- Start
            val jsonRoot = JSONObject(sBuilder.toString())

            //Create a object of QuizSubject class to save data and return it
            if (jsonRoot.getBoolean("result")) {
                val jsonArraySub = jsonRoot.getJSONArray("data")
                var jsonSub: JSONObject
                var jsonChap: JSONObject
                var jsonArrayChap = JSONArray()
                for (i in 0 until jsonArraySub.length()) {
                    val quizChapterArrayList = ArrayList<QuizChapter>()
                    jsonSub = jsonArraySub.getJSONObject(i)
                    val quizSubject = QuizSubject(jsonSub.getInt("sub_id"), jsonSub.getString("sub_name"))
                    jsonArrayChap = jsonSub.getJSONArray("chapters")
                    for (j in 0 until jsonArrayChap.length()) {
                        jsonChap = jsonArrayChap.getJSONObject(j)
                        val quizChapter = QuizChapter(jsonChap.getInt("ch_id"), jsonChap.getString("ch_name"))
                        quizChapterArrayList.add(quizChapter)
                    }
                    val quizSubjectChapterCombined = QuizSubjectChapterCombined(quizSubject, quizChapterArrayList)
                    quizSubjectChapterCombinedArrayList.add(quizSubjectChapterCombined)
                }
            }
            //Json Parsing -- End
            bReader.close()
            if (insertToCache(quizSubjectChapterCombinedArrayList)) {
                messageBundle.putString("MESSAGE", "Success")
                message.data = messageBundle
                quizSaveSubChapFromServerToCacheHandler.sendMessage(message)
            }
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            messageBundle.putString("MESSAGE", e.message)
            message.data = messageBundle
            quizSaveSubChapFromServerToCacheHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            messageBundle.putString("MESSAGE", e.message)
            message.data = messageBundle
            quizSaveSubChapFromServerToCacheHandler.sendMessage(message)
        }
    }

    private fun insertToCache(quizSubjectChapterCombinedArrayList: ArrayList<QuizSubjectChapterCombined>): Boolean {
        quizCacheAllSubjectDataInstance.clearAllSubs()
        quizCacheAllChapterDataInstance.clearAllChaps()
        if (!quizSubjectChapterCombinedArrayList.isEmpty()) {
            //Clear previous data to save new data
            var quizSubjectChapterCombined: QuizSubjectChapterCombined
            var quizSubject: QuizSubject
            var quizChapter: QuizChapter?
            var quizChapterArrayList: ArrayList<QuizChapter>
            var tempSubId: Int
            for (i in quizSubjectChapterCombinedArrayList.indices) {
                quizSubjectChapterCombined = quizSubjectChapterCombinedArrayList[i]
                quizSubject = quizSubjectChapterCombined.quizSubject
                tempSubId = quizSubject.subId
                quizCacheAllSubjectDataInstance.insertSub(tempSubId, quizSubject.subName)
                quizChapterArrayList = quizSubjectChapterCombined.quizChapters
                for (j in quizChapterArrayList.indices) {
                    quizChapter = quizChapterArrayList[j]
                    quizCacheAllChapterDataInstance.insertChap(tempSubId, quizChapter!!.chapId, quizChapter.chapName)
                }
            }
        }
        return true
    }

    companion object {
        private const val TAG = "QuizSaveSubChapThread"
    }

    init {
        quizCacheAllChapterDataInstance = QuizCacheAllChapterData(context)
        quizCacheAllSubjectDataInstance = QuizCacheAllSubjectData(context)
    }
}
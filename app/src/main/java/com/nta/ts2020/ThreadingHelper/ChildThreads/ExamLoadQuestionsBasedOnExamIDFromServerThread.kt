package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import com.google.gson.Gson
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.EXAM
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_QUIZ_ANS_KEY_LOAD_QUES
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.LocalStorage.ExamAssetToLocalDatabase
import com.nta.ts2020.LocalStorage.ExamCacheAllQuestionRelatedStuff
import com.nta.ts2020.LocalStorage.ExamCacheQuestionDataBasedOnExamId
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.PojoClasses.ExamQuestionDataBasedOnExamIdPojo
import com.nta.ts2020.PojoClasses.ParcelPojoClass.CacheQuestionDataParcel
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class ExamLoadQuestionsBasedOnExamIDFromServerThread(context: Context?, localQuestionDataBasedOnExamIdInstance: Handler, ExamID: String, private val type: Int) : Runnable {

    // UI Stuff ....
    var context: Context? = null

    // Handlers ....
    var handlerQuestionDataBasedOnExamId: Handler? = null

    // Message ....
    var localQuestionsBasedOnExamIDFromServerInstance: Message? = null

    // Bundle ....
    var localQuestionsBasedOnExamIDFromServerBundleDataInstance: Bundle? = null

    //Transection
    var localExamCacheQuestionDataBasedOnExamIdInstance: ExamCacheQuestionDataBasedOnExamId? = null
    var localExamCacheAllQuestionRelatedStuffInstance: ExamCacheAllQuestionRelatedStuff

    // POJOs ....
    var examQuestionDataBasedOnExamIdPojoInstance: ExamQuestionDataBasedOnExamIdPojo? = null
    var examAllQuestionsSubjectWisePojoInstance: ExamAllQuestionsSubjectWisePojo? = null

    // Collections ....
    var localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo?>? = null

    // GSONS ....
    var localGsonObject: Gson

    // Network Request Stuff ....
    var requestURL: URL? = null
    var requestURLConnection: HttpURLConnection? = null

    // Stream Writers ....
    var requestDataStreamWriterInstance: OutputStreamWriter? = null

    // Stream Readers ....
    var serverResponseStreamReaderInstance: InputStreamReader? = null
    var serverResponseBufferedReaderInstance: BufferedReader? = null

    //parcel
    var cacheQuestionDataParcelInstance: CacheQuestionDataParcel? = null
    var serverResponseCode = 0
    var i = 0
    var ExamId: String

    // Strings ....
    var jsonRequestDataString: String? = null
    var jsonResponseDataString: String? = null
    var serverURL: String? = null
    var updatingDataInCacheStatus: String? = null

    //Array....
    lateinit var SubjectIdArray: Array<String?>
    lateinit var SubjectWiseMarksArray: Array<String?>
    lateinit var NumberOfQuestionsArray: Array<String?>
    lateinit var SubjectNamesArray: Array<String?>
    private val TAG = "LoadQuesFrmSrvrThread"
    private var isEssayAvailable = false
    override fun run() {
        try {
            val UserID = CacheUserData(context!!).userDataFromCache.userId
            if (type == EXAM) {
                //Send data to server for getting values
                jsonRequestDataString = "exam_id=$ExamId"
                jsonRequestDataString += "&user_id=$UserID"

                // Establishing URL Connection with the Server ....
                serverURL = ConstantVariables.URL_LOAD_QUESTION_BASED_ON_EXAM_ID_FROM_SERVER
            } else {
                jsonRequestDataString = "id=$ExamId"
                jsonRequestDataString += "&user_id=$UserID"
                serverURL = URL_QUIZ_ANS_KEY_LOAD_QUES
            }
            requestURL = URL(serverURL)
            requestURLConnection = requestURL!!.openConnection() as HttpURLConnection
            requestURLConnection!!.doOutput = true
            requestURLConnection!!.readTimeout = READ_TIMEOUT_VALUE
            requestURLConnection!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            Log.d(TAG, "Parameter's:$jsonRequestDataString")
            Log.d(TAG, "#requestURLConnection Code: $requestURLConnection")

            // Sending the data along with the request to the Server ....
            requestDataStreamWriterInstance = OutputStreamWriter(requestURLConnection!!.outputStream)
            requestDataStreamWriterInstance!!.write(jsonRequestDataString)
            requestDataStreamWriterInstance!!.close()

            // Getting Response from the Server ....
            serverResponseCode = requestURLConnection!!.responseCode
            Log.d(TAG, "#Server's Response Code: $serverResponseCode")
            if (serverResponseCode == HttpURLConnection.HTTP_OK) {
                serverResponseStreamReaderInstance = InputStreamReader(requestURLConnection!!.inputStream)
                serverResponseBufferedReaderInstance = BufferedReader(serverResponseStreamReaderInstance)


                // Reading the Response Data ....
                if (serverResponseBufferedReaderInstance != null) jsonResponseDataString = serverResponseBufferedReaderInstance!!.readLine()
                Log.d(TAG, "#Server Response: $jsonResponseDataString")

                // Closing the Stream ....
                serverResponseBufferedReaderInstance!!.close()

                // Retrieving Profile Pojo from JSON ....
                Log.d(TAG, "###FunctionOutput_isDataFound: $jsonResponseDataString")

                // Retrieving Profile Pojo from JSON ....
                val jsonRoot = JSONObject(jsonResponseDataString)

                //Get Json String
                examQuestionDataBasedOnExamIdPojoInstance!!.examId = jsonRoot.getString("exam_id")
                Log.d(TAG, "#Server Response: setExamId Status " + examQuestionDataBasedOnExamIdPojoInstance!!.examId)
                examQuestionDataBasedOnExamIdPojoInstance!!.examName = jsonRoot.getString("exam_name")
                Log.d(TAG, "#Server Response: setExamName Status " + examQuestionDataBasedOnExamIdPojoInstance!!.examName)
                examQuestionDataBasedOnExamIdPojoInstance!!.totalQuestions = jsonRoot.getString("total_questions")
                Log.d(TAG, "#Server Response: setTotalQuestions Status " + examQuestionDataBasedOnExamIdPojoInstance!!.totalQuestions)
                examQuestionDataBasedOnExamIdPojoInstance!!.totalMarks = jsonRoot.getString("total_marks")
                Log.d(TAG, "#Server Response: setTotalMarks Status " + examQuestionDataBasedOnExamIdPojoInstance!!.totalMarks)
                examQuestionDataBasedOnExamIdPojoInstance!!.timeDurationInSeconds = jsonRoot.getString("duration")
                Log.d(TAG, "#Server Response: getTimeDurationInSeconds Status " + examQuestionDataBasedOnExamIdPojoInstance!!.timeDurationInSeconds)
                examQuestionDataBasedOnExamIdPojoInstance!!.instructions = jsonRoot.getString("instructions")
                Log.d(TAG, "#Server Response: setInstructions Status " + examQuestionDataBasedOnExamIdPojoInstance!!.instructions)

                // Getting JSON Array node
                val SubjectId = jsonRoot.getJSONArray("subject_ids")
                Log.d(TAG, "#Server Response: getJSONArray SubjectId Status " + SubjectId.length())


                //Memmory Allocation Of Array's
                SubjectIdArray = arrayOfNulls(SubjectId.length())
                SubjectWiseMarksArray = arrayOfNulls(SubjectId.length())
                NumberOfQuestionsArray = arrayOfNulls(SubjectId.length())
                SubjectNamesArray = arrayOfNulls(SubjectId.length())
                val NumberOfQuestionsJSONObject = jsonRoot.getJSONObject("number_of_questions")
                val SubjectWiseMarksJSONObject = jsonRoot.getJSONObject("number_of_marks")
                val SubjectNamesJSONObject = jsonRoot.getJSONObject("subject_names")
                var EassyDataJSONObject = JSONObject()
                isEssayAvailable = jsonRoot.getString("eassy") != "0"
                if (isEssayAvailable) EassyDataJSONObject = jsonRoot.getJSONObject("eassy")


                //Store Data From Json To Pojo
                for (k in 0 until SubjectId.length()) {
                    SubjectIdArray[k] = SubjectId[k].toString()
                    NumberOfQuestionsArray[k] = NumberOfQuestionsJSONObject.getString(SubjectIdArray[k])
                    Log.d(TAG, "#ArraySize NumberOfQuestionsArray= " + NumberOfQuestionsArray[k])
                    SubjectWiseMarksArray[k] = SubjectWiseMarksJSONObject.getString(SubjectIdArray[k])
                    Log.d(TAG, "#ArrayinLoop SubjectWiseMarksArray= " + SubjectWiseMarksArray[k])
                    SubjectNamesArray[k] = SubjectNamesJSONObject.getString(SubjectIdArray[k])
                }
                examQuestionDataBasedOnExamIdPojoInstance!!.subjectId = SubjectIdArray
                examQuestionDataBasedOnExamIdPojoInstance!!.numberOfQuestions = NumberOfQuestionsArray
                examQuestionDataBasedOnExamIdPojoInstance!!.numberOfMarks = SubjectWiseMarksArray
                Log.d(TAG, "#ArraySize SubjectWiseMarksArray= " + examQuestionDataBasedOnExamIdPojoInstance!!.numberOfMarks)
                examQuestionDataBasedOnExamIdPojoInstance!!.subjectNames = SubjectNamesArray
                Log.d(TAG, "#ArraySize SubjectNamesArray= " + examQuestionDataBasedOnExamIdPojoInstance!!.subjectNames)
                Log.d(TAG, "#ArraySize = " + SubjectNamesArray.size)
                for (k in SubjectNamesArray.indices) {

                    // Getting JSON Array node
                    val SubjectWiseData = jsonRoot.getJSONArray(SubjectNamesArray[k])

                    // looping through All Contacts
                    //JST SubjectWiseData.length()
                    for (j in 0 until SubjectWiseData.length()) {
                        val SubjectWiseDataJSONObject = SubjectWiseData.getJSONObject(j)
                        examAllQuestionsSubjectWisePojoInstance = ExamAllQuestionsSubjectWisePojo()
                        examAllQuestionsSubjectWisePojoInstance!!.subjectId = SubjectNamesArray[k]
                        examAllQuestionsSubjectWisePojoInstance!!.questionType = SubjectWiseDataJSONObject.getString("question_type")
                        examAllQuestionsSubjectWisePojoInstance!!.questionId = SubjectWiseDataJSONObject.getString("question_id")
                        examAllQuestionsSubjectWisePojoInstance!!.questionText = SubjectWiseDataJSONObject.getString("question_text")
                        examAllQuestionsSubjectWisePojoInstance!!.noOptions = SubjectWiseDataJSONObject.getString("no_options")
                        examAllQuestionsSubjectWisePojoInstance!!.optionA = SubjectWiseDataJSONObject.getString("option_A")
                        examAllQuestionsSubjectWisePojoInstance!!.optionB = SubjectWiseDataJSONObject.getString("option_B")
                        examAllQuestionsSubjectWisePojoInstance!!.optionC = SubjectWiseDataJSONObject.getString("option_C")
                        examAllQuestionsSubjectWisePojoInstance!!.optionD = SubjectWiseDataJSONObject.getString("option_D")
                        examAllQuestionsSubjectWisePojoInstance!!.optionE = SubjectWiseDataJSONObject.getString("option_E")
                        examAllQuestionsSubjectWisePojoInstance!!.rightMarks = SubjectWiseDataJSONObject.getString("right_marks")
                        examAllQuestionsSubjectWisePojoInstance!!.wrongMarks = SubjectWiseDataJSONObject.getString("wrong_marks")
                        examAllQuestionsSubjectWisePojoInstance!!.rightAns = SubjectWiseDataJSONObject.getString("correct_ans")
                        examAllQuestionsSubjectWisePojoInstance!!.essayId = SubjectWiseDataJSONObject.getString("EASSAY_ID")
                        if (isEssayAvailable && examAllQuestionsSubjectWisePojoInstance!!.essayId != "0" && !EassyDataJSONObject.isNull(examAllQuestionsSubjectWisePojoInstance!!.essayId)) {
                            examAllQuestionsSubjectWisePojoInstance!!.essayData = EassyDataJSONObject.getString(examAllQuestionsSubjectWisePojoInstance!!.essayId)
                        } else {
                            examAllQuestionsSubjectWisePojoInstance!!.essayData = ""
                        }
                        localQuestionDataBasedOnExamIdList!!.add(examAllQuestionsSubjectWisePojoInstance!!)
                    }
                }
                ExamAssetToLocalDatabase(context).deleteData()
                //Insert  SubjectStuff in cache
                updatingDataInCacheStatus = localExamCacheAllQuestionRelatedStuffInstance.setCacheAllQuestionRelatedStuff(examQuestionDataBasedOnExamIdPojoInstance!!)
                Log.d(TAG, "#updatingDataInCacheStatus: $updatingDataInCacheStatus")
                cacheQuestionDataParcelInstance = CacheQuestionDataParcel(localQuestionDataBasedOnExamIdList)
                localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putParcelable("CacheQuestionDataParcel", cacheQuestionDataParcelInstance)
                localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putInt("TYPE", type)
                localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putString("SUBJECT", Gson().toJson(examQuestionDataBasedOnExamIdPojoInstance))
                Log.d(TAG, "#updatingDataInCacheStatus: $updatingDataInCacheStatus")
                localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Success")

                // Storing the Bundle object in the Message Object ....
                localQuestionsBasedOnExamIDFromServerInstance!!.data = localQuestionsBasedOnExamIDFromServerBundleDataInstance

                // The "handleMessage()" function from the "CloudClassStudentsMappingHandler" class  automatically gets invoked after this function call ....
                handlerQuestionDataBasedOnExamId!!.sendMessage(localQuestionsBasedOnExamIDFromServerInstance)
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Error")
            localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putInt("TYPE", type)

            // Storing the Bundle object in the Message Object ....
            localQuestionsBasedOnExamIDFromServerInstance!!.data = localQuestionsBasedOnExamIDFromServerBundleDataInstance

            // The "handleMessage()" function from the "localQuestionsBasedOnExamIDFromServerInstance" class  automatically gets invoked after this function call ....
            handlerQuestionDataBasedOnExamId!!.sendMessage(localQuestionsBasedOnExamIDFromServerInstance)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putString("save_all_Examination_Data_output", "Error")
            localQuestionsBasedOnExamIDFromServerBundleDataInstance!!.putInt("TYPE", type)
            localQuestionsBasedOnExamIDFromServerInstance!!.data = localQuestionsBasedOnExamIDFromServerBundleDataInstance
            handlerQuestionDataBasedOnExamId!!.sendMessage(localQuestionsBasedOnExamIDFromServerInstance)
        }
    }

    fun initializeLocalVariables(context: Context?, handlerQuestionDataBasedOnExamId: Handler) {
        // UI Stuff ....
        this.context = context

        //pojo
        examQuestionDataBasedOnExamIdPojoInstance = ExamQuestionDataBasedOnExamIdPojo()

        //ArrayList
        localQuestionDataBasedOnExamIdList = ArrayList()

        // Handlers ....
        this.handlerQuestionDataBasedOnExamId = handlerQuestionDataBasedOnExamId

        // Bundle ....
        localQuestionsBasedOnExamIDFromServerBundleDataInstance = Bundle()

        // Message....
        localQuestionsBasedOnExamIDFromServerInstance = handlerQuestionDataBasedOnExamId.obtainMessage()
    }

    init {
        initializeLocalVariables(context, localQuestionDataBasedOnExamIdInstance)

        // localExamCacheQuestionDataBasedOnExamIdInstance = new ExamCacheQuestionDataBasedOnExamId(context);
        localExamCacheAllQuestionRelatedStuffInstance = context?.let { ExamCacheAllQuestionRelatedStuff(it) }!!
        localGsonObject = Gson()
        ExamId = ExamID
    }
}
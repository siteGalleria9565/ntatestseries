package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_EXAM_FINAL_SUBMISSION
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.ExamCacheFinalSubmission
import com.nta.ts2020.PojoClasses.FinalSubmissionPojo
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class SaveFinalSubmissionDataToServerThread(private val context: Context) : Runnable {
    private val examCacheFinalSubmission: ExamCacheFinalSubmission = ExamCacheFinalSubmission(context)
    override fun run() {
        val finalSubmissionData = examCacheFinalSubmission.getFinalSubmissionData("sync")
        //Creating json from java object to send it to server;
        val gson = Gson()
        if (!finalSubmissionData.isEmpty()) {
            val jsonObject = gson.toJson(finalSubmissionData)
            Log.d(TAG, "Json before sending \n$jsonObject")

            //Send json to server
            var os: BufferedOutputStream? = null
            val `is`: InputStream? = null
            var conn: HttpURLConnection? = null
            try {
                val url = URL(URL_EXAM_FINAL_SUBMISSION)
                conn = url.openConnection() as HttpURLConnection
                conn.readTimeout = READ_TIMEOUT_VALUE
                conn.connectTimeout = CONNECT_TIMEOUT_VALUE
                conn.requestMethod = "POST"
                conn.doInput = true
                conn.doOutput = true
                conn.setFixedLengthStreamingMode(jsonObject.toByteArray().size)

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json;charset=utf-8")
                conn.setRequestProperty("X-Requested-With", "XMLHttpRequest")

                //open
                conn.connect()

                //setup send
                os = BufferedOutputStream(conn.outputStream)
                os.write(jsonObject.toByteArray())
                //clean up
                os.flush()

                // Read Server Response
                val bufferedReader = BufferedReader(InputStreamReader(conn.inputStream))
                var line: String?
                val sb = StringBuilder()
                while (bufferedReader.readLine().also { line = it } != null) {
                    // Append server response in string
                    sb.append(line).append("\n")
                }
                val response = sb.toString()
                Log.d(TAG, "Response from server: $response")

                //update status if success
                try {
                    val jsonResponse = JSONObject(response)
                    if (jsonResponse.getBoolean("status")) {
                        //If everything is updated successfully
                        examCacheFinalSubmission.updateSyncStatus(finalSubmissionData, true)
                    } else {
                        val examIdJsonArray = jsonResponse.getJSONArray("exam_ids")
                        //update sync status for only which are synced
                        val finalSubmissionPojoArrayList = ArrayList<FinalSubmissionPojo>()
                        for (i in 0 until examIdJsonArray.length()) {
                            for (j in finalSubmissionData.indices) {
                                if (examIdJsonArray[i] == finalSubmissionData[j].examId) {
                                    finalSubmissionPojoArrayList.add(finalSubmissionData[j])
                                }
                            }
                        }
                        examCacheFinalSubmission.updateSyncStatus(finalSubmissionPojoArrayList, true)
                    }
                } catch (e: JSONException) {
                    if (BuildConfig.DEBUG) e.printStackTrace()
                }
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                Log.w(TAG, e.message)
            } finally {
                //clean up
                try {
                    os?.close()
                    `is`?.close()
                } catch (e: IOException) {
                    if (BuildConfig.DEBUG) e.printStackTrace()
                }
                conn?.disconnect()
            }
        }
    }

    companion object {
        private const val TAG = "FinalSubmissionThread"
    }

    init {
        //        finalSubmissionData = new ExamCacheFinalSubmission(context).getFinalSubmissionData("sync");
    }
}
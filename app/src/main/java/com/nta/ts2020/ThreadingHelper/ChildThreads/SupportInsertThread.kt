package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.os.Bundle
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_SUPPORT_INSERT
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.ThreadingHelper.Handlers.SupportInsertHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedOutputStream
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class SupportInsertThread(private val parameter: String, private val supportInsertHandler: SupportInsertHandler) : Runnable {
    override fun run() {
//Variables for handler
        val bundleMessage = Bundle()
        val message = supportInsertHandler.obtainMessage()

//        Log.d(TAG,parameter);

        //Connection Variables
        var conn: HttpURLConnection? = null
        var os: BufferedOutputStream? = null
        var br: BufferedReader? = null
        try {
            val url = URL(URL_SUPPORT_INSERT)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn!!.connectTimeout = CONNECT_TIMEOUT_VALUE
            conn.readTimeout = READ_TIMEOUT_VALUE
            conn.setFixedLengthStreamingMode(parameter.toByteArray().size)

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8")
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest")

            //open
            conn.connect()

            //setup send
            os = BufferedOutputStream(conn.outputStream)
            os.write(parameter.toByteArray())
            os.flush()
            br = BufferedReader(InputStreamReader(conn.inputStream))
            var line: String? = ""
            val sb = StringBuilder()
            while (br.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }

//            Log.d(TAG, "Server response: " + sb.toString());
            val jsonRoot = JSONObject(sb.toString())
            if (jsonRoot.getBoolean("status")) {
                //Set message to handler
                bundleMessage.putString("STATUS", "Success")
                message.data = bundleMessage
                supportInsertHandler.sendMessage(message)
            } else {
                //Set message to handler
                bundleMessage.putString("STATUS", "Failed")
                message.data = bundleMessage
                supportInsertHandler.sendMessage(message)
            }
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            //Set message to handler
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            supportInsertHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            supportInsertHandler.sendMessage(message)
        } finally {
            try {
                os?.close()
                br?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                //Set message to handler
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                supportInsertHandler.sendMessage(message)
            }
            conn?.disconnect()
        }
    }

    companion object {
        private const val TAG = "SupportInsertThread"
    }

}
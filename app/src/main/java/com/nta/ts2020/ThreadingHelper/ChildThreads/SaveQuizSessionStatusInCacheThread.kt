package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.util.Log
import com.nta.ts2020.LocalStorage.QuizCacheGetCurrentExamSession
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import java.util.*

class SaveQuizSessionStatusInCacheThread(private val context: Context, // Collections ....
                                         var localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo>) : Runnable {
    private val localQuizCacheQuestionDataBasedOnExamIdInstance: QuizCacheGetCurrentExamSession = QuizCacheGetCurrentExamSession(context)

    override fun run() {


        //Insert  QuestionData in cache
        val updatingDataInCacheStatus = localQuizCacheQuestionDataBasedOnExamIdInstance.setCurrentExamSession(localQuestionDataBasedOnExamIdList)
        Log.d(TAG, "Saved   QuestionData downloaded from Cloud to Cache. $updatingDataInCacheStatus")
    }

    companion object {
        private const val TAG = "SavDwnldChatHistCachThd"
    }

}
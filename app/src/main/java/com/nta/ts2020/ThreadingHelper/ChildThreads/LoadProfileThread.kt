package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.content.Context
import android.os.Bundle
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_LOAD_PROFILE
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.ThreadingHelper.Handlers.LoadProfileHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class LoadProfileThread(private val context: Context, private val parameterJson: String, private val loadProfileHandler: LoadProfileHandler) : Runnable {
    override fun run() {
        var connection: HttpURLConnection? = null
        var bufferedReader: BufferedReader? = null
        var request: OutputStreamWriter? = null
        val bundleMessage = Bundle()
        val message = loadProfileHandler.obtainMessage()

//        Log.d(TAG,"Req json parameter: " + parameterJson);
        val parameter = "request=$parameterJson"
        try {
            //Download json from url        new ThreadPoolService().loadProfileFromServer(context, parameterJson, new LoadProfileHandler(ExamProfAddressActivity.this, ADDRESS_ACTIVITY, progressDialog));
            val url = URL(URL_LOAD_PROFILE)
            connection = url.openConnection() as HttpURLConnection
            connection.doOutput = true
            connection!!.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

            //Write parameters to the stream
            request = OutputStreamWriter(connection.outputStream)
            request.write(parameter)

            //Clean up
            request.flush()

            //Read Response
            bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
            var line: String?
            val stringBuilder = StringBuilder()
            while (bufferedReader.readLine().also { line = it } != null) {
                stringBuilder.append(line).append("\n")
            }
            val jsonRoot = JSONObject(stringBuilder.toString())
            //            Log.d(TAG, "Json from server: " + jsonRoot.toString());
            if (jsonRoot.getBoolean("status")) {
                bundleMessage.putString("STATUS", "Success")
                bundleMessage.putString("JSON_DATA", jsonRoot.toString())
            } else {
                bundleMessage.putString("MESSAGE", "NO_RESPONSE")
            }
            message.data = bundleMessage
            loadProfileHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            loadProfileHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            loadProfileHandler.sendMessage(message)
        } finally {
            try {
                request?.close()
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                loadProfileHandler.sendMessage(message)
            }
            connection?.disconnect()
        }
    }

    companion object {
        private const val TAG = "LoadProfileThread"
    }

}
package com.nta.ts2020.ThreadingHelper.ChildThreads

import android.os.Bundle
import com.nta.ts2020.Activities.EBookSubjectlistActivity
import com.nta.ts2020.AppConstants.ConstantVariables.CONNECT_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.READ_TIMEOUT_VALUE
import com.nta.ts2020.AppConstants.ConstantVariables.URL_E_BOOK_SUBJECTS_LIST
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheEBookSubjects
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.EBookSubjectsPojo
import com.nta.ts2020.ThreadingHelper.Handlers.EBookSubjectsListHandler
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class EBookSubjectsListThread(private val context: EBookSubjectlistActivity, private val eBookSubjectsListHandler: EBookSubjectsListHandler) : Runnable {
    override fun run() {
        var connection: HttpURLConnection? = null
        var bufferedReader: BufferedReader? = null
        var request: OutputStreamWriter? = null
        val bundleMessage = Bundle()
        val message = eBookSubjectsListHandler.obtainMessage()
        val eBookSubjectsPojoArrayList = ArrayList<EBookSubjectsPojo>()
        val parameter = "user_id=" + CacheUserData(context).userDataFromCache.userId
        try {
            //Download json from url
            val url = URL(URL_E_BOOK_SUBJECTS_LIST)
            connection = url.openConnection() as HttpURLConnection
            connection.doOutput = true
            connection.readTimeout = READ_TIMEOUT_VALUE
            connection.connectTimeout = CONNECT_TIMEOUT_VALUE
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")

            //Write parameters to the stream
            request = OutputStreamWriter(connection.outputStream)
            request.write(parameter)

            //Clean up
            request.flush()

            //Read Response
            bufferedReader = BufferedReader(InputStreamReader(connection.inputStream))
            var line: String?
            val stringBuilder = StringBuilder()
            while (bufferedReader.readLine().also { line = it } != null) {
                stringBuilder.append(line).append("\n")
            }
            val jsonRoot = JSONObject(stringBuilder.toString())
            //            Log.d(TAG, "Json from server: " + jsonRoot.toString());
            if (jsonRoot.getBoolean("status")) {
                val jsonSubjects = jsonRoot.getJSONObject("subjects")
                val keys = jsonSubjects.keys()
                while (keys.hasNext()) {
                    val eBookSubjectsPojo = EBookSubjectsPojo()
                    val tempId = keys.next()
                    val tempName = jsonSubjects.getString(tempId)
                    eBookSubjectsPojo.id = tempId
                    eBookSubjectsPojo.name = tempName
                    eBookSubjectsPojoArrayList.add(eBookSubjectsPojo)
                }
                val cacheEBookSubjects = CacheEBookSubjects(context)
                cacheEBookSubjects.clearRecords()
                val databaseStatus = cacheEBookSubjects.saveEBookSubjectsList(eBookSubjectsPojoArrayList)
                bundleMessage.putString("STATUS", "Success")
                bundleMessage.putBoolean("DATABASE_RESPONSE", databaseStatus)
            } else {
                bundleMessage.putString("STATUS", "Success")
                bundleMessage.putBoolean("DATABASE_RESPONSE", true)
                bundleMessage.putString("MESSAGE", "NO_RESPONSE")
            }
            message.data = bundleMessage
            eBookSubjectsListHandler.sendMessage(message)
        } catch (e: IOException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            eBookSubjectsListHandler.sendMessage(message)
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            bundleMessage.putString("MESSAGE", e.message)
            message.data = bundleMessage
            eBookSubjectsListHandler.sendMessage(message)
        } finally {
            try {
                request?.close()
                bufferedReader?.close()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                bundleMessage.putString("MESSAGE", e.message)
                message.data = bundleMessage
                eBookSubjectsListHandler.sendMessage(message)
            }
            connection?.disconnect()
        }
    }

    companion object {
        private const val TAG = "EBookSubjectsListThread"
    }

}
package com.nta.ts2020.UtilsHelper

import retrofit2.Call
import retrofit2.Response

abstract class CallBack<T> {
    abstract fun onResponse(call: Call<T>?, response: Response<T>?)
    abstract fun onFailure(call: Call<T>?, t: Throwable?)
}
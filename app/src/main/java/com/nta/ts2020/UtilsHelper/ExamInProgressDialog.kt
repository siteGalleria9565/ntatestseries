package com.nta.ts2020.UtilsHelper

import android.app.ProgressDialog
import android.content.Context

class ExamInProgressDialog(context: Context?) : ProgressDialog(context) {
    override fun show() {
        setTitle("Loading .... ")
        setMessage("Please wait")
        isIndeterminate = true
        setCancelable(false)
        super.show()
    }
}
package com.nta.ts2020.UtilsHelper

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.core.content.ContextCompat
import com.nta.ts2020.PojoClasses.QuesWiseAnalysisPojo
import com.nta.ts2020.R
import de.codecrafters.tableview.SortableTableView
import de.codecrafters.tableview.providers.TableDataRowBackgroundProvider
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders

class QuesWiseAnalysisTableView : SortableTableView<QuesWiseAnalysisPojo?> {
    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attributes: AttributeSet?) : super(context, attributes) {}
    constructor(context: Context?, attributes: AttributeSet?, styleAttributes: Int) : super(context, attributes, styleAttributes) {
//        final SimpleTableHeaderAdapter simpleTableHeaderAdapter = new SimpleTableHeaderAdapter(context, "Status", "Your Answer", "Correct Answer", "Your Score"
//                , "Your Time", "Topper Time");
////        simpleTableHeaderAdapter.setTextColor(ContextCompat.getColor(context, R.color.table_header_text));
//        setHeaderAdapter(simpleTableHeaderAdapter);

//        setHeaderSortStateViewProvider(SortStateViewProviders.brightArrows());

//        final TableColumnWeightModel tableColumnWeightModel = new TableColumnWeightModel(7);
//        tableColumnWeightModel.setColumnWeight(0, 2);
//        tableColumnWeightModel.setColumnWeight(1, 5);
//        tableColumnWeightModel.setColumnWeight(2, 3);
//        tableColumnWeightModel.setColumnWeight(3, 2);
//        tableColumnWeightModel.setColumnWeight(4, 3);
//        tableColumnWeightModel.setColumnWeight(5, 3);
//        tableColumnWeightModel.setColumnWeight(6, 3);
//        setColumnModel(tableColumnWeightModel);
    }


    override fun setDataRowBackgroundProvider(backgroundProvider: TableDataRowBackgroundProvider<in QuesWiseAnalysisPojo?>?) {
        val rowColorEven = ContextCompat.getColor(context, R.color.table_data_row_even)
        val rowColorOdd = ContextCompat.getColor(context, R.color.table_data_row_odd)
        super.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(rowColorEven, rowColorOdd))    }


    var mLastMotionY = 0f
    var mLastMotionX = 0f

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        when (ev.action) {
            MotionEvent.ACTION_DOWN -> parent.requestDisallowInterceptTouchEvent(true)
            MotionEvent.ACTION_MOVE -> {
                if (mLastMotionX != 0f && mLastMotionY != 0f && Math.abs(mLastMotionX - ev.rawX) > Math.abs(mLastMotionY - ev.rawY)) {
                    parent.requestDisallowInterceptTouchEvent(false)
                } else {
                    parent.requestDisallowInterceptTouchEvent(true)
                }
                mLastMotionX = ev.rawX
                mLastMotionY = ev.rawY
            }
            MotionEvent.ACTION_UP -> parent.requestDisallowInterceptTouchEvent(true)
        }
        return super.dispatchTouchEvent(ev)
    }
}
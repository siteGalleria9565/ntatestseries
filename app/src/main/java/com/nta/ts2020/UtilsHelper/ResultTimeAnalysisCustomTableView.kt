package com.nta.ts2020.UtilsHelper

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.core.content.ContextCompat
import com.nta.ts2020.PojoClasses.ShowSubjectWiseTimeAnalysisDataInTablePojo
import com.nta.ts2020.R
import de.codecrafters.tableview.SortableTableView
import de.codecrafters.tableview.providers.TableDataRowBackgroundProvider
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders

class ResultTimeAnalysisCustomTableView : SortableTableView<ShowSubjectWiseTimeAnalysisDataInTablePojo?> {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attributes: AttributeSet?) : super(context, attributes) {}
    constructor(context: Context?, attributes: AttributeSet?, styleAttributes: Int) : super(context, attributes, styleAttributes) {}


    override fun setDataRowBackgroundProvider(backgroundProvider: TableDataRowBackgroundProvider<in ShowSubjectWiseTimeAnalysisDataInTablePojo?>?) {
        val rowColorEven = ContextCompat.getColor(context, R.color.table_data_row_even)
        val rowColorOdd = ContextCompat.getColor(context, R.color.table_data_row_odd)
        super.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(rowColorEven, rowColorOdd))
    }

    var mLastMotionY = 0f
    var mLastMotionX = 0f
    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        when (ev.action) {
            MotionEvent.ACTION_DOWN -> parent.requestDisallowInterceptTouchEvent(true)
            MotionEvent.ACTION_MOVE -> {
                if (mLastMotionX != 0f && mLastMotionY != 0f && Math.abs(mLastMotionX - ev.rawX) > Math.abs(mLastMotionY - ev.rawY)) {
                    parent.requestDisallowInterceptTouchEvent(false)
                } else {
                    parent.requestDisallowInterceptTouchEvent(true)
                }
                mLastMotionX = ev.rawX
                mLastMotionY = ev.rawY
            }
            MotionEvent.ACTION_UP -> parent.requestDisallowInterceptTouchEvent(true)
        }
        return super.dispatchTouchEvent(ev)
    }
}
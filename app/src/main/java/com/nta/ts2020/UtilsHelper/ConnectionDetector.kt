package com.nta.ts2020.UtilsHelper

import android.content.Context
import android.net.ConnectivityManager

class ConnectionDetector(private val _context: Context) {
    // connected to the internet
    val isConnectingToInternet: Boolean
        get() {
            val cm = _context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            if (activeNetwork != null && activeNetwork.isConnected) { // connected to the internet
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    return true
                } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    return true
                }
            }
            return false
        }

}
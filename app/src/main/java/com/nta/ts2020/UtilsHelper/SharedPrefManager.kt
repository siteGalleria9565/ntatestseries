package com.nta.ts2020.UtilsHelper

import android.content.Context
import android.content.SharedPreferences

class SharedPrefManager(context: Context) {
    private val pref: SharedPreferences

    //Intro Slider
    var isFirstTimeLaunch: Boolean
        get() = pref.getBoolean(IS_FIRST_TIME_LAUNCH, true)
        set(isFirstTime) {
            val editor = pref.edit()
            editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime)
            editor.apply()
        }

    //Contacts sync
    var isContactsSynced: Boolean
        get() = pref.getBoolean(CONTACTS_SYNC, false)
        set(status) {
            val editor = pref.edit()
            editor.putBoolean(CONTACTS_SYNC, status)
            editor.apply()
        }

    //Fcm token sync
    var isFcmTokenSynced: Boolean
        get() = pref.getBoolean(FCM_TOKEN_SYNC, false)
        set(status) {
            val editor = pref.edit()
            editor.putBoolean(FCM_TOKEN_SYNC, status)
            editor.apply()
        }

    companion object {
        // Shared preferences file name
        private const val PREF_NAME = "in.co.examin"
        private const val IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch"
        private const val CONTACTS_SYNC = "ContactsSync"
        private const val FCM_TOKEN_SYNC = "FcmTokenSync"
    }

    init {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }
}
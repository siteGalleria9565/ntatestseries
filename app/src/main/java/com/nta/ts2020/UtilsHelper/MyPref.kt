package com.nta.ts2020.UtilsHelper

import android.content.Context
import android.content.SharedPreferences

class MyPref(// Context
        var mContext: Context?) {
    var pref: SharedPreferences? = null

    // Editor for Shared preferences
    var editor: SharedPreferences.Editor? = null

    fun setPref(flagid: String?, fid: Int) {
        editor!!.putInt(flagid, fid)
        editor!!.commit()
    }

    fun getPref(flagid: String?, fid: Int): Int {
        return pref!!.getInt(flagid, fid)
    }

    fun setPref(key: String?, `val`: String?) {
        editor!!.putString(key, `val`)
        editor!!.commit()
    }

    fun getPref(key: String?, `val`: String?): String? {
        return pref!!.getString(key, `val`)
    }

    fun setPref(pref: String?, `val`: Boolean) {
        editor!!.putBoolean(pref, `val`)
        editor!!.commit()
    }

    fun getPref(key: String?, `val`: Boolean): Boolean {
        return pref!!.getBoolean(key, `val`)
    }

    fun clearPref() {
        editor!!.clear()
        editor!!.commit()
    }

    companion object {
        private const val PREF_NAME = "application.preference"
    }

    init {
        if (mContext != null) {
            pref = mContext!!.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            editor = pref?.edit()
        }
    }
}
package com.nta.ts2020.Interfaces;

import com.nta.ts2020.PojoClasses.ScholerShipPojo;

public interface OnScholarshipApplyClick {
    public void onClick(ScholerShipPojo scholerShipPojo, String view);
}

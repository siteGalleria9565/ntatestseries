package com.nta.ts2020.FCMHelperServices

import com.google.firebase.iid.FirebaseInstanceIdService
import com.nta.ts2020.UtilsHelper.SharedPrefManager

class FcmInstanceIDService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val context = applicationContext
        val sharedPrefManager = SharedPrefManager(context)
        sharedPrefManager.isFcmTokenSynced = false
    }
}
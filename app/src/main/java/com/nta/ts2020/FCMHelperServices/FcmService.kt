package com.nta.ts2020.FCMHelperServices

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.text.Html
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.Activities.ExamNotificationsDetailsActivity
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R
import org.json.JSONException
import org.json.JSONObject

class FcmService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val jsonRoot = JSONObject(remoteMessage.data as Map<*, *>)
        var notificationBuilder: NotificationCompat.Builder? = null
        try {
            val notifyId = jsonRoot.getString("id")
            //Check if user is logged in or not
            val intent: Intent
            intent = if (CacheUserData(applicationContext).userDataFromCache.isUserLoggedIn) {
                Intent(this, ExamNotificationsDetailsActivity::class.java)
            } else {
                Intent(this, ExamLoginScreenActivity::class.java)
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("notification_id", notifyId)
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
            var vibration = LongArray(0)
            var tone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            if (!jsonRoot.isNull("vibration")) {
                when (jsonRoot.getString("vibration")) {
                    "long" -> vibration = longArrayOf(1000, 1000, 1000, 1000, 1000)
                    "short" -> vibration = longArrayOf(300, 300, 300, 300, 300)
                }
            }
            if (!jsonRoot.isNull("tone")) {
                when (jsonRoot.getString("tone")) {
                    "normal" -> tone = Uri.parse("android.resource://" + packageName + "/" + R.raw.tone_normal)
                    "alert" -> tone = Uri.parse("android.resource://" + packageName + "/" + R.raw.tone_alert)
                }
            }
            notificationBuilder = NotificationCompat.Builder(this)
                    .setContentTitle(jsonRoot.getString("title"))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentText(Html.fromHtml(jsonRoot.getString("message")))
                    .setVibrate(vibration)
                    .setAutoCancel(true)
                    .setSound(tone)
                    .setContentIntent(pendingIntent)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(0, notificationBuilder.build())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}
package com.nta.ts2020.Activities

import android.animation.ObjectAnimator
import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.view.View
import android.webkit.WebView
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter
import com.github.aakira.expandablelayout.ExpandableLinearLayout
import com.github.aakira.expandablelayout.Utils
import com.nta.ts2020.Adapters.AddedBookmarksRecyclerAdapter
import com.nta.ts2020.LocalStorage.CacheBookmark
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.NestedScrollWebView
import java.util.*

class AddBookmarksActivity : AppCompatActivity() {
    private var context: AddBookmarksActivity? = null
    private var recyclerView: RecyclerView? = null
    private var linLayoutNoData: LinearLayout? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var imageViewError: ImageView? = null

    //Question dialog variables
    private var quest: WebView? = null
    private var webViewOptionA: WebView? = null
    private var webViewOptionB: WebView? = null
    private var webViewOptionC: WebView? = null
    private var webViewOptionD: WebView? = null
    private var webViewOptionE: WebView? = null
    private var webViewOptionARadio: WebView? = null
    private var webViewOptionBRadio: WebView? = null
    private var webViewOptionCRadio: WebView? = null
    private var webViewOptionDRadio: WebView? = null
    private var webViewOptionERadio: WebView? = null
    private var radioButtonOptionA: RadioButton? = null
    private var radioButtonOptionB: RadioButton? = null
    private var radioButtonOptionC: RadioButton? = null
    private var radioButtonOptionD: RadioButton? = null
    private var radioButtonOptionE: RadioButton? = null

    //Added Later
    private var paragraphDataView: NestedScrollWebView? = null
    private var multiRowsRadioGroup: RadioGroup? = null
    private var showCheckQuestionOnFragmentLayout: LinearLayout? = null
    private var showRadioQuestionOnFragmentLayout: LinearLayout? = null
    private var linLayoutOptionA: LinearLayout? = null
    private var linLayoutOptionB: LinearLayout? = null
    private var linLayoutOptionC: LinearLayout? = null
    private var linLayoutOptionD: LinearLayout? = null
    private var linLayoutOptionE: LinearLayout? = null
    private var linLayoutOptionARadio: LinearLayout? = null
    private var linLayoutOptionBRadio: LinearLayout? = null
    private var linLayoutOptionCRadio: LinearLayout? = null
    private var linLayoutOptionDRadio: LinearLayout? = null
    private var linLayoutOptionERadio: LinearLayout? = null
    private var paragraphHeadingLayout: RelativeLayout? = null
    private var paragraphHeadingButtonHolder: RelativeLayout? = null
    private var paragrapRelativeLayout: RelativeLayout? = null
    private var paragraphHeading: TextView? = null
    private var paragraphExpandableLayout: ExpandableLinearLayout? = null
    private var checkBoxOptionA: CheckBox? = null
    private var checkBoxOptionB: CheckBox? = null
    private var checkBoxOptionC: CheckBox? = null
    private var checkBoxOptionD: CheckBox? = null
    private var checkBoxOptionE: CheckBox? = null
    private var examAllQuestionsSubjectWisePojoArrayList: MutableList<ExamAllQuestionsSubjectWisePojo>? = null
    private var bookmarksAdapter: AddedBookmarksRecyclerAdapter? = null
    private var cacheBookmark: CacheBookmark? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookmarks)
        context = this
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.title = "Bookmarks"
        }
        recyclerView = findViewById<View>(R.id.rvBookmarksList) as RecyclerView
        //Message Layout
        linLayoutNoData = findViewById<View>(R.id.llBookmarksError) as LinearLayout
        textViewMessage = findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        imageViewError = findViewById<View>(R.id.ivExamResultListError) as ImageView

        //Get bookmarked question from cache
        cacheBookmark = CacheBookmark(context!!)
        examAllQuestionsSubjectWisePojoArrayList = cacheBookmark!!.bookmarkedQuestions
        if ((examAllQuestionsSubjectWisePojoArrayList as ArrayList<ExamAllQuestionsSubjectWisePojo>?)!!.isEmpty()) {
            showErrorLayout()
        } else {
            //Create and set adapter to recycler view
            bookmarksAdapter = (examAllQuestionsSubjectWisePojoArrayList as ArrayList<ExamAllQuestionsSubjectWisePojo>?).let { it?.let { it1 -> AddedBookmarksRecyclerAdapter(it1) } }
            recyclerView!!.layoutManager = LinearLayoutManager(context)
            recyclerView!!.adapter = bookmarksAdapter
            bookmarksAdapter!!.setOnItemClickListener(object : AddedBookmarksRecyclerAdapter.OnItemClickListener {
                override fun onClick(examAllQuestionsSubjectWisePojo: ExamAllQuestionsSubjectWisePojo?) {
                    val dialog = Dialog(context!!, R.style.Dialog_NoTitle)
                    dialog.setContentView(R.layout.quiz_activity_questions_screen)
                    val questionNo = dialog.findViewById<View>(R.id.llExamPanelQuestionNo) as LinearLayout
                    showCheckQuestionOnFragmentLayout = dialog.findViewById<View>(R.id.linLayout) as LinearLayout
                    showRadioQuestionOnFragmentLayout = dialog.findViewById<View>(R.id.radioLayout) as LinearLayout
                    quest = dialog.findViewById<View>(R.id.questioText) as WebView

                    //Option Radio Buttons Initialization -- Suvajit
                    linLayoutOptionA = dialog.findViewById<View>(R.id.linLayoutOptionA) as LinearLayout
                    linLayoutOptionB = dialog.findViewById<View>(R.id.linLayoutOptionB) as LinearLayout
                    linLayoutOptionC = dialog.findViewById<View>(R.id.linLayoutOptionC) as LinearLayout
                    linLayoutOptionD = dialog.findViewById<View>(R.id.linLayoutOptionD) as LinearLayout
                    linLayoutOptionE = dialog.findViewById<View>(R.id.linLayoutOptionE) as LinearLayout
                    multiRowsRadioGroup = findViewById<View>(R.id.multiRowRadioOptionGroup) as RadioGroup
                    webViewOptionA = dialog.findViewById<View>(R.id.webViewOptionA) as WebView
                    webViewOptionB = dialog.findViewById<View>(R.id.webViewOptionB) as WebView
                    webViewOptionC = dialog.findViewById<View>(R.id.webViewOptionC) as WebView
                    webViewOptionD = dialog.findViewById<View>(R.id.webViewOptionD) as WebView
                    webViewOptionE = dialog.findViewById<View>(R.id.webViewOptionE) as WebView

                    //Resume
                    linLayoutOptionARadio = dialog.findViewById<View>(R.id.linLayoutOptionARadio) as LinearLayout
                    linLayoutOptionBRadio = dialog.findViewById<View>(R.id.linLayoutOptionBRadio) as LinearLayout
                    linLayoutOptionCRadio = dialog.findViewById<View>(R.id.linLayoutOptionCRadio) as LinearLayout
                    linLayoutOptionDRadio = dialog.findViewById<View>(R.id.linLayoutOptionDRadio) as LinearLayout
                    linLayoutOptionERadio = dialog.findViewById<View>(R.id.linLayoutOptionERadio) as LinearLayout
                    webViewOptionARadio = dialog.findViewById<View>(R.id.webViewOptionARadio) as WebView
                    webViewOptionBRadio = dialog.findViewById<View>(R.id.webViewOptionBRadio) as WebView
                    webViewOptionCRadio = dialog.findViewById<View>(R.id.webViewOptionCRadio) as WebView
                    webViewOptionDRadio = dialog.findViewById<View>(R.id.webViewOptionDRadio) as WebView
                    webViewOptionERadio = dialog.findViewById<View>(R.id.webViewOptionERadio) as WebView
                    radioButtonOptionA = dialog.findViewById<View>(R.id.radioButtonA) as RadioButton
                    radioButtonOptionB = dialog.findViewById<View>(R.id.radioButtonB) as RadioButton
                    radioButtonOptionC = dialog.findViewById<View>(R.id.radioButtonC) as RadioButton
                    radioButtonOptionD = dialog.findViewById<View>(R.id.radioButtonD) as RadioButton
                    radioButtonOptionE = dialog.findViewById<View>(R.id.radioButtonE) as RadioButton
                    radioButtonOptionA!!.isEnabled = false
                    radioButtonOptionB!!.isEnabled = false
                    radioButtonOptionC!!.isEnabled = false
                    radioButtonOptionD!!.isEnabled = false
                    radioButtonOptionE!!.isEnabled = false

                    //Option Radio Buttons -- End -- Suvajit

                    //Expandable Paragraph View Initialization --Start-- Suvajit
                    paragraphDataView = dialog.findViewById<View>(R.id.webViewParagraphData) as NestedScrollWebView
                    paragraphHeadingLayout = dialog.findViewById<View>(R.id.relativeLayoutParagraphHeading) as RelativeLayout
                    paragraphHeadingButtonHolder = dialog.findViewById<View>(R.id.relativeLayoutParagrahButtonHolder) as RelativeLayout
                    paragraphHeading = dialog.findViewById<View>(R.id.textViewParagraphHeading) as TextView
                    paragraphExpandableLayout = dialog.findViewById<View>(R.id.expandableLayoutParagraph) as ExpandableLinearLayout
                    paragrapRelativeLayout = dialog.findViewById<View>(R.id.relativeLayoutParagraph) as RelativeLayout
                    paragraphHeadingLayout!!.setOnClickListener { paragraphExpandableLayout!!.toggle() }
                    paragraphExpandableLayout!!.setListener(object : ExpandableLayoutListenerAdapter() {
                        override fun onPreOpen() {
                            super.onPreOpen()
                            createRotateAnimator(paragraphHeadingButtonHolder, 0f, 180f).start()
                        }

                        override fun onPreClose() {
                            super.onPreClose()
                            createRotateAnimator(paragraphHeadingButtonHolder, 180f, 0f).start()
                        }
                    })
                    //Expandable Paragraph View Initialization --End-- Suvajit


                    //PAR fragment Initialization --Start-- Suvajit
                    checkBoxOptionA = dialog.findViewById<View>(R.id.checkBoxA) as CheckBox
                    checkBoxOptionB = dialog.findViewById<View>(R.id.checkBoxB) as CheckBox
                    checkBoxOptionC = dialog.findViewById<View>(R.id.checkBoxC) as CheckBox
                    checkBoxOptionD = dialog.findViewById<View>(R.id.checkBoxD) as CheckBox
                    checkBoxOptionE = dialog.findViewById<View>(R.id.checkBoxE) as CheckBox
                    checkBoxOptionA!!.isEnabled = false
                    checkBoxOptionB!!.isEnabled = false
                    checkBoxOptionC!!.isEnabled = false
                    checkBoxOptionD!!.isEnabled = false
                    checkBoxOptionE!!.isEnabled = false

                    //PAR fragment Initialization --End-- Suvajit
                    questionNo.visibility = View.GONE
                    dataShow(examAllQuestionsSubjectWisePojo)
                    dialog.show()
                }

                override fun onDeleteButtonClick(examAllQuestionsSubjectWisePojo: ExamAllQuestionsSubjectWisePojo?) {
                    val isDeleted = examAllQuestionsSubjectWisePojo?.questionId?.let { cacheBookmark!!.removeBookmark(it) }
                    if (isDeleted!!) {
                        (examAllQuestionsSubjectWisePojoArrayList as ArrayList<ExamAllQuestionsSubjectWisePojo>?)?.remove(examAllQuestionsSubjectWisePojo)
                        bookmarksAdapter!!.notifyDataSetChanged()
                    } else {
                        Toast.makeText(context, "Something went wrong.", Toast.LENGTH_SHORT).show()
                    }
                    if ((examAllQuestionsSubjectWisePojoArrayList as ArrayList<ExamAllQuestionsSubjectWisePojo>?)!!.isEmpty()) {
                        showErrorLayout()
                    }
                }
            })
        }
    }

    fun dataShow(examAllQuestionsSubjectWisePojo: ExamAllQuestionsSubjectWisePojo?) {
        val quesType = examAllQuestionsSubjectWisePojo?.questionType


        //Expandable View Paragraph --Start-- Suvajit
        paragraphHeading!!.text = PARAGRAPH_HEADING
        paragraphExpandableLayout!!.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR))
        if (examAllQuestionsSubjectWisePojo?.essayData != "") {
            paragrapRelativeLayout!!.visibility = View.VISIBLE
            paragraphHeadingLayout!!.visibility = View.VISIBLE
            paragraphDataView!!.loadData(Html.fromHtml(examAllQuestionsSubjectWisePojo?.essayData?.trim { it <= ' ' }).toString() + "<br>", "text/html", "UTF-8")
        } else {
            paragrapRelativeLayout!!.visibility = View.GONE
            paragraphHeadingLayout!!.visibility = View.GONE
        }


        //Expandable View Paragraph --End-- Suvajit
        val questionData = examAllQuestionsSubjectWisePojo?.questionText

//        quest.getSettings().setJavaScriptEnabled(true);
        quest!!.loadData(Html.fromHtml(questionData?.trim { it <= ' ' }).toString(), "text/html", "UTF-8")


        //Radio Option --Start-- Suvajit
        val optionA = examAllQuestionsSubjectWisePojo?.optionA
        val optionB = examAllQuestionsSubjectWisePojo?.optionB
        val optionC = examAllQuestionsSubjectWisePojo?.optionC
        val optionD = examAllQuestionsSubjectWisePojo?.optionD
        val optionE = examAllQuestionsSubjectWisePojo?.optionE
        if (quesType != "SC") {
            showRadioQuestionOnFragmentLayout!!.visibility = View.GONE
            showCheckQuestionOnFragmentLayout!!.visibility = View.VISIBLE
            if (optionA != null && optionA != "") {
                linLayoutOptionA!!.visibility = View.VISIBLE
                webViewOptionA!!.loadData(Html.fromHtml(optionA).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionA!!.visibility = View.GONE
            }
            if (optionB != null && optionB != "") {
                linLayoutOptionB!!.visibility = View.VISIBLE
                webViewOptionB!!.loadData(Html.fromHtml(optionB).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionB!!.visibility = View.GONE
            }
            if (optionC != null && optionC != "") {
                linLayoutOptionC!!.visibility = View.VISIBLE
                webViewOptionC!!.loadData(Html.fromHtml(optionC).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionC!!.visibility = View.GONE
            }
            if (optionD != null && optionD != "") {
                linLayoutOptionD!!.visibility = View.VISIBLE
                webViewOptionD!!.loadData(Html.fromHtml(optionD).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionD!!.visibility = View.GONE
            }
            if (optionE != null && optionE != "") {
                linLayoutOptionE!!.visibility = View.VISIBLE
                webViewOptionE!!.loadData(Html.fromHtml(optionE).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionE!!.visibility = View.GONE
            }
        } else {
            showCheckQuestionOnFragmentLayout!!.visibility = View.GONE
            showRadioQuestionOnFragmentLayout!!.visibility = View.VISIBLE

            //Clear right wrong ans colors
            webViewOptionARadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            webViewOptionBRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            webViewOptionCRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            webViewOptionDRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            webViewOptionERadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            linLayoutOptionARadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            linLayoutOptionBRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            linLayoutOptionCRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            linLayoutOptionDRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            linLayoutOptionERadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
            if (optionA != null && optionA != "") {
                linLayoutOptionARadio!!.visibility = View.VISIBLE
                webViewOptionARadio!!.loadData(Html.fromHtml(optionA).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionARadio!!.visibility = View.GONE
            }
            if (optionB != null && optionB != "") {
                linLayoutOptionBRadio!!.visibility = View.VISIBLE
                webViewOptionBRadio!!.loadData(Html.fromHtml(optionB).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionBRadio!!.visibility = View.GONE
            }
            if (optionC != null && optionC != "") {
                linLayoutOptionCRadio!!.visibility = View.VISIBLE
                webViewOptionCRadio!!.loadData(Html.fromHtml(optionC).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionCRadio!!.visibility = View.GONE
            }
            if (optionD != null && optionD != "") {
                linLayoutOptionDRadio!!.visibility = View.VISIBLE
                webViewOptionDRadio!!.loadData(Html.fromHtml(optionD).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionDRadio!!.visibility = View.GONE
            }
            if (optionE != null && optionE != "") {
                linLayoutOptionERadio!!.visibility = View.VISIBLE
                webViewOptionERadio!!.loadData(Html.fromHtml(optionE).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionERadio!!.visibility = View.GONE
            }

            //Radio Option --End-- Suvajit
        }
    }

    private fun createRotateAnimator(target: View?, from: Float, to: Float): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(target, "rotation", from, to)
        animator.duration = 300
        animator.interpolator = Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR)
        return animator
    }

    private fun showErrorLayout() {
        findViewById<View>(R.id.tvBookmarksInstruction).visibility = View.GONE
        linLayoutNoData!!.visibility = View.VISIBLE
        textViewMessage!!.text = "No Bookmarks"
        textViewSubMessage!!.setText(R.string.bookmarkError)
        imageViewError!!.setImageResource(R.drawable.ic_no_data)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    companion object {
        private const val PARAGRAPH_HEADING = "Tap to show/hide Paragraph"
    }
}
package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.nta.ts2020.AppConstants.ConstantVariables.DATABASE_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.LocalStorage.ExamCacheFinalSubmission
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.SaveResultDataFromServerToCacheHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService

class ExamResultGridViewActivity : AppCompatActivity() {
    var id: String? = null
    private var progressDialog: ProgressDialog? = null
    private var context: Context? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var examName: String? = null
    private var isHorizontal = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_result)
        context = this
        val extras = intent.extras
        //        if (extras != null)
        id = extras!!.getString("ID")
        examName = extras.getString("EXAM_NAME")
        isHorizontal = extras.getBoolean("IS_HORIZONTAL")
        if (supportActionBar != null) {
            supportActionBar!!.setTitle(examName)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Enable/Disable compare with topper layout for quiz and exam
        val rvCompareWithTopper = findViewById<View>(R.id.rvResultCompareWithTopper) as RelativeLayout
        val tvCompareWithTopperLabel = findViewById<View>(R.id.tvResultCompareWithTopper) as TextView
        val ivCompareWithTopperIcon = findViewById<View>(R.id.ivResultCompareWithTopper) as ImageView
        if ("Q".equals(id!![0].toString(), ignoreCase = true)) {
            rvCompareWithTopper.setBackgroundResource(R.color.colorPrimaryDark)
            ivCompareWithTopperIcon.setImageResource(R.drawable.ic_compare_topper_disabled)
            tvCompareWithTopperLabel.setTextColor(ContextCompat.getColor(context as ExamResultGridViewActivity, R.color.material_grey_700))
        }
        rvCompareWithTopper.setOnClickListener {
            if (!"Q".equals(id!![0].toString(), ignoreCase = true)) {
                val intent = Intent(this@ExamResultGridViewActivity, ExamResultCompareWithTopperActivity::class.java)
                intent.putExtra("EXAM_NAME", examName)
                intent.putExtra("ID", id)
                //Start details activity
                startActivity(intent)
            } else {
                Toast.makeText(context, "Topper data not available for quiz.", Toast.LENGTH_SHORT).show()
            }
        }

        //Swipe refresh
        swipeRefreshLayout = findViewById<View>(R.id.swipeRefreshResultGrid) as SwipeRefreshLayout

        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true
        loadData()
        swipeRefreshLayout!!.setColorSchemeResources(
                R.color.material_blue_700,
                R.color.material_green_700,
                R.color.material_yellow_700,
                R.color.material_red_700)
        swipeRefreshLayout!!.setOnRefreshListener { loadData() }
    }

    private fun loadData() {
        if (isNetworkConnected) {
            progressDialog!!.show()
            val saveResultDataFromServerToCacheHandler = SaveResultDataFromServerToCacheHandler(this@ExamResultGridViewActivity)
            ThreadPoolService().saveResultDataFromServerToCache(context, id, isHorizontal, saveResultDataFromServerToCacheHandler)
        } else {
            if (ExamCacheFinalSubmission(context!!).getFinalSubmissionData(id).isEmpty()) {
                findViewById<View>(R.id.resultGridContainer).visibility = View.GONE
                findViewById<View>(R.id.resultGridError).visibility = View.VISIBLE
                val icon = findViewById<View>(R.id.ivExamResultListError) as ImageView
                icon.setImageResource(R.drawable.ic_no_network)
                val message = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
                message.setText(R.string.result_grid_error_msg)
            } else {
                findViewById<View>(R.id.resultGridContainer).visibility = View.VISIBLE
                findViewById<View>(R.id.resultGridError).visibility = View.GONE
            }
        }
        if (swipeRefreshLayout!!.isRefreshing) swipeRefreshLayout!!.isRefreshing = false
    }

    fun subjectWiseScore(view: View?) {

        //Create intent
        val intent = Intent(this@ExamResultGridViewActivity, ExamSubjectWiseScoreActivity::class.java)
        intent.putExtra("ID", id)
        //Start details activity
        startActivity(intent)
    }

    fun timeAnalysis(view: View?) {
        //Create intent
        val intent = Intent(this@ExamResultGridViewActivity, ExamResultTimeAnalysisActivity::class.java)
        intent.putExtra("ID", id)
        //Start details activity
        startActivity(intent)
    }

    fun questionWiseTime(view: View?) {

        //Create intent
        val intent = Intent(this@ExamResultGridViewActivity, ExamResultQuesWiseAnalysisActivity::class.java)
        intent.putExtra("EXAM_NAME", examName)
        intent.putExtra("ID", id)
        //Start details activity
        startActivity(intent)
    }

    fun questionAndAnswerKey(view: View?) {

        //Create intent
        val intent = Intent(this@ExamResultGridViewActivity, ExamResultQuesAndAnsKeyActivity::class.java)
        intent.putExtra("ID", id)
        //Start details activity
        startActivity(intent)
    }

    //    public void compareWithTopper(View view) {
    //
    //        //Create intent
    //        Intent intent = new Intent(ExamResultGridViewActivity.this, ExamResultCompareWithTopperActivity.class);
    //
    //        intent.putExtra("EXAM_NAME",examName);
    //        intent.putExtra("ID", id);
    //        //Start details activity
    //        startActivity(intent);
    //    }
    //deprecated
    //    public void otherActivity(View view) {
    //
    //        //Create intent
    //        Intent intent = new Intent(ExamResultGridViewActivity.this, ResultOtherActivity.class);
    //
    //        intent.putExtra("ID", id);
    //        //Start details activity
    //        startActivity(intent);
    //
    //    }
    fun dismissProgressDialog(status: Int) {
        when (status) {
            SUCCESS -> {
                findViewById<View>(R.id.resultGridContainer).visibility = View.VISIBLE
                findViewById<View>(R.id.resultGridError).visibility = View.GONE
            }
            NETWORK_ERROR, DATABASE_ERROR -> {
                findViewById<View>(R.id.resultGridContainer).visibility = View.GONE
                findViewById<View>(R.id.resultGridError).visibility = View.VISIBLE
                val icon = findViewById<View>(R.id.ivExamResultListError) as ImageView
                icon.setImageResource(R.drawable.ic_no_network)
                val message = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
                message.setText(R.string.result_grid_error_msg)
            }
        }
        progressDialog!!.dismiss()
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
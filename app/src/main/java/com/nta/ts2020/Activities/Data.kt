package com.nta.ts2020.Activities

import java.util.*

/**
 * Created by Workstation-PC on 03-10-2018.
 */
class Data {
    var title: String? = null

    var subject: String? = null

    var videoId: String? = null

    var icon: String? = null

}

internal class Response {
    var result: String? = null

    var pageno: String? = null

    var totalRecord = 0

    var limit: String? = null

    var iconPrefixUrl: String? = null

    var data: ArrayList<Data>? = null

}

internal class RootObject {
    var response: Response? = null

}
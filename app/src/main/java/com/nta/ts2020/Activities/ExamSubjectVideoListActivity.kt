package com.nta.ts2020.Activities

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.VolleyLog
import com.android.volley.toolbox.StringRequest
import com.nta.ts2020.PaginationAdapter
import com.nta.ts2020.PojoClasses.ExamResult
import com.nta.ts2020.R
import me.relex.circleindicator.CircleIndicator
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ExamSubjectVideoListActivity : AppCompatActivity() {
    private val SliderDataList = ArrayList<String>()
    private val SliderDataListredirect = ArrayList<String>()
    var subjectname: String? = null
    var timer: Timer? = null
    val DELAY_MS: Long = 1000
    val PERIOD_MS: Long = 5000
    private var mViewPager: ViewPager? = null
    var indicator: CircleIndicator? = null
    var adpter: ExamImagesViewPagerCustomPagerAdapter? = null
    private var pagercpage = 0
    var subname: String? = null
    var adapter: PaginationAdapter? = null
    var linearLayoutManager: LinearLayoutManager? = null
    var recyclerView: RecyclerView? = null
    var isLoading1 = false
    var isLastPage1 = false
    var totalpage = 0
    var divide = 0
    var totalPageCount1 = 14
    var currentPage = PAGE_START
    var videoService: ExamVideoService? = null
    var layoutError: LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subject_video_list)
        subname = intent.getStringExtra("subjectname")
        recyclerView = findViewById<View>(R.id.main_recycler) as RecyclerView
        adapter = PaginationAdapter(this@ExamSubjectVideoListActivity)
        linearLayoutManager = LinearLayoutManager(this@ExamSubjectVideoListActivity, LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = adapter
        recyclerView!!.addOnScrollListener(object : ExamPaginationScrollListener(linearLayoutManager!!) {
            override fun loadMoreItems() {
                this@ExamSubjectVideoListActivity.isLoading1 = true
                currentPage += 1
                loadNextPage()
            }

            override val totalPageCount: Int = totalPageCount1


            override val isLastPage: Boolean = isLastPage1


            override val isLoading: Boolean = isLoading1

        })
        videoService = ExamVideoApiBaseUrl.client?.create(ExamVideoService::class.java)
        loadFirstPage()
        mViewPager = findViewById<View>(R.id.pager) as ViewPager
        indicator = findViewById<View>(R.id.indicator) as CircleIndicator
        val handler = Handler()
        val Update = Runnable {
            if (pagercpage == 6 - 1) {
                pagercpage = 0
            }
            mViewPager!!.setCurrentItem(pagercpage++, true)
        }
        timer = Timer() // This will create a new Thread
        timer!!.schedule(object : TimerTask() {
            // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)
        subjectname = intent.getStringExtra("subjectname")
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.title = subjectname
        }
        makeJsonObjectRequest2("http://examin.co.in/examin-admin/android_ad_footer/footer_android_ad.php")
    }

    private fun loadFirstPage() {
        Log.d(TAG, "loadFirstPage: ")
        callTopRatedMoviesApi()?.enqueue(object : Callback<ExamVideos?> {
            override fun onResponse(call: Call<ExamVideos?>, response: Response<ExamVideos?>) {
                totalpage = getPages(response)
                Toast.makeText(this@ExamSubjectVideoListActivity, "Total Pages $totalpage", Toast.LENGTH_SHORT).show()
                divide = totalpage / 10
                divide += 1
                TotalPages = divide
                val results = fetchResults(response)
                adapter!!.addAll(results)
                if (currentPage <= divide) adapter!!.addLoadingFooter() else isLastPage1 = true
            }

            override fun onFailure(call: Call<ExamVideos?>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun callTopRatedMoviesApi(): Call<ExamVideos?>? {
        return videoService!!.getVideosList(subname, currentPage)
    }

    private fun fetchResults(response: Response<ExamVideos?>): List<ExamResult> {
        val topRatedMovies = response.body()
        return topRatedMovies!!.data
    }

    private fun getPages(response: Response<ExamVideos?>): Int {
        val topRatedMovies = response.body()
        return topRatedMovies!!.total_record
    }



    private fun loadNextPage() {
        Log.d(TAG, "loadNextPage: $currentPage")
        callTopRatedMoviesApi()?.enqueue(object : Callback<ExamVideos?> {
            override fun onResponse(call: Call<ExamVideos?>, response: Response<ExamVideos?>) {
                adapter!!.removeLoadingFooter()
                isLoading1 = false
                val results = fetchResults(response)
                adapter!!.addAll(results)
                if (currentPage != divide) adapter!!.addLoadingFooter() else isLastPage1 = true
            }

            override fun onFailure(call: Call<ExamVideos?>, t: Throwable) {
                t.printStackTrace()
                layoutError!!.visibility = View.VISIBLE
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun makeJsonObjectRequest2(url: String) {
        val jsonObjReq = StringRequest(Request.Method.GET,
                url, com.android.volley.Response.Listener { response ->
            Log.e("RESPONSE", response)
            parseActivityName2(response)
        },
                com.android.volley.Response.ErrorListener { error ->
                    VolleyLog.e("tagg", "Error: " + error.message)
                    Toast.makeText(this@ExamSubjectVideoListActivity, error.message, Toast.LENGTH_SHORT).show()
                })
        jsonObjReq.retryPolicy = DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        AppController.instance?.addToRequestQueue(jsonObjReq)
    }

    private fun parseActivityName2(res: String) {
        try {
            val jsonObject = JSONObject(res)
            val jsonObject1 = jsonObject.getJSONObject("response")
            val jsonArray = jsonObject1.getJSONArray("ad_urls")
            for (i in 0 until jsonArray.length()) {
                val jsonObject2 = jsonArray.getJSONObject(i)
                val videoid = jsonObject2.getString("ad_image_url")
                val redirecturl = jsonObject2.getString("ad_redirect_url")
                SliderDataList.add("http://examin.co.in/admin-panel/footer_app_ad/$videoid")
                SliderDataListredirect.add(redirecturl)
                adpter = ExamImagesViewPagerCustomPagerAdapter(this@ExamSubjectVideoListActivity, SliderDataList, SliderDataListredirect)
                mViewPager!!.adapter = adpter
            }
        } catch (e: JSONException) {
            Log.e("tagg", "Json parsing error: " + e.message)
            runOnUiThread {
                Toast.makeText(this@ExamSubjectVideoListActivity,
                        "Json parsing error: " + e.message,
                        Toast.LENGTH_LONG)
                        .show()
            }
        }
    }

    companion object {
        var TotalPages = 0
        private const val TAG = "MainActivity"
        const val PAGE_START = 1
    }
}



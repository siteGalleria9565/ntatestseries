package com.nta.ts2020.Activities

import android.app.AlertDialog
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.R

class WebViewActivity : AppCompatActivity(), View.OnClickListener {
    var fwdbtn: ImageButton? = null
    var backbtn: ImageButton? = null
    var refresh_btn: ImageButton? = null
    var close_btn: ImageButton? = null
    var webview: WebView? = null
    private var processDialog: ProgressDialog? = null
    var Url: String? = null
    var IsloadFirst = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view2)
        fwdbtn = findViewById<View>(R.id.fwdbtn) as ImageButton
        backbtn = findViewById<View>(R.id.bckbtn) as ImageButton
        backbtn!!.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
        fwdbtn!!.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
        close_btn = findViewById<View>(R.id.close_btn) as ImageButton
        refresh_btn = findViewById<View>(R.id.refresh_btn) as ImageButton
        //        share_btn = (Button) rootView.findViewById(R.id.share_btn);
        webview = findViewById<View>(R.id.webview) as WebView
        webview!!.settings.javaScriptEnabled = true

        backbtn!!.setOnClickListener(this)
        close_btn!!.setOnClickListener(this)
        //        share_btn.setOnClickListener(Webview.this);
        refresh_btn!!.setOnClickListener(this)
        fwdbtn!!.setOnClickListener(this)
        if (intent != null) {
            Url = intent.getStringExtra("URL")
        }
        updateURL()

        webview!!.webViewClient = Callback()
    }

    private fun updateURL() {
        webview?.loadUrl(Url)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.fwdbtn -> if (webview!!.canGoForward()) {
                webview!!.goForward()
            }
            R.id.bckbtn -> if (webview!!.canGoBack()) {
                webview!!.goBack()
            }
            R.id.close_btn -> onBackPressed()
            R.id.refresh_btn -> webview!!.loadUrl("javascript:window.location.reload( true )")
        }
    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        //                builder.setTitle("Scholarship");
        builder.setMessage("Are you sure want to exit ?")
        builder.setNegativeButton("NO") { dialog, id -> // TODO Auto-generated method stub
            dialog.dismiss()
        }
        builder.setPositiveButton("YES") { dialogInterface, i ->
            finish()
            dialogInterface.dismiss()
        }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    private inner class Callback :WebViewClient(){
        override fun onPageFinished(view: WebView?, url: String?) {
                        if (processDialog != null && processDialog!!.isShowing) {
                processDialog!!.dismiss()
                processDialog = null
                IsloadFirst = !IsloadFirst
            }
            refresh_btn!!.isEnabled = true
            refresh_btn!!.setImageResource(R.drawable.ic_refresh)
            if (!webview!!.canGoForward()) {
                fwdbtn!!.isEnabled = false
                //                    fwdbtn.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            } else {
                fwdbtn!!.isEnabled = true
                fwdbtn!!.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
            }
            if (!webview!!.canGoBack()) {
                backbtn!!.isEnabled = false
                //                    fwdbtn.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            } else {
                backbtn!!.isEnabled = true
                fwdbtn!!.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
            }
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            if (IsloadFirst) {
                if (processDialog == null || !processDialog!!.isShowing) {
                    processDialog = ProgressDialog(this@WebViewActivity)
                    processDialog!!.show()
                    processDialog!!.setCancelable(false)
                    val window = processDialog!!.window
                    window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    processDialog!!.setContentView(R.layout.process_dialog)
                }
            }
        }

        override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
            Url = url
            refresh_btn!!.isEnabled = false
            refresh_btn!!.setImageResource(R.drawable.ic_close_white)
            refresh_btn!!.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
            view.loadUrl(url)
            return false
        }
    }

//    private inner class Callback : WebViewClient() {
//        override fun onPageFinished(view: WebView, url: String) {
////            if (processDialog != null && processDialog!!.isShowing) {
////                processDialog!!.dismiss()
////                processDialog = null
////                IsloadFirst = !IsloadFirst
////            }
////            refresh_btn!!.isEnabled = true
////            refresh_btn!!.setImageResource(R.drawable.ic_refresh)
////            if (!webview!!.canGoForward()) {
////                fwdbtn!!.isEnabled = false
////                //                    fwdbtn.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
////            } else {
////                fwdbtn!!.isEnabled = true
////                fwdbtn!!.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
////            }
////            if (!webview!!.canGoBack()) {
////                backbtn!!.isEnabled = false
////                //                    fwdbtn.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
////            } else {
////                backbtn!!.isEnabled = true
////                fwdbtn!!.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
////            }
//        }
//
//        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {

//            super.onPageStarted(view, url, favicon)
//        }
//
//        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
////            Url = url
////            refresh_btn!!.isEnabled = false
////            refresh_btn!!.setImageResource(R.drawable.ic_close_white)
////            refresh_btn!!.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
////            view.loadUrl(url)
//            return false
//        }
//    }
}
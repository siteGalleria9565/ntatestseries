package com.nta.ts2020.Activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.R
import java.util.*

class ExamSubjectsListAdapter(var context: Context, var trainDataModelArrayList: ArrayList<String>) : RecyclerView.Adapter<ExamSubjectsListAdapter.ViewHolder>() {
    var view1: View? = null
    var viewHolder1: ViewHolder? = null
    var i = 1

    class ViewHolder(v: View?) : RecyclerView.ViewHolder(v!!) {
        var subjectnametv: TextView

        init {
            subjectnametv = v!!.findViewById(R.id.subjectnametv)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        view1 = LayoutInflater.from(context).inflate(R.layout.single_subject_names, parent, false)
        viewHolder1 = ViewHolder(view1)
        return viewHolder1!!
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.subjectnametv.text = trainDataModelArrayList[position]
        holder.subjectnametv.setOnClickListener {
            val intent = Intent(context, ExamSubjectVideoListActivity::class.java)
            intent.putExtra("subjectname", trainDataModelArrayList[position])
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return trainDataModelArrayList.size
    }

}
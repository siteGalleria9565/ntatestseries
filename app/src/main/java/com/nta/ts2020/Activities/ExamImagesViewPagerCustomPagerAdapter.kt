package com.nta.ts2020.Activities

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.nta.ts2020.R
import java.util.*

class ExamImagesViewPagerCustomPagerAdapter(var mContext: Context, var arrayList: ArrayList<String>, var arrayListredirect: ArrayList<String>) : PagerAdapter() {
    var mLayoutInflater: LayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
        return arrayList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = mLayoutInflater.inflate(R.layout.pager_imageview, container, false)
        val imageviewdp: ImageView
        imageviewdp = itemView.findViewById<View>(R.id.imageviewdp) as ImageView
        Glide.with(mContext)
                .load(arrayList[position])
                .into(imageviewdp)
        imageviewdp.setOnClickListener {
            val intent = Intent(mContext, ExamWebViewActivity::class.java)
            intent.putExtra("url", arrayListredirect[position])
            mContext.startActivity(intent)
        }
        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

}
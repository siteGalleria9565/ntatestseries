package com.nta.ts2020.Activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.nta.ts2020.Activities.EbookQuestionPaperListActivity
import com.nta.ts2020.Adapters.EbookQuestionPaperAdapter
import com.nta.ts2020.Adapters.EbookQuestionPaperAdapter.OnButtonClickListeners
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.PojoClasses.QuestionPaperPDF
import com.nta.ts2020.R
import java.io.File
import java.util.*

class EbookQuestionPaperListActivity : AppCompatActivity() {
    private var gridView: GridView? = null
    private var linLayoutNoData: LinearLayout? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var imageViewError: ImageView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var context: EbookQuestionPaperListActivity? = null
    var eBookPojoArrayList = ArrayList<QuestionPaperPDF>()
    private var name: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ebook_question_paper_list)
        if (intent.extras != null) {
            eBookPojoArrayList = intent.getParcelableArrayListExtra("PDF_LIST")
            name = intent.getStringExtra("NAME")
        }
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setTitle(name + " " + eBookPojoArrayList[0].year)
        }
        context = this
        gridView = findViewById<View>(R.id.gvEBookList) as GridView
        //Message Layout
        linLayoutNoData = findViewById<View>(R.id.llEBookListError) as LinearLayout
        textViewMessage = findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        imageViewError = findViewById<View>(R.id.ivExamResultListError) as ImageView
        swipeRefreshLayout = findViewById<View>(R.id.swRefreshEBookList) as SwipeRefreshLayout
        loadData(SUCCESS)
    }

    fun loadData(status: Int) {
        //fetch data from cache
        var status = status
        if (eBookPojoArrayList.isEmpty() && status != NETWORK_ERROR) status = NO_DATA
        if (status == SUCCESS) {
            linLayoutNoData!!.visibility = View.GONE
            gridView!!.visibility = View.VISIBLE
            val eBookListAdapter = EbookQuestionPaperAdapter(eBookPojoArrayList, object : OnButtonClickListeners {
                override fun onDownloadButtonClick(eBookPojo: QuestionPaperPDF?, viewHolder: EbookQuestionPaperAdapter.ViewHolder?) {
                    if (checkAndRequestPermissions()) {
                        if (isNetworkConnected) {
//                            String url = eBookPojo.getFile_link().replaceAll(" ", "%20");
                            val intent = Intent(this@EbookQuestionPaperListActivity, ExamPdfOpenActivity::class.java)
                            intent.putExtra("url", eBookPojo?.file_link)
                            startActivity(intent)
                        } else {
                            Toast.makeText(context, "File added to the queue. Please connect to the internet to download.", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

                override fun onOpenButtonClick(eBookPojo: QuestionPaperPDF?) {
                    val builder = CustomTabsIntent.Builder()
                    val customTabsIntent = builder.build()
                    customTabsIntent.launchUrl(this@EbookQuestionPaperListActivity, Uri.parse(eBookPojo?.file_link))
//                    var pdfName: String? = "Pdf View"
//                    if (checkAndRequestPermissions()) pdfName = eBookPojo?.paper_title
//                    startActivity(Intent(context, ExamPdfViewActivity::class.java)
//                            .putExtra("PDF_NAME", pdfName)
//                            .putExtra("PDF", pdf.toString()))
                }
            })
            //        gridView.addItemDecoration(new QuizDividerItemDecoration(this));
            gridView!!.adapter = eBookListAdapter
        } else if (status == NO_DATA) {
            linLayoutNoData!!.visibility = View.VISIBLE
            gridView!!.visibility = View.GONE
            textViewMessage!!.setText(R.string.result_list_message_no_data)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_no_data)
            imageViewError!!.setImageResource(R.drawable.ic_no_data)
        } else if (status == NETWORK_ERROR) {
            linLayoutNoData!!.visibility = View.VISIBLE
            gridView!!.visibility = View.GONE
            textViewMessage!!.setText(R.string.result_list_message_network_error)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_network_error)
            imageViewError!!.setImageResource(R.drawable.ic_no_network)
        }
        if (swipeRefreshLayout!!.isRefreshing) {
            swipeRefreshLayout!!.isRefreshing = false
        }
    }

    private val isNetworkConnected: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun checkAndRequestPermissions(): Boolean {
        val writePermission = ContextCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val listPermissionNeeded: MutableList<String> = ArrayList()
        if (writePermission != PackageManager.PERMISSION_GRANTED) listPermissionNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!listPermissionNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context!!, listPermissionNeeded.toTypedArray(), PERMISSION_REQUEST)
            return false
        }
        return true
    }

    companion object {
        private const val PERMISSION_REQUEST = 0
    }
}
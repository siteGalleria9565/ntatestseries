package com.nta.ts2020.Activities

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.github.aakira.expandablelayout.Utils
import com.nta.ts2020.Adapters.QuizSubjectRecyclerViewAdapter
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.LocalStorage.QuizAssetToLocalDatabase
import com.nta.ts2020.LocalStorage.QuizCacheAllChapterData
import com.nta.ts2020.LocalStorage.QuizCacheAllSubjectData
import com.nta.ts2020.LocalStorage.QuizCacheSetCurrentSessionData
import com.nta.ts2020.PojoClasses.QuizChapter
import com.nta.ts2020.PojoClasses.QuizSubChapterListItemModel
import com.nta.ts2020.PojoClasses.QuizSubject
import com.nta.ts2020.PojoClasses.QuizSubjectChapterCombined
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.QuizSaveSubChapFromServerToCacheHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import java.util.*

class ExamQuizSelectionActivity : AppCompatActivity(), View.OnClickListener {
    private var quizCacheAllChapterDataInstance: QuizCacheAllChapterData? = null
    private var quizCacheAllSubjectDataInstance: QuizCacheAllSubjectData? = null
    private var context: Activity? = null
    var mQuizAssetToLocalDatabase: QuizAssetToLocalDatabase? = null
    private var status = ""
    private var checkCountStatus = 0
    private var buttonNext: Button? = null
    private var recyclerView: RecyclerView? = null
    val progressDialog: ProgressDialog? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var linLayoutNoData: LinearLayout? = null
    private var imageViewError: ImageView? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.quiz_activity_selection)
        if (supportActionBar != null) {
            supportActionBar!!.title = "Quiz"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Initializations and declarations
        context = this
        recyclerView = findViewById<View>(R.id.recyclerView) as RecyclerView
        linLayoutNoData = findViewById<View>(R.id.quizListError) as LinearLayout
        imageViewError = findViewById<View>(R.id.ivExamResultListError) as ImageView
        textViewMessage = findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        buttonNext = findViewById<View>(R.id.buttonNext) as Button
        val buttonResume = findViewById<View>(R.id.buttonResumeQuiz) as Button
        buttonNext!!.visibility = View.INVISIBLE
        buttonResume.setOnClickListener(this)

        //Initialize DbHandler variable for future use
        val quizSaveSubChapFromServerToCacheHandler = QuizSaveSubChapFromServerToCacheHandler(this)
        quizCacheAllChapterDataInstance = QuizCacheAllChapterData(context as ExamQuizSelectionActivity)
        quizCacheAllSubjectDataInstance = QuizCacheAllSubjectData(context as ExamQuizSelectionActivity)
        val quizCacheSetCurrentSessionData = QuizCacheSetCurrentSessionData(context as ExamQuizSelectionActivity)
        mQuizAssetToLocalDatabase = QuizAssetToLocalDatabase(this)
        mQuizAssetToLocalDatabase!!.writableDatabase
        swipeRefreshLayout = findViewById<View>(R.id.swRefreshQuizSelection) as SwipeRefreshLayout
        swipeRefreshLayout!!.setColorSchemeResources(
                R.color.material_blue_700,
                R.color.material_green_700,
                R.color.material_yellow_700,
                R.color.material_red_700)
        if (isNetworkConnected) {
            swipeRefreshLayout!!.isRefreshing = true
            // new ThreadPoolService().saveEBookListFromServerToCache(context, subjectId, eBookListHandler, eBookType);
            ThreadPoolService().quizSaveSubChapFromServerToCache(context, quizSaveSubChapFromServerToCacheHandler)
        }
        swipeRefreshLayout!!.setOnRefreshListener {
            if (isNetworkConnected) {
                // new ThreadPoolService().saveEBookListFromServerToCache(context, finalSubjectId, eBookListHandler, eBookType);
                ThreadPoolService().quizSaveSubChapFromServerToCache(context, quizSaveSubChapFromServerToCacheHandler)
            } else {
                loadData(ConstantVariables.SUCCESS)
            }
        }
        if (quizCacheSetCurrentSessionData.currentExamSession != null && quizCacheSetCurrentSessionData.currentExamSession.size != 0) {
            status = quizCacheSetCurrentSessionData.currentExamSession[0].examStatus.toString()
            checkCountStatus = quizCacheSetCurrentSessionData.currentExamSession[0].countResum!!.toInt()
        }
        if (status == "Resume" && checkCountStatus <= 3) {
            buttonResume.visibility = View.VISIBLE
        } else {
            buttonResume.visibility = View.GONE
            status = "Initial"
            mQuizAssetToLocalDatabase!!.deleteData()
        }
        if (!isNetworkConnected) {
            loadData(ConstantVariables.SUCCESS)
        }
    }

    fun loadData(status: Int) {
        //TODO: Move both button listeners to the implemented OnclickListener
        //Show quizSubject names and chapter names on expandable menu
//        recyclerView.addItemDecoration(new QuizDividerItemDecoration(context));
        var status = status
        val data: MutableList<QuizSubChapterListItemModel> = ArrayList()
        val allSubList = quizCacheAllSubjectDataInstance!!.allSubs
        if (allSubList.isEmpty() && status != ConstantVariables.NETWORK_ERROR) status = ConstantVariables.NO_DATA
        if (status == ConstantVariables.SUCCESS) {
            linLayoutNoData!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE
            buttonNext!!.visibility = View.VISIBLE
            recyclerView!!.layoutManager = LinearLayoutManager(context)
            for (i in allSubList.indices) {
                val quizSubject = allSubList[i]
                val quizChapters = quizCacheAllChapterDataInstance!!.getChapters(quizSubject.subId)
                val quizSubjectChapterCombined = QuizSubjectChapterCombined(quizSubject, quizChapters)
                data.add(QuizSubChapterListItemModel(
                        quizSubjectChapterCombined,
                        R.color.expandableLayoutHeading,
                        R.color.white,
                        Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR))) //Style of expand animation
            }
            recyclerView!!.adapter = QuizSubjectRecyclerViewAdapter(data)
        } else if (status == ConstantVariables.NO_DATA) {
            linLayoutNoData!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
            buttonNext!!.visibility = View.INVISIBLE
            textViewMessage!!.setText(R.string.result_list_message_no_data)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_no_data)
            imageViewError!!.setImageResource(R.drawable.ic_no_data)
        } else if (status == ConstantVariables.NETWORK_ERROR) {
            linLayoutNoData!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
            buttonNext!!.visibility = View.INVISIBLE
            textViewMessage!!.setText(R.string.result_list_message_network_error)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_network_error)
            imageViewError!!.setImageResource(R.drawable.ic_no_network)
        }
        if (swipeRefreshLayout!!.isRefreshing) {
            swipeRefreshLayout!!.isRefreshing = false
        }
        buttonNext!!.setOnClickListener {
            val dialog = Dialog(context!!, R.style.Dialog_NoTitle)
            dialog.setContentView(R.layout.quiz_dialog_selected_chapters)
            val listViewDi0alogChapterlist = dialog.findViewById<View>(R.id.listViewDialogChapter) as ListView
            val dialogButton = dialog.findViewById<View>(R.id.buttonDialogStartQuiz) as Button
            val spinnerSelectTime = dialog.findViewById<View>(R.id.spinnerSelectTime) as Spinner
            val spinnerSelectLanguage = dialog.findViewById<View>(R.id.spinnerSelectLanguage) as Spinner
            dialogButton.setOnClickListener {
                val intent = Intent(context, ExamShowQuestionInstructionActivity::class.java)
                intent.putExtra("chapter_ids", QuizChapter.chapterIds.toString())
                intent.putExtra("num_of_subjects", QuizSubject.subjectIds.size.toString())
                intent.putExtra("selected_time", spinnerSelectTime.selectedItem.toString())
                intent.putExtra("selected_language", spinnerSelectLanguage.selectedItem.toString())
                intent.putExtra("Status", "Initial")
                intent.putExtra("TYPE", "QUIZ")
                dialog.dismiss()
                startActivity(intent)
                finish()
            }

            //QuizChapter List
            val chapterList = arrayOfNulls<String>(QuizChapter.chapterIds.size)
            for (i in QuizChapter.chapterIds.indices) {
                chapterList[i] = quizCacheAllChapterDataInstance!!.getChapterName(QuizChapter.chapterIds[i].toInt())
            }
            val adapter: ArrayAdapter<*> = ArrayAdapter(context!!, R.layout.quiz_dialog_selected_chapters_row, R.id.textViewDialogChapterName, chapterList)
            listViewDi0alogChapterlist.adapter = adapter

            //Spinner Select Time
            val timeCount = Math.min(QuizSubject.subjectIds.size, 6) * 10 //Math.min() is used so that max base 'timeCount' can be 60 so that it does not exceed 180 min which is 3hrs
            val timeList = arrayOf(timeCount.toString(), (timeCount * 2).toString(), (timeCount * 3).toString())
            val adapterSpinner = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, timeList)
            adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerSelectTime.adapter = adapterSpinner
            val adapterSpinnerLanguage = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, arrayOf("English", "Hindi"))
            adapterSpinnerLanguage.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerSelectLanguage.adapter = adapterSpinnerLanguage
            if (QuizSubject.subjectIds.size > 0) {
                dialog.show()
            } else {
                Toast.makeText(context, "Please select at least one chapter", Toast.LENGTH_SHORT).show()
            }
        }
    }

    //Check if connected to a network
    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    override fun onDestroy() {
        super.onDestroy()
        QuizChapter.chapterIds.clear()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.buttonResumeQuiz -> {
                val intent = Intent(context, ExamShowQuestionInstructionActivity::class.java)
                intent.putExtra("Status", status)
                intent.putExtra("TYPE", "QUIZ")
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
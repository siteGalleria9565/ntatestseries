package com.nta.ts2020.Activities

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.google.gson.Gson
import com.nta.ts2020.Adapters.ScholarShipAdapter
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.Interfaces.OnScholarshipApplyClick
import com.nta.ts2020.PojoClasses.ScholerShipPojo
import com.nta.ts2020.R
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ScholarShip : AppCompatActivity(), OnScholarshipApplyClick, SearchView.OnQueryTextListener, WebResponseListener {
    var scholerShipPojoList: MutableList<ScholerShipPojo?> = ArrayList()
    var scholarShipAdapter: ScholarShipAdapter? = null
    var recyclerView: RecyclerView? = null
    var gson: Gson? = null
    var swipeRefreshLayout: SwipeRefreshLayout? = null
    var llEBookquestionpaperError: LinearLayout? = null
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.scholarship_menu, menu)
        val searchItem = menu.findItem(R.id.search_scholarship)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scholar_ship)
        llEBookquestionpaperError = findViewById(R.id.llEBookquestionpaperError)
        swipeRefreshLayout = findViewById(R.id.swRefreshscholership)
        gson = Gson()
        title = "Scholarships"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        recyclerView = findViewById(R.id.scholarship_RV)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        recyclerView?.layoutManager = layoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()
        setValues()
        swipeRefreshLayout?.setOnRefreshListener(OnRefreshListener {
            if (isNetworkConnected) {
                setValues()
            } else {
                recyclerView?.visibility = View.GONE
                llEBookquestionpaperError?.visibility = View.VISIBLE
                swipeRefreshLayout?.isRefreshing = false
            }
        })
    }

    fun setValues() {
        if (isNetworkConnected) {
            swipeRefreshLayout!!.isRefreshing = true
            WebRequest(this).GET_METHOD(ConstantVariables.URL_LOAD_SCHOLARSHIPS, this, WebCallType.LOAD_SCHOLARSHIPS, false)
        } else {
            swipeRefreshLayout!!.isRefreshing = false
            llEBookquestionpaperError!!.visibility = View.VISIBLE
        }
    }

    fun filter(text: String) {
        val temp: MutableList<ScholerShipPojo?> = ArrayList()
        for (d in scholerShipPojoList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d?.title!!.toLowerCase().contains(text.toLowerCase())) {
                temp.add(d)
            }
        }
        //update recyclerview
        scholarShipAdapter!!.updateList(temp)
    }

    override fun onQueryTextSubmit(s: String): Boolean {
        filter(s)
        return false
    }

    override fun onQueryTextChange(s: String): Boolean {
        filter(s)
        return false
    }

    override fun onError(message: String?) {
        swipeRefreshLayout!!.isRefreshing = false
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        if (webCallType === WebCallType.LOAD_SCHOLARSHIPS) {
            scholerShipPojoList = ArrayList()
            var jsonObject: JSONObject? = null
            try {
                jsonObject = JSONObject(response.toString())
                val jsonArray = jsonObject.getJSONArray("Scholarships")
                for (i in 0 until jsonArray.length()) {
                    val jsonObject1 = jsonArray.getJSONObject(i)
                    Log.d(TAG, jsonObject1.toString())
                    val scholerShipPojo = ScholerShipPojo()
                    scholerShipPojo.title = jsonObject1.getString("title")
                    scholerShipPojo.description = jsonObject1.getString("small_desc")
                    scholerShipPojo.url = jsonObject1.getString("url")
                    scholerShipPojo.setcompleteDesc(jsonObject1.getString("complete_desc"))
                    scholerShipPojoList.add(scholerShipPojo)
                }
                scholarShipAdapter = ScholarShipAdapter(scholerShipPojoList, this, this)
                recyclerView!!.adapter = scholarShipAdapter
                recyclerView!!.visibility = View.VISIBLE
                swipeRefreshLayout!!.isRefreshing = false
                llEBookquestionpaperError!!.visibility = View.GONE
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun onClick(scholerShipPojo: ScholerShipPojo, view: String) {
        if (view == "linear_layout") {
            val intent = Intent(this@ScholarShip, ScholarshipDescription::class.java)
            intent.putExtra("complete_desc", scholerShipPojo.getcompleteDesc())
            intent.putExtra("url", scholerShipPojo.url)
            intent.putExtra("title", scholerShipPojo.title)
            startActivity(intent)
        } else {
            val httpIntent = Intent(Intent.ACTION_VIEW)
            httpIntent.data = Uri.parse(scholerShipPojo.url)
            startActivity(httpIntent)
        }
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    companion object {
        private const val TAG = "Scholarship"
    }
}
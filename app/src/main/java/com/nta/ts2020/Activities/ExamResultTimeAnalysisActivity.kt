package com.nta.ts2020.Activities

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.nta.ts2020.Adapters.ResultTimeAnalysisAdapter
import com.nta.ts2020.LocalStorage.CacheResultList
import com.nta.ts2020.LocalStorage.ExamCacheFinalSubmission
import com.nta.ts2020.PojoClasses.FinalSubmissionPojo
import com.nta.ts2020.PojoClasses.ResultListPojo
import com.nta.ts2020.PojoClasses.ShowSubjectWiseTimeAnalysisDataInTablePojo
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.ResultTimeAnalysisCustomTableView
import de.codecrafters.tableview.model.TableColumnWeightModel
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders
import java.util.*

class ExamResultTimeAnalysisActivity : AppCompatActivity() {
    // Collections ....
    var finalSubmissionPojosList: ArrayList<FinalSubmissionPojo>? = null
    private var productList: ArrayList<ShowSubjectWiseTimeAnalysisDataInTablePojo>? = null
    var cacheResultListList: ArrayList<ResultListPojo>? = null

    //Cache
    var cacheResultListInstance: CacheResultList? = null
    var examCacheFinalSubmissionInstance: ExamCacheFinalSubmission? = null

    //HashMap
    var subjectBasedOnId: HashMap<String, String>? = null
    var subjectWiseMarks: Float? = null
    var Percentage = 0f

    //TextView
    private var examName: TextView? = null
    private var subjectWiseMaxMarks = 0
    private val TAG = "RsltTimeAnlysisActvty"
    private var context: AppCompatActivity? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_time_analysis)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.title = "Time Analysis"
        }
        context = this
        initialization()
        var id: String? = null
        val extras = intent.extras
        if (extras != null) {
            id = extras.getString("ID")
        }

        //Final Data Cache
        finalSubmissionPojosList = examCacheFinalSubmissionInstance!!.getFinalSubmissionData(id)
        cacheResultListList = cacheResultListInstance!!.getResultListFromCache(id)
        val timeCalculate = finalSubmissionPojosList!![0].allottedTime.toLong()
        for (i in cacheResultListList?.get(0)?.subjectIds!!.indices) subjectBasedOnId!![cacheResultListList?.get(0)?.subjectIds!![i]] = cacheResultListList?.get(0)?.subjectNames!![i]
        val subWiseMarksList = finalSubmissionPojosList!![0].subWiseMarks

        /*
             *  totalRightAns+" "+totalAttemptQuestion+" "+totalSkipedQuestion+" "+totalRightMarks+" "+totalWrongMarks;
            */
        val hour = timeCalculate / 3600
        val minutes = timeCalculate / 60 % 60
        val second = timeCalculate % 60
        val timeStamp = hour.toString() + ":" + minutes + ":" + second + "0"
        examName!!.text = cacheResultListList?.get(0)?.examName

        /*
        * Show Table subject wise data
        * */productList = ArrayList()
        AddValuesToBARENTRY()
        val customTableView = findViewById<View>(R.id.ctvResultTimeAnalysis) as ResultTimeAnalysisCustomTableView
        if (customTableView != null) {
            val tableColumnWeightModel = TableColumnWeightModel(4)
            tableColumnWeightModel.setColumnWeight(0, 6)
            tableColumnWeightModel.setColumnWeight(1, 6)
            tableColumnWeightModel.setColumnWeight(2, 6)
            tableColumnWeightModel.setColumnWeight(3, 7)
            customTableView.columnModel = tableColumnWeightModel
            val rowColorEven = ContextCompat.getColor(context as ExamResultTimeAnalysisActivity, R.color.table_data_row_even)
            val rowColorOdd = ContextCompat.getColor(context as ExamResultTimeAnalysisActivity, R.color.table_data_row_odd)
            customTableView.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(rowColorEven, rowColorOdd))
            val resultTimeAnalysisAdapter = ResultTimeAnalysisAdapter(context, productList)
            customTableView.dataAdapter = resultTimeAnalysisAdapter
            val simpleTableHeaderAdapter = SimpleTableHeaderAdapter(context, "Subject Name", "Percent", "Your Time", "Topper Time")
            simpleTableHeaderAdapter.setTextSize(15)
            simpleTableHeaderAdapter.setTextColor(ContextCompat.getColor(context as ExamResultTimeAnalysisActivity, R.color.table_header_text))
            customTableView.headerAdapter = simpleTableHeaderAdapter
        }
    }

    fun AddValuesToBARENTRY() {
        for (i in 1 until finalSubmissionPojosList!![0].subjectIds!!.size + 1) {

            //Set Questions According to section
            subjectWiseMarks = java.lang.Float.valueOf(finalSubmissionPojosList!![0].subWiseMarks!![i - 1])
            subjectWiseMaxMarks = finalSubmissionPojosList!![0].subWiseMaxMarks!![i - 1].toInt()
            val subjectWiseTime = finalSubmissionPojosList!![0].subWiseTime!![i - 1]
            Percentage = subjectWiseMarks!! / subjectWiseMaxMarks * 100

            //Set table Value
            subjectBasedOnId!![finalSubmissionPojosList!![0].subjectIds!![i - 1]]?.let { ShowSubjectWiseTimeAnalysisDataInTablePojo(it, "$Percentage%", subjectWiseMarks.toString() + " in " + subjectWiseTime, "1.25 in 0:16") }?.let { productList!!.add(it) }
        }
    }

    fun initialization() {
        examCacheFinalSubmissionInstance = ExamCacheFinalSubmission(this)
        cacheResultListInstance = CacheResultList(this)
        examName = findViewById<View>(R.id.ExamName) as TextView

        //HashMa
        subjectBasedOnId = HashMap()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
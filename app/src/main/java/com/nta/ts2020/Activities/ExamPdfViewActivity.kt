package com.nta.ts2020.Activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.github.barteksc.pdfviewer.PDFView
import com.nta.ts2020.R
import java.io.File

class ExamPdfViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_view)
        if (intent.extras != null) {
            if (supportActionBar != null) {
                supportActionBar!!.title = intent.extras!!.getString("PDF_NAME")
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            }
            val pdf = intent.extras!!.getString("PDF")
            val pdfView = findViewById<View>(R.id.pdfView) as PDFView
            val file = File(pdf)
            pdfView.fromFile(file).load()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
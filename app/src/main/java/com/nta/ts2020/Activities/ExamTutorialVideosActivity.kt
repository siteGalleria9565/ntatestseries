package com.nta.ts2020.Activities

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.StringRequest
import com.nta.ts2020.Activities.AppController
import com.nta.ts2020.R
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ExamTutorialVideosActivity : AppCompatActivity() {
    var subjectsrecyclerview: RecyclerView? = null
    var adapter: RecyclerView.Adapter<*>? = null
    var layoutManager: RecyclerView.LayoutManager? = null
    var subnames = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial_videos)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.title = "Subjects"
        }
        subjectsrecyclerview = findViewById(R.id.subjectsrecyclerview)
        layoutManager = LinearLayoutManager(this@ExamTutorialVideosActivity)
        subjectsrecyclerview?.layoutManager = layoutManager
        makeJsonObjectRequest("http://examin.co.in/examin-admin/YoutubeVideos/video_subject_list.php")
    }

    private fun makeJsonObjectRequest(url: String) {
        val stringRequest = StringRequest(Request.Method.GET, url, Response.Listener { response ->
            Log.e("RESPONSE", response)
            parseActivityName(response)
        }, Response.ErrorListener { error ->
            VolleyLog.e("tagg", "Error: " + error.message)
            Toast.makeText(this@ExamTutorialVideosActivity, error.message, Toast.LENGTH_SHORT).show()
        })
        stringRequest.retryPolicy = DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        AppController.instance?.addToRequestQueue(stringRequest)
    }

    private fun parseActivityName(res: String) {
        try {
            val jsonObject = JSONObject(res)
            val jsonObject1 = jsonObject.getJSONObject("response")
            val jsonArray = jsonObject1.getJSONArray("subject_list")
            for (i in 0 until jsonArray.length()) {
                val jsonObject2 = jsonArray.getJSONObject(i)
                val subjectname = jsonObject2.getString("subject")
                subnames.add(subjectname)
                adapter = ExamSubjectsListAdapter(this@ExamTutorialVideosActivity, subnames)
                subjectsrecyclerview!!.adapter = adapter
            }
        } catch (e: JSONException) {
            Log.e("tagg", "Json parsing error: " + e.message)
            runOnUiThread {
                Toast.makeText(this@ExamTutorialVideosActivity,
                        "Json parsing error: " + e.message,
                        Toast.LENGTH_LONG)
                        .show()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.nta.ts2020.Activities.AddNewAddress
import com.nta.ts2020.Activities.ExamProfAddressActivity
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.AppConstants.ConstantVariables.SELECT_ADDRESS_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.STUDENT_PERSONAL_DETAILS
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.LoadProfileHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class SelectAddressActivity : AppCompatActivity(), View.OnClickListener, WebResponseListener {
    private var rb_profile_address: RadioButton? = null
    private var rb_new_address: RadioButton? = null
    private var tv_edit_current: TextView? = null
    private var tv_edit_new: TextView? = null
    private var ll_profile_address: LinearLayout? = null
    private var ll_new_address: LinearLayout? = null
    private var btn_submit: Button? = null
    private var tv_add_new_address: TextView? = null
    private var webRequest: WebRequest? = null
    var progressDialog: ProgressDialog? = null
    private var llEBookSubjectsListError: LinearLayout? = null
    private var swipe_layout: SwipeRefreshLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_address)
        init()
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.title = "Shipping Address"
        }
    }

    private fun init() {
        rb_profile_address = findViewById(R.id.rb_profile_address)
        rb_profile_address?.setOnClickListener(this)
        rb_new_address = findViewById(R.id.rb_new_address)
        rb_new_address?.setOnClickListener(this)
        tv_edit_current = findViewById(R.id.tv_edit_current)
        tv_edit_current?.setOnClickListener(this)
        tv_edit_new = findViewById(R.id.tv_edit_new)
        tv_edit_new?.setOnClickListener(this)
        ll_profile_address = findViewById(R.id.ll_profile_address)
        ll_new_address = findViewById(R.id.ll_new_address)
        btn_submit = findViewById(R.id.btn_submit)
        btn_submit?.setOnClickListener(this)
        tv_add_new_address = findViewById(R.id.tv_add_new_address)
        tv_add_new_address?.setOnClickListener(this)
        llEBookSubjectsListError = findViewById(R.id.llEBookSubjectsListError)
        webRequest = WebRequest(this)
        swipe_layout = findViewById(R.id.swipe_layout)
        swipe_layout?.isRefreshing = false
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("please wait...")
        val json = JSONObject()
        var parameterJson = ""
        val url = ConstantVariables.URL_GET_NEW_ADDRESS + "?app=NTA&user_id=" + CacheUserData(this).userDataFromCache.userId
        if (isNetworkConnected) {
            progressDialog!!.show()
            llEBookSubjectsListError?.setVisibility(View.GONE)
            btn_submit?.visibility = View.VISIBLE
            try {
                val jsonArray = JSONArray()
                jsonArray.put("ADDRESS")
                jsonArray.put("CITY")
                jsonArray.put("STATE")
                jsonArray.put("COUNTRY")
                jsonArray.put("PINCODE")
                jsonArray.put("AADHAR_NO")
                json.put("TABLE", STUDENT_PERSONAL_DETAILS)
                json.put("STUDENT_ID", CacheUserData(this).userDataFromCache.userId)
                json.put("data", jsonArray)
                parameterJson = json.toString()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            ThreadPoolService().loadProfileFromServer(this, parameterJson, LoadProfileHandler(this@SelectAddressActivity, SELECT_ADDRESS_ACTIVITY, progressDialog!!))
            webRequest!!.GET_METHOD(url, this, WebCallType.GET_NEW_ADDRESS, true)
        } else {
            llEBookSubjectsListError?.setVisibility(View.VISIBLE)
            btn_submit?.setVisibility(View.GONE)
        }
        swipe_layout?.setOnRefreshListener(OnRefreshListener {
            if (isNetworkConnected) {
                init()
            } else {
                swipe_layout?.setRefreshing(false)
                btn_submit?.setVisibility(View.GONE)
                llEBookSubjectsListError?.setVisibility(View.VISIBLE)
            }
        })
    }

    fun loadData(jsonParameter: String?) {
        try {
            val json = JSONObject(jsonParameter)
            val tvAddress: String
            val tvCity: String
            val tvState: String
            val tvCountry: String
            val tvPinCode: String
            tvAddress = json.getString("ADDRESS")
            tvCity = json.getString("CITY")
            tvState = json.getString("STATE")
            tvCountry = json.getString("COUNTRY")
            tvPinCode = json.getString("PINCODE")
            rb_profile_address!!.text = "$tvAddress, $tvCity, $tvState, $tvCountry, $tvPinCode"
            ll_profile_address!!.visibility = View.VISIBLE
        } catch (e: JSONException) {
            e.printStackTrace()
            Toast.makeText(this, "OOPS! Something went wrong", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rb_profile_address -> if (rb_new_address!!.isChecked) rb_new_address!!.isChecked = false
            R.id.rb_new_address -> if (rb_profile_address!!.isChecked) rb_profile_address!!.isChecked = false
            R.id.tv_edit_current -> startActivityForResult(Intent(this, ExamProfAddressActivity::class.java), 100)
            R.id.tv_edit_new -> startActivityForResult(Intent(this, AddNewAddress::class.java).putExtra("new_address", rb_new_address!!.text.toString()), 200)
            R.id.btn_submit -> {
            }
            R.id.tv_add_new_address -> startActivityForResult(Intent(this, AddNewAddress::class.java), 200)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            init()
        }
        if (requestCode == 200) {
            if (data != null) {
                rb_new_address!!.text = data.getStringExtra("new_address")
                ll_new_address!!.visibility = View.VISIBLE
                tv_add_new_address!!.visibility = View.GONE
            }
        }
    }

    override fun onError(message: String?) {
        progressDialog!!.dismiss()
    }

    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        if (response != null) {
            try {
                val jsonObject = JSONObject(response.toString())
                if (jsonObject.getBoolean("status")) {
                    val shipping_address = jsonObject.getJSONObject("data").getString("shipping_address")
                    ll_new_address!!.visibility = View.VISIBLE
                    rb_new_address!!.text = shipping_address
                    tv_add_new_address!!.visibility = View.GONE
                    progressDialog!!.dismiss()
                } else {
                    ll_new_address!!.visibility = View.GONE
                    tv_add_new_address!!.visibility = View.VISIBLE
                    progressDialog!!.dismiss()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }
}
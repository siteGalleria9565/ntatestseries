package com.nta.ts2020.Activities

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ExamVideoApiBaseUrl {
    private var retrofit: Retrofit? = null
    val client: Retrofit?
        get() {
            if (retrofit == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val httpClient = OkHttpClient.Builder()
                httpClient.addInterceptor(interceptor)
                val client = httpClient.build()
                val gson = GsonBuilder()
                        .setLenient()
                        .create()
                retrofit = Retrofit.Builder()
                        .baseUrl("http://examin.co.in/examin-admin/YoutubeVideos/")
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
            }
            return retrofit
        }
}
package com.nta.ts2020.Activities

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.nta.ts2020.Adapters.EbookCourseListAdapter
import com.nta.ts2020.Adapters.EbookCourseListAdapter.OnClickPaper
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.PojoClasses.QuestionPaperPDF
import com.nta.ts2020.R
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class EbookQuestionPaperActivity : AppCompatActivity(), WebResponseListener, OnClickPaper {
    private var rv_course_name: RecyclerView? = null
    private var llEBookquestionpaperError: LinearLayout? = null
    private var webRequest: WebRequest? = null
    private var gson: Gson? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    var mQuestionPDFList: List<QuestionPaperPDF> = ArrayList()
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    var course_list_array = JSONArray()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ebook_question_paper)
        init()
        if (supportActionBar != null) {
            supportActionBar!!.title = "Question Paper"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        swipeRefreshLayout!!.setColorSchemeResources(
                R.color.material_blue_700,
                R.color.material_green_700,
                R.color.material_yellow_700,
                R.color.material_red_700)
        swipeRefreshLayout!!.setOnRefreshListener {
            if (isNetworkConnected) {
                init()
            } else {
                swipeRefreshLayout!!.isRefreshing = false
                rv_course_name!!.visibility = View.GONE
                llEBookquestionpaperError!!.visibility = View.VISIBLE
            }
        }
    }

    private fun init() {
        swipeRefreshLayout = findViewById<View>(R.id.swRefreshEBookSubjectsList) as SwipeRefreshLayout
        rv_course_name = findViewById(R.id.rv_course_name)
        llEBookquestionpaperError = findViewById(R.id.llEBookquestionpaperError)
        webRequest = WebRequest(this)
        gson = Gson()
        linearLayoutManager = LinearLayoutManager(this)
        if (isNetworkConnected) {
            swipeRefreshLayout!!.isRefreshing = true
            webRequest!!.GET_METHOD(ConstantVariables.URL_PDF_PREVIOUS_YEAR_QUESTION_PAPER, this, WebCallType.QUESTION_PAPER, true)
        } else {
            swipeRefreshLayout!!.isRefreshing = false
            llEBookquestionpaperError?.visibility = View.VISIBLE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onError(message: String?) {
        swipeRefreshLayout!!.isRefreshing = false
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        llEBookquestionpaperError!!.visibility = View.GONE
        swipeRefreshLayout!!.isRefreshing = false
        val mCourseNameList: MutableList<String> = ArrayList()
        try {
            val jsonObject = JSONObject(response.toString())
            course_list_array = jsonObject.getJSONArray("courses")
            for (i in 0 until course_list_array.length()) {
                mCourseNameList.add(course_list_array.getJSONObject(i).getString("course_name"))
            }
            val ebookCourseListAdapter = EbookCourseListAdapter(this, this, mCourseNameList, null, "")
            rv_course_name!!.layoutManager = linearLayoutManager
            rv_course_name!!.adapter = ebookCourseListAdapter
            rv_course_name!!.visibility = View.VISIBLE
            Log.d("ZZ", "onResponse: " + mQuestionPDFList.size)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private val isNetworkConnected: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    override fun onSelectPaper(name: String?, position: Int) {
        val mList: MutableList<String> = ArrayList()
        val jsonObject: JSONObject
        try {
            jsonObject = course_list_array.getJSONObject(position).getJSONObject("year")
            val iterator = jsonObject.keys()
            while (iterator.hasNext()) {
                val key = iterator.next()
                mList.add(key)
            }
            val i = Intent(this, EbookQuestionPaperYearActivity::class.java)
            i.putExtra("PDF_LIST", jsonObject.toString())
            i.putStringArrayListExtra("YEAR_LIST", mList as ArrayList<String>)
            i.putExtra("NAME", name)
            startActivity(i)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        //        for (int i = 0; i < mQuestionPDFList.size(); i++) {
//            if (mQuestionPDFList.get(i).getCourse_name().equals(name)) {
//                mList.add(mQuestionPDFList.get(i).getYear());
//            }
//        }
//        HashSet hs = new HashSet(mList);
//        mList = new ArrayList<>(hs);
    }

    override fun onSelectYear(year: String?, name: String?) {}
}
package com.nta.ts2020.Activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.Clas
import com.nta.ts2020.PojoClasses.Course
import com.nta.ts2020.PojoClasses.CourseClasses
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.MyPref
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONObject

class SelectCourseClass : AppCompatActivity(), WebResponseListener, View.OnClickListener {
    lateinit var webRequest: WebRequest
    lateinit var sCourse: Spinner
    lateinit var sClass: Spinner
    lateinit var gson: Gson
    lateinit var myPref: MyPref
    var coursesList = mutableListOf<String>()
    var classList = mutableListOf<String>()
    var classes = mutableListOf<Clas>()
    var courses = mutableListOf<Course>()
    var courseClasses : CourseClasses? = null
    lateinit var btnSubmit : Button
    var courseID = ""
    var classID = ""
    var userDataPojo : UserDataPojo?= null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_course_class)
        inti()

        webRequest.GET_METHOD(ConstantVariables.URL_GET_COURSE_CLASS,this,WebCallType.GET_COURSES_CLASSES,true)

        sCourse.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                classList.clear()
                courseID = courses[position].id
                classes = courses[position].class_
                classes.forEach{
                    classList.add(it.class_)
                }
                setClassAdapter()
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
        sClass.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                classID = classes[position].id
                Log.d("ll","$classID \n $courseID")
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
    }

    private fun setClassAdapter() {
        sClass.adapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,classList)
    }

    private fun inti() {
        userDataPojo = CacheUserData(this).userDataFromCache
        webRequest = WebRequest(this)
        sCourse = findViewById(R.id.s_course)
        sClass = findViewById(R.id.s_class)
        gson = Gson()
        myPref = MyPref(this)
        btnSubmit = findViewById(R.id.btn_submit)
        btnSubmit.setOnClickListener(this)
    }

    override fun onError(message: String?) {
        Toast.makeText(this,"Something went wrong!", Toast.LENGTH_LONG).show()
    }

    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        when(webCallType){
            WebCallType.UPDATE_PROFILE -> {
                Log.d("llll",response.toString())
                val jsonRoot = JSONObject(response.toString())
                val jsonObject = jsonRoot.getJSONObject("data")
                val responseStatus = jsonRoot.getInt("code")
                val responseMessage = jsonRoot.getString("msg")
                var databaseResponse = false
                if (responseStatus == 200) {
                    val userDataPojo = UserDataPojo()
                    userDataPojo.userId = jsonObject.getInt("ID").toString()
                    userDataPojo.fName = jsonObject.getString("FIRST_NAME")
                    userDataPojo.lName = jsonObject.getString("LAST_NAME")
                    userDataPojo.email = jsonObject.getString("EMAIL")
                    userDataPojo.phNo = jsonObject.getString("MOBILE_NO")
                    userDataPojo.profPic = jsonObject.getString("PROFILE_PIC")
                    userDataPojo.courseId = jsonObject.getString("COURSE_ID")
                    userDataPojo.courseName = ""
                    userDataPojo.classId = jsonObject.getString("BATCH_ID")
                    userDataPojo.className = ""
                    userDataPojo.setUserLoginStatus(true)
                    databaseResponse = CacheUserData(this).updateUserDetails(userDataPojo)
                    if(databaseResponse)
                        Toast.makeText(this,responseMessage,Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Something went wrong",Toast.LENGTH_LONG).show()
                }
            }
            WebCallType.GET_COURSES_CLASSES ->{
                val jsonObject = JSONObject(response.toString())
                if(jsonObject.getString("status") == "true"){
                    courseClasses = gson.fromJson(response.toString(),CourseClasses::class.java)
                    myPref.setPref(ConstantVariables.COURSES_CLASSES,gson.toJson(response.toString()))
                    courses = courseClasses!!.courses
                    courses.forEach{
                        coursesList.add(it.course)
                    }
                    sCourse.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, coursesList)
                    classes = courses[0].class_
                    classes.forEach{
                        classList.add(it.class_)
                    }
                    sClass.adapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item, classList)
                }
            }
            else -> {

            }
        }
    }

    override fun onClick(v: View) {
        if(v.id == R.id.btn_submit){
            val jsonObject = JSONObject()
            jsonObject.put("ID",userDataPojo?.userId)
            jsonObject.put("EMAIL",userDataPojo?.email)
            jsonObject.put("COURSE_ID", courseID)
            jsonObject.put("BATCH_ID",classID)
            webRequest.POST_METHOD(ConstantVariables.URL_UPDATE_PROFILE,jsonObject,WebCallType.UPDATE_PROFILE,null,this,true)
        }
    }
}
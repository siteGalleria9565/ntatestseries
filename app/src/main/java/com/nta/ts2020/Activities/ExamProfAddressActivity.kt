package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.AppConstants.ConstantVariables.ADDRESS_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.OTHER_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.STUDENT_PERSONAL_DETAILS
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.LoadProfileHandler
import com.nta.ts2020.ThreadingHelper.Handlers.UpdateProfileHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class ExamProfAddressActivity : AppCompatActivity(), View.OnClickListener {
    private var context: Context? = null
    private var tvAddress: TextView? = null
    private var tvCity: TextView? = null
    private var tvState: TextView? = null
    private var tvCountry: TextView? = null
    private var tvPinCode: TextView? = null
    private var tvAadhaar: TextView? = null
    private var progressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_address)


        //Set Title
        if (supportActionBar != null) {
            supportActionBar!!.title = "Address"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Context
        context = this

        //Initialization
        tvAddress = findViewById<View>(R.id.tvAddAddress) as TextView
        tvCity = findViewById<View>(R.id.tvAddCity) as TextView
        tvState = findViewById<View>(R.id.tvAddState) as TextView
        tvCountry = findViewById<View>(R.id.tvAddCountry) as TextView
        tvPinCode = findViewById<View>(R.id.tvAddPinCode) as TextView
        tvAadhaar = findViewById<View>(R.id.tvAddAadhaar) as TextView
        val btSave = findViewById<View>(R.id.btAddSave) as Button
        btSave.setOnClickListener(this)

        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true

        //Load profile data
        val json = JSONObject()
        var parameterJson = ""
        try {
            val jsonArray = JSONArray()
            jsonArray.put("ADDRESS")
            jsonArray.put("CITY")
            jsonArray.put("STATE")
            jsonArray.put("COUNTRY")
            jsonArray.put("PINCODE")
            jsonArray.put("AADHAR_NO")
            json.put("TABLE", STUDENT_PERSONAL_DETAILS)
            json.put("STUDENT_ID", CacheUserData(context as ExamProfAddressActivity).userDataFromCache.userId)
            json.put("data", jsonArray)
            parameterJson = json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        progressDialog!!.show()
        ThreadPoolService().loadProfileFromServer(context, parameterJson, LoadProfileHandler(this@ExamProfAddressActivity, ADDRESS_ACTIVITY, progressDialog!!))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btAddSave -> if (isValid) {
                //Save to server
                val parameterJson = buildParameter()
                //                    Log.d(TAG, "Address Parameters: " + parameterJson);
                progressDialog!!.show()
                val updateProfileHandler = UpdateProfileHandler(this@ExamProfAddressActivity, progressDialog!!)
                ThreadPoolService().saveProfileToServer(applicationContext, parameterJson, OTHER_ACTIVITY, updateProfileHandler)
            }
        }
    }

    /**
     * Builds parameter in json format to return it to the server
     *
     * @return json format in the mentioned format in docs
     */
    private fun buildParameter(): String {
        var parameterJson = ""
        val json = JSONObject()
        parameterJson = try {
            json.put("TABLE", STUDENT_PERSONAL_DETAILS)
            json.put("STUDENT_ID", CacheUserData(context!!).userDataFromCache.userId)
            json.put("ADDRESS", tvAddress!!.text.toString().trim { it <= ' ' })
            json.put("CITY", tvCity!!.text.toString().trim { it <= ' ' })
            json.put("STATE", tvState!!.text.toString().trim { it <= ' ' })
            json.put("COUNTRY", tvCountry!!.text.toString().trim { it <= ' ' })
            json.put("PINCODE", tvPinCode!!.text.toString().trim { it <= ' ' })
            if (tvAadhaar!!.text.toString() != "") json.put("AADHAR_NO", tvAadhaar!!.text.toString().trim { it <= ' ' })
            json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
            return "ERROR"
        }
        return parameterJson
    }

    private val isValid: Boolean
        private get() {
            var status = true
            if (tvAddress!!.text.toString() == "") {
                tvAddress!!.error = "Enter Address"
                status = false
            }
            if (tvCity!!.text.toString() == "") {
                tvCity!!.error = "Enter City"
                status = false
            }
            if (tvState!!.text.toString() == "") {
                tvState!!.error = "Enter State"
                status = false
            }
            if (tvCountry!!.text.toString() == "") {
                tvCountry!!.error = "Enter Country"
                status = false
            }
            if (tvPinCode!!.text.toString() == "") {
                tvPinCode!!.error = "Enter Pin Code"
                status = false
            }
            return status
        }

    fun loadData(jsonParameter: String?) {
        try {
            val json = JSONObject(jsonParameter)
            tvAddress!!.text = json.getString("ADDRESS")
            tvCity!!.text = json.getString("CITY")
            tvState!!.text = json.getString("STATE")
            tvCountry!!.text = json.getString("COUNTRY")
            tvPinCode!!.text = json.getString("PINCODE")
            tvAadhaar!!.text = json.getString("AADHAR_NO")
        } catch (e: JSONException) {
            e.printStackTrace()
            Toast.makeText(context, "OOPS! Something went wrong", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "ProfAddActivity"
    }
}
package com.nta.ts2020.Activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.R

class ScholarshipDescription : AppCompatActivity() {
    var progressBar: ProgressBar? = null
    var bt_applyNow: Button? = null
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    var wvScholarshipdesc: WebView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scholarship_description)
        title = intent.getStringExtra("title")
        progressBar = findViewById(R.id.pb_scholarship_desc)
        bt_applyNow = findViewById(R.id.bt_apply_now_desc)
        progressBar?.setVisibility(View.VISIBLE)
        wvScholarshipdesc = findViewById(R.id.wv_scholarship_description)
        setData()
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        bt_applyNow?.setOnClickListener(View.OnClickListener {
            val httpIntent = Intent(Intent.ACTION_VIEW)
            httpIntent.data = Uri.parse(intent.getStringExtra("url"))
            startActivity(httpIntent)
        })
    }

    private fun setData() {
        wvScholarshipdesc!!.loadData(Html.fromHtml(intent.getStringExtra("complete_desc")).toString(), "text/html", "UTF-8")
        progressBar!!.visibility = View.GONE
    }
}
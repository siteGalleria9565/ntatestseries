package com.nta.ts2020.Activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.text.*
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.AppConstants.ConstantVariables.isValidPassword
import com.nta.ts2020.PojoClasses.BatchID
import com.nta.ts2020.PojoClasses.CourseModel
import com.nta.ts2020.PojoClasses.RegisterPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.RegistrationHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class ExamRegisterActivity : AppCompatActivity(), View.OnClickListener, TextWatcher, WebResponseListener {
    var progressDialog: ProgressDialog? = null
    var regProgressDialog: ProgressDialog? = null
    private var context: ExamRegisterActivity? = null

    private var registerPojo: RegisterPojo? = null
    private var firstName: EditText? = null
    private var lastName: EditText? = null
    private var email: EditText? = null
    private var password: EditText? = null
    var passwd: String? = null
    private var webRequest: WebRequest? = null
    private var gson: Gson? = null
    private var progressBar: ProgressBar? = null
    private lateinit var backbtn : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Change notification bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
        }
        setContentView(R.layout.activity_register)

        //Remove ActionBar
        if (supportActionBar != null) supportActionBar!!.hide()
        context = this
        webRequest = WebRequest(this)
        gson = Gson()
        backbtn = findViewById(R.id.back_btn)
        backbtn.setOnClickListener(this)
        registerPojo = RegisterPojo()
        firstName = findViewById<View>(R.id.editTextRegFirstName) as EditText
        firstName!!.filters = arrayOf(editTextFilter)
        lastName = findViewById<View>(R.id.editTextRegLastName) as EditText
        lastName!!.filters = arrayOf(editTextFilter)
        email = findViewById<View>(R.id.editTextRegEmail) as EditText
        password = findViewById<View>(R.id.editTextRegPassword) as EditText
        val buttonRegister = findViewById<View>(R.id.buttonRegister) as Button
        val logIn = findViewById<View>(R.id.textViewRegLogIn) as TextView
        progressBar = findViewById<View>(R.id.progress_bar) as ProgressBar
        buttonRegister.setOnClickListener(context)
        logIn.setOnClickListener(context)
        firstName!!.addTextChangedListener(this)
        lastName!!.addTextChangedListener(this)
        email!!.addTextChangedListener(this)
        password!!.addTextChangedListener(this)
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.back_btn -> onBackPressed()
            R.id.buttonRegister -> {
                // Check if no view has focus then hide soft keyboard
                val view = this.currentFocus
                if (view != null) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }
                //Validation
                if (isValid) {
                    if (!isNetworkConnected) {
                        Snackbar.make(firstName!!, "Please Check Internet Connection", Snackbar.LENGTH_SHORT).show()
                    } else {
                        regProgressDialog = ProgressDialog.show(context, "Registering ....", "Please wait",
                                true, false)
                        //Preparing data to send to thread
                        registerPojo!!.setfName(firstName!!.text.toString())
                        registerPojo!!.setlName(lastName!!.text.toString())
                        registerPojo!!.email = email!!.text.toString()
                        registerPojo!!.phNumber = ""
                        registerPojo!!.password = password!!.text.toString()
                        registerPojo!!.city = ""
                        registerPojo!!.state = ""
                        registerPojo!!.currentAddress = ""
                        registerPojo!!.dOB = ""
                        registerPojo!!.referralcode = ""
                        registerPojo!!.course = ""
                        registerPojo!!.classe = ""
                        val registrationHandler = context?.let { RegistrationHandler(it) }
                        ThreadPoolService().register(context, registrationHandler, registerPojo)
                    }
                }
            }
            R.id.textViewRegLogIn -> {
                val intent = Intent(this@ExamRegisterActivity, ExamLoginScreenActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
        }
    }

    private val isValid: Boolean
        get() {
            var status = true

            if (firstName!!.text.toString().trim { it <= ' ' } == "") {
                firstName!!.error = "Enter first name"
                status = false
            }
            if (lastName!!.text.toString().trim { it <= ' ' } == "") {
                lastName!!.error = "Enter last name"
                status = false
            }
            if (email!!.text.toString().trim { it <= ' ' } == "") {
                email!!.error = "Enter email"
                status = false
            } else if (!Patterns.EMAIL_ADDRESS.matcher(email!!.text.toString().trim { it <= ' ' }).matches()) {
                email!!.error = "Enter valid email address"
                status = false
            }
            if (password!!.text.toString().trim { it <= ' ' } == "") {
                password!!.error = "Enter password"
                status = false
            } else {
                passwd = password!!.text.toString().trim { it <= ' ' }
                if (!isValidPassword(passwd!!)) status = false
            }
            return status
        }

    val isNetworkConnected: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    fun startNextActivity() {
        val intent = Intent(this@ExamRegisterActivity, OTPActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun startLoginIntent() {
        val intent = Intent(this@ExamRegisterActivity, ExamLoginScreenActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (firstName!!.text.hashCode() == s.hashCode()) {
            firstName!!.error = null
        }
        if (lastName!!.text.hashCode() == s.hashCode()) {
            lastName!!.error = null
        }
        if (email!!.text.hashCode() == s.hashCode()) {
            email!!.error = null
        }
        if (password!!.text.hashCode() == s.hashCode()) {
            password!!.error = null
        }
    }

    override fun afterTextChanged(s: Editable) {}


    override fun onError(message: String?) {
        Snackbar.make(email!!, "Something went wrong please try again", Snackbar.LENGTH_SHORT).show()
    }

    override fun onResponse(response: Any?, webCallType: WebCallType?) {

    }


    companion object {
        private const val TAG = "ExamRegisterActivity"
        private const val DATE_PICKER_ID = 1

        // put your condition here
        val editTextFilter: InputFilter
            get() = object : InputFilter {
                override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
                    var keepOriginal = true
                    val sb = StringBuilder(end - start)
                    for (i in start until end) {
                        val c = source[i]
                        if (isCharAllowed(c)) // put your condition here
                            sb.append(c) else keepOriginal = false
                    }
                    return if (keepOriginal) null else {
                        if (source is Spanned) {
                            val sp = SpannableString(sb)
                            TextUtils.copySpansFrom(source, start, sb.length, null, sp, 0)
                            sp
                        } else {
                            sb
                        }
                    }
                }

                private fun isCharAllowed(c: Char): Boolean {
                    val ps = Pattern.compile("^[a-zA-Z ]+$")
                    val ms = ps.matcher(c.toString())
                    return ms.matches()
                }
            }
    }
}
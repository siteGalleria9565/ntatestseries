package com.nta.ts2020.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.text.Html
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.facebook.login.LoginManager
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.nta.ts2020.Activities.AddBookmarksActivity
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.Adapters.BannerAdapter
import com.nta.ts2020.Adapters.HorizontalMenuAdapter
import com.nta.ts2020.Adapters.NotificationRecyclerAdapter
import com.nta.ts2020.AppConstants.ConstantVariables.ABOUT_US
import com.nta.ts2020.AppConstants.ConstantVariables.EXTERNAL_FOLDER_PATH
import com.nta.ts2020.AppConstants.ConstantVariables.E_BOOK
import com.nta.ts2020.AppConstants.ConstantVariables.FORMULA_BOOK
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.PRIVACY_POLICY
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.AppConstants.ConstantVariables.TERMS_CONDITIONS
import com.nta.ts2020.AppConstants.ConstantVariables.logout
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.*
import com.nta.ts2020.PojoClasses.HorizontalMenuPojo
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.BannerHandler
import com.nta.ts2020.ThreadingHelper.Handlers.HorizontalMenuHandler
import com.nta.ts2020.ThreadingHelper.Handlers.NotificationsHandler
import com.nta.ts2020.ThreadingHelper.Handlers.UploadProfImageHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.UtilsHelper.ConnectionDetector
import com.nta.ts2020.UtilsHelper.MyPref
import com.nta.ts2020.UtilsHelper.SharedPrefManager
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import com.yalantis.ucrop.UCrop
import org.json.JSONObject
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

//Select exam center for mock test
class ExamMainDashboardActivity : AppCompatActivity(), View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, WebResponseListener {
    private var imageViewHeaderPic: ImageView? = null
    private var context: ExamMainDashboardActivity? = null
    private var recyclerViewNotifications: RecyclerView? = null
    private var btn_share_app: Button? = null
    var btn_question_paper: LinearLayout? = null
    var videos_layout: RelativeLayout? = null
    private var cv_apply_now: CardView? = null
    private var btn_mock_test: Button? = null

    //exam module asset
    //==========================================================
    var nextActivityIntent: Intent? = null

    //Threads
    private var threadPoolService: ThreadPoolService? = null
    private var drawer: DrawerLayout? = null
    private var textViewName: TextView? = null
    private var textViewEmail: TextView? = null
    private var horizontalMenuHeading: TextView? = null
    private var uri: Uri? = null
    private var isPagerSnapHelperAttached = false

    //===========================================================================================
    //Firebase analytics
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    private var holderView: View? = null
    private lateinit var contentView1: View
    private val isDrawerOpen = false
    var connectionDetector: ConnectionDetector? = null
    private var btn_facebook: TextView? = null
    private var btn_webpage: LinearLayout? = null
    private val FACEBOOK_URL = "https://www.facebook.com/NTA.official.update"
    private var userDataPojo = UserDataPojo()
    private lateinit var myPref: MyPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //context
        context = this
        userDataPojo = CacheUserData(this).userDataFromCache
        //Initialise firebase analytics
        myPref = MyPref(this)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
        val userId = CacheUserData(context!!).userDataFromCache.userId
        //WebRequest(this).GET_METHOD("$URL_REF_CODE?user_id=$userId", this, WebCallType.GET_REF_CODE, false)

        //Check if user is logged in or not
        if (userDataPojo.userId == null) {
            startActivity(Intent(this, ExamLoginScreenActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP))
            finish()
        }

        setContentView(R.layout.startup_navigation_drawer)
        connectionDetector = ConnectionDetector(this@ExamMainDashboardActivity)


        //Nav Drawer -- Start
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        holderView = findViewById(R.id.holder)
        contentView1 = findViewById(R.id.content)
        btn_share_app = findViewById(R.id.btn_share_app)
        btn_share_app?.setOnClickListener(this)
        drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer?.addDrawerListener(toggle)
        toggle.syncState()
        //drawer.setScrimColor(getResources().getColor(R.color.colorPrimaryDark));
        drawer?.setScrimColor(resources.getColor(R.color.black_transparent))
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.itemIconTintList = null
        navigationView.setNavigationItemSelectedListener(this)
        val navHeaderView = navigationView.getHeaderView(0)
        navigationView.setBackgroundColor(resources.getColor(R.color.colorPrimary))

        textViewName = navHeaderView.findViewById<View>(R.id.textViewNavHeaderName) as TextView
        textViewEmail = navHeaderView.findViewById<View>(R.id.textViewNavHeaderEmail) as TextView
        btn_question_paper = findViewById(R.id.btn_question_paper)
        btn_question_paper?.setOnClickListener {
            val i = Intent(context, EbookQuestionPaperActivity::class.java)
            startActivity(i)
        }
        videos_layout = findViewById(R.id.videos_layout)
        videos_layout?.setOnClickListener {
            if (connectionDetector!!.isConnectingToInternet) {
                startActivity(Intent(this@ExamMainDashboardActivity, ExamTutorialVideosActivity::class.java))
            } else {
                Toast.makeText(this@ExamMainDashboardActivity, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show()
            }
        }
        imageViewHeaderPic = navHeaderView.findViewById<View>(R.id.imageViewHeaderPic) as ImageView
        val imageViewChangeImage = navHeaderView.findViewById<View>(R.id.imageViewChamgeHeaderImage) as ImageView
        imageViewChangeImage.setOnClickListener(this)
        val userDataPojo = CacheUserData(context!!).userDataFromCache
        textViewName!!.text = "${userDataPojo.fName} ${userDataPojo.lName}"
        textViewEmail!!.text = userDataPojo.email
        //Set image from the server
        try {
            if (userDataPojo.profPic != "0") Glide.with(context!!).load(userDataPojo.profPic).into(imageViewHeaderPic!!)
        } catch (e: NullPointerException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
        }

        //Nav Drawer -- End

        //Load cached banners
        loadBanner(SUCCESS)

        //Banner data download
        val bannerHandler = BannerHandler(context!!)
        ThreadPoolService().loadBanner(context, bannerHandler)

        //Horizontal Menu data download
        horizontalMenuHeading = findViewById<View>(R.id.tvHorizontalMenuHeading) as TextView
        horizontalMenuHeading!!.visibility = View.GONE

        //Load cached horizontal menu
        loadHorizontalMenu(SUCCESS)

        //Download new horizontal menu
        val horizontalMenuHandler = HorizontalMenuHandler(context!!)
        ThreadPoolService().loadHorizontalMenu(context, horizontalMenuHandler)


        //Bottom Menu
        val exam = findViewById<View>(R.id.dashBoardMenuExam) as LinearLayout
        val quiz = findViewById<View>(R.id.dashBoardMenuQuiz) as LinearLayout
        val result = findViewById<View>(R.id.dashBoardMenuResult) as LinearLayout
        val eBook = findViewById<View>(R.id.dashBoardMenuEBook) as LinearLayout
        val fxBook = findViewById<View>(R.id.dashBoardMenuFxBook) as LinearLayout
        exam.setOnClickListener(this)
        quiz.setOnClickListener(this)
        result.setOnClickListener(this)
        eBook.setOnClickListener(this)
        fxBook.setOnClickListener(this)

        //ListView Notification -- Suvajit
        recyclerViewNotifications = findViewById<View>(R.id.notifications) as RecyclerView
        val reloadNotification = findViewById<View>(R.id.buttonReloadNotification) as Button
        reloadNotification.setOnClickListener(this)
        btn_mock_test = findViewById(R.id.btn_mock_test)
        btn_mock_test?.setOnClickListener(View.OnClickListener {
            val i = Intent(this@ExamMainDashboardActivity, WebViewActivity::class.java)
            i.putExtra("URL", "https://www.ntatpcsr.in/login/")
            startActivity(i)
        })
        cv_apply_now = findViewById(R.id.cv_apply_now)
        cv_apply_now?.setOnClickListener(View.OnClickListener {
            val deleteDialog = AlertDialog.Builder(context!!).create()
            //                builder.setTitle("Scholarship");
//                builder.setMessage(Html.fromHtml("\tAISEE is a merit based scholarship examination  For engineering and medical aspirant student over India<br> Eligibility: 12 Appearing and pass out candidate can Apply <br> Fees: Rs. 1200 <br> Exam Mode: Online exam <br> Center: All Over India<br> Exam dates:  April 15- April 30,2020 <br>         May 15-May 30,2020 <br> Slot Booking for exam: October 30,2019"));
            val v1 = LayoutInflater.from(context).inflate(R.layout.scholarship_apply_dialog, null, false)
            deleteDialog.setView(v1)
            val tv_description = v1.findViewById<TextView>(R.id.tv_description)
            tv_description.text = Html.fromHtml("\tAISEE is a merit based scholarship examination  For engineering and medical aspirant student over India<br> Eligibility: 12 Appearing and pass out candidate can Apply <br> Fees: Rs. 1200 <br> Exam Mode: Online exam <br> Center: All Over India<br> Exam dates:  April 15- April 30,2020 <br>         May 15-May 30,2020 <br> Slot Booking for exam: October 30,2019")
            val btn_apply = v1.findViewById<Button>(R.id.btn_apply)
            btn_apply.setOnClickListener {
                goToUrl("https://www.aisee.co.in/aisee-2020-online-scholarship-form/aisee-2020-registration-information")
                deleteDialog.dismiss()
            }
            val btn_slab = v1.findViewById<Button>(R.id.btn_slab)
            btn_slab.setOnClickListener {
                goToUrl("https://www.aisee.co.in/layout-of-college-scholarship.php")
                deleteDialog.dismiss()
            }
            deleteDialog.show()
        })
        if (isNetworkConnected) {
            //Sync with server -- Suvajit
            threadPoolService = ThreadPoolService()
            threadPoolService!!.sendFinalSubmissionDataToServer(this)
            val sharedPrefManager = SharedPrefManager(context!!)
            if (!sharedPrefManager.isContactsSynced) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), CONTACTS_ACCESS)
                } else {
                    threadPoolService!!.syncContacts(this)
                }
            }

            //Fcm token sync
            val token = FirebaseInstanceId.getInstance().token
            Log.d(TAG, "FCM token: $token")
            if (!sharedPrefManager.isFcmTokenSynced) {
                threadPoolService!!.updateFcmToken(applicationContext, token)
            }

            //Show progress dialog
            findViewById<View>(R.id.linlaHeaderProgress).visibility = View.VISIBLE
            //Save notification to cache from server
            val notificationsHandler = NotificationsHandler(this)
            threadPoolService!!.saveNotificationsFromServerToCache(this, notificationsHandler)
        } else {
            loadNotifications(SUCCESS)
        }
        btn_webpage = findViewById(R.id.btn_webpage)
        btn_webpage?.setOnClickListener(View.OnClickListener {
            val i = Intent(this@ExamMainDashboardActivity, ScholarShip::class.java)
            startActivity(i)

        })
        btn_facebook = findViewById(R.id.btn_facebook)
        btn_facebook?.setOnClickListener(View.OnClickListener {
            try {
                getFacebookIntent("https://www.facebook.com/NTA.official.update")
                val facebookIntent = Intent(Intent.ACTION_VIEW)
                val facebookUrl = getFacebookPageURL(this@ExamMainDashboardActivity)
                facebookIntent.data = Uri.parse(facebookUrl)
                startActivity(facebookIntent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    //function outside of code
    private fun goToUrl(url: String) {
        val uriUrl = Uri.parse(url)
        val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(launchBrowser)
    }

    private fun getFacebookIntent(url: String): Intent {
        val pm = context!!.packageManager
        var uri = Uri.parse(url)
        try {
            val applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0)
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=$url")
            } else {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intent)
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }
        return Intent(Intent.ACTION_VIEW, uri)
    }

    private fun getFacebookPageURL(context: Context): String {
        val packageManager = context.packageManager
        return try {
            val versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode
            if (versionCode >= 3002850) { //newer versions of fb app
                "fb://facewebmodal/f?href=$FACEBOOK_URL"
            } else { //older versions of fb app
                // return "fb://page/" + FACEBOOK_PAGE_ID;
                "fb://page/$FACEBOOK_URL"
            }
        } catch (e: PackageManager.NameNotFoundException) {
            FACEBOOK_URL //normal web url
        }
    }

    fun loadNotifications(status: Int) {
//        Log.d(TAG, "Notification Status: " + status);
        val errorMessage = findViewById<View>(R.id.tvDashboardErrorNotification) as TextView

        //TODO:Set error layouts for status
        if (status == SUCCESS) {
            findViewById<View>(R.id.errorNotification).visibility = View.GONE
            //            Log.d(TAG, "loadNotifications() invoked");
            val notificationsPojoArrayList = CacheNotifications(context!!).notificationsFromCache
            val notificationAdapter = NotificationRecyclerAdapter(context!!, notificationsPojoArrayList)
            recyclerViewNotifications!!.layoutManager = LinearLayoutManager(context)
            recyclerViewNotifications!!.isNestedScrollingEnabled = false
            recyclerViewNotifications!!.adapter = notificationAdapter
        } else if (status == NO_DATA) {
            findViewById<View>(R.id.errorNotification).visibility = View.VISIBLE
            errorMessage.text = "No new notifications."
        } else if (status == NETWORK_ERROR) {
            findViewById<View>(R.id.errorNotification).visibility = View.VISIBLE
            errorMessage.text = "Opps! Network error, please reload"
        }
        findViewById<View>(R.id.linlaHeaderProgress).visibility = View.GONE
    }

    override fun onClick(v: View) {
        var menuSelected = ""
        when (v.id) {
            R.id.imageViewChamgeHeaderImage -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_PERMISSION)
            } else {
                openImageFilePicker()
            }
            R.id.buttonReloadNotification -> {
                val notificationsHandler = NotificationsHandler(this)
                threadPoolService!!.saveNotificationsFromServerToCache(this, notificationsHandler)
                findViewById<View>(R.id.errorNotification).visibility = View.GONE
                findViewById<View>(R.id.linlaHeaderProgress).visibility = View.VISIBLE
            }
            R.id.dashBoardMenuExam -> {
                nextActivityIntent = Intent(this@ExamMainDashboardActivity, ExamShowAllExaminationdatlistActivity::class.java)
                startActivity(nextActivityIntent)
                menuSelected = "Exam"
            }
            R.id.dashBoardMenuQuiz -> {
                startActivity(Intent(context, ExamQuizSelectionActivity::class.java))
                menuSelected = "Quiz"
            }
            R.id.dashBoardMenuResult -> {
                startActivity(Intent(context, ExamResultDataListActivity::class.java))
                menuSelected = "ExamResult"
            }
            R.id.dashBoardMenuEBook -> {
                startActivity(Intent(context, EBookSubjectlistActivity::class.java).putExtra("E_BOOK_TYPE", E_BOOK))
                menuSelected = "E-Book"
            }
            R.id.dashBoardMenuFxBook -> {
                startActivity(Intent(context, EBookSubjectlistActivity::class.java).putExtra("E_BOOK_TYPE", FORMULA_BOOK))
                menuSelected = "Formula Book"
            }
            R.id.btn_share_app -> {
                //                startActivity(new Intent(this,SelectAddressActivity.class));
                val message = "Download this NTA Test Series apcp. Enter my code $refCode and earn rewards!\nhttps://bit.ly/2YyzVhZ"
                val share = Intent(Intent.ACTION_SEND)
                share.type = "text/plain"
                share.putExtra(Intent.EXTRA_TEXT, message)
                startActivity(Intent.createChooser(share, "Share Using"))
            }
        }
        val bundle = Bundle()
        bundle.putString("menu_type", menuSelected)
        mFirebaseAnalytics!!.logEvent("ExamIn_menu", bundle)
    }

    @SuppressLint("WrongConstant")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.nav_profile -> startActivity(Intent(this@ExamMainDashboardActivity, ExamUserProfileActivity::class.java))
            R.id.nav_bookmarks -> startActivity(Intent(this@ExamMainDashboardActivity, AddBookmarksActivity::class.java))
            R.id.nav_support -> startActivity(Intent(this@ExamMainDashboardActivity, ExamSupportDetailsActivity::class.java))
            R.id.nav_privacy_policy -> startActivity(Intent(this@ExamMainDashboardActivity, ExamWebViewPoliciesActivity::class.java).putExtra("TYPE", PRIVACY_POLICY))
            R.id.nav_terms_condition -> startActivity(Intent(this@ExamMainDashboardActivity, ExamWebViewPoliciesActivity::class.java).putExtra("TYPE", TERMS_CONDITIONS))
            R.id.nav_about_us -> startActivity(Intent(this@ExamMainDashboardActivity, ExamWebViewPoliciesActivity::class.java).putExtra("TYPE", ABOUT_US))
            R.id.nav_logout -> {
                logout(context!!)
                myPref.clearPref()
                LoginManager.getInstance().logOut()
            }
            R.id.nav_tutorialvideos -> startActivity(Intent(this@ExamMainDashboardActivity, ExamTutorialVideosActivity::class.java).putExtra("TYPE", ABOUT_US))
        }
        drawer!!.closeDrawer(Gravity.START)
        return false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openImageFilePicker()
        }
        if (requestCode == CONTACTS_ACCESS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            threadPoolService!!.syncContacts(this)
        }
    }

    private fun openImageFilePicker() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, RESULT_LOAD_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            val selectedImage = data.data
            val cR = context!!.contentResolver
            val mime = MimeTypeMap.getSingleton()
            val extension = mime.getExtensionFromMimeType(cR.getType(selectedImage!!))
            val timeStamp = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())
            val imagesFolder = File(Environment.DIRECTORY_PICTURES+ "ProfileImages")
            imagesFolder.mkdirs()
            val image = File(imagesFolder, "EXM_$timeStamp.$extension")
            val croppedImage: Uri = Uri.fromFile(image)

            uri = croppedImage
            val myFile = File(croppedImage!!.path)
            val imagePath = myFile.absolutePath
            if (isNetworkConnected) {
                val uploadProfImageHandler = UploadProfImageHandler(this@ExamMainDashboardActivity)
                ThreadPoolService().uploadProfImage(context, imagePath, uploadProfImageHandler)
            }

//            //Start Image Cropping
//            UCrop.of(selectedImage, croppedImage)
//                    .withAspectRatio(1f, 1f)
//                    .withMaxResultSize(500, 500)
//                    .start(this@ExamMainDashboardActivity)
        }
        if (resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            val croppedImageUri = UCrop.getOutput(data!!)
            uri = croppedImageUri
            val myFile = File(croppedImageUri!!.path)
            val imagePath = myFile.absolutePath
            if (isNetworkConnected) {
                val uploadProfImageHandler = UploadProfImageHandler(this@ExamMainDashboardActivity)
                ThreadPoolService().uploadProfImage(context, imagePath, uploadProfImageHandler)
            }

//            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            val cropError = UCrop.getError(data!!)
        }
    }

    fun loadProfImage(status: Int) {
        if (status == SUCCESS) {
            imageViewHeaderPic!!.setImageURI(uri)
        }
        val image = File(uri!!.path)
        if (image.exists()) image.delete()
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    override fun onResume() {
        super.onResume()
        val userDataPojo = CacheUserData(context!!).userDataFromCache
        textViewName!!.text = userDataPojo.fName + " " + userDataPojo.lName
        textViewEmail!!.text = userDataPojo.email
    }

    fun loadBanner(status: Int) {
        var status = status
        val bannerPojoArrayList = context?.let { CacheBanner(it).banner }
        if (bannerPojoArrayList!!.isEmpty()) status = NO_DATA
        if (status == SUCCESS) {
            val rvBanner = findViewById<View>(R.id.rvBanner) as RecyclerView
            val bannerAdapter = bannerPojoArrayList?.let { BannerAdapter(it) }
            //to avoid display half elements
            if (!isPagerSnapHelperAttached) {
                PagerSnapHelper().attachToRecyclerView(rvBanner)
                isPagerSnapHelperAttached = true
            }
            rvBanner.adapter = bannerAdapter

            //Banner auto scroll
            val delay = 5000
            val bannerRunnable: Runnable = object : Runnable {
                var count = 0
                override fun run() {
                    // Call smooth scroll
                    if (count < bannerAdapter!!.itemCount) {
                        rvBanner.smoothScrollToPosition(++count)
                    }
                    if (count == bannerAdapter.itemCount) count = -1
                    Handler().postDelayed(this, delay.toLong())
                }
            }
            rvBanner.postDelayed(bannerRunnable, delay.toLong())
        }
    }

    fun loadHorizontalMenu(status: Int) {
        var status = status
        val horizontalMenuPojoArrayList = CacheHorizontalMenu(context!!).horizontalMenu

        //Check for exam resume status
        val currentExamSessionPojoArrayList = ExamCacheSetCurrentSessionData(context!!).currentExamSession
        if (!currentExamSessionPojoArrayList.isEmpty()) {
            val currentSession = currentExamSessionPojoArrayList[0]
            val examStatus = currentSession.examStatus
            val resumeCount = currentSession.countResum!!.toInt()
            val id = currentSession.submitExamID
            if (examStatus == "Resume" && resumeCount <= 3) {
                //Check if the resume exam id already exist in the horizontal menu
                for (i in horizontalMenuPojoArrayList.indices) {
                    val tempId = horizontalMenuPojoArrayList[i].id
                    if (id == tempId) {
                        horizontalMenuPojoArrayList.removeAt(i)
                        break
                    }
                }
                val questionRelatedStuffs = ExamCacheAllQuestionRelatedStuff(context!!).getCacheAllQuestionRelatedStuff(id)[0]
                val horizontalMenuPojo = HorizontalMenuPojo()
                horizontalMenuPojo.id = id
                horizontalMenuPojo.tag = "Resume"
                horizontalMenuPojo.title = questionRelatedStuffs.examName
                horizontalMenuPojo.description = "Resume this exam"
                horizontalMenuPojo.examType = "EXM"
                horizontalMenuPojoArrayList.add(0, horizontalMenuPojo)
            }
        }

        //Check for quiz resume status
        val currentQuizSessionPojoArrayList = QuizCacheSetCurrentSessionData(context!!).currentExamSession
        if (currentQuizSessionPojoArrayList.isNotEmpty()) {
            val currentSession = currentQuizSessionPojoArrayList[0]
            val examStatus = currentSession.examStatus
            val resumeCount = currentSession.countResum!!.toInt()
            val id = currentSession.submitExamID
            if (examStatus == "Resume" && resumeCount <= 3) {
                val questionRelatedStuffs = QuizCacheAllQuestionRelatedStuff(context!!).cacheAllQuestionRelatedStuff[0]
                val horizontalMenuPojo = HorizontalMenuPojo()
                horizontalMenuPojo.id = id
                horizontalMenuPojo.tag = "Resume"
                horizontalMenuPojo.title = questionRelatedStuffs.examName
                horizontalMenuPojo.description = "Resume this quiz"
                horizontalMenuPojo.examType = "QUIZ"
                horizontalMenuPojoArrayList.add(0, horizontalMenuPojo)
            }
        }
        if (horizontalMenuPojoArrayList.isEmpty()) status = NO_DATA
        if (status == SUCCESS) {
            horizontalMenuHeading!!.visibility = View.VISIBLE
            val rvHorizontalMenu = findViewById<View>(R.id.rvHorizontalMenu) as RecyclerView
            val horizontalMenuAdapter = HorizontalMenuAdapter(horizontalMenuPojoArrayList)
            rvHorizontalMenu.adapter = horizontalMenuAdapter
        } else if (status == NO_DATA) {
            horizontalMenuHeading!!.visibility = View.GONE
        }
    }

    override fun onError(message: String?) {
        Toast.makeText(context, "Check Your Internet Connection", Toast.LENGTH_SHORT).show()
    }

    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        try {
            val jsonObject = JSONObject(response.toString())
            refCode = jsonObject.getString("referral_code")
        } catch (e: Exception) {
            e.message
        }
    }

    companion object {
        private const val TAG = "MainDashboardActivity"
        private const val RESULT_LOAD_IMAGE = 1
        private const val REQUEST_WRITE_PERMISSION = 2
        private const val CONTACTS_ACCESS = 3
        private const val URL_REF_CODE = "https://www.examin.co.in/myaccount/dashboard/exams/Android_user_referral_code_api.php"
        private const val END_SCALE = 0.7f
        private var refCode = ""
    }
}
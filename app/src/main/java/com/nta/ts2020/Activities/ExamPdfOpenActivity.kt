package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.R

class ExamPdfOpenActivity : AppCompatActivity() {
    var pdf: String? = null
    var webView: WebView? = null
    var pDialog: ProgressDialog? = null
    var url = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_list)
        webView = findViewById(R.id.webView)
        pdf = intent.getStringExtra("pdf")
        //        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//            getSupportActionBar().setTitle(pdf.replace("pdf/", ""));
//
//        }
        url = intent.getStringExtra("url")
        init()
        listener()
    }

    private fun init() {
        try {
            val url1 = "https://drive.google.com/viewerng/viewer?embedded=true&url=$url"
            webView!!.settings.javaScriptEnabled = true
            webView!!.settings.builtInZoomControls = true
            webView!!.settings.setSupportZoom(true)
            pDialog = ProgressDialog(this@ExamPdfOpenActivity)
            pDialog!!.setTitle("PDF")
            pDialog!!.setMessage("Loading...")
            pDialog!!.isIndeterminate = false
            pDialog!!.setCancelable(false)
            webView!!.loadUrl(url1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun listener() {
        webView!!.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
                super.onPageStarted(view, url, favicon)
                pDialog!!.show()
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                pDialog!!.dismiss()
                webView!!.loadUrl("javascript:(function() { " +
                        "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none'; })()")
                pDialog!!.dismiss()
                webView!!.visibility = View.VISIBLE
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
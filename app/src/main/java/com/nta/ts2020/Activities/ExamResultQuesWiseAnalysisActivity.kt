package com.nta.ts2020.Activities

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.nta.ts2020.Adapters.QuesWiseAnalysisAdapter
import com.nta.ts2020.LocalStorage.ExamCacheFinalSubmission
import com.nta.ts2020.PojoClasses.FinalSubmissionPojo
import com.nta.ts2020.PojoClasses.QuesWiseAnalysisPojo
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.QuesWiseAnalysisTableView
import de.codecrafters.tableview.model.TableColumnWeightModel
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders
import org.json.JSONArray
import java.util.*

class ExamResultQuesWiseAnalysisActivity : AppCompatActivity() {
    // Collections ....
    var finalSubmissionPojosList: ArrayList<FinalSubmissionPojo>? = null
    var jsonarr: JSONArray? = null
    var jsonDataHeader: JSONArray? = null

    //Cache
    var examCacheFinalSubmissionInstance: ExamCacheFinalSubmission? = null
    private var extras: Bundle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_ques_wise_analysis)

        //Context
        val context: Context = this
        extras = intent.extras
        val id = extras!!.getString("ID")
        val examName = extras!!.getString("EXAM_NAME")
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.title = "Question Wise Time"
        }
        initialization()

        //Final Data Cache
        finalSubmissionPojosList = examCacheFinalSubmissionInstance!!.getFinalSubmissionData(id)
        val quesWiseAnalysisPojoList: MutableList<QuesWiseAnalysisPojo> = ArrayList()
        for (i in finalSubmissionPojosList!![0].responses!!.indices) {
            val finalSubmissionPojo = finalSubmissionPojosList!![0]
            val mStatus: String = if (finalSubmissionPojosList!![0].responses!![i] == "0") "Not Attempted" else "Attempted"
            val quesWiseAnalysisPojo = QuesWiseAnalysisPojo()
            quesWiseAnalysisPojo.quesNo = (i + 1).toString()
            quesWiseAnalysisPojo.status = mStatus
            quesWiseAnalysisPojo.yourAns = finalSubmissionPojo.responses!![i]
            quesWiseAnalysisPojo.correctAns = "Updating"
            quesWiseAnalysisPojo.yourScore = "Updating"
            quesWiseAnalysisPojo.yourTime = finalSubmissionPojo.quesWiseTime!![i]
            quesWiseAnalysisPojo.topperTime = "Updating"
            quesWiseAnalysisPojoList.add(quesWiseAnalysisPojo)
        }
        val quesWiseAnalysisTableView = findViewById<View>(R.id.ctvQuesWiseAnalysis) as QuesWiseAnalysisTableView
        val tableColumnWeightModel = TableColumnWeightModel(7)
        tableColumnWeightModel.setColumnWeight(0, 5)
        tableColumnWeightModel.setColumnWeight(1, 6)
        tableColumnWeightModel.setColumnWeight(2, 6)
        tableColumnWeightModel.setColumnWeight(3, 7)
        tableColumnWeightModel.setColumnWeight(4, 6)
        tableColumnWeightModel.setColumnWeight(5, 6)
        tableColumnWeightModel.setColumnWeight(6, 6)
        quesWiseAnalysisTableView.columnModel = tableColumnWeightModel
        val rowColorEven = ContextCompat.getColor(context, R.color.table_data_row_even)
        val rowColorOdd = ContextCompat.getColor(context, R.color.table_data_row_odd)
        quesWiseAnalysisTableView.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.alternatingRowColors(rowColorEven, rowColorOdd))
        val quesWiseAnalysisAdapter = QuesWiseAnalysisAdapter(context, quesWiseAnalysisPojoList)
        quesWiseAnalysisTableView.dataAdapter = quesWiseAnalysisAdapter
        val simpleTableHeaderAdapter = SimpleTableHeaderAdapter(context, "Q.No.", "Status", "Your Answer", "Correct Answer", "Your Score"
                , "Your Time", "Topper Time")
        simpleTableHeaderAdapter.setTextSize(15)
        simpleTableHeaderAdapter.setTextColor(ContextCompat.getColor(context, R.color.table_header_text))
        quesWiseAnalysisTableView.headerAdapter = simpleTableHeaderAdapter
        val tvTitle = findViewById<View>(R.id.quesWiseAnalysisExamName) as TextView
        tvTitle.text = examName
    }

    fun initialization() {
        examCacheFinalSubmissionInstance = ExamCacheFinalSubmission(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
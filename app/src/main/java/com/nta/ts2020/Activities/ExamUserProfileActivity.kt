package com.nta.ts2020.Activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.mikhaellopez.circularimageview.CircularImageView
import com.nta.ts2020.Activities.ExamProfAcademicDetailsActivity
import com.nta.ts2020.Activities.ExamProfAddressActivity
import com.nta.ts2020.Activities.ExamProfPersonalDetailsActivity
import com.nta.ts2020.Activities.ExamProfSecurityActivity
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R

class ExamUserProfileActivity : AppCompatActivity(), View.OnClickListener {
    private var context: Context? = null
    private var textViewName: TextView? = null
    private var textViewEmail: TextView? = null
    private var textViewCourse: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        if (supportActionBar != null) {
            supportActionBar!!.title = "Profile"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        //Context
        context = this

        //Initialize and Set profile data
        textViewName = findViewById<View>(R.id.tvEditProfName) as TextView
        textViewEmail = findViewById<View>(R.id.tvEditProfEmail) as TextView
        textViewCourse = findViewById<View>(R.id.tvEditProfCourse) as TextView
        val imageViewHeaderPic = findViewById<View>(R.id.ciEditProfPic) as CircularImageView
        val imageViewChangeImage = findViewById<View>(R.id.ivProfChangeImg) as ImageView
        imageViewChangeImage.setOnClickListener(this)
        val userDataPojo = CacheUserData(context as ExamUserProfileActivity).userDataFromCache
        //        textViewName.setText(userDataPojo.getFName() + " " + userDataPojo.getLName()); //-- declared on resume
//        textViewEmail.setText(userDataPojo.getEmail());  //-- declared on resume
//        textViewCourse.setText("Course: " + userDataPojo.getCourseName() + "Class: " + userDataPojo.getClassName());  //-- declared on resume
        //Set image from the server
        if (userDataPojo.profPic != "0") Glide.with(context as ExamUserProfileActivity).load(userDataPojo.profPic).into(imageViewHeaderPic)

        //Menu
        val cvPersonalDetails = findViewById<View>(R.id.cvPersonalDetails) as CardView
        cvPersonalDetails.setOnClickListener(this)
        val cvAcademicDetails = findViewById<View>(R.id.cvAcademicDetails) as CardView
        cvAcademicDetails.setOnClickListener(this)
        val cvSecurity = findViewById<View>(R.id.cvSecurity) as CardView
        cvSecurity.setOnClickListener(this)
        val cvAddress = findViewById<View>(R.id.cvAddress) as CardView
        cvAddress.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.cvPersonalDetails -> startActivity(Intent(this@ExamUserProfileActivity, ExamProfPersonalDetailsActivity::class.java))
            R.id.cvAcademicDetails -> startActivity(Intent(this@ExamUserProfileActivity, ExamProfAcademicDetailsActivity::class.java))
            R.id.cvSecurity -> startActivity(Intent(this@ExamUserProfileActivity, ExamProfSecurityActivity::class.java))
            R.id.cvAddress -> startActivity(Intent(this@ExamUserProfileActivity, ExamProfAddressActivity::class.java))
            R.id.ivProfChangeImg -> {
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val userDataPojo = CacheUserData(context!!).userDataFromCache
        textViewName!!.text = userDataPojo.fName + " " + userDataPojo.lName
        textViewEmail!!.text = userDataPojo.email
        textViewCourse!!.text = "Course: " + userDataPojo.courseName + " Class: " + userDataPojo.className
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
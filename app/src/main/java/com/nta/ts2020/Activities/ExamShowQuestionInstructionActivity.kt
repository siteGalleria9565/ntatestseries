package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.LocalStorage.*
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.PojoClasses.ExamQuestionDataBasedOnExamIdPojo
import com.nta.ts2020.PojoClasses.QuizChapter
import com.nta.ts2020.PojoClasses.QuizSubject
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.CacheStoreDataInLocalStorageHandler
import com.nta.ts2020.ThreadingHelper.Handlers.ExamQuestionDataBasedOnExamIdHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import java.util.*

class ExamShowQuestionInstructionActivity : AppCompatActivity() {
    /**
     * The function returns the Progress Dialog associated with this Activity ....
     *
     * @return
     */
    // Dialogs ...

    lateinit var progressDialog: ProgressDialog

    //Threads
    var localThreadPoolServiceInstance: ThreadPoolService? = null
    var loadExamQuestionDataBasedOnExamIdHandlerHandler: ExamQuestionDataBasedOnExamIdHandler? = null
    var localCacheStoreDataInLocalStorageHandlerInstance: CacheStoreDataInLocalStorageHandler? = null

    // Collections ....
    var localExamQuestionDataBasedOnExamIdPojoList: ArrayList<ExamQuestionDataBasedOnExamIdPojo>? = null

    //Bundle
    var extras: Bundle? = null
    var bundle: Bundle? = null

    //Cache
    var cacheExamCacheAllQuestionRelatedStuffInstance: ExamCacheAllQuestionRelatedStuff? = null
    var examCacheGetCurrentExamSessionInstance: ExamCacheGetCurrentExamSession? = null
    var cacheExamCacheQuestionDataBasedOnExamIdInstance: ExamCacheQuestionDataBasedOnExamId? = null
    var instructionWebView: WebView? = null
    var nextActivityButton: Button? = null
    var ExamID: String? = null
    var ExamStatus: String? = null

    //Log
    private val TAG = "AllExamDataActivity"
    private var examCacheSetCurrentSessionDataInstance: ExamCacheSetCurrentSessionData? = null
    private var examType: String? = null
    private var localQuestionDataBasedOnExamIdPojoList: ArrayList<ExamQuestionDataBasedOnExamIdPojo>? = null
    private var cacheQuizCacheAllQuestionRelatedStuffInstance: QuizCacheAllQuestionRelatedStuff? = null
    private var quizCacheSetCurrentSessionDataInstance: QuizCacheSetCurrentSessionData? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exam_show_question_instructions_activity)
        if (supportActionBar != null) {
            supportActionBar!!.title = "Instructions"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Initialized
        initialization()
        extras = intent.extras
        if (extras != null) {
            examType = extras!!.getString("TYPE")
            ExamStatus = extras!!.getString("Status")
            when (examType) {
                "EXM" -> {
                    ExamID = extras!!.getString("ExamID")
                    Log.d(TAG, "#IntentData Pass By ShowAllExaminationDataList Activity with id $ExamID")
                    if (ExamStatus == "Resume") {
                        nextActivityButton!!.setBackgroundResource(R.drawable.instruction_start_test)
                        nextActivityButton!!.isEnabled = true
                        LoadData(null)
                    } else {
                        ExamAssetToLocalDatabase.getHelper(this)?.deleteCurrentSessionData()
                        /******************************************************************************************************
                         * Starting the Progress Dialog so as to Save the Data in the Background ....
                         */
                        progressDialog = ProgressDialog.show(this, "Loading ....", "Please wait", true, false)
                        Log.d(TAG, "#Invoked Thread Pool Service")
                        localThreadPoolServiceInstance!!.LoadQuestionsBasedOnExamIDFromServer(this, loadExamQuestionDataBasedOnExamIdHandlerHandler, ExamID, ConstantVariables.EXAM)
                        nextActivityButton!!.setBackgroundResource(R.drawable.instruction_loading)
                        nextActivityButton!!.isEnabled = false
                    }
                }
                "QUIZ" -> {
                    QuizSubject.subjectIds.clear()
                    QuizChapter.chapterIds.clear()
                    val chapterIds = extras!!.getString("chapter_ids")
                    val numOfSubjects = extras!!.getString("num_of_subjects")
                    val selectedTime = extras!!.getString("selected_time")
                    val selectedLanguage = extras!!.getString("selected_language")
                    val examStatus = extras!!.getString("Status")
                    Log.d(TAG, "#Invoked Thread Pool Service")
                    if (examStatus == "Resume") {
                        nextActivityButton!!.setBackgroundResource(R.drawable.instruction_start_test)
                        nextActivityButton!!.isEnabled = true
                        LoadData(null)
                    } else {
                        QuizAssetToLocalDatabase.getHelper(this)?.deleteCurrentSessionData()
                        /******************************************************************************************************
                         * Starting the Progress Dialog so as to Save the Data in the Background ....
                         */
                        progressDialog = ProgressDialog.show(this, "Loading ....", "Please wait", true, false)
                        localThreadPoolServiceInstance!!.LoadQuestionsFromServer(this, loadExamQuestionDataBasedOnExamIdHandlerHandler, chapterIds, numOfSubjects, selectedTime, selectedLanguage)
                        nextActivityButton!!.setBackgroundResource(R.drawable.instruction_loading)
                        nextActivityButton!!.isEnabled = false
                    }
                }
            }
        }
    }

    fun LoadData(examAllQuestionsSubjectWisePojoList: ArrayList<ExamAllQuestionsSubjectWisePojo>?) {
        var QuestionInstruction: String? = null
        val time: String? = null
        val section = 4
        when (examType) {
            "EXM" -> {
                localExamQuestionDataBasedOnExamIdPojoList = cacheExamCacheAllQuestionRelatedStuffInstance!!.getCacheAllQuestionRelatedStuff(ExamID)
                Log.d("ListOfDataSize", "Show= $examAllQuestionsSubjectWisePojoList")
                if (ExamStatus != "Resume" && examAllQuestionsSubjectWisePojoList != null) {


                    //first insert this record
                    ExamID?.let { examCacheSetCurrentSessionDataInstance!!.setCurrentExamSession(it, (60 * localExamQuestionDataBasedOnExamIdPojoList?.get(0)?.timeDurationInSeconds!!.toInt()).toString()) }
                    localThreadPoolServiceInstance!!.saveQuestionDataInCache(this, localCacheStoreDataInLocalStorageHandlerInstance, examAllQuestionsSubjectWisePojoList)
                    localThreadPoolServiceInstance!!.saveSessionStatusInCache(this, localCacheStoreDataInLocalStorageHandlerInstance, examAllQuestionsSubjectWisePojoList)
                }
                Log.d(TAG, "#IntentData Pass By dismissProgressDialog with id $ExamID")
                Log.d(TAG, "#examCacheAllExaminationDataInstance with ExamID" + localExamQuestionDataBasedOnExamIdPojoList?.get(0)?.examId)
                QuestionInstruction = localExamQuestionDataBasedOnExamIdPojoList?.get(0)?.instructions
                if (QuestionInstruction == "0") {
                    QuestionInstruction = "<html> <body> <h3>Paper Instructions:</h3>  <ol> <li>The question paper contains <b>$section sections </b> and <b>300 questions.</b> <br/><br/> </li> <ul><li><b>Section 1 (Physics)</b> contains100 questions .</li><li><b>Section 2 (Chemistry)</b> contains100 questions .</li><li><b>Section 3 (Mathematics)</b> contains100 questions .</li><li><b>Section 4 (Biology)</b> contains100 questions .</li> </ul><br/> <li>For each correct answer you will be awarded with <b>4 marks</b></li> <li>For each wrong answer you will be awarded with <b>-1 mark</b></li> <li>Total allotted time for this test is <b>$time Seconds</b> </li> </ol> </body></html>"
                }
                instructionWebView!!.loadData(QuestionInstruction, "text/html", "UTF-8")
            }
            "QUIZ" -> {
                localQuestionDataBasedOnExamIdPojoList = cacheQuizCacheAllQuestionRelatedStuffInstance!!.cacheAllQuestionRelatedStuff
                ExamID = localQuestionDataBasedOnExamIdPojoList?.get(0)?.examId
                Log.d("ListOfDataSize", "Show= $examAllQuestionsSubjectWisePojoList")
                if (ExamStatus != "Resume" && examAllQuestionsSubjectWisePojoList != null) {
                    quizCacheSetCurrentSessionDataInstance!!.setCurrentExamSession(ExamID, (60 * localQuestionDataBasedOnExamIdPojoList?.get(0)?.timeDurationInSeconds!!.toInt()).toString())
                    localThreadPoolServiceInstance!!.saveQuizQuestionDataInCache(this, localCacheStoreDataInLocalStorageHandlerInstance, examAllQuestionsSubjectWisePojoList)
                    localThreadPoolServiceInstance!!.saveQuizSessionStatusInCache(this, examAllQuestionsSubjectWisePojoList)
                }
                Log.d(TAG, "#quizCacheAllExaminationDataInstance with ExamID" + localQuestionDataBasedOnExamIdPojoList?.get(0)?.examId)
                QuestionInstruction = localQuestionDataBasedOnExamIdPojoList?.get(0)?.instructions
                if (QuestionInstruction == "0") {
                    QuestionInstruction = "<html> <body> <h3>Paper Instructions:</h3>  <ol> <li>The question paper contains <b>$section sections </b> and <b>300 questions.</b> <br/><br/> </li> <ul><li><b>Section 1 (Physics)</b> contains100 questions .</li><li><b>Section 2 (Chemistry)</b> contains100 questions .</li><li><b>Section 3 (Mathematics)</b> contains100 questions .</li><li><b>Section 4 (Biology)</b> contains100 questions .</li> </ul><br/> <li>For each correct answer you will be awarded with <b>4 marks</b></li> <li>For each wrong answer you will be awarded with <b>-1 mark</b></li> <li>Total allotted time for this test is <b>$time Seconds</b> </li> </ol> </body></html>"
                }
                instructionWebView!!.loadData(QuestionInstruction, "text/html", "UTF-8")
            }
        }
    }

    fun stopWatch() {
        nextActivityButton!!.setBackgroundResource(R.drawable.instruction_start_test)
        nextActivityButton!!.isEnabled = true
    }

    fun initialization() {
        bundle = Bundle()
        localThreadPoolServiceInstance = ThreadPoolService()
        nextActivityButton = findViewById<View>(R.id.startTestActivity) as Button
        nextActivityButton!!.setOnClickListener(mClickListners)
        loadExamQuestionDataBasedOnExamIdHandlerHandler = ExamQuestionDataBasedOnExamIdHandler(this@ExamShowQuestionInstructionActivity, nextActivityButton)
        localCacheStoreDataInLocalStorageHandlerInstance = CacheStoreDataInLocalStorageHandler(this, this@ExamShowQuestionInstructionActivity)
        cacheExamCacheAllQuestionRelatedStuffInstance = ExamCacheAllQuestionRelatedStuff(this)
        cacheExamCacheQuestionDataBasedOnExamIdInstance = ExamCacheQuestionDataBasedOnExamId(this)
        examCacheSetCurrentSessionDataInstance = ExamCacheSetCurrentSessionData(this)
        examCacheGetCurrentExamSessionInstance = ExamCacheGetCurrentExamSession(this)
        instructionWebView = findViewById<View>(R.id.instructionWebView) as WebView

        //Quiz
        cacheQuizCacheAllQuestionRelatedStuffInstance = QuizCacheAllQuestionRelatedStuff(this)
        quizCacheSetCurrentSessionDataInstance = QuizCacheSetCurrentSessionData(this)
    }

    var mClickListners = View.OnClickListener {
        val intent = Intent(this@ExamShowQuestionInstructionActivity, ExamShowQuestionDataBaseOnExamIdListActivity::class.java)
        intent.putExtra("ExamID", ExamID)
        intent.putExtra("TYPE", examType)
        startActivity(intent)
        finish()
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    override fun onBackPressed() {
        startActivity(Intent(this@ExamShowQuestionInstructionActivity, ExamMainDashboardActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            startActivity(Intent(this@ExamShowQuestionInstructionActivity, ExamMainDashboardActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        }
        return super.onOptionsItemSelected(item)
    }
}
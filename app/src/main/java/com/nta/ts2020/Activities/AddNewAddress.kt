package com.nta.ts2020.Activities

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.*

class AddNewAddress : AppCompatActivity(), WebResponseListener {
    var address: EditText? = null
    var country: EditText? = null
    var pin: EditText? = null
    var save: Button? = null
    private var city: AutoCompleteTextView? = null
    private var state: AutoCompleteTextView? = null
    var completeAddress: StringBuffer? = null
    var states = arrayOf("Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam, Bihar", "Chandigarh",
            "Chhattisgarh", "Dadra and Nagar Haveli", "Daman & Diu", "Delhi", "Goa", "Gujarat", "Haryana",
            "Himachal Pradesh", "Jammu & Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh",
            "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Punjab", "Rajasthan", "Sikkim",
            "Tamil Nadu", "Telangana", "Tripura", "Uttarakhand", "Uttar Pradesh", "West Bengal")
    lateinit var cities: Array<String?>
    private var webRequest: WebRequest? = null
    override fun onNavigateUp(): Boolean {
        finish()
        return true
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_address)
        address = findViewById(R.id.tvAddAddress)
        city = findViewById(R.id.tvAddCity)
        state = findViewById(R.id.tvAddState)
        country = findViewById(R.id.tvAddCountry)
        pin = findViewById(R.id.tvAddPinCode)
        save = findViewById(R.id.btAddSave)
        webRequest = WebRequest(this)
        completeAddress = StringBuffer()
        title = "New Address"
        Objects.requireNonNull(supportActionBar)!!.setDisplayHomeAsUpEnabled(true)
        editAddress()
        save?.setOnClickListener {
            if (validate()) {
                completeAddress!!.append(address?.text.toString()).append(", ").append(city?.text.toString()).append(", ").append(state?.text.toString()).append(", ").append(country?.text.toString()).append(", ").append(pin?.text.toString())
                val userId = CacheUserData(this@AddNewAddress).userDataFromCache.userId
                val URL = ConstantVariables.URL_ADD_NEW_ADDRESS + "?app=NTA&user_id=" + userId + "&shipping_address=" + completeAddress.toString()
                webRequest!!.GET_METHOD(URL, this@AddNewAddress, WebCallType.ADD_NEW_ADDRESS, true)
            }
        }
        SetCitiesAsync().execute()
    }

    private fun editAddress() {
        if (intent.extras != null) {
            if (intent.getStringExtra("new_address") != "") {
                val temp = intent.getStringExtra("new_address")
                val addressList = temp.split(",".toRegex()).toTypedArray()
                address!!.setText(addressList[0].trim { it <= ' ' })
                city!!.setText(addressList[1].trim { it <= ' ' })
                state!!.setText(addressList[2].trim { it <= ' ' })
                country!!.setText(addressList[3].trim { it <= ' ' })
                pin!!.setText(addressList[4].trim { it <= ' ' })
            }
        }
    }

    fun validate(): Boolean {
        var status = true
        if (address!!.text.toString().isEmpty()) {
            address!!.error = "Enter address!"
            status = false
        }
        if (city!!.text.toString().isEmpty()) {
            city!!.error = "Enter City!"
            status = false
        }
        if (state!!.text.toString().isEmpty()) {
            state!!.error = "Enter state!"
            status = false
        }
        if (country!!.text.toString().isEmpty()) {
            country!!.error = "Enter country!"
            status = false
        }
        if (pin!!.text.toString().isEmpty()) {
            pin!!.error = "Enter PIN"
            status = false
        }
        return status
    }

    override fun onError(message: String?) {}
    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        if (response != null) {
            Log.d("ZZZ", "onResponse: $response")
            val intent = Intent()
            intent.putExtra("new_address", completeAddress.toString())
            setResult(7, intent)
            finish()
        }
    }

    private inner class SetCitiesAsync : AsyncTask<Void?, Int?, Long?>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }


        override fun onPostExecute(aLong: Long?) {
            super.onPostExecute(aLong)
            val cityAdapter = ArrayAdapter(this@AddNewAddress, android.R.layout.simple_list_item_1, cities)
            val stateAdapter = ArrayAdapter(this@AddNewAddress, android.R.layout.simple_list_item_1, states)
            state!!.threshold = 0
            state!!.setAdapter(stateAdapter)
            state!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
                hideKeyboard(this@AddNewAddress)
                city!!.setText("")
                state!!.error = null
                state!!.setSelection(state!!.text.toString().length)
            }
            city!!.threshold = 0
            city!!.setAdapter(cityAdapter)
            city!!.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
                hideKeyboard(this@AddNewAddress)
                city!!.error = null
                val cityName = city!!.text.toString()
                city!!.setText(cityName.substring(0, cityName.indexOf(",")))
                city!!.setSelection(city!!.text.toString().length)
                state!!.setText(cityName.substring(cityName.indexOf(",") + 2))
            }
        }

        override fun doInBackground(vararg params: Void?): Long? {
            cities = citiesString
            return null
        }
    }

    val citiesString: Array<String?>
        get() {
            val cityWithState: List<String>? = citiesJson
            lateinit var stockArr: Array<String?>
            if (cityWithState != null) {
                stockArr = cityWithState.toTypedArray()
            }
            return stockArr
        }

    private val citiesJson: List<String>?
        get() {
            val cityWithState: MutableList<String> = ArrayList()
            try {
                val jsonArray = JSONArray(loadJSONFromAsset())
                for (i in 0 until jsonArray.length()) {
                    val jsonObject = JSONObject(jsonArray.getJSONObject(i).toString())
                    cityWithState.add(jsonObject.getString("name") + ", " + jsonObject.getString("state"))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
                return null
            }
            return cityWithState
        }

    private fun loadJSONFromAsset(): String? {
        val json: String
        json = try {
            val `is` = application.assets.open("cities.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, StandardCharsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    companion object {
        fun hideKeyboard(activity: Activity) {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}
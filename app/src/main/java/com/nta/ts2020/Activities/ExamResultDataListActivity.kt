package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.nta.ts2020.AppConstants.ConstantVariables.RESULT_DATA_LIST_ACTIVITY
import com.nta.ts2020.Fragments.ExamResultListFragment
import com.nta.ts2020.Fragments.QuizResultListFragment
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.SaveResultListFromServerToCacheHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import java.util.*

/**
 * Created by Suvajit on 27/1/17.
 */
class ExamResultDataListActivity : AppCompatActivity() {
    //Log
    private val TAG = "MyResultActivity"
    private var progressDialog: ProgressDialog? = null
    private var context: Context? = null
    private var examFragment: ExamResultListFragment? = null
    private var quizFragment: QuizResultListFragment? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.my_result_list_activity)
        if (supportActionBar != null) {
            supportActionBar!!.title = "Results"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        context = this
        val viewPager = findViewById<View>(R.id.vpMyResult) as ViewPager
        setupViewPager(viewPager)
        val tabLayout = findViewById<View>(R.id.tabMyResult) as TabLayout
        tabLayout.setupWithViewPager(viewPager)


        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true
        val saveResultListFromServerToCacheHandler = SaveResultListFromServerToCacheHandler(this@ExamResultDataListActivity, RESULT_DATA_LIST_ACTIVITY)
        if (isNetworkConnected) {
            progressDialog!!.show()
            ThreadPoolService().saveResultListFromServerToCache(this, saveResultListFromServerToCacheHandler)
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        examFragment = ExamResultListFragment()
        quizFragment = QuizResultListFragment()
        adapter.addFragment(examFragment!!, "Exam")
        adapter.addFragment(quizFragment!!, "Quiz")
        viewPager.adapter = adapter
    }

    private inner class ViewPagerAdapter internal constructor(manager: FragmentManager?) : FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> = ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    fun loadData(status: Int) {
        examFragment!!.loadData(status)
        quizFragment!!.loadData(status)
        progressDialog!!.dismiss()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
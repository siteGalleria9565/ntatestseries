package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.ExamInProgressDialog

class ExamNotificationWebViewActivity : AppCompatActivity() {
    private var context: Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_webview)
        context = this
        var isPdf = false
        if (intent.extras != null) {
            val url = intent.extras!!.getString("url")
            val name = intent.extras!!.getString("name")

            //Set actionbar tittle
            if (supportActionBar != null) {
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
                supportActionBar!!.title = name
            }
            val progressDialog: ProgressDialog = ExamInProgressDialog(context)
            progressDialog.show()
            Log.d(TAG, url)
            if (url != null) if (url.length > 3) {
                val extension = url.substring(url.length - 4)
                if (extension == ".pdf") isPdf = true
            }
            val webView = findViewById<View>(R.id.notificationWebView) as WebView
            webView.settings.setAppCacheMaxSize(10 * 1024 * 1024.toLong()) // 10MB
            webView.settings.setAppCachePath(applicationContext.cacheDir.absolutePath)
            webView.settings.allowFileAccess = true
            webView.settings.setAppCacheEnabled(true)
            webView.settings.javaScriptEnabled = true
            webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
            webView.settings.builtInZoomControls = true
            webView.settings.displayZoomControls = false
            if (isPdf) {
                webView.loadUrl(PDF_VIEWER_URL + url)
                Log.d("##", PDF_VIEWER_URL + url)
            } else webView.loadUrl(url)
            webView.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    super.onPageFinished(view, url)
                    //Hide the pop-out button from the pdf viewer
                    webView.loadUrl("javascript:(function() { " +
                            "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none'; })()")
                    progressDialog.dismiss()
                }

                override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                    super.onReceivedError(view, request, error)
                    findViewById<View>(R.id.notificationWebView).visibility = View.GONE
                    findViewById<View>(R.id.notificationWebViewError).visibility = View.VISIBLE
                    val icon = findViewById<View>(R.id.ivExamResultListError) as ImageView
                    icon.setImageResource(R.drawable.ic_no_network)
                    val message = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
                    message.setText(R.string.sub_message_internet_error)
                }
            }
        } else {
            Toast.makeText(this, "Something went wrong.", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    companion object {
        private const val PDF_VIEWER_URL = "https://docs.google.com/gview?embedded=true&url="
        private const val TAG = "NotifcatonWbViwActivity"
    }
}
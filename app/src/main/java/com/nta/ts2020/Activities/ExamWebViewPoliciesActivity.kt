package com.nta.ts2020.Activities

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.AppConstants.ConstantVariables.ABOUT_US
import com.nta.ts2020.AppConstants.ConstantVariables.PRIVACY_POLICY
import com.nta.ts2020.AppConstants.ConstantVariables.TERMS_CONDITIONS
import com.nta.ts2020.R

class ExamWebViewPoliciesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview_policies)
        val type = intent.extras!!.getString("TYPE")
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        val webView = findViewById<View>(R.id.wvPolicies) as WebView
        webView.setBackgroundColor(0)
        webView.setBackgroundResource(R.drawable.transparent_background)
        if (type != null) {
            when (type) {
                PRIVACY_POLICY -> {
                    webView.loadUrl("file:///android_asset/html_policies/privacy.html")
                    actionBar?.setTitle("Privacy & policy")
                }
                TERMS_CONDITIONS -> {
                    webView.loadUrl("file:///android_asset/html_policies/terms.html")
                    actionBar?.setTitle("Terms & conditions")
                }
                ABOUT_US -> {
                    webView.loadUrl("file:///android_asset/html_policies/about_us.html")
                    actionBar?.setTitle("About us")
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
package com.nta.ts2020.Activities

import android.animation.ObjectAnimator
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter
import com.github.aakira.expandablelayout.ExpandableLinearLayout
import com.github.aakira.expandablelayout.Utils
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.nta.ts2020.Adapters.QuesAnsKeyGridAdapter
import com.nta.ts2020.AppConstants.ConstantVariables.QUES_ANS_KEY
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.LocalStorage.ExamCacheFinalSubmission
import com.nta.ts2020.PojoClasses.ExamAllQuestionsSubjectWisePojo
import com.nta.ts2020.PojoClasses.ExamQuestionDataBasedOnExamIdPojo
import com.nta.ts2020.PojoClasses.FinalSubmissionPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.ExamQuestionDataBasedOnExamIdHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.UtilsHelper.NestedScrollWebView
import java.util.*

class ExamResultQuesAndAnsKeyActivity : AppCompatActivity(), View.OnClickListener {
    private var context: ExamResultQuesAndAnsKeyActivity? = null
    private var progressDialog: ProgressDialog? = null
    private var quest: WebView? = null
    private var webViewOptionA: WebView? = null
    private var webViewOptionB: WebView? = null
    private var webViewOptionC: WebView? = null
    private var webViewOptionD: WebView? = null
    private var webViewOptionE: WebView? = null
    private var webViewOptionARadio: WebView? = null
    private var webViewOptionBRadio: WebView? = null
    private var webViewOptionCRadio: WebView? = null
    private var webViewOptionDRadio: WebView? = null
    private var webViewOptionERadio: WebView? = null
    private var radioButtonOptionA: RadioButton? = null
    private var radioButtonOptionB: RadioButton? = null
    private var radioButtonOptionC: RadioButton? = null
    private var radioButtonOptionD: RadioButton? = null
    private var radioButtonOptionE: RadioButton? = null
    private var clock: TextView? = null
    private var QuestionNo: TextView? = null
    private var grid: TextView? = null
    private var time: TextView? = null
    private var totalQuestionTextView: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var nextButton: RelativeLayout? = null
    private var PreviousButton: RelativeLayout? = null
    private var tabLayout: TabLayout? = null

    //Added Later
    private var paragraphDataView: NestedScrollWebView? = null
    private var multiRowsRadioGroup: RadioGroup? = null
    private var showCheckQuestionOnFragmentLayout: LinearLayout? = null
    private var showRadioQuestionOnFragmentLayout: LinearLayout? = null
    private var linLayoutOptionA: LinearLayout? = null
    private var linLayoutOptionB: LinearLayout? = null
    private var linLayoutOptionC: LinearLayout? = null
    private var linLayoutOptionD: LinearLayout? = null
    private var linLayoutOptionE: LinearLayout? = null
    private var linLayoutOptionARadio: LinearLayout? = null
    private var linLayoutOptionBRadio: LinearLayout? = null
    private var linLayoutOptionCRadio: LinearLayout? = null
    private var linLayoutOptionDRadio: LinearLayout? = null
    private var linLayoutOptionERadio: LinearLayout? = null
    private var paragraphHeadingLayout: RelativeLayout? = null
    private var paragraphHeadingButtonHolder: RelativeLayout? = null
    private var paragrapRelativeLayout: RelativeLayout? = null
    private var paragraphHeading: TextView? = null
    private var paragraphExpandableLayout: ExpandableLinearLayout? = null
    private var checkBoxOptionA: CheckBox? = null
    private var checkBoxOptionB: CheckBox? = null
    private var checkBoxOptionC: CheckBox? = null
    private var checkBoxOptionD: CheckBox? = null
    private var checkBoxOptionE: CheckBox? = null
    private var localExamAllQuestionsSubjectWisePojoList: ArrayList<ExamAllQuestionsSubjectWisePojo>? = null
    private var countValue = 0
    private var examQuestionDataBasedOnExamIdPojo: ExamQuestionDataBasedOnExamIdPojo? = null
    private var finalSubmissionPojo: FinalSubmissionPojo? = null
    private var subjectNames: Array<String?> = emptyArray()
    private var dialog: AlertDialog? = null
    var localTabQuestionStatus: HashMap<String?, Int>? = null
    private var errorLayout: LinearLayout? = null
    private var examContainer: RelativeLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exam_activity_questions_screen)

        //Context
        context = this
        var id: String? = ""
        if (intent.extras != null) {
            id = intent.extras!!.getString("ID")
        }

        //Initialization
        localTabQuestionStatus = HashMap()
        showCheckQuestionOnFragmentLayout = findViewById<View>(R.id.linLayout) as LinearLayout
        showRadioQuestionOnFragmentLayout = findViewById<View>(R.id.radioLayout) as LinearLayout
        errorLayout = findViewById<View>(R.id.examErrorLayout) as LinearLayout
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        examContainer = findViewById<View>(R.id.examContainer) as RelativeLayout
        tabLayout = findViewById<View>(R.id.showQuestionTabs) as TabLayout
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL
        val font = Typeface.createFromAsset(assets, "fontawesome-webfont.ttf")
        clock = findViewById<View>(R.id.clock) as TextView
        clock!!.setTypeface(font)
        clock!!.visibility = View.GONE
        quest = findViewById<View>(R.id.questioText) as WebView
        QuestionNo = findViewById<View>(R.id.Qno) as TextView
        nextButton = findViewById<View>(R.id.NextButton) as RelativeLayout
        PreviousButton = findViewById<View>(R.id.PreviousButton) as RelativeLayout
        grid = findViewById<View>(R.id.gridClick) as TextView
        grid!!.setTypeface(font)
        time = findViewById<View>(R.id.time) as TextView
        time!!.visibility = View.GONE
        totalQuestionTextView = findViewById<View>(R.id.totalQuestion) as TextView
        val reviewCheckBox = findViewById<View>(R.id.reviewCheckBox) as CheckBox
        reviewCheckBox.visibility = View.GONE
        nextButton!!.setOnClickListener(this)
        PreviousButton!!.setOnClickListener(this)
        grid!!.setOnClickListener(this)

        //Option Radio Buttons Initialization -- Suvajit
        linLayoutOptionA = findViewById<View>(R.id.linLayoutOptionA) as LinearLayout
        linLayoutOptionB = findViewById<View>(R.id.linLayoutOptionB) as LinearLayout
        linLayoutOptionC = findViewById<View>(R.id.linLayoutOptionC) as LinearLayout
        linLayoutOptionD = findViewById<View>(R.id.linLayoutOptionD) as LinearLayout
        linLayoutOptionE = findViewById<View>(R.id.linLayoutOptionE) as LinearLayout
        multiRowsRadioGroup = findViewById<View>(R.id.multiRowRadioOptionGroup) as RadioGroup
        webViewOptionA = findViewById<View>(R.id.webViewOptionA) as WebView
        webViewOptionB = findViewById<View>(R.id.webViewOptionB) as WebView
        webViewOptionC = findViewById<View>(R.id.webViewOptionC) as WebView
        webViewOptionD = findViewById<View>(R.id.webViewOptionD) as WebView
        webViewOptionE = findViewById<View>(R.id.webViewOptionE) as WebView

        //Resume
        linLayoutOptionARadio = findViewById<View>(R.id.linLayoutOptionARadio) as LinearLayout
        linLayoutOptionBRadio = findViewById<View>(R.id.linLayoutOptionBRadio) as LinearLayout
        linLayoutOptionCRadio = findViewById<View>(R.id.linLayoutOptionCRadio) as LinearLayout
        linLayoutOptionDRadio = findViewById<View>(R.id.linLayoutOptionDRadio) as LinearLayout
        linLayoutOptionERadio = findViewById<View>(R.id.linLayoutOptionERadio) as LinearLayout
        webViewOptionARadio = findViewById<View>(R.id.webViewOptionARadio) as WebView
        webViewOptionBRadio = findViewById<View>(R.id.webViewOptionBRadio) as WebView
        webViewOptionCRadio = findViewById<View>(R.id.webViewOptionCRadio) as WebView
        webViewOptionDRadio = findViewById<View>(R.id.webViewOptionDRadio) as WebView
        webViewOptionERadio = findViewById<View>(R.id.webViewOptionERadio) as WebView
        radioButtonOptionA = findViewById<View>(R.id.radioButtonA) as RadioButton
        radioButtonOptionB = findViewById<View>(R.id.radioButtonB) as RadioButton
        radioButtonOptionC = findViewById<View>(R.id.radioButtonC) as RadioButton
        radioButtonOptionD = findViewById<View>(R.id.radioButtonD) as RadioButton
        radioButtonOptionE = findViewById<View>(R.id.radioButtonE) as RadioButton
        radioButtonOptionA!!.isEnabled = false
        radioButtonOptionB!!.isEnabled = false
        radioButtonOptionC!!.isEnabled = false
        radioButtonOptionD!!.isEnabled = false
        radioButtonOptionE!!.isEnabled = false

        //Option Radio Buttons -- End -- Suvajit

        //Expandable Paragraph View Initialization --Start-- Suvajit
        paragraphDataView = findViewById<View>(R.id.webViewParagraphData) as NestedScrollWebView
        paragraphHeadingLayout = findViewById<View>(R.id.relativeLayoutParagraphHeading) as RelativeLayout
        paragraphHeadingButtonHolder = findViewById<View>(R.id.relativeLayoutParagrahButtonHolder) as RelativeLayout
        paragraphHeading = findViewById<View>(R.id.textViewParagraphHeading) as TextView
        paragraphExpandableLayout = findViewById<View>(R.id.expandableLayoutParagraph) as ExpandableLinearLayout
        paragrapRelativeLayout = findViewById<View>(R.id.relativeLayoutParagraph) as RelativeLayout
        paragraphHeadingLayout!!.setOnClickListener(this)
        paragraphExpandableLayout!!.setListener(object : ExpandableLayoutListenerAdapter() {
            override fun onPreOpen() {
                super.onPreOpen()
                createRotateAnimator(paragraphHeadingButtonHolder, 0f, 180f).start()
            }

            override fun onPreClose() {
                super.onPreClose()
                createRotateAnimator(paragraphHeadingButtonHolder, 180f, 0f).start()
            }
        })
        //Expandable Paragraph View Initialization --End-- Suvajit

        //PAR fragment Initialization --Start-- Suvajit
        checkBoxOptionA = findViewById<View>(R.id.checkBoxA) as CheckBox
        checkBoxOptionB = findViewById<View>(R.id.checkBoxB) as CheckBox
        checkBoxOptionC = findViewById<View>(R.id.checkBoxC) as CheckBox
        checkBoxOptionD = findViewById<View>(R.id.checkBoxD) as CheckBox
        checkBoxOptionE = findViewById<View>(R.id.checkBoxE) as CheckBox
        checkBoxOptionA!!.isEnabled = false
        checkBoxOptionB!!.isEnabled = false
        checkBoxOptionC!!.isEnabled = false
        checkBoxOptionD!!.isEnabled = false
        checkBoxOptionE!!.isEnabled = false

        //PAR fragment Initialization --End-- Suvajit


        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true
        //End initialization
        val finalSubmissionPojoArrayList = ExamCacheFinalSubmission(context!!).getFinalSubmissionData(id)
        finalSubmissionPojo = finalSubmissionPojoArrayList[0]
        progressDialog!!.show()
        val examQuestionDataBasedOnExamIdHandler = ExamQuestionDataBasedOnExamIdHandler(context!!, null)
        ThreadPoolService().LoadQuestionsBasedOnExamIDFromServer(context, examQuestionDataBasedOnExamIdHandler, id, QUES_ANS_KEY)
    }

    //Called after data loaded from server
    fun loadData(localQuestionDataBasedOnExamIdList: ArrayList<ExamAllQuestionsSubjectWisePojo>?,
                 examQuestionDataBasedOnExamIdPojo: ExamQuestionDataBasedOnExamIdPojo?, status: Int) {
        if (status == SUCCESS && localQuestionDataBasedOnExamIdList != null && examQuestionDataBasedOnExamIdPojo != null) {
            errorLayout!!.visibility = View.GONE
            examContainer!!.visibility = View.VISIBLE
            this.examQuestionDataBasedOnExamIdPojo = examQuestionDataBasedOnExamIdPojo
            localExamAllQuestionsSubjectWisePojoList = localQuestionDataBasedOnExamIdList
            subjectNames = examQuestionDataBasedOnExamIdPojo.subjectNames
            for (subjectName in subjectNames) {
                tabLayout!!.addTab(tabLayout!!.newTab().setText(subjectName))
                localTabQuestionStatus!![subjectName] = 0
            }
            tabLayout!!.addOnTabSelectedListener(object : OnTabSelectedListener {
                var item: String? = null
                var k = 0
                override fun onTabSelected(tab: TabLayout.Tab) {
                    item = tab.text.toString()
                    if (localTabQuestionStatus!![item] == 0) {
                        k = 0
                        while (k < localExamAllQuestionsSubjectWisePojoList!!.size) {
                            if (item == localExamAllQuestionsSubjectWisePojoList!![k].subjectId) {
                                break
                            }
                            k++
                        }
                        dataShow(k)
                    } else localTabQuestionStatus!![item]?.let { dataShow(it) }
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {
                    item = tab.text.toString()
                    if (localTabQuestionStatus!![item] == 0) {
                        k = 0
                        while (k < localExamAllQuestionsSubjectWisePojoList!!.size) {
                            if (item == localExamAllQuestionsSubjectWisePojoList!![k].subjectId) {
                                break
                            }
                            k++
                        }
                        dataShow(k)
                    } else localTabQuestionStatus!![item]?.let { dataShow(it) }
                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                    item = tab.text.toString()
                    if (localTabQuestionStatus!![item] == 0) {
                        k = 0
                        while (k < localExamAllQuestionsSubjectWisePojoList!!.size) {
                            if (item == localExamAllQuestionsSubjectWisePojoList!![k].subjectId) {
                                break
                            }
                            k++
                        }
                        dataShow(k)
                    } else localTabQuestionStatus!![item]?.let { dataShow(it) }
                }
            })
            totalQuestionTextView!!.text = String.format("Total Question %s", examQuestionDataBasedOnExamIdPojo.totalQuestions)
            dataShow(0)
        } else {
            examContainer!!.visibility = View.GONE
            errorLayout!!.visibility = View.VISIBLE
            textViewSubMessage!!.setText(R.string.sub_message_internet_error)
        }
        progressDialog!!.dismiss()
    }

    fun dataShow(CountVal: Int) {
        if (localExamAllQuestionsSubjectWisePojoList != null && examQuestionDataBasedOnExamIdPojo != null) {
            countValue = CountVal
            val currentQuesId = localExamAllQuestionsSubjectWisePojoList!![countValue].questionId
            val responsePos = finalSubmissionPojo!!.quesIds!!.indexOf(currentQuesId)
            val answerStatus = finalSubmissionPojo!!.responses!![responsePos]
            val rightAns = localExamAllQuestionsSubjectWisePojoList!![countValue].rightAns
            val answerStatusList = ArrayList(Arrays.asList(*answerStatus.split(" ".toRegex()).toTypedArray()))
            val rightAnsList = ArrayList(Arrays.asList(*rightAns!!.split(",".toRegex()).toTypedArray()))
            val quesType = localExamAllQuestionsSubjectWisePojoList!![countValue].questionType


            //Expandable View Paragraph --Start-- Suvajit
            paragraphHeading!!.text = PARAGRAPH_HEADING
            paragraphExpandableLayout!!.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR))
            if (localExamAllQuestionsSubjectWisePojoList!![CountVal].essayData != "") {
                paragrapRelativeLayout!!.visibility = View.VISIBLE
                paragraphHeadingLayout!!.visibility = View.VISIBLE
                paragraphDataView!!.loadData(Html.fromHtml(localExamAllQuestionsSubjectWisePojoList!![CountVal].essayData!!.trim { it <= ' ' }).toString() + "<br>", "text/html", "UTF-8")
            } else {
                paragrapRelativeLayout!!.visibility = View.GONE
                paragraphHeadingLayout!!.visibility = View.GONE
            }


            //Expandable View Paragraph --End-- Suvajit
            localTabQuestionStatus!![localExamAllQuestionsSubjectWisePojoList!![countValue].subjectId] = countValue
            val questionData = localExamAllQuestionsSubjectWisePojoList!![countValue].questionText
            QuestionNo!!.text = (countValue + 1).toString()

//        quest.getSettings().setJavaScriptEnabled(true);
            quest!!.loadData(Html.fromHtml(questionData!!.trim { it <= ' ' }).toString(), "text/html", "UTF-8")


            //Radio Option --Start-- Suvajit
            val optionA = localExamAllQuestionsSubjectWisePojoList!![countValue].optionA
            val optionB = localExamAllQuestionsSubjectWisePojoList!![countValue].optionB
            val optionC = localExamAllQuestionsSubjectWisePojoList!![countValue].optionC
            val optionD = localExamAllQuestionsSubjectWisePojoList!![countValue].optionD
            val optionE = localExamAllQuestionsSubjectWisePojoList!![countValue].optionE
            if (quesType != "SC") {
                showRadioQuestionOnFragmentLayout!!.visibility = View.GONE
                showCheckQuestionOnFragmentLayout!!.visibility = View.VISIBLE
                if (optionA != null && optionA != "") {
                    linLayoutOptionA!!.visibility = View.VISIBLE
                    webViewOptionA!!.loadData(Html.fromHtml(optionA).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionA!!.visibility = View.GONE
                }
                if (optionB != null && optionB != "") {
                    linLayoutOptionB!!.visibility = View.VISIBLE
                    webViewOptionB!!.loadData(Html.fromHtml(optionB).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionB!!.visibility = View.GONE
                }
                if (optionC != null && optionC != "") {
                    linLayoutOptionC!!.visibility = View.VISIBLE
                    webViewOptionC!!.loadData(Html.fromHtml(optionC).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionC!!.visibility = View.GONE
                }
                if (optionD != null && optionD != "") {
                    linLayoutOptionD!!.visibility = View.VISIBLE
                    webViewOptionD!!.loadData(Html.fromHtml(optionD).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionD!!.visibility = View.GONE
                }
                if (optionE != null && optionE != "") {
                    linLayoutOptionE!!.visibility = View.VISIBLE
                    webViewOptionE!!.loadData(Html.fromHtml(optionE).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionE!!.visibility = View.GONE
                }


                //PAR fragment --Start-- Suvajit
                if (answerStatusList.contains("A") || answerStatusList.contains("a")) {
                    webViewOptionA!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    linLayoutOptionA!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    checkBoxOptionA!!.isChecked = true
                } else {
                    webViewOptionA!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    linLayoutOptionA!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    checkBoxOptionA!!.isChecked = false
                }
                if (answerStatusList.contains("B") || answerStatusList.contains("b")) {
                    webViewOptionB!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    linLayoutOptionB!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    checkBoxOptionB!!.isChecked = true
                } else {
                    webViewOptionB!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    linLayoutOptionB!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    checkBoxOptionB!!.isChecked = false
                }
                if (answerStatusList.contains("C") || answerStatusList.contains("c")) {
                    webViewOptionC!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    linLayoutOptionC!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    checkBoxOptionC!!.isChecked = true
                } else {
                    webViewOptionC!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    linLayoutOptionC!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    checkBoxOptionC!!.isChecked = false
                }
                if (answerStatusList.contains("D") || answerStatusList.contains("d")) {
                    webViewOptionD!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    linLayoutOptionD!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    checkBoxOptionD!!.isChecked = true
                } else {
                    webViewOptionD!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    linLayoutOptionD!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    checkBoxOptionD!!.isChecked = false
                }
                if (answerStatusList.contains("E") || answerStatusList.contains("e")) {
                    webViewOptionE!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    linLayoutOptionE!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                    checkBoxOptionE!!.isChecked = true
                } else {
                    webViewOptionE!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    linLayoutOptionE!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                    checkBoxOptionE!!.isChecked = false
                }


                //Show right ans color
                if (rightAnsList.contains("A") || rightAnsList.contains("a")) {
                    webViewOptionA!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    linLayoutOptionA!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                }
                if (rightAnsList.contains("B") || rightAnsList.contains("b")) {
                    webViewOptionB!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    linLayoutOptionB!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                }
                if (rightAnsList.contains("C") || rightAnsList.contains("c")) {
                    webViewOptionC!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    linLayoutOptionC!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                }
                if (rightAnsList.contains("D") || rightAnsList.contains("d")) {
                    webViewOptionD!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    linLayoutOptionD!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                }
                if (rightAnsList.contains("E") || rightAnsList.contains("e")) {
                    webViewOptionE!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    linLayoutOptionE!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                }

                //PAR fragment --End-- Suvajit
            } else {
                showCheckQuestionOnFragmentLayout!!.visibility = View.GONE
                showRadioQuestionOnFragmentLayout!!.visibility = View.VISIBLE

                //Clear right wrong ans colors
                webViewOptionARadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                webViewOptionBRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                webViewOptionCRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                webViewOptionDRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                webViewOptionERadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                linLayoutOptionARadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                linLayoutOptionBRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                linLayoutOptionCRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                linLayoutOptionDRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                linLayoutOptionERadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.white))
                when (answerStatus) {
                    "a", "A" -> {
                        linLayoutOptionARadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        webViewOptionARadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        radioButtonOptionA!!.isChecked = true
                    }
                    "b", "B" -> {
                        linLayoutOptionBRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        webViewOptionBRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        radioButtonOptionB!!.isChecked = true
                    }
                    "c", "C" -> {
                        linLayoutOptionCRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        webViewOptionCRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        radioButtonOptionC!!.isChecked = true
                    }
                    "d", "D" -> {
                        linLayoutOptionDRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        webViewOptionDRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        radioButtonOptionD!!.isChecked = true
                    }
                    "e", "E" -> {
                        linLayoutOptionERadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        webViewOptionERadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_red_200))
                        radioButtonOptionE!!.isChecked = true
                    }
                    else -> multiRowsRadioGroup!!.clearCheck()
                }
                when (rightAns) {
                    "a", "A" -> {
                        linLayoutOptionARadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                        webViewOptionARadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    }
                    "b", "B" -> {
                        linLayoutOptionBRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                        webViewOptionBRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    }
                    "c", "C" -> {
                        linLayoutOptionCRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                        webViewOptionCRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    }
                    "d", "D" -> {
                        linLayoutOptionDRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                        webViewOptionDRadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    }
                    "e", "E" -> {
                        linLayoutOptionERadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                        webViewOptionERadio!!.setBackgroundColor(ContextCompat.getColor(context!!, R.color.material_green_200))
                    }
                    else -> {
                    }
                }
                if (optionA != null && optionA != "") {
                    linLayoutOptionARadio!!.visibility = View.VISIBLE
                    webViewOptionARadio!!.loadData(Html.fromHtml(optionA).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionARadio!!.visibility = View.GONE
                }
                if (optionB != null && optionB != "") {
                    linLayoutOptionBRadio!!.visibility = View.VISIBLE
                    webViewOptionBRadio!!.loadData(Html.fromHtml(optionB).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionBRadio!!.visibility = View.GONE
                }
                if (optionC != null && optionC != "") {
                    linLayoutOptionCRadio!!.visibility = View.VISIBLE
                    webViewOptionCRadio!!.loadData(Html.fromHtml(optionC).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionCRadio!!.visibility = View.GONE
                }
                if (optionD != null && optionD != "") {
                    linLayoutOptionDRadio!!.visibility = View.VISIBLE
                    webViewOptionDRadio!!.loadData(Html.fromHtml(optionD).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionDRadio!!.visibility = View.GONE
                }
                if (optionE != null && optionE != "") {
                    linLayoutOptionERadio!!.visibility = View.VISIBLE
                    webViewOptionERadio!!.loadData(Html.fromHtml(optionE).toString(), "text/html", "UTF-8")
                } else {
                    linLayoutOptionERadio!!.visibility = View.GONE
                }

                //Radio Option --End-- Suvajit
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.NextButton ->                 //Show nextButton Question Data
                if (countValue > examQuestionDataBasedOnExamIdPojo!!.totalQuestions!!.toInt() - 2) {
                    dataShow(countValue)
                    Toast.makeText(this, "This Is Last Question", Toast.LENGTH_SHORT).show()
                } else {
                    //SetQuestion Position
                    setTabPositionAccordingToQuestion()
                    countValue++
                    dataShow(countValue)
                }
            R.id.PreviousButton ->
                //Show Previous Question
//                Log.d("PreviousButton", "YES");
                if (countValue == 0) {
                    Toast.makeText(this, "This Is first Question", Toast.LENGTH_SHORT).show()
                } else {

                    //SetQuestion Position
                    setTabPositionAccordingToQuestion()
                    countValue--
                    //                    Log.d("previousCountValueELSE", String.valueOf(countValue));
                    dataShow(countValue)
                }
            R.id.gridClick -> gridViewActionCustomDialog()
            R.id.relativeLayoutParagraphHeading -> paragraphExpandableLayout!!.toggle()
        }
    }

    private fun gridViewActionCustomDialog() {
        val builder: AlertDialog.Builder
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = inflater.inflate(R.layout.exam_gridview_numberserieslayout, findViewById<View>(R.id.layout_root) as ViewGroup, false)
        val gridview = layout.findViewById<View>(R.id.gridview) as GridView
        val close = layout.findViewById<View>(R.id.text1) as TextView
        //        Log.d("insideDialog", "yes");
        var quesAnsKeyGridAdapter = gridview.adapter as QuesAnsKeyGridAdapter
        if (quesAnsKeyGridAdapter == null) {
//            Log.d("insideDialogif", "yes");
            quesAnsKeyGridAdapter = finalSubmissionPojo!!.responses?.let { QuesAnsKeyGridAdapter(this, it) }!!
            quesAnsKeyGridAdapter.notifyDataSetChanged()
            gridview.adapter = quesAnsKeyGridAdapter
        }
        gridview.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->
            var k = position
            k = k + 1
            //                Log.d("countjfjijeeri", String.valueOf(position + 1));
            if (countValue != position) {
                dataShow(position)
                setTabPositionAccordingToQuestion()
            }
            dialog!!.dismiss()
        }
        builder = AlertDialog.Builder(this)
        builder.setView(layout)
        dialog = builder.create()
        dialog?.show()
    }

    fun createRotateAnimator(target: View?, from: Float, to: Float): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(target, "rotation", from, to)
        animator.duration = 300
        animator.interpolator = Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR)
        return animator
    }

    fun setTabPositionAccordingToQuestion() {
        val CurrentSubject = localExamAllQuestionsSubjectWisePojoList!![countValue].subjectId
        for (k in subjectNames.indices) {
            if (CurrentSubject == subjectNames[k]) {
                //SetTab Selection
                tabLayout!!.setScrollPosition(k, 0f, true)
            }
        }
    }

    companion object {
        private const val PARAGRAPH_HEADING = "Tap to show/hide Paragraph"
    }
}
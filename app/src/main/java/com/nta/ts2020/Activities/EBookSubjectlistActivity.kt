package com.nta.ts2020.Activities

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.nta.ts2020.Adapters.EBookSubjectsListRecyclerAdapter
import com.nta.ts2020.AppConstants.ConstantVariables.E_BOOK
import com.nta.ts2020.AppConstants.ConstantVariables.FORMULA_BOOK
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.LocalStorage.CacheEBookSubjects
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.CourseClasses
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.EBookSubjectsListHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService

class EBookSubjectlistActivity : AppCompatActivity() {
    private var recyclerView: RecyclerView? = null
    private var linLayoutNoData: LinearLayout? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var imageViewError: ImageView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var context: EBookSubjectlistActivity? = null
    private var eBookType: String? = null
    private var userDataPojo : UserDataPojo? = null
    private val REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ebook_subjects)

        //context
        context = this
        if (intent.extras != null) {
            eBookType = intent.extras!!.getString("E_BOOK_TYPE")
        }
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            when (eBookType) {
                E_BOOK -> supportActionBar!!.title = "E-Book"
                FORMULA_BOOK -> supportActionBar!!.title = "Formula Book"
            }
        }
        userDataPojo = CacheUserData(this).userDataFromCache
        if(userDataPojo != null)
            if(userDataPojo!!.courseId.isNullOrEmpty() || userDataPojo!!.classId.isNullOrEmpty() || userDataPojo!!.courseId =="null" || userDataPojo!!.classId == "null"){
                startActivity(Intent(this, SelectCourseClass::class.java))
                finish()
            }
        recyclerView = findViewById<View>(R.id.rvEBookSubjectsList) as RecyclerView
        //Message Layout
        linLayoutNoData = findViewById<View>(R.id.llEBookSubjectsListError) as LinearLayout
        textViewMessage = findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        imageViewError = findViewById<View>(R.id.ivExamResultListError) as ImageView
        swipeRefreshLayout = findViewById<View>(R.id.swRefreshEBookSubjectsList) as SwipeRefreshLayout
        swipeRefreshLayout!!.setColorSchemeResources(
                R.color.material_blue_700,
                R.color.material_green_700,
                R.color.material_yellow_700,
                R.color.material_red_700)
        val eBookSubjectsListHandler = EBookSubjectsListHandler(context!!)
        if (isNetworkConnected) {
            swipeRefreshLayout!!.isRefreshing = true
            ThreadPoolService().saveEBookSubjectsListFromServerToCache(context, eBookSubjectsListHandler)
        }
        swipeRefreshLayout!!.setOnRefreshListener {
            if (isNetworkConnected) {
                ThreadPoolService().saveEBookSubjectsListFromServerToCache(context, eBookSubjectsListHandler)
            } else {
                loadData(SUCCESS)
            }
        }
        if (!isNetworkConnected) loadData(SUCCESS)
    }

    private val isNetworkConnected: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    fun loadData(status: Int) {
        //fetch data from cache
        var status = status
        val eBookSubjectsPojoArrayList = context?.let { CacheEBookSubjects(it).eBookSubjectsList }
        if (eBookSubjectsPojoArrayList!!.isEmpty() && status != NETWORK_ERROR) status = NO_DATA
        when (status) {
            SUCCESS -> {
                linLayoutNoData!!.visibility = View.GONE
                recyclerView!!.visibility = View.VISIBLE
                val eBookSubjectsListAdapter = EBookSubjectsListRecyclerAdapter(eBookSubjectsPojoArrayList, eBookType)
                recyclerView!!.layoutManager = LinearLayoutManager(context)
                //          recyclerView.addItemDecoration(new QuizDividerItemDecoration(this));
                recyclerView!!.adapter = eBookSubjectsListAdapter
            }
            NO_DATA -> {
                linLayoutNoData!!.visibility = View.VISIBLE
                recyclerView!!.visibility = View.GONE
                textViewMessage!!.setText(R.string.result_list_message_no_data)
                textViewSubMessage!!.setText(R.string.result_list_sub_message_no_data)
                imageViewError!!.setImageResource(R.drawable.ic_no_data)
            }
            NETWORK_ERROR -> {
                linLayoutNoData!!.visibility = View.VISIBLE
                recyclerView!!.visibility = View.GONE
                textViewMessage!!.setText(R.string.result_list_message_network_error)
                textViewSubMessage!!.setText(R.string.result_list_sub_message_network_error)
                imageViewError!!.setImageResource(R.drawable.ic_no_network)
            }
        }
        if (swipeRefreshLayout!!.isRefreshing) {
            swipeRefreshLayout!!.isRefreshing = false
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
package com.nta.ts2020.Activities

import android.animation.ObjectAnimator
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.util.Log
import android.view.*
import android.view.View.OnTouchListener
import android.webkit.WebView
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter
import com.github.aakira.expandablelayout.ExpandableLinearLayout
import com.github.aakira.expandablelayout.Utils
import com.github.clans.fab.FloatingActionButton
import com.github.clans.fab.FloatingActionMenu
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.nta.ts2020.Adapters.ExamGridAdapter
import com.nta.ts2020.AppConstants.ConstantVariables.TIMESTAMP_FORMAT
import com.nta.ts2020.LocalStorage.*
import com.nta.ts2020.PojoClasses.*
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.UtilsHelper.NestedScrollWebView
import java.text.SimpleDateFormat
import java.util.*

class ExamShowQuestionDataBaseOnExamIdListActivity : AppCompatActivity(), OnItemSelectedListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    //Testing
    var clock: TextView? = null
    var quest: WebView? = null
    var webViewOptionA: WebView? = null
    var webViewOptionB: WebView? = null
    var webViewOptionC: WebView? = null
    var webViewOptionD: WebView? = null
    var webViewOptionE: WebView? = null
    var webViewOptionARadio: WebView? = null
    var webViewOptionBRadio: WebView? = null
    var webViewOptionCRadio: WebView? = null
    var webViewOptionDRadio: WebView? = null
    var webViewOptionERadio: WebView? = null
    var radioButtonOptionA: RadioButton? = null
    var radioButtonOptionB: RadioButton? = null
    var radioButtonOptionC: RadioButton? = null
    var radioButtonOptionD: RadioButton? = null
    var radioButtonOptionE: RadioButton? = null
    var QuestionNo: TextView? = null
    var grid: TextView? = null
    var time: TextView? = null
    var nextButton: RelativeLayout? = null
    var PreviousButton: RelativeLayout? = null
    private val startTimer = true
    private var countDownTimer: CountDownTimer? = null
    private val yellowColorTime = 2
    private val redColorTime = 59
    private var remainingTime: Long = 0
    private var previousRemainingTime: Long = 0
    private val interval = 1 * 1000.toLong()
    private lateinit var tabItems: Array<String?>
    private var dialog: Dialog? = null
    private var instructionDialog: Dialog? = null
    var buildAlertDial: AlertDialog? = null
    private var countValue = 0
    private var totalQuestions = 0
    private var examGridAdapter: ExamGridAdapter? = null
    var gridview: GridView? = null
    private lateinit var reviewLaterQuestions: Array<String?>
    var QuestionInstruction: String? = null
    var totalQuestionTextView: TextView? = null
    private var tabLayout: TabLayout? = null
    lateinit var totalQuestionSubjectWise: Array<String?>
    var reviewCheckBox: CheckBox? = null
    private var answerStatus: String? = null
    var ExamStatus: String? = null

    // Collections ....
    var localExamAllQuestionsSubjectWisePojoList: ArrayList<ExamAllQuestionsSubjectWisePojo>? = null
    var localExamGetSubjectArrayStuffPojoList: ArrayList<ExamGetSubjectArrayStuffPojo>? = null
    var localQuestionDataStuffBasedOnExamIdPojoList: ArrayList<ExamQuestionDataBasedOnExamIdPojo>? = null
    var localExamGetCurrentExamSessionPojoList: ArrayList<ExamGetCurrentExamSessionPojo>? = null
    var localExamSetCurrentExamSessionPojoList: ArrayList<ExamSetCurrentExamSessionPojo>? = null
    var localTabQuestionStatus: HashMap<String?, Int>? = null

    //Bundle
    var extras: Bundle? = null

    //Cache Exam
    var cacheExamCacheQuestionDataBasedOnExamIdInstance: ExamCacheQuestionDataBasedOnExamId? = null
    var examCacheAllQuestionRelatedStuffInstance: ExamCacheAllQuestionRelatedStuff? = null
    var examCacheGetCurrentExamSessionInstance: ExamCacheGetCurrentExamSession? = null
    var examCacheSetCurrentSessionDataInstance: ExamCacheSetCurrentSessionData? = null

    //Cache Quiz
    var quizCacheExamCacheQuestionDataBasedOnExamIdInstance: QuizCacheQuestionDataBasedOnExamId? = null
    var quizExamCacheAllQuestionRelatedStuffInstance: QuizCacheAllQuestionRelatedStuff? = null
    var quizExamCacheGetCurrentExamSessionInstance: QuizCacheGetCurrentExamSession? = null
    var quizExamCacheSetCurrentSessionDataInstance: QuizCacheSetCurrentSessionData? = null

    //Cache Bookmark
    var cacheBookmark: CacheBookmark? = null

    //Pojo
    var localExamGetCurrentExamSessionPojoInstance: ExamGetCurrentExamSessionPojo? = null
    var localExamSetCurrentExamSessionPojoInstance: ExamSetCurrentExamSessionPojo? = null
    var ExamID: String? = null

    //floating action button
    private var fabMenu: FloatingActionMenu? = null
    private var instru: FloatingActionButton? = null
    private var submitbt: FloatingActionButton? = null
    private var ShowAllQuestions: FloatingActionButton? = null
    var showRadioQuestionOnFragmentLayout: LinearLayout? = null
    var showCheckQuestionOnFragmentLayout: LinearLayout? = null

    //Log
    private val TAG = "QuestionDataActivity"

    //Added Later
    private var paragraphDataView: NestedScrollWebView? = null
    private var multiRowsRadioGroup: RadioGroup? = null
    private var linLayoutOptionA: LinearLayout? = null
    private var linLayoutOptionB: LinearLayout? = null
    private var linLayoutOptionC: LinearLayout? = null
    private var linLayoutOptionD: LinearLayout? = null
    private var linLayoutOptionE: LinearLayout? = null
    private var linLayoutOptionARadio: LinearLayout? = null
    private var linLayoutOptionBRadio: LinearLayout? = null
    private var linLayoutOptionCRadio: LinearLayout? = null
    private var linLayoutOptionDRadio: LinearLayout? = null
    private var linLayoutOptionERadio: LinearLayout? = null
    private var paragraphHeadingLayout: RelativeLayout? = null
    private var paragraphHeadingButtonHolder: RelativeLayout? = null
    private var paragrapRelativeLayout: RelativeLayout? = null
    private var paragraphHeading: TextView? = null
    private var paragraphExpandableLayout: ExpandableLinearLayout? = null
    private var checkBoxOptionA: CheckBox? = null
    private var checkBoxOptionB: CheckBox? = null
    private var checkBoxOptionC: CheckBox? = null
    private var checkBoxOptionD: CheckBox? = null
    private var checkBoxOptionE: CheckBox? = null
    private var answerStatusList: ArrayList<String>? = null
    private var context: ExamShowQuestionDataBaseOnExamIdListActivity? = null
    private var examType: String? = null
    private var bookmark: CheckBox? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exam_activity_questions_screen_fab)

        //Hide Action Bar
        supportActionBar!!.hide()

        //set screen always on
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        context = this
        extras = intent.extras

        //set Default Staus
        ExamStatus = "Resume"
        //        Log.d(TAG, "Oncreate() Call");
        if (extras != null) {
            examType = extras!!.getString("TYPE")
            //Initialized
            initialization(examType)
            showCheckQuestionOnFragmentLayout = findViewById<View>(R.id.linLayout) as LinearLayout
            showRadioQuestionOnFragmentLayout = findViewById<View>(R.id.radioLayout) as LinearLayout
            ExamID = extras!!.getString("ExamID")
            when (examType) {
                "EXM" -> {
                    //Array Of Data
                    localExamAllQuestionsSubjectWisePojoList = cacheExamCacheQuestionDataBasedOnExamIdInstance!!.questionDataBasedOnExamId
                    //Question and options Data
                    localQuestionDataStuffBasedOnExamIdPojoList = examCacheAllQuestionRelatedStuffInstance!!.getCacheAllQuestionRelatedStuff(ExamID)

                    //Single Subject Related Stuff
                    localExamGetSubjectArrayStuffPojoList = examCacheAllQuestionRelatedStuffInstance!!.getCacheAllQuestionSubjectRelatedStuff(ExamID)
                    localExamGetCurrentExamSessionPojoList = examCacheGetCurrentExamSessionInstance!!.currentExamSession

                    //GetCurrentSession
                    localExamSetCurrentExamSessionPojoList = examCacheSetCurrentSessionDataInstance!!.currentExamSession
                }
                "QUIZ" -> {
                    localExamAllQuestionsSubjectWisePojoList = quizCacheExamCacheQuestionDataBasedOnExamIdInstance!!.questionDataBasedOnExamId

                    //Question and options Data
                    localQuestionDataStuffBasedOnExamIdPojoList = quizExamCacheAllQuestionRelatedStuffInstance!!.cacheAllQuestionRelatedStuff

                    //Single Subject Related Stuff
                    localExamGetSubjectArrayStuffPojoList = quizExamCacheAllQuestionRelatedStuffInstance!!.cacheAllQuestionSubjectRelatedStuff
                    localExamGetCurrentExamSessionPojoList = quizExamCacheGetCurrentExamSessionInstance!!.currentExamSession

                    //GetCurrentSession
                    localExamSetCurrentExamSessionPojoList = quizExamCacheSetCurrentSessionDataInstance!!.currentExamSession
                }
            }


            //Set MaxQuestion Limit
            totalQuestions = localQuestionDataStuffBasedOnExamIdPojoList!![0].totalQuestions!!.toInt()
            totalQuestionTextView!!.text = "Total Question $totalQuestions"
            reviewLaterQuestions = arrayOfNulls(localExamGetCurrentExamSessionPojoList!!.size)
            for (i in localExamGetCurrentExamSessionPojoList!!.indices) {
                reviewLaterQuestions[i] = localExamGetCurrentExamSessionPojoList!![i].colorStatus
            }
            totalQuestions -= 1
            if (isNetworkConnected) {
                //Net Not Available
                dataShow(localExamSetCurrentExamSessionPojoList!![0].currentPosition!!.toInt())
            } else {
                dataShow(localExamSetCurrentExamSessionPojoList!![0].currentPosition!!.toInt())
            }
            tabItems = arrayOfNulls(localExamGetSubjectArrayStuffPojoList!!.size)
            totalQuestionSubjectWise = arrayOfNulls(localExamGetSubjectArrayStuffPojoList!!.size)
            for (i in localExamGetSubjectArrayStuffPojoList!!.indices) {
                tabItems[i] = localExamGetSubjectArrayStuffPojoList!![i].subjectNames
                //Set Questions According to section
                totalQuestionSubjectWise[i] = localExamGetSubjectArrayStuffPojoList!![i].numberOfQuestions
                tabLayout!!.addTab(tabLayout!!.newTab().setText(tabItems[i]))
            }
            for (p in tabItems.indices) localTabQuestionStatus!![tabItems[p]] = 0
        }
        //Set Tab Layout
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout!!.addOnTabSelectedListener(object : OnTabSelectedListener {
            var item: String? = null
            var k = 0
            override fun onTabSelected(tab: TabLayout.Tab) {
                item = tab.text.toString()
                if (localTabQuestionStatus!![item] == 0) {
                    k = 0
                    while (k < localExamAllQuestionsSubjectWisePojoList!!.size) {
                        if (item == localExamAllQuestionsSubjectWisePojoList!![k].subjectId) {
                            break
                        }
                        k++
                    }
                    dataShow(k)
                } else localTabQuestionStatus!![item]?.let { dataShow(it) }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                item = tab.text.toString()
                if (localTabQuestionStatus!![item] == 0) {
                    k = 0
                    while (k < localExamAllQuestionsSubjectWisePojoList!!.size) {
                        if (item == localExamAllQuestionsSubjectWisePojoList!![k].subjectId) {
                            break
                        }
                        k++
                    }
                    dataShow(k)
                } else localTabQuestionStatus!![item]?.let { dataShow(it) }
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                item = tab.text.toString()
                if (localTabQuestionStatus!![item] == 0) {
                    k = 0
                    while (k < localExamAllQuestionsSubjectWisePojoList!!.size) {
                        if (item == localExamAllQuestionsSubjectWisePojoList!![k].subjectId) {
                            break
                        }
                        k++
                    }
                    dataShow(k)
                } else localTabQuestionStatus!![item]?.let { dataShow(it) }
            }
        })
        setTabPositionAccordingToQuestion()
    }

    override fun onStart() {
        super.onStart()
        //        Log.d(TAG, "onStart");
    }

    fun initialization(type: String?) {
        when (type) {
            "EXM" -> {
                cacheExamCacheQuestionDataBasedOnExamIdInstance = ExamCacheQuestionDataBasedOnExamId(this)
                examCacheAllQuestionRelatedStuffInstance = ExamCacheAllQuestionRelatedStuff(this)
                examCacheGetCurrentExamSessionInstance = ExamCacheGetCurrentExamSession(this)
                examCacheSetCurrentSessionDataInstance = ExamCacheSetCurrentSessionData(this)
            }
            "QUIZ" -> {
                quizCacheExamCacheQuestionDataBasedOnExamIdInstance = QuizCacheQuestionDataBasedOnExamId(this)
                quizExamCacheAllQuestionRelatedStuffInstance = QuizCacheAllQuestionRelatedStuff(this)
                quizExamCacheGetCurrentExamSessionInstance = QuizCacheGetCurrentExamSession(this)
                quizExamCacheSetCurrentSessionDataInstance = QuizCacheSetCurrentSessionData(this)
            }
        }
        val font = Typeface.createFromAsset(assets, "fontawesome-webfont.ttf")
        localTabQuestionStatus = HashMap()
        tabLayout = findViewById<View>(R.id.showQuestionTabs) as TabLayout


        //fab buttons -- Start -- Suvajit
        fabMenu = findViewById<View>(R.id.fab_menu) as FloatingActionMenu
        instru = findViewById<View>(R.id.fab_show_instructions) as FloatingActionButton
        ShowAllQuestions = findViewById<View>(R.id.fab_show_all_ques) as FloatingActionButton
        submitbt = findViewById<View>(R.id.fab_submit) as FloatingActionButton
        instru!!.setOnClickListener(this)
        ShowAllQuestions!!.setOnClickListener(this)
        submitbt!!.setOnClickListener(this)
        fabMenu!!.setOnTouchListener(OnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                return@OnTouchListener fabMenu!!.isOpened
            } else if (event.action == MotionEvent.ACTION_UP) {
                if (fabMenu!!.isOpened) {
                    fabMenu!!.close(fabMenu!!.isAnimated)
                    return@OnTouchListener true
                }
            }
            false
        })

        //fab buttons -- End -- Suvajit

        //Bookmark questions -- Start -- Suvajit
        cacheBookmark = context?.let { CacheBookmark(it) }
        bookmark = findViewById<View>(R.id.cbExamPanelBookmark) as CheckBox
        bookmark!!.setOnCheckedChangeListener(this)
        //Bookmark questions -- End -- Suvajit
        clock = findViewById<View>(R.id.clock) as TextView
        clock!!.typeface = font
        quest = findViewById<View>(R.id.questioText) as WebView
        QuestionNo = findViewById<View>(R.id.Qno) as TextView
        nextButton = findViewById<View>(R.id.NextButton) as RelativeLayout
        PreviousButton = findViewById<View>(R.id.PreviousButton) as RelativeLayout
        grid = findViewById<View>(R.id.gridClick) as TextView
        grid!!.typeface = font
        time = findViewById<View>(R.id.time) as TextView
        totalQuestionTextView = findViewById<View>(R.id.totalQuestion) as TextView
        reviewCheckBox = findViewById<View>(R.id.reviewCheckBox) as CheckBox
        nextButton!!.setOnClickListener(this)
        PreviousButton!!.setOnClickListener(this)
        grid!!.setOnClickListener(this)
        reviewCheckBox!!.setOnClickListener(this)

        //Option Radio Buttons Initialization -- Suvajit
        linLayoutOptionA = findViewById<View>(R.id.linLayoutOptionA) as LinearLayout
        linLayoutOptionB = findViewById<View>(R.id.linLayoutOptionB) as LinearLayout
        linLayoutOptionC = findViewById<View>(R.id.linLayoutOptionC) as LinearLayout
        linLayoutOptionD = findViewById<View>(R.id.linLayoutOptionD) as LinearLayout
        linLayoutOptionE = findViewById<View>(R.id.linLayoutOptionE) as LinearLayout
        multiRowsRadioGroup = findViewById<View>(R.id.multiRowRadioOptionGroup) as RadioGroup
        webViewOptionA = findViewById<View>(R.id.webViewOptionA) as WebView
        webViewOptionB = findViewById<View>(R.id.webViewOptionB) as WebView
        webViewOptionC = findViewById<View>(R.id.webViewOptionC) as WebView
        webViewOptionD = findViewById<View>(R.id.webViewOptionD) as WebView
        webViewOptionE = findViewById<View>(R.id.webViewOptionE) as WebView

        //Resume
        linLayoutOptionARadio = findViewById<View>(R.id.linLayoutOptionARadio) as LinearLayout
        linLayoutOptionBRadio = findViewById<View>(R.id.linLayoutOptionBRadio) as LinearLayout
        linLayoutOptionCRadio = findViewById<View>(R.id.linLayoutOptionCRadio) as LinearLayout
        linLayoutOptionDRadio = findViewById<View>(R.id.linLayoutOptionDRadio) as LinearLayout
        linLayoutOptionERadio = findViewById<View>(R.id.linLayoutOptionERadio) as LinearLayout
        webViewOptionARadio = findViewById<View>(R.id.webViewOptionARadio) as WebView
        webViewOptionBRadio = findViewById<View>(R.id.webViewOptionBRadio) as WebView
        webViewOptionCRadio = findViewById<View>(R.id.webViewOptionCRadio) as WebView
        webViewOptionDRadio = findViewById<View>(R.id.webViewOptionDRadio) as WebView
        webViewOptionERadio = findViewById<View>(R.id.webViewOptionERadio) as WebView
        radioButtonOptionA = findViewById<View>(R.id.radioButtonA) as RadioButton
        radioButtonOptionB = findViewById<View>(R.id.radioButtonB) as RadioButton
        radioButtonOptionC = findViewById<View>(R.id.radioButtonC) as RadioButton
        radioButtonOptionD = findViewById<View>(R.id.radioButtonD) as RadioButton
        radioButtonOptionE = findViewById<View>(R.id.radioButtonE) as RadioButton

        //Option Radio Buttons -- End -- Suvajit

        //Expandable Paragraph View Initialization --Start-- Suvajit
        paragraphDataView = findViewById<View>(R.id.webViewParagraphData) as NestedScrollWebView
        paragraphHeadingLayout = findViewById<View>(R.id.relativeLayoutParagraphHeading) as RelativeLayout
        paragraphHeadingButtonHolder = findViewById<View>(R.id.relativeLayoutParagrahButtonHolder) as RelativeLayout
        paragraphHeading = findViewById<View>(R.id.textViewParagraphHeading) as TextView
        paragraphExpandableLayout = findViewById<View>(R.id.expandableLayoutParagraph) as ExpandableLinearLayout
        paragrapRelativeLayout = findViewById<View>(R.id.relativeLayoutParagraph) as RelativeLayout
        paragraphHeadingLayout!!.setOnClickListener(this)
        paragraphExpandableLayout!!.setListener(object : ExpandableLayoutListenerAdapter() {
            override fun onPreOpen() {
                super.onPreOpen()
                createRotateAnimator(paragraphHeadingButtonHolder, 0f, 180f).start()
            }

            override fun onPreClose() {
                super.onPreClose()
                createRotateAnimator(paragraphHeadingButtonHolder, 180f, 0f).start()
            }
        })
        //Expandable Paragraph View Initialization --End-- Suvajit

        //PAR fragment Initialization --Start-- Suvajit
        checkBoxOptionA = findViewById<View>(R.id.checkBoxA) as CheckBox
        checkBoxOptionB = findViewById<View>(R.id.checkBoxB) as CheckBox
        checkBoxOptionC = findViewById<View>(R.id.checkBoxC) as CheckBox
        checkBoxOptionD = findViewById<View>(R.id.checkBoxD) as CheckBox
        checkBoxOptionE = findViewById<View>(R.id.checkBoxE) as CheckBox
        checkBoxOptionA!!.setOnCheckedChangeListener(this)
        checkBoxOptionB!!.setOnCheckedChangeListener(this)
        checkBoxOptionC!!.setOnCheckedChangeListener(this)
        checkBoxOptionD!!.setOnCheckedChangeListener(this)
        checkBoxOptionE!!.setOnCheckedChangeListener(this)

        //PAR fragment Initialization --End-- Suvajit
    }

    fun dataShow(CountVal: Int) {
        countValue = CountVal
        Log.d(TAG, "Count value on DataShow:$countValue")
        when (examType) {
            "EXM" -> localExamGetCurrentExamSessionPojoList = examCacheGetCurrentExamSessionInstance!!.currentExamSession
            "QUIZ" -> localExamGetCurrentExamSessionPojoList = quizExamCacheGetCurrentExamSessionInstance!!.currentExamSession
        }
        if (localExamGetCurrentExamSessionPojoList!!.size != totalQuestions) {

            //Array Of Data
            //TODO:Optimise code by getting data by id in a variable in the beginning [Like subData = pojoSubdata.get(id)]
            when (examType) {
                "EXM" -> localExamAllQuestionsSubjectWisePojoList = cacheExamCacheQuestionDataBasedOnExamIdInstance!!.questionDataBasedOnExamId
                "QUIZ" -> localExamAllQuestionsSubjectWisePojoList = quizCacheExamCacheQuestionDataBasedOnExamIdInstance!!.questionDataBasedOnExamId
            }


            //set bookmark status
            val isBookmarked = localExamAllQuestionsSubjectWisePojoList!![countValue].questionId?.let { cacheBookmark!!.isBookmarked(it) }
            bookmark!!.isChecked = isBookmarked!!
            reviewLaterQuestions = arrayOfNulls(localExamGetCurrentExamSessionPojoList!!.size)
            for (i in localExamGetCurrentExamSessionPojoList!!.indices) {
                reviewLaterQuestions[i] = localExamGetCurrentExamSessionPojoList!![i].colorStatus
            }
        }

//        Log.d(TAG, "#IntentData Pass By dismissProgressDialog with id " + ExamID);

//        Log.d(TAG, "#examCacheAllExaminationDataInstance with SubjecyName" + localExamAllQuestionsSubjectWisePojoList.get(countValue).getEBookAuthor());
        answerStatus = localExamGetCurrentExamSessionPojoList!![countValue].response
        answerStatusList = ArrayList(Arrays.asList(*answerStatus!!.split(" ".toRegex()).toTypedArray())) //For multiple selection
        //        Log.d(TAG, "InitParAns" + answerStatusList.toString());
        val quesType = localExamAllQuestionsSubjectWisePojoList!![countValue].questionType


        //Expandable View Paragraph --Start-- Suvajit
        paragraphHeading!!.text = PARAGRAPH_HEADING
        paragraphExpandableLayout!!.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR))
        if (localExamAllQuestionsSubjectWisePojoList!![CountVal].essayData != "") {
            paragrapRelativeLayout!!.visibility = View.VISIBLE
            paragraphHeadingLayout!!.visibility = View.VISIBLE
            paragraphDataView!!.loadData(Html.fromHtml(localExamAllQuestionsSubjectWisePojoList!![CountVal].essayData!!.trim { it <= ' ' }).toString() + "<br>", "text/html", "UTF-8")
        } else {
            paragrapRelativeLayout!!.visibility = View.GONE
            paragraphHeadingLayout!!.visibility = View.GONE
        }


        //Expandable View Paragraph --End-- Suvajit
        localTabQuestionStatus!![localExamAllQuestionsSubjectWisePojoList!![countValue].subjectId] = countValue
        val questionData = localExamAllQuestionsSubjectWisePojoList!![countValue].questionText
        QuestionInstruction = localQuestionDataStuffBasedOnExamIdPojoList!![0].instructions
        QuestionNo!!.text = (countValue + 1).toString()

//        quest.getSettings().setJavaScriptEnabled(true);
        quest!!.loadData(Html.fromHtml(questionData!!.trim { it <= ' ' }).toString(), "text/html", "UTF-8")


        //Radio Option --Start-- Suvajit
        val optionA = localExamAllQuestionsSubjectWisePojoList!![countValue].optionA!!.trim { it <= ' ' }
        val optionB = localExamAllQuestionsSubjectWisePojoList!![countValue].optionB!!.trim { it <= ' ' }
        val optionC = localExamAllQuestionsSubjectWisePojoList!![countValue].optionC!!.trim { it <= ' ' }
        val optionD = localExamAllQuestionsSubjectWisePojoList!![countValue].optionD!!.trim { it <= ' ' }
        val optionE = localExamAllQuestionsSubjectWisePojoList!![countValue].optionE!!.trim { it <= ' ' }
        if (quesType != "SC") {
            showRadioQuestionOnFragmentLayout!!.visibility = View.GONE
            showCheckQuestionOnFragmentLayout!!.visibility = View.VISIBLE
            if (optionA != null && optionA != "") {
                linLayoutOptionA!!.visibility = View.VISIBLE
                webViewOptionA!!.loadData(Html.fromHtml(optionA).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionA!!.visibility = View.GONE
            }
            if (optionB != null && optionB != "") {
                linLayoutOptionB!!.visibility = View.VISIBLE
                webViewOptionB!!.loadData(Html.fromHtml(optionB).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionB!!.visibility = View.GONE
            }
            if (optionC != null && optionC != "") {
                linLayoutOptionC!!.visibility = View.VISIBLE
                webViewOptionC!!.loadData(Html.fromHtml(optionC).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionC!!.visibility = View.GONE
            }
            if (optionD != null && optionD != "") {
                linLayoutOptionD!!.visibility = View.VISIBLE
                webViewOptionD!!.loadData(Html.fromHtml(optionD).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionD!!.visibility = View.GONE
            }
            if (optionE != null && optionE != "") {
                linLayoutOptionE!!.visibility = View.VISIBLE
                webViewOptionE!!.loadData(Html.fromHtml(optionE).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionE!!.visibility = View.GONE
            }


            //PAR fragment --Start-- Suvajit
            if (answerStatusList!!.contains("A") || answerStatusList!!.contains("a")) {
                checkBoxOptionA!!.isChecked = true
            } else {
                checkBoxOptionA!!.isChecked = false
            }
            if (answerStatusList!!.contains("B") || answerStatusList!!.contains("b")) {
                checkBoxOptionB!!.isChecked = true
            } else {
                checkBoxOptionB!!.isChecked = false
            }
            if (answerStatusList!!.contains("C") || answerStatusList!!.contains("c")) {
                checkBoxOptionC!!.isChecked = true
            } else {
                checkBoxOptionC!!.isChecked = false
            }
            if (answerStatusList!!.contains("D") || answerStatusList!!.contains("d")) {
                checkBoxOptionD!!.isChecked = true
            } else {
                checkBoxOptionD!!.isChecked = false
            }
            if (answerStatusList!!.contains("E") || answerStatusList!!.contains("e")) {
                checkBoxOptionE!!.isChecked = true
            } else {
                checkBoxOptionE!!.isChecked = false
            }

            //PAR fragment --End-- Suvajit
        } else {
            showCheckQuestionOnFragmentLayout!!.visibility = View.GONE
            showRadioQuestionOnFragmentLayout!!.visibility = View.VISIBLE
            when (answerStatus) {
                "a", "A" -> radioButtonOptionA!!.isChecked = true
                "b", "B" -> radioButtonOptionB!!.isChecked = true
                "c", "C" -> radioButtonOptionC!!.isChecked = true
                "d", "D" -> radioButtonOptionD!!.isChecked = true
                "e", "E" -> radioButtonOptionE!!.isChecked = true
                else -> multiRowsRadioGroup!!.clearCheck()
            }
            if (optionA != null && optionA != "") {
                linLayoutOptionARadio!!.visibility = View.VISIBLE
                webViewOptionARadio!!.loadData(Html.fromHtml(optionA).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionARadio!!.visibility = View.GONE
            }
            if (optionB != null && optionB != "") {
                linLayoutOptionBRadio!!.visibility = View.VISIBLE
                webViewOptionBRadio!!.loadData(Html.fromHtml(optionB).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionBRadio!!.visibility = View.GONE
            }
            if (optionC != null && optionC != "") {
                linLayoutOptionCRadio!!.visibility = View.VISIBLE
                webViewOptionCRadio!!.loadData(Html.fromHtml(optionC).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionCRadio!!.visibility = View.GONE
            }
            if (optionD != null && optionD != "") {
                linLayoutOptionDRadio!!.visibility = View.VISIBLE
                webViewOptionDRadio!!.loadData(Html.fromHtml(optionD).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionDRadio!!.visibility = View.GONE
            }
            if (optionE != null && optionE != "") {
                linLayoutOptionERadio!!.visibility = View.VISIBLE
                webViewOptionERadio!!.loadData(Html.fromHtml(optionE).toString(), "text/html", "UTF-8")
            } else {
                linLayoutOptionERadio!!.visibility = View.GONE
            }
            multiRowsRadioGroup!!.setOnCheckedChangeListener { group, checkedId ->
                val cb = group.findViewById<View>(checkedId) as CompoundButton
                if (cb != null && cb.isChecked) {
//                        Log.e("RadioSuvajit", String.valueOf(cb.getTag()));
                    if (reviewLaterQuestions[countValue] != "1") reviewLaterQuestions[countValue] = "2"
                    answerStatus = cb.tag.toString()
                    updateAnswer(answerStatus, reviewLaterQuestions[countValue])
                }
            }

            //Radio Option --End-- Suvajit
        }


        //set Review Status
        if (reviewLaterQuestions[countValue] == "1") {
            reviewCheckBox!!.isChecked = true
        } else reviewCheckBox!!.isChecked = false

        //Hold Current remaining Time
        previousRemainingTime = remainingTime
    }

    override fun onClick(v: View) {
        val id = v.id
        when (id) {
            R.id.NextButton -> {
                //Show nextButton Question Data
                if (countValue == totalQuestions) {
                    //SetQuestion Position
                    if (!(reviewLaterQuestions[countValue] == "2" || reviewLaterQuestions[countValue] == "1")) {
                        reviewLaterQuestions[countValue] = "3"
                        updateAnswer(answerStatus, "3")
                    }
                    countValue = 0
                    setTabPositionAccordingToQuestion()
                    Log.d(TAG, "Count value on Next:$countValue")
                    dataShow(0)
                } else {
                    //SetQuestion Position
                    if (!(reviewLaterQuestions[countValue] == "2" || reviewLaterQuestions[countValue] == "1")) {
                        reviewLaterQuestions[countValue] = "3"
                        updateAnswer(answerStatus, "3")
                    }
                    countValue++
                    setTabPositionAccordingToQuestion()
                    Log.d(TAG, "Count value on Next:$countValue")
                    dataShow(countValue)
                }
                fabMenu!!.close(true)
            }
            R.id.PreviousButton -> {

                //Show Previous Question
//                Log.d("PreviousButton", "YES");
                if (countValue == 0) {
                    if (!(reviewLaterQuestions[countValue] == "2" || reviewLaterQuestions[countValue] == "1")) {
                        reviewLaterQuestions[countValue] = "3"
                        updateAnswer(answerStatus, "3")
                    }
                    countValue = totalQuestions
                    setTabPositionAccordingToQuestion()
                    Log.d(TAG, "Count value on Previous:$countValue")
                    dataShow(countValue)
                } else {

                    //SetQuestion Position
                    if (!(reviewLaterQuestions[countValue] == "2" || reviewLaterQuestions[countValue] == "1")) {
                        reviewLaterQuestions[countValue] = "3"
                        updateAnswer(answerStatus, "3")
                    }
                    countValue--
                    setTabPositionAccordingToQuestion()
                    Log.d(TAG, "Count value on Previous:$countValue")
                    dataShow(countValue)
                }
                fabMenu!!.close(true)
            }
            R.id.gridClick -> {
                Log.d(TAG, "Count value on Grid:$countValue")
                gridViewActionCustomDialog()
            }
            R.id.reviewCheckBox -> if (reviewCheckBox!!.isChecked) {
                reviewLaterQuestions[countValue] = "1"
                reviewCheckBox!!.isChecked = true
                updateAnswer(answerStatus, "1")
            } else {
                reviewLaterQuestions[countValue] = "0"
                reviewCheckBox!!.isChecked = false
                updateAnswer(answerStatus, "0")
            }
            R.id.fab_show_instructions -> {
                fabMenu!!.close(true)
                showInstructionDialog()
            }
            R.id.fab_show_all_ques -> {
                fabMenu!!.close(true)
                showAllQuestionDialog()
            }
            R.id.fab_submit -> {
                fabMenu!!.close(true)
                showSubmitDialog()
            }
            R.id.relativeLayoutParagraphHeading -> paragraphExpandableLayout!!.toggle()
        }
    }

    private fun updateAnswer(sAnswerKey: String?, CurrentColorStatus: String?) {
        val CurrentExmaID = localExamAllQuestionsSubjectWisePojoList!![countValue].questionId
        localExamGetCurrentExamSessionPojoInstance = ExamGetCurrentExamSessionPojo()
        localExamGetCurrentExamSessionPojoInstance!!.questionId = localExamAllQuestionsSubjectWisePojoList!![countValue].questionId
        localExamGetCurrentExamSessionPojoInstance!!.response = sAnswerKey
        localExamGetCurrentExamSessionPojoInstance!!.timeDuration = (previousRemainingTime - remainingTime).toString()
        localExamGetCurrentExamSessionPojoInstance!!.colorStatus = CurrentColorStatus
        when (examType) {
            "EXM" -> CurrentExmaID?.let { examCacheGetCurrentExamSessionInstance!!.updateCurrentExamSession(it, localExamGetCurrentExamSessionPojoInstance!!) }
            "QUIZ" -> CurrentExmaID?.let { quizExamCacheGetCurrentExamSessionInstance!!.updateCurrentExamSession(it, localExamGetCurrentExamSessionPojoInstance!!) }
        }

        //Hold Current remaining Time
        previousRemainingTime = remainingTime
    }

    private fun gridViewActionCustomDialog() {
        val builder: AlertDialog.Builder
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = inflater.inflate(R.layout.exam_gridview_numberserieslayout, findViewById<View>(R.id.layout_root) as ViewGroup, false)
        gridview = layout.findViewById<View>(R.id.gridview) as GridView
        val close = layout.findViewById<View>(R.id.text1) as TextView
        //        Log.d("insideDialog", "yes");
        examGridAdapter = gridview!!.adapter as ExamGridAdapter
        if (examGridAdapter == null) {
//            Log.d("insideDialogif", "yes");
            examGridAdapter = localExamGetCurrentExamSessionPojoList?.let { ExamGridAdapter(this, it, reviewLaterQuestions) }
            examGridAdapter!!.notifyDataSetChanged()
            gridview!!.adapter = examGridAdapter
        }
        gridview!!.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->
            var k = position
            k = k + 1
            //Toast.makeText(v.getContext(), "Position is " + k, Toast.LENGTH_LONG).show();
//                Log.d("countjfjijeeri", String.valueOf(position + 1));
            if (countValue != position) {
                if (!(reviewLaterQuestions[countValue] == "2" || reviewLaterQuestions[countValue] == "1")) {
                    reviewLaterQuestions[countValue] = "3"
                    updateAnswer(answerStatus, "3")
                }
                dataShow(position)
                setTabPositionAccordingToQuestion()
            }
            dialog!!.dismiss()
        }
        builder = AlertDialog.Builder(this)
        builder.setView(layout)
        dialog = builder.create()
        dialog?.show()
    }

    fun setTabPositionAccordingToQuestion() {
        val CurrentSubject = localExamAllQuestionsSubjectWisePojoList!![countValue].subjectId
        Log.d(TAG, "Tab SubjectId: $CurrentSubject")
        for (k in totalQuestionSubjectWise.indices) {
            if (CurrentSubject == tabItems[k]) {
                //SetTab Selection
                tabLayout!!.setScrollPosition(k, 0f, true)
            }
        }
    }

    private fun showSubmitDialog() {
        ExamStatus = "Submit"
        val builder1 = AlertDialog.Builder(this@ExamShowQuestionDataBaseOnExamIdListActivity)
        builder1.setTitle("Submit Test")
        builder1.setMessage("Do you want to submit ongoing test ")
        builder1.setCancelable(false)
        builder1.setPositiveButton("Yes") { dialogs, id ->
            dialogs.cancel()
            submitTestRecord()
        }
        builder1.setNegativeButton("No") { dialogs, id -> dialogs.cancel() }
        buildAlertDial = builder1.create()
        buildAlertDial?.show()
    }

    override fun onStop() {
        super.onStop()
        //        Log.d(TAG, "onStop");
    }

    override fun onPostResume() {
        super.onPostResume()
        //        Log.d(TAG, "onPostResume");
    }

    override fun onPause() {
        super.onPause()
        when (examType) {
            "EXM" -> if (examCacheSetCurrentSessionDataInstance!!.currentExamSession[0].examStatus != "Submit") {
                localExamSetCurrentExamSessionPojoInstance = ExamSetCurrentExamSessionPojo()
                localExamSetCurrentExamSessionPojoInstance!!.remainingTime = remainingTime.toString()
                localExamSetCurrentExamSessionPojoInstance!!.examStatus = ExamStatus
                localExamSetCurrentExamSessionPojoInstance!!.currentPosition = countValue.toString()
                localQuestionDataStuffBasedOnExamIdPojoList!![0].examId?.let { examCacheSetCurrentSessionDataInstance!!.updateCurrentExamSession(it, localExamSetCurrentExamSessionPojoInstance!!) }
            }
            "QUIZ" -> if (quizExamCacheSetCurrentSessionDataInstance!!.currentExamSession[0].examStatus != "Submit") {
                localExamSetCurrentExamSessionPojoInstance = ExamSetCurrentExamSessionPojo()
                localExamSetCurrentExamSessionPojoInstance!!.remainingTime = remainingTime.toString()
                localExamSetCurrentExamSessionPojoInstance!!.examStatus = ExamStatus
                localExamSetCurrentExamSessionPojoInstance!!.currentPosition = countValue.toString()
                localQuestionDataStuffBasedOnExamIdPojoList!![0].examId?.let { quizExamCacheSetCurrentSessionDataInstance!!.updateCurrentExamSession(it, localExamSetCurrentExamSessionPojoInstance!!) }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (countDownTimer != null) countDownTimer!!.cancel()
        when (examType) {
            "EXM" ->                 //GetCurrentSession
                localExamSetCurrentExamSessionPojoList = examCacheSetCurrentSessionDataInstance!!.currentExamSession
            "QUIZ" -> localExamSetCurrentExamSessionPojoList = quizExamCacheSetCurrentSessionDataInstance!!.currentExamSession
        }
        val resumeCountStatus = localExamSetCurrentExamSessionPojoList!![0].countResum
        val tempTime = localExamSetCurrentExamSessionPojoList!![0].remainingTime!!.toInt()
        myCountDownTimer(1000 * tempTime.toLong(), 1)
        Toast.makeText(this, "Resumes Left " + (3 - resumeCountStatus!!.toInt()).toString(), Toast.LENGTH_SHORT).show()
        if (resumeCountStatus.toInt() >= 3) submitTestRecord()
        previousRemainingTime = tempTime.toLong()

//        Log.d(TAG, "onResume");
    }

    private fun submitTestRecord() {

        //TODO: get allotted time and total marks from server if possible
        val allottedTime = localQuestionDataStuffBasedOnExamIdPojoList!![0].timeDurationInSeconds!!.toInt()
        val date = SimpleDateFormat(TIMESTAMP_FORMAT, Locale.getDefault()).format(Date())
        localExamSetCurrentExamSessionPojoInstance = ExamSetCurrentExamSessionPojo()
        localExamSetCurrentExamSessionPojoInstance!!.remainingTime = remainingTime.toString()
        localExamSetCurrentExamSessionPojoInstance!!.examStatus = ExamStatus
        localExamSetCurrentExamSessionPojoInstance!!.currentPosition = countValue.toString()
        when (examType) {
            "EXM" -> localQuestionDataStuffBasedOnExamIdPojoList!![0].examId?.let { examCacheSetCurrentSessionDataInstance!!.updateCurrentExamSession(it, localExamSetCurrentExamSessionPojoInstance!!) }
            "QUIZ" -> localQuestionDataStuffBasedOnExamIdPojoList!![0].examId?.let { quizExamCacheSetCurrentSessionDataInstance!!.updateCurrentExamSession(it, localExamSetCurrentExamSessionPojoInstance!!) }
        }


        //Added Later -- Suvajit
        val intExamId = ExamID!!.toInt()
        val finalSubmissionPojo = FinalSubmissionPojo()
        val finalSubmissionPojoArrayList = ArrayList<FinalSubmissionPojo>()
        val userDataPojo = context?.let { CacheUserData(it).userDataFromCache }
        finalSubmissionPojo.userId = userDataPojo?.userId!!.toInt()
        finalSubmissionPojo.examType = examType
        finalSubmissionPojo.date = date
        finalSubmissionPojo.allottedTime = allottedTime
        finalSubmissionPojo.totalQues = allottedTime
        finalSubmissionPojo.totalMarks = allottedTime
        when (examType) {
            "EXM" -> {
                finalSubmissionPojo.examId = intExamId.toString()
                finalSubmissionPojo.subjectIds = examCacheAllQuestionRelatedStuffInstance!!.getSubjectIds(intExamId)
                finalSubmissionPojo.nosOfQuesSubWise = examCacheAllQuestionRelatedStuffInstance!!.getNosOfQuesSubWise(intExamId)
                finalSubmissionPojo.subWiseMaxMarks = examCacheAllQuestionRelatedStuffInstance!!.getSubWiseMaxMarks(intExamId)
                finalSubmissionPojo.quesIds = examCacheGetCurrentExamSessionInstance!!.quesIds
                finalSubmissionPojo.responses = examCacheGetCurrentExamSessionInstance!!.responses
            }
            "QUIZ" -> {
                finalSubmissionPojo.examId = "Q$intExamId"
                finalSubmissionPojo.subjectIds = quizExamCacheAllQuestionRelatedStuffInstance!!.getSubjectIds(intExamId)
                finalSubmissionPojo.nosOfQuesSubWise = quizExamCacheAllQuestionRelatedStuffInstance!!.getNosOfQuesSubWise(intExamId)
                finalSubmissionPojo.subWiseMaxMarks = quizExamCacheAllQuestionRelatedStuffInstance!!.getSubWiseMaxMarks(intExamId)
                finalSubmissionPojo.quesIds = quizExamCacheGetCurrentExamSessionInstance!!.quesIds
                finalSubmissionPojo.responses = quizExamCacheGetCurrentExamSessionInstance!!.responses
            }
        }
        val subjectWiseStuff = HashMap<String?, FloatArray?>()
        var tempArray: FloatArray? = floatArrayOf(0f, 0f, 0f, 0f, 0f, 0f)
        when (examType) {
            "EXM" -> localExamGetCurrentExamSessionPojoList = examCacheGetCurrentExamSessionInstance!!.currentExamSession
            "QUIZ" -> localExamGetCurrentExamSessionPojoList = quizExamCacheGetCurrentExamSessionInstance!!.currentExamSession
        }
        for (p in localExamGetSubjectArrayStuffPojoList!!.indices) {
            subjectWiseStuff[localExamGetSubjectArrayStuffPojoList!![p].subjectNames] = floatArrayOf(0f, 0f, 0f, 0f, 0f, 0f)
        }
        var totalRightAns = 0
        var totalWronAns = 0
        var totalAttemptQuestion = 0
        var totalSkipedQuestion = 0
        var totalRightMarks = 0
        var totalWrongMarks = 0
        for (i in localExamAllQuestionsSubjectWisePojoList!!.indices) {
            if (localExamGetCurrentExamSessionPojoList!![i].response != "") {
                if (localExamGetCurrentExamSessionPojoList!![i].response.equals(localExamAllQuestionsSubjectWisePojoList!![i].rightAns, ignoreCase = true)) {
                    if (subjectWiseStuff[localExamAllQuestionsSubjectWisePojoList!![i].subjectId] != null) {
                        tempArray = subjectWiseStuff[localExamAllQuestionsSubjectWisePojoList!![i].subjectId]
                    }
                    tempArray!![0] = tempArray[0] + java.lang.Float.valueOf(localExamAllQuestionsSubjectWisePojoList!![i].rightMarks!!)
                    tempArray[1] = java.lang.Float.valueOf(localExamGetCurrentExamSessionPojoList!![i].timeDuration!!)
                    tempArray[2]++
                    tempArray[4] += java.lang.Float.valueOf(localExamAllQuestionsSubjectWisePojoList!![i].rightMarks!!)
                    totalRightMarks += java.lang.Float.valueOf(localExamAllQuestionsSubjectWisePojoList!![i].rightMarks!!).toInt()

                    //total right Question
                    totalRightAns++
                    subjectWiseStuff[localExamAllQuestionsSubjectWisePojoList!![i].subjectId] = tempArray
                } else {
                    if (subjectWiseStuff[localExamAllQuestionsSubjectWisePojoList!![i].subjectId] != null) {
                        tempArray = subjectWiseStuff[localExamAllQuestionsSubjectWisePojoList!![i].subjectId]
                    }
                    tempArray!![0] -= java.lang.Float.valueOf(localExamAllQuestionsSubjectWisePojoList!![i].wrongMarks!!)
                    tempArray!![1] = java.lang.Float.valueOf(localExamGetCurrentExamSessionPojoList!![i].timeDuration!!)
                    tempArray[3]++
                    tempArray[5] += java.lang.Float.valueOf(localExamAllQuestionsSubjectWisePojoList!![i].wrongMarks!!)
                    totalWrongMarks += java.lang.Float.valueOf(localExamAllQuestionsSubjectWisePojoList!![i].wrongMarks!!).toInt()

                    //TotalWrongAnswer
                    totalWronAns++
                    subjectWiseStuff[localExamAllQuestionsSubjectWisePojoList!![i].subjectId] = tempArray
                }


                //SujectWiseTime
                subjectWiseStuff[localExamAllQuestionsSubjectWisePojoList!![i].subjectId] = tempArray

                //totalQuesAttemt
                totalAttemptQuestion++
            }
        }
        val subjectWiseDataCalculation = ArrayList<String>()
        val subjectWiseTimeCalculation = ArrayList<String>()
        val subjectWiseTotalMarks = ArrayList<String>()
        var totalObtainMarks = 0f
        for (p in localExamGetSubjectArrayStuffPojoList!!.indices) {
            tempArray = subjectWiseStuff[localExamGetSubjectArrayStuffPojoList!![p].subjectNames]

            //subject wise total marks obtain
            subjectWiseTotalMarks.add(tempArray!![0].toString())
            val data = java.lang.Float.valueOf(localExamGetSubjectArrayStuffPojoList!![p].numberOfQuestions!!) - tempArray[3]

            //subject wise Data
            subjectWiseDataCalculation.add(tempArray[2].toString() + " " + tempArray[3] + " " + data + " " + tempArray[4] + " " + tempArray[5])


            //subject wise time taken
            subjectWiseTimeCalculation.add(tempArray[1].toString())
            totalObtainMarks += tempArray[0]
        }
        totalSkipedQuestion = totalQuestions + 1 - totalAttemptQuestion


        //Calculate total Question Analysis
        val setTotalQuesData = "$totalRightAns $totalAttemptQuestion $totalSkipedQuestion $totalRightMarks $totalWrongMarks"

        //
        finalSubmissionPojo.subWiseMarks = subjectWiseTotalMarks
        finalSubmissionPojo.subWiseQuesData = subjectWiseDataCalculation
        finalSubmissionPojo.totalObtainedMarks = totalObtainMarks
        finalSubmissionPojo.totalQuesData = setTotalQuesData
        finalSubmissionPojo.subWiseTime = subjectWiseTimeCalculation
        when (examType) {
            "EXM" -> finalSubmissionPojo.quesWiseTime = examCacheGetCurrentExamSessionInstance!!.quesWiseTime
            "QUIZ" -> finalSubmissionPojo.quesWiseTime = quizExamCacheGetCurrentExamSessionInstance!!.quesWiseTime
        }
        finalSubmissionPojo.overAllTime = allottedTime * 60 - remainingTime
        finalSubmissionPojo.setSyncStatus(false)

//        Log.d(TAG, "Final submission data prepared with Exam id of: " + finalSubmissionPojo.getExamId());
        val examCacheFinalSubmissionInstance = ExamCacheFinalSubmission(context!!)
        finalSubmissionPojoArrayList.add(finalSubmissionPojo)
        if (examCacheFinalSubmissionInstance.setFinalSubmissionData(finalSubmissionPojoArrayList)) {
            //Sync Data to server
            ThreadPoolService().sendFinalSubmissionDataToServer(context)
        }
        val intent = Intent(this, ExamShowTestResultActivity::class.java)
        intent.putExtra("testID", ExamID)
        intent.putExtra("TYPE", examType)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        val builder1 = AlertDialog.Builder(this@ExamShowQuestionDataBaseOnExamIdListActivity)
        builder1.setTitle("Abort Test")
        builder1.setMessage("Do you really want to abort the ongoing test? You can resume it later.")
        builder1.setCancelable(false)
        builder1.setPositiveButton(
                "Yes"
        ) { dialogs, id ->
            dialogs.cancel()
            startActivity(Intent(this@ExamShowQuestionDataBaseOnExamIdListActivity, ExamMainDashboardActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }
        builder1.setNegativeButton(
                "No"
        ) { dialogs, id -> dialogs.cancel() }
        buildAlertDial = builder1.create()
        buildAlertDial?.show()
    }

    private fun showInstructionDialog() {
        instructionDialog = Dialog(this@ExamShowQuestionDataBaseOnExamIdListActivity)
        instructionDialog!!.setContentView(R.layout.exam_instruction_dialog)
        instructionDialog!!.setCanceledOnTouchOutside(true)
        // initialize the item of the dialog box, whose id is demo1
        val inst = instructionDialog!!.findViewById<View>(R.id.instruc) as WebView
        val dismisBtn = instructionDialog!!.findViewById(R.id.dismisDialog) as View

        // it call when click on the item whose id is demo1.
        dismisBtn.setOnClickListener { // diss miss the dialog
            instructionDialog!!.dismiss()
        }
        if (QuestionInstruction == "0") {
            QuestionInstruction = "<html> <body> <h3>Paper Instructions:</h3>  <ol> <li>The question paper contains <b>A sections </b> and <b>300 questions.</b> <br/><br/> </li> <ul><li><b>Section 1 (Physics)</b> contains100 questions .</li><li><b>Section 2 (Chemistry)</b> contains100 questions .</li><li><b>Section 3 (Mathematics)</b> contains100 questions .</li><li><b>Section 4 (Biology)</b> contains100 questions .</li> </ul><br/> <li>For each correct answer you will be awarded with <b>4 marks</b></li> <li>For each wrong answer you will be awarded with <b>-1 mark</b></li> <li>Total allotted time for this test is <b>$time Seconds</b> </li> </ol> </body></html>"
        }
        if (true) {
//            Log.d("kjiiyrefnk", "yes");
            inst.loadData(QuestionInstruction, "text/html", "UTF-8")
        } else {
//            Log.d("frkeejisfiyrefnk", "yes");
            inst.loadData("DATA", "text/html", "UTF-8")
        }


        // it show the dialog box
        instructionDialog!!.show()
    }

    private fun showAllQuestionDialog() {
        instructionDialog = Dialog(this@ExamShowQuestionDataBaseOnExamIdListActivity)
        instructionDialog!!.setContentView(R.layout.exam_show_all_question_dialog_layout)
        instructionDialog!!.setTitle("All Questions")
        instructionDialog!!.setCanceledOnTouchOutside(true)
        // initialize the item of the dialog box, whose id is demo1
        val ShowAllQuestionWebView = instructionDialog!!.findViewById<View>(R.id.ShowAllQuestionWebView) as WebView
        //        View dismisBtn = (View) instructionDialog.findViewById(R.id.ShowQuestiondismisDialog);
//
//        // it call when click on the item whose id is demo1.
//        dismisBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // diss miss the dialog
//                instructionDialog.dismiss();
//            }
//        });
        var QuestionData = """<style>div {
    background-color: white;
    width: 200px;
    border: 10px solid #363636;
    padding: 10px;
    margin: 10px;
}</style>"""
        for (i in localExamAllQuestionsSubjectWisePojoList!!.indices) {
            QuestionData += "<h4>Question Number (" + (i + 1) + ")</h4><div>" + Html.fromHtml(localExamAllQuestionsSubjectWisePojoList!![i].questionText) + "</div>"
        }
        ShowAllQuestionWebView.loadData(QuestionData, "text/html", "UTF-8")

        // it show the dialog box
        instructionDialog!!.show()
    }

    private fun myCountDownTimer(startTimes: Long, intervals: Long) {
        if (startTimer) {
            countDownTimer = object : CountDownTimer(startTimes, intervals) {
                override fun onTick(millisUntilFinished: Long) {
                    remainingTime = millisUntilFinished / 1000
                    val hour = remainingTime / 3600
                    val minutes = remainingTime / 60 % 60
                    val second = remainingTime % 60
                    if (minutes <= yellowColorTime && minutes != 0L) {
                        time!!.setTextColor(Color.YELLOW)
                        time!!.text = String.format("%02d:%02d:%02d", hour,
                                minutes, second)
                    } else if (minutes == 0L && second <= redColorTime) {
                        time!!.setTextColor(Color.RED)
                        time!!.text = String.format("%02d:%02d:%02d", hour,
                                minutes, second)
                    } else {
                        time!!.text = String.format("%02d:%02d:%02d", hour,
                                minutes, second)
                    }
                    // System.out.println("Timer  : " + remainingTime);
                }

                override fun onFinish() {
                    // System.out.println("Timer Completed.");
                    time!!.text = "Time Over"
                    time!!.textSize = 14f
                    time!!.setTextColor(Color.RED)
                    submitTestRecord()
                }
            }
            countDownTimer?.start()
        } else {
            countDownTimer!!.cancel()
        }
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
        val item = adapterView.getItemAtPosition(i).toString()
        var k: Int
        k = 0
        while (k < localExamAllQuestionsSubjectWisePojoList!!.size) {
            if (item == localExamAllQuestionsSubjectWisePojoList!![k].subjectId) {
                break
            }
            k++
        }
        dataShow(k)
    }

    override fun onNothingSelected(adapterView: AdapterView<*>?) {}
    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        val checkedTag = buttonView.tag as String
        when (checkedTag) {
            "a", "A" -> updateParAns(checkedTag, isChecked)
            "b", "B" -> updateParAns(checkedTag, isChecked)
            "c", "C" -> updateParAns(checkedTag, isChecked)
            "d", "D" -> updateParAns(checkedTag, isChecked)
            "e", "E" -> updateParAns(checkedTag, isChecked)
            "bookmark" -> if (isChecked) {
                cacheBookmark!!.saveBookmarkedQuestion(localExamAllQuestionsSubjectWisePojoList!![countValue])
            } else {
                localExamAllQuestionsSubjectWisePojoList!![countValue].questionId?.let { cacheBookmark!!.removeBookmark(it) }
            }
        }
    }

    private fun updateParAns(checkedTag: String, isChecked: Boolean) {
        if (answerStatusList!!.contains(checkedTag)) answerStatusList!!.remove(checkedTag)
        if (isChecked && !answerStatusList!!.contains(checkedTag)) answerStatusList!!.add(checkedTag) else answerStatusList!!.remove(checkedTag)
        if (reviewLaterQuestions[countValue] != "1") reviewLaterQuestions[countValue] = "2"
        if (answerStatusList!!.size < 2) {
            reviewLaterQuestions[countValue] = "3"
        }
        answerStatus = answerStatusList.toString().replace("[", "").replace("]", "").replace(", ", " ")
        //        Log.d(TAG, "ParAns: " + answerStatus);
        updateAnswer(answerStatus, reviewLaterQuestions[countValue])
    }

    fun createRotateAnimator(target: View?, from: Float, to: Float): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(target, "rotation", from, to)
        animator.duration = 300
        animator.interpolator = Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR)
        return animator
    }

    companion object {
        private const val PARAGRAPH_HEADING = "Tap to show/hide Paragraph"
    }
}
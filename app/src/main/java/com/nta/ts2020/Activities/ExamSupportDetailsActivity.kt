package com.nta.ts2020.Activities

import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout
import com.konifar.fab_transformation.FabTransformation
import com.nta.ts2020.Adapters.SupportRecyclerAdapter
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.LocalStorage.CacheSupport
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.SupportPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.SupportInsertHandler
import com.nta.ts2020.ThreadingHelper.Handlers.SupportListHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.UtilsHelper.ExamInProgressDialog
import org.json.JSONException
import org.json.JSONObject

class ExamSupportDetailsActivity : AppCompatActivity(), View.OnClickListener, TextWatcher {
    private var revealLayout: LinearLayout? = null
    private var fab: FloatingActionButton? = null
    private var isBackAlreadyPressed = false
    private var queryList: RecyclerView? = null
    private var context: ExamSupportDetailsActivity? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var linLayoutNoData: LinearLayout? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var insertQuery: EditText? = null
    private var insertQueryTitle: EditText? = null
    private var insertQueryTitleContainer: TextInputLayout? = null
    private var insertQueryContainer: TextInputLayout? = null
    private var imageViewError: ImageView? = null
    private var supportListHandler: SupportListHandler? = null
    private var examInProgressDialog: ExamInProgressDialog? = null
    private var dialog: Dialog? = null
    private var editQueryText: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activty_support)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.title = "Support"
        }
        //Context
        context = this

        //Initialize variables
        swipeRefreshLayout = findViewById<View>(R.id.srSupport) as SwipeRefreshLayout
        swipeRefreshLayout!!.setColorSchemeResources(
                R.color.material_blue_700,
                R.color.material_green_700,
                R.color.material_yellow_700,
                R.color.material_red_700)
        queryList = findViewById<View>(R.id.rvSupportList) as RecyclerView
        fab = findViewById<View>(R.id.fabNewQuery) as FloatingActionButton
        fab!!.setOnClickListener(this)

        //Reveal layout
        revealLayout = findViewById<View>(R.id.fabRevelLayout) as LinearLayout
        revealLayout!!.setOnClickListener(this)
        insertQueryTitle = revealLayout!!.findViewById<View>(R.id.etSupportTitle) as EditText
        insertQueryTitle!!.addTextChangedListener(this)
        insertQueryTitleContainer = revealLayout!!.findViewById<View>(R.id.etSupportTittleContainer) as TextInputLayout
        insertQuery = revealLayout!!.findViewById<View>(R.id.etSupportQuery) as EditText
        insertQuery!!.addTextChangedListener(this)
        insertQueryContainer = revealLayout!!.findViewById<View>(R.id.etSupportQueryContainer) as TextInputLayout
        val insertQueryButton = revealLayout!!.findViewById<View>(R.id.btSupportSubmit) as Button
        insertQueryButton.setOnClickListener(this)
        queryList!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    fab!!.hide()
                } else {
                    if (!revealLayout!!.isShown) fab!!.show()
                }
            }
        })

        //Message Layout
        linLayoutNoData = findViewById<View>(R.id.llExamResultListNoData) as LinearLayout
        textViewMessage = findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        imageViewError = findViewById<View>(R.id.ivExamResultListError) as ImageView
        supportListHandler = SupportListHandler(context!!)
        Log.e("networkcheck", "" + isNetworkConnected)
        if (isNetworkConnected) {
            swipeRefreshLayout!!.isRefreshing = true
            ThreadPoolService().getSupportList(context, supportListHandler)
        } else {
            loadData(SUCCESS)
        }
        swipeRefreshLayout!!.setOnRefreshListener {
            if (isNetworkConnected) {
                ThreadPoolService().getSupportList(context, supportListHandler)
            } else {
                loadData(SUCCESS)
            }
        }
    }

    override fun onBackPressed() {
        if (revealLayout!!.isShown && !isBackAlreadyPressed) {
            fabRevelHide()
        } else super.onBackPressed()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.fabNewQuery -> fabRevelShow()
            R.id.fabRevelLayout -> fabRevelHide()
            R.id.btSupportSubmit -> insertNewQuery()
        }
    }

    private fun insertNewQuery() {
        if (isValid) {
            //Build parameter
            val jsonParameter = JSONObject()
            try {
                jsonParameter.put("title", insertQueryTitle!!.text.toString())
                jsonParameter.put("query", insertQuery!!.text.toString())
                jsonParameter.put("user_id", CacheUserData(context!!).userDataFromCache.userId)
                val parameter = jsonParameter.toString()
                insertQueryTitle!!.setText("")
                insertQuery!!.setText("")
                examInProgressDialog = ExamInProgressDialog(context)
                examInProgressDialog!!.show()
                val supportInsertHandler = SupportInsertHandler(context!!)
                ThreadPoolService().insertNewQuert(parameter, supportInsertHandler)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    private val isValid: Boolean
        private get() {
            var status = true
            if (insertQueryTitle!!.text.toString() == "") {
                insertQueryTitleContainer!!.error = "Enter Title"
                status = false
            }
            if (insertQuery!!.text.toString() == "") {
                insertQueryContainer!!.error = "Enter your Query"
                status = false
            }
            return status
        }

    private fun fabRevelShow() {
        isBackAlreadyPressed = false
        if (swipeRefreshLayout!!.isRefreshing) swipeRefreshLayout!!.isRefreshing = false
        swipeRefreshLayout!!.isEnabled = false
        FabTransformation.with(fab)
                .duration(150)
                .transformTo(revealLayout)
    }

    private fun fabRevelHide() {
        swipeRefreshLayout!!.isEnabled = true
        isBackAlreadyPressed = true
        FabTransformation.with(fab)
                .duration(150)
                .transformFrom(revealLayout)

        // Check if no view has focus then hide soft keyboard
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private val isNetworkConnected: Boolean
        get() {
            val cm = context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isAvailable && cm.activeNetworkInfo.isConnected
        }

    fun loadData(status: Int) {
        var status = status
        val supportPojoArrayList = CacheSupport(context!!).queries
        if (supportPojoArrayList.isEmpty() && status != NETWORK_ERROR) status = NO_DATA
        if (status == SUCCESS) {
            linLayoutNoData!!.visibility = View.GONE
            queryList!!.visibility = View.VISIBLE
            val supportAdapter = SupportRecyclerAdapter(supportPojoArrayList)
            queryList!!.layoutManager = LinearLayoutManager(context)
            queryList!!.adapter = supportAdapter
            supportAdapter.setOnItemClickListener(object : SupportRecyclerAdapter.OnItemClickListener{
                override fun onClick(supportPojo: SupportPojo?) {
                        dialog = Dialog(context!!, R.style.Dialog_NoTitle)
                        dialog!!.setContentView(R.layout.dialog_support_answer)
                        val dialogQueryTitle = dialog!!.findViewById<View>(R.id.tvqQueryDialogTitle) as TextView
                        val dialogQueryToken = dialog!!.findViewById<View>(R.id.tvqQueryDialogToken) as TextView
                        val dialogQueryDate = dialog!!.findViewById<View>(R.id.tvqQueryDialogDate) as TextView
                        val dialogQueryQuery = dialog!!.findViewById<View>(R.id.tvqQueryDialogQuery) as TextView
                        val dialogQueryAnswer = dialog!!.findViewById<View>(R.id.tvqQueryDialogAnswer) as TextView
                        val dialogQueryNoAnswer = dialog!!.findViewById<View>(R.id.tvQueryDialogNoAnswer) as TextView
                        val editQuery = dialog!!.findViewById<View>(R.id.ivQueryDialogEditQuery) as ImageView
                        val done = dialog!!.findViewById<View>(R.id.ivQueryDialogDone) as ImageView
                        editQueryText = dialog!!.findViewById<View>(R.id.etQueryDialogQuery) as EditText
                        dialogQueryTitle.text = supportPojo?.title
                        dialogQueryToken.text = supportPojo?.queryToken
                        dialogQueryDate.text = supportPojo?.date
                        dialogQueryQuery.text = supportPojo?.query
                        if (supportPojo?.answer == "" || supportPojo?.answer == null) {
                            dialogQueryAnswer.visibility = View.GONE
                            dialogQueryNoAnswer.visibility = View.VISIBLE

                            //Show edit option if there is no response
                            editQuery.visibility = View.VISIBLE
                            editQuery.setOnClickListener {
                                dialogQueryQuery.visibility = View.GONE
                                editQuery.visibility = View.GONE
                                done.visibility = View.VISIBLE
                                editQueryText!!.visibility = View.VISIBLE
                            }
                            done.setOnClickListener {
                                if (editQueryText!!.text.toString().trim { it <= ' ' } == "") {
                                    editQueryText!!.error = "Please enter new query."
                                } else {
                                    //Build parameter
                                    val jsonParameter = JSONObject()
                                    try {
                                        jsonParameter.put("id", supportPojo?.queryId)
                                        jsonParameter.put("query", editQueryText!!.text.toString())
                                        val parameter = jsonParameter.toString()
                                        examInProgressDialog!!.show()
                                        val supportInsertHandler = SupportInsertHandler(context!!)
                                        ThreadPoolService().insertNewQuert(parameter, supportInsertHandler)
                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                    }
                                }
                            }
                        } else {
                            dialogQueryAnswer.visibility = View.VISIBLE
                            dialogQueryNoAnswer.visibility = View.GONE
                            editQuery.visibility = View.GONE
                            dialogQueryAnswer.text = supportPojo.answer
                        }
                        dialog!!.show()
                }

            })
        } else if (status == NO_DATA) {
            linLayoutNoData!!.visibility = View.VISIBLE
            queryList!!.visibility = View.GONE
            textViewMessage!!.setText(R.string.result_list_message_no_data)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_no_data)
            imageViewError!!.setImageResource(R.drawable.ic_no_data)
        } else if (status == NETWORK_ERROR) {
            linLayoutNoData!!.visibility = View.VISIBLE
            queryList!!.visibility = View.GONE
            textViewMessage!!.setText(R.string.result_list_message_network_error)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_network_error)
            imageViewError!!.setImageResource(R.drawable.ic_no_network)
        }
        if (swipeRefreshLayout!!.isRefreshing) {
            swipeRefreshLayout!!.isRefreshing = false
        }
        swipeRefreshLayout!!.isRefreshing = false
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (insertQueryTitle!!.text.hashCode() == s.hashCode()) {
            insertQueryTitleContainer!!.error = null
        }
        if (insertQuery!!.text.hashCode() == s.hashCode()) {
            insertQueryContainer!!.error = null
        }
    }

    override fun afterTextChanged(s: Editable) {}
    fun postQueryInsert(status: Int) {
        if (status == SUCCESS) {
            swipeRefreshLayout!!.isRefreshing = true
            ThreadPoolService().getSupportList(context, supportListHandler)
            if (revealLayout!!.isShown) {
                fabRevelHide()
            }
            //            if (dialog.isShowing()) {
//                dialog.dismiss();
//            }
        }
        examInProgressDialog!!.dismiss()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "ExamSupportDetailsActivity"
    }
}
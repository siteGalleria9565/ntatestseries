package com.nta.ts2020.Activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView
import com.nta.ts2020.R

open class ExamYoutubePlay : ExamYouTubeFailureRecoveryActivity() {
    private var id: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.youtube)
        val b = intent.extras
        id = b!!["id"] as String?
        Log.e("id", id)
        val youTubeView = findViewById<View>(R.id.youtube_view) as YouTubePlayerView
        youTubeView
                .initialize(
                        getString(R.string.youtube_api_key),
                        this)
    }

    override fun onInitializationSuccess(provider: YouTubePlayer.Provider,
                                         player: YouTubePlayer, wasRestored: Boolean) {
        // TODO Auto-generated method stub
        if (!wasRestored) {
            player.loadVideo(id)
        }
    }

    // TODO Auto-generated method stub
    override val youTubePlayerProvider: YouTubePlayer.Provider
        get() =// TODO Auto-generated method stub
            findViewById<View>(R.id.youtube_view) as YouTubePlayerView
}
package com.nta.ts2020.Activities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.nta.ts2020.PojoClasses.ExamResult
import java.util.*

class ExamVideos {
    @SerializedName("data")
    @Expose
    var data: List<ExamResult> = ArrayList()

    @SerializedName("total_record")
    @Expose
    var total_record = 0

}
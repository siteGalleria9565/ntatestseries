package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.Activities.ExamPostRegistrationScreenActivity
import com.nta.ts2020.AppConstants.ConstantVariables.POST_REGISTRATION_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.isValidPassword
import com.nta.ts2020.PojoClasses.ClassPojo
import com.nta.ts2020.PojoClasses.CoursePojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.PostRegistrationHandler
import com.nta.ts2020.ThreadingHelper.Handlers.SaveCoursesFromServerToCacheHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ExamPostRegistrationScreenActivity : AppCompatActivity(), View.OnClickListener {
    private var context: ExamPostRegistrationScreenActivity? = null
    private var progressDialog: ProgressDialog? = null
    private var coursesList: ArrayList<CoursePojo>? = null
    private var classesSpinner: Spinner? = null
    private var coursesSpinner: Spinner? = null
    private var tvNewPass: EditText? = null
    private var tvConfPass: EditText? = null
    private var userId: String? = null
    private var newSocialPassword: String? = null
    var passwordCheck: ExamRegisterActivity? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_registration)
        passwordCheck = ExamRegisterActivity()
        //Context
        context = this
        userId = ""
        if (intent.extras != null) userId = intent.extras!!.getString("USER_ID")

        //Initialize variables
        tvNewPass = findViewById<View>(R.id.tvPostRegNewPass) as EditText
        tvConfPass = findViewById<View>(R.id.tvPostRegConfPass) as EditText
        coursesSpinner = findViewById<View>(R.id.spPostRegCourses) as Spinner
        classesSpinner = findViewById<View>(R.id.spPostRegClasses) as Spinner
        val saveLogin = findViewById<View>(R.id.btPostRegSaveLogin) as Button
        saveLogin.setOnClickListener(this)
        coursesSpinner!!.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                val classesArrayList = coursesList!![position].classes
                val tempClassesList = ArrayList<String?>()
                for ((_, name) in classesArrayList!!) {
                    tempClassesList.add(name)
                }
                classesSpinner!!.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, tempClassesList)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true
        progressDialog!!.show()

        //Save courses and classes to cache
        ThreadPoolService().saveCoursesFromServerToCache(this, SaveCoursesFromServerToCacheHandler(this, POST_REGISTRATION_ACTIVITY))
        saveLogin.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btPostRegSaveLogin -> if (isNetworkConnected) {
                if (isValid) {
                    progressDialog!!.show()
                    val jsonParameter = buildParameter()
                    val postRegistrationHandler = PostRegistrationHandler(this@ExamPostRegistrationScreenActivity, POST_REGISTRATION_ACTIVITY)
                    if (jsonParameter != "") ThreadPoolService().PostRegistrationRequest(context, jsonParameter, postRegistrationHandler) else Toast.makeText(context, "Please try again later", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(context, "Network Error: Please connect to the internet", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    fun loadData(coursePojoArrayList: ArrayList<CoursePojo>?) {
        coursesList = coursePojoArrayList
        val initialCoursePojo = CoursePojo()
        val initialClass = ArrayList<ClassPojo>()
        val initialClassPojo = ClassPojo()
        initialClassPojo.name = "Select Class"
        initialClassPojo.id = "-1"
        initialClass.add(initialClassPojo)
        initialCoursePojo.name = "Select Course"
        initialCoursePojo.classes = initialClass
        coursesList!!.add(0, initialCoursePojo)
        val tempCourses = ArrayList<String?>()
        for (i in coursesList!!.indices) tempCourses.add(i, coursesList!![i].name)
        val spinnerAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, tempCourses)
        coursesSpinner!!.adapter = spinnerAdapter
        progressDialog!!.dismiss()
    }

    private val isValid: Boolean
        private get() {
            var status = true
            if (tvNewPass!!.text.toString() == "") {
                tvNewPass!!.error = "Enter new password"
                status = false
            } else {
                newSocialPassword = tvNewPass!!.text.toString().trim { it <= ' ' }
                if (!isValidPassword(tvNewPass!!)) status = false
            }
            if (tvConfPass!!.text.toString() == "") {
                tvConfPass!!.error = "Confirm password"
                status = false
            }
            if (tvNewPass!!.text.toString() != ""
                    && tvConfPass!!.text.toString() != ""
                    && tvNewPass!!.text.toString() != tvConfPass!!.text.toString()) {
                Toast.makeText(context, "Password doesn't match , Please re-enter", Toast.LENGTH_SHORT).show()
                tvNewPass!!.setText("")
                tvConfPass!!.setText("")
                status = false
            }
            if (coursesSpinner!!.selectedItem.toString() == "Select Course" || coursesSpinner!!.selectedItem == null) {
                val errorTextSpinner = coursesSpinner!!.selectedView as TextView
                errorTextSpinner.error = ""
                status = false
            }
            if (classesSpinner!!.selectedItem.toString() == "Select Class" || classesSpinner!!.selectedItem == null) {
                val errorTextSpinner = classesSpinner!!.selectedView as TextView
                errorTextSpinner.error = ""
                status = false
            }
            return status
        }

    private fun buildParameter(): String {
        var parameterJson = ""
        val json = JSONObject()
        if (userId != "") parameterJson = try {
            val selectedCoursePos = coursesSpinner!!.selectedItemPosition
            val selectedClassPos = classesSpinner!!.selectedItemPosition
            val courseId = coursesList!![selectedCoursePos].id
            val classId = coursesList!![selectedCoursePos].classes!![selectedClassPos].id
            json.put("user_id", userId)
            json.put("password", tvNewPass!!.text.toString())
            json.put("course_id", courseId)
            json.put("class_id", classId)
            json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
            return "ERROR"
        }
        return parameterJson
    }

    fun startNextActivity() {
        startActivity(Intent(this@ExamPostRegistrationScreenActivity, ExamMainDashboardActivity::class.java))
        finish()
        progressDialog!!.dismiss()
    }
}
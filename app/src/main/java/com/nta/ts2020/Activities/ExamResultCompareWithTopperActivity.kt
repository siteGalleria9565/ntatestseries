package com.nta.ts2020.Activities

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.db.chart.animation.Animation
import com.db.chart.model.Bar
import com.db.chart.model.BarSet
import com.db.chart.renderer.AxisRenderer
import com.db.chart.renderer.XRenderer
import com.db.chart.renderer.YRenderer
import com.db.chart.view.BarChartView
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.PojoClasses.TopperPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.TopperHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.UtilsHelper.ExamInProgressDialog
import java.util.*

class ExamResultCompareWithTopperActivity : AppCompatActivity() {
    private var context: ExamResultCompareWithTopperActivity? = null
    private var progressDialog: ExamInProgressDialog? = null
    private var mChart: BarChartView? = null
    private var errorLayout: LinearLayout? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var tvExamName: TextView? = null
    private var imageViewError: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.result_compare_with_topper)
        if (supportActionBar != null) {
            supportActionBar!!.title = "Compare with Topper"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Context
        context = this
        var id = ""
        var examName = ""
        val extras = intent.extras
        if (extras != null) {
            id = extras.getString("ID").toString()
            examName = extras.getString("EXAM_NAME").toString()
        }
        progressDialog = ExamInProgressDialog(context)
        mChart = findViewById<View>(R.id.topperBarChart) as BarChartView
        tvExamName = findViewById<View>(R.id.topperExamName) as TextView
        tvExamName!!.text = examName
        errorLayout = findViewById<View>(R.id.topperErrorLayout) as LinearLayout
        textViewMessage = findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        imageViewError = findViewById<View>(R.id.ivExamResultListError) as ImageView
        if (isNetworkConnected) {
            progressDialog!!.show()
            val topperHandler = TopperHandler(context!!)
            ThreadPoolService().getTopperDataFromServer(context, id, topperHandler)
        } else {
            loadData(ArrayList(), SUCCESS)
            Toast.makeText(context, "Network Error: Please connect to the internet", Toast.LENGTH_SHORT).show()
        }
    }

    fun loadData(topperPojoArrayList: ArrayList<TopperPojo>, status: Int) {
        var status = status
        if (topperPojoArrayList.isEmpty() && status != NETWORK_ERROR) status = NO_DATA
        when (status) {
            SUCCESS -> {
                errorLayout!!.visibility = View.GONE
                mChart!!.visibility = View.VISIBLE
                tvExamName!!.visibility = View.VISIBLE
                mChart!!.setBarSpacing(20f)
                val barSet = BarSet()
                val colors = resources.getIntArray(R.array.color_array)
                var color = colors[6]
                var name: String?
                var isBellowMe = false // Denotes those who are after my rank
                var i = 0
                while (i < topperPojoArrayList.size) {
                    if (topperPojoArrayList[i].isMe) {
                        name = topperPojoArrayList[i].name + " (Me)"
                        color = colors[4]
                        isBellowMe = true
                    } else {
                        name = topperPojoArrayList[i].name
                        if (isBellowMe) color = colors[1]
                    }
                    val bar = Bar(name, topperPojoArrayList[i].marks!!.toFloat())
                    bar.color = color
                    barSet.addBar(bar)
                    i++
                }
                mChart!!.addData(barSet)
                mChart!!.setXLabels(AxisRenderer.LabelPosition.OUTSIDE)
                        .setYLabels(AxisRenderer.LabelPosition.OUTSIDE)
                        .show(Animation())
            }
            NO_DATA -> {
                textViewMessage!!.setText(R.string.result_list_message_no_data)
                textViewSubMessage!!.setText(R.string.sub_message_no_data_error)
                imageViewError!!.setImageResource(R.drawable.ic_no_data)
                errorLayout!!.visibility = View.VISIBLE
                tvExamName!!.visibility = View.GONE
                mChart!!.visibility = View.GONE
            }
            NETWORK_ERROR -> {
                textViewMessage!!.setText(R.string.result_list_message_network_error)
                textViewSubMessage!!.setText(R.string.sub_message_internet_error)
                imageViewError!!.setImageResource(R.drawable.ic_no_network)
                errorLayout!!.visibility = View.VISIBLE
                tvExamName!!.visibility = View.GONE
                mChart!!.visibility = View.GONE
            }
        }
        progressDialog!!.dismiss()
    }

    private val isNetworkConnected: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
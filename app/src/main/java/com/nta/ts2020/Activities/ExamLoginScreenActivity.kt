package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.Image
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.LoginHandler
import com.nta.ts2020.ThreadingHelper.Handlers.SocialLoginHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.UtilsHelper.ConnectionDetector
import com.nta.ts2020.UtilsHelper.ExamInProgressDialog
import com.nta.ts2020.UtilsHelper.MyPref
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ExamLoginScreenActivity : AppCompatActivity(), View.OnClickListener, TextWatcher, GoogleApiClient.OnConnectionFailedListener, WebResponseListener {
    private lateinit var context: ExamLoginScreenActivity
    var progressDialog: ProgressDialog? = null
        private set
    private  var email: EditText? = null
    private lateinit var password: EditText
    private var notificationId: String? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var facebook: ImageView? = null
    var callbackManager: CallbackManager? = null
    private lateinit var tv_forget_password : TextView
    var bottomSheetDialog: BottomSheetDialog? = null
    private var webRequest: WebRequest? = null
    lateinit var btn_send: Button
    var btnLogin: Button? = null
    var examInProgressDialog: ExamInProgressDialog? = null
    lateinit var myPref: MyPref
    lateinit var backbtn : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Context
        context = this
        myPref = MyPref(this)
        if (intent.extras != null) {
            notificationId = intent.extras!!.getString("notification_id")
        }
        //User already logged in, so start next activity
            if (CacheUserData(context).userDataFromCache.isUserLoggedIn) startNextActivity()


        //Change notification bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        }
        FacebookSdk.sdkInitialize(applicationContext)
        callbackManager = CallbackManager.Factory.create()
        setContentView(R.layout.activity_login)
        loginWithFacebook()
        //Remove ActionBar
        if (supportActionBar != null) supportActionBar!!.hide()
        examInProgressDialog = ExamInProgressDialog(this)
        //Initialize all elements in the activity
        val googlePlus = findViewById<View>(R.id.loginSocialGoogle) as ImageView
        googlePlus.setOnClickListener(this)
        //        ImageView linkedIn = (ImageView) findViewById(R.id.loginSocialLinkedIn);
//        linkedIn.setOnClickListener(this);
        facebook = findViewById<View>(R.id.loginSocialFacebook) as ImageView
        facebook!!.setOnClickListener(this)
        email = findViewById<View>(R.id.editTextLogInEmail) as EditText
        password = findViewById<View>(R.id.editTextLogInPassword) as EditText
        tv_forget_password = findViewById(R.id.tv_forget_password)
        tv_forget_password.setOnClickListener(this)
        webRequest = WebRequest(this)
        backbtn = findViewById(R.id.back_btn)
        backbtn.setOnClickListener(this)
        btnLogin = findViewById<View>(R.id.buttonLogIn) as Button
        val txtRegister = findViewById<View>(R.id.buttonRegister) as Button

        //Set Listeners
        txtRegister.setOnClickListener(this)
        btnLogin!!.setOnClickListener(this)
        email!!.addTextChangedListener(this)
        password!!.addTextChangedListener(this)
        password!!.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                btnLogin!!.performClick()
            }
            false
        }

        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true

        //Google SignIn
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.back_btn -> onBackPressed()
            R.id.buttonLogIn -> {
                // Check if no view has focus then hide soft keyboard
                val view = this.currentFocus
                if (view != null) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }
                if (isValid) {
                    //Prepare parameter for sending it to server
                    val builderParameter = Uri.Builder()
                    builderParameter.appendQueryParameter("email", email!!.text.toString())
                            .appendQueryParameter("password", password!!.text.toString())
                            .appendQueryParameter("institution_id" , "1")
                            .build()
                    val parameter = builderParameter.toString().substring(1)

                    //Show Progress dialog
                    progressDialog!!.show()

                    //Create Handler and call threadPool service  and call thread
                    val loginHandler = LoginHandler(context)
                    ThreadPoolService().login(context, loginHandler, parameter)
                }
            }
            R.id.buttonRegister -> if (isNetworkConnected) {
                examInProgressDialog!!.show()
                webRequest!!.GET_METHOD(ConstantVariables.URL_COURSE, this, WebCallType.COURSE, false)
            } else Snackbar.make(btnLogin!!, "Please check Internet Connection", Snackbar.LENGTH_LONG).show()
            R.id.loginSocialGoogle -> if (isNetworkConnected) {
                progressDialog!!.show()
                val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
                startActivityForResult(signInIntent, RC_SIGN_IN)
            } else {
                Toast.makeText(context, "Please connect to the internet.", Toast.LENGTH_SHORT).show()
            }
            R.id.loginSocialFacebook -> if (isNetworkConnected) {
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"))
            } else {
                Toast.makeText(context, "Please connect to the internet.", Toast.LENGTH_SHORT).show()
            }
            R.id.tv_forget_password -> {
                bottomSheetDialog = BottomSheetDialog(this)
                val view1 = LayoutInflater.from(this).inflate(R.layout.bottomsheet_forget_password, null, false)
                bottomSheetDialog!!.setContentView(view1)
                bottomSheetDialog!!.show()
                val editTextEmail = view1.findViewById<EditText>(R.id.editTextEmail)
                btn_send = view1.findViewById(R.id.btn_send)
                btn_send.setOnClickListener(View.OnClickListener {
                    if (editTextEmail.text.isNotEmpty()) {
                        val build = "?app=NTA&email=" + editTextEmail.text.toString()
                        val hashMap = HashMap<Any?, Any?>()
                        val jsonObject = JSONObject()
                        jsonObject.put("email", editTextEmail.text.toString())
                        jsonObject.put("password",password.text.toString())
                        jsonObject.put("institution_id",1)
                        //hashMap["app"] = "NTA"
                        //hashMap["email"] = editTextEmail.text.toString()
                        //                        webRequest.POST_METHOD(ConstantVariables.URL_FORGET_PASSWORD,null,hashMap,ExamLoginScreenActivity.this,null,true);
                        val connectionDetector = ConnectionDetector(this@ExamLoginScreenActivity)
                        if (connectionDetector.isConnectingToInternet) {
                            webRequest!!.POST_METHOD(ConstantVariables.URL_FORGET_PASSWORD, jsonObject, WebCallType.FORGOT_PASSWORD,null, this@ExamLoginScreenActivity, true)
                            examInProgressDialog!!.show()
                        } else {
                            Toast.makeText(this@ExamLoginScreenActivity, "Please Check Internet Connection", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this@ExamLoginScreenActivity, "Please Enter Email ID", Toast.LENGTH_SHORT).show()
                    }
                })
            }
        }
    }

    private val isNetworkConnected: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    private val isValid: Boolean
        get() {
            var status = true
            if (email!!.text.toString() == "") {
                email!!.error = "Enter email"
                status = false
            } else {
                if (!Patterns.EMAIL_ADDRESS.matcher(email!!.text.toString()).matches()) {
                    email!!.error = "Enter valid email address"
                    status = false
                }
            }
            if (password!!.text.toString() == "") {
                password!!.error = "Enter password"
                status = false
            }
            return status
        }

    fun startNextActivity() {
        if (notificationId != null) {
            startActivity(Intent(this@ExamLoginScreenActivity, ExamNotificationsDetailsActivity::class.java).putExtra("notification_id", notificationId))
        } else {
            startActivity(Intent(this@ExamLoginScreenActivity, ExamMainDashboardActivity::class.java))
        }
        finish()
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (email!!.text.hashCode() == s.hashCode()) {
            email!!.error = null
        }
        if (password!!.text.hashCode() == s.hashCode()) {
            password!!.error = null
        }
    }

    override fun afterTextChanged(s: Editable) {}
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            progressDialog!!.dismiss()
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult?) {
        Log.d(TAG, "handleSignInResult:" + result!!.isSuccess)
        if (result.isSuccess) {
            // Signed in successfully, show authenticated UI.
            val acct = result.signInAccount
            val json = JSONObject()
            try {
                json.put("name", acct!!.displayName)
                json.put("email", acct.email)
                json.put("image_url", acct.photoUrl)
                progressDialog!!.show()

                //Sign out user after getting details
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback { TODO("Not yet implemented") }
                val socialLoginHandler = SocialLoginHandler(this@ExamLoginScreenActivity)
                ThreadPoolService().socialLogin(context, json.toString(), socialLoginHandler)
            } catch (e: JSONException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
            }
        } else {
            // Signed out, show unauthenticated UI.
            Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}
    fun loadData(status: Int, isRegistered: Boolean, userId: String?) {
        if (status == ConstantVariables.SUCCESS) {
            if (!isRegistered) {
                startActivity(Intent(this@ExamLoginScreenActivity, ExamPostRegistrationScreenActivity::class.java).putExtra("USER_ID", userId))
                finish()
            } else {
                startNextActivity()
            }
        }
        progressDialog!!.dismiss()
    }

    private fun loginWithFacebook() {
        LoginManager.getInstance().logOut()
        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        Toast.makeText(context, "done", Toast.LENGTH_LONG).show()
                        setFacebookData(loginResult)
                    }

                    override fun onCancel() {
                        // App code
                    }

                    override fun onError(exception: FacebookException) {
                        // App code
                    }
                })
    }

    private fun setFacebookData(loginResult: LoginResult) {
        val request = GraphRequest.newMeRequest(
                loginResult.accessToken
        ) { `object`, response -> // Application code
            try {
                Log.i(TAG, "Facebook Response $response")
                val email = response.jsonObject.getString("email")
                if (email == "") {
                    Toast.makeText(context, "Please provide email.", Toast.LENGTH_SHORT).show()
                }
                val firstName = response.jsonObject.getString("first_name")
                val lastName = response.jsonObject.getString("last_name")
                val gender = response.jsonObject.getString("gender")
                val fullName = "$firstName $lastName"
                val profile = Profile.getCurrentProfile()
                val id = profile.id
                val link = profile.linkUri.toString()
                val pic = Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString()
                val img_value = "http://graph.facebook.com/$id/picture?type=large&redirect=false"
                val json = JSONObject()
                json.put("name", fullName)
                json.put("email", email)
                json.put("image_url", pic)
                progressDialog!!.show()
                val socialLoginHandler = SocialLoginHandler(this@ExamLoginScreenActivity)
                ThreadPoolService().socialLogin(context, json.toString(), socialLoginHandler)
            } catch (e: JSONException) {
                if (BuildConfig.DEBUG) e.printStackTrace()
                Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show()
            }
        }
        val parameters = Bundle()
        parameters.putString("fields", "id,email,first_name,last_name,gender")
        request.parameters = parameters
        request.executeAsync()
    }

    override fun onError(message: String?) {
        Log.d("jj", message)
        examInProgressDialog!!.dismiss()
        Snackbar.make(btnLogin!!, "Something went wrong please try again", Snackbar.LENGTH_LONG).show()
    }

    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        Log.d("jj", response.toString())
        val result = response.toString()
        when (webCallType) {
            WebCallType.COURSE -> {
                examInProgressDialog!!.dismiss()
                startActivity(Intent(this@ExamLoginScreenActivity, ExamRegisterActivity::class.java).putExtra("Course_List", result))
            }
            WebCallType.FORGOT_PASSWORD -> try {
                val jsonObject = JSONObject(result)
                val message = jsonObject.getString("msg")
                if (jsonObject.getInt("code") == 200) {
                    bottomSheetDialog!!.dismiss()
                    examInProgressDialog!!.dismiss()
                    Snackbar.make(email!!, "$message please check inbox and spam.", Snackbar.LENGTH_SHORT).show()
                } else {
                    examInProgressDialog!!.dismiss()
                    Toast.makeText(context, "" + message, Toast.LENGTH_LONG).show()
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            else -> null
        }
    }

    companion object {
        private const val TAG = "ExamLoginScreenActivity"
        private const val RC_SIGN_IN = 0
        private const val CUSTOM_TABS_SERVICE_ACTION = "android.support.customtabs.action.CustomTabsService"
        private const val CHROME_PACKAGE = "com.android.chrome"
    }
}
package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.AppConstants.ConstantVariables.EX_STUDENT_USER
import com.nta.ts2020.AppConstants.ConstantVariables.OTHER_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.isValidPassword
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.UpdateProfileHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.nta.ts2020.WebService.WebRequest
import org.json.JSONException
import org.json.JSONObject

class ExamProfSecurityActivity : AppCompatActivity(), View.OnClickListener {
    private var context: Context? = null
    private var tvNewPass: EditText? = null
    private var tvConfPass: EditText? = null
    private var newPasswd: String? = null
    private lateinit var webRequest : WebRequest
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_security)

        //Set Title
        if (supportActionBar != null) {
            supportActionBar!!.title = "Security"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Context
        context = this

        //Initialization
        webRequest = WebRequest(this)
        tvNewPass = findViewById<View>(R.id.tvSecurityNewPass) as EditText
        tvConfPass = findViewById<View>(R.id.tvSecurityConfPass) as EditText
        val btnSave = findViewById<View>(R.id.btSecuritySave) as Button
        btnSave.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btSecuritySave -> if (isValid) {
                //Save to server

                //                    Log.d(TAG, "Security Parameters: " + parameterJson);
                //progress dialog
               // webRequest.POST_METHOD(ConstantVariables.URL_RESET_PASSWORD)
                val progressDialog = ProgressDialog(context)
                progressDialog.setTitle("Loading ....")
                progressDialog.setMessage("Please wait")
                progressDialog.setCancelable(false)
                progressDialog.isIndeterminate = true
                progressDialog.show()
//                val updateProfileHandler = UpdateProfileHandler(this@ExamProfSecurityActivity, progressDialog)
//                ThreadPoolService().saveProfileToServer(applicationContext, parameterJson, OTHER_ACTIVITY, updateProfileHandler)
            }
        }
    }

    /**
     * Builds parameter in json format to return it to the server
     *
     * @return json format in the mentioned format in docs
     */
    private fun buildParameter(): String {
        var parameterJson = ""
        val json = JSONObject()
        parameterJson = try {
            json.put("TABLE", EX_STUDENT_USER)
            json.put("ID", CacheUserData(context!!).userDataFromCache.userId)
            json.put("PASSWORD", tvNewPass!!.text.toString().trim { it <= ' ' })
            json.toString()
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            return "ERROR"
        }
        return parameterJson
    }

    private val isValid: Boolean
        private get() {
            var status = true
            if (tvNewPass!!.text.toString().trim { it <= ' ' } == "") {
                tvNewPass!!.error = "Enter new password"
                status = false
            } else newPasswd = tvNewPass!!.text.toString().trim { it <= ' ' }
            if (!isValidPassword(tvNewPass!!)) status = false
            if (tvConfPass!!.text.toString().trim { it <= ' ' } == "") {
                tvConfPass!!.error = "Confirm password"
                status = false
            }
            if (tvNewPass!!.text.toString().trim { it <= ' ' } != ""
                    && tvConfPass!!.text.toString().trim { it <= ' ' } != ""
                    && tvNewPass!!.text.toString().trim { it <= ' ' } != tvConfPass!!.text.toString()) {
                Toast.makeText(context, "Password doesn't match , Please re-enter", Toast.LENGTH_SHORT).show()
                tvNewPass!!.setText("")
                tvConfPass!!.setText("")
                status = false
            }
            return status
        }

    fun loadData(jsonParameter: String?) {}
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "ExamProfSecurityActivity"
    }
}
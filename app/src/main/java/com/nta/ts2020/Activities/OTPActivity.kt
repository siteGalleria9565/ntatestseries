package com.nta.ts2020.Activities

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.UserDataPojo
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.MyPref
import com.nta.ts2020.WebService.WebCallType
import com.nta.ts2020.WebService.WebRequest
import com.nta.ts2020.WebService.WebResponseListener
import org.json.JSONObject
import org.w3c.dom.Text


class OTPActivity : AppCompatActivity(), View.OnClickListener, WebResponseListener {
    private lateinit var etOTP: EditText
    private lateinit var submitBtn : Button
    private lateinit var webRequest: WebRequest
    private lateinit var userDataPojo : UserDataPojo
    private lateinit var myPref: MyPref
    private lateinit var gson: Gson
    private var databaseResponse = false
    private lateinit var cacheUserData : CacheUserData
    private lateinit var tv_error : TextView
    var millisToGo: Long = 15 * 1000 * 60
    private lateinit var tv_timer : TextView
    private lateinit var tv_email : TextView
    private lateinit var backBtn : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_o_t_p)

        backBtn = findViewById(R.id.back_btn)
        backBtn.setOnClickListener(this)
        tv_email = findViewById(R.id.tv_email_id)
        tv_timer = findViewById(R.id.tv_timer)
        tv_error = findViewById(R.id.tv_invalid_otp_error)
        cacheUserData = CacheUserData(this)
        etOTP = findViewById(R.id.pinView)
        submitBtn = findViewById(R.id.btn_submit)
        webRequest = WebRequest(this)
        gson = Gson()
        myPref = MyPref(this)

        submitBtn.setOnClickListener(this)
        userDataPojo = CacheUserData(this).userDataFromCache
        tv_email.text = userDataPojo.email

        setTimer()
        etOTP.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (etOTP.text.length == 6) {
                    submitBtn.isEnabled = true
                    submitBtn.setBackgroundResource(R.drawable.background_btn)
                } else {
                    submitBtn.isEnabled = false
                    submitBtn.setBackgroundColor(R.drawable.grey_background_btn)
                }
            }

        })
    }

    private fun setTimer() {
       object : CountDownTimer(millisToGo, 1000) {
            override fun onTick(millis: Long) {

                val text = "${((millis / (1000 * 60) % 60))} : ${((millis / 1000).toInt() % 60)}"
                tv_timer.text = text
            }
            override fun onFinish() {
                onBackPressed()
            }
        }.start()
    }

    override fun onClick(v: View) {
        if(v.id == R.id.btn_submit){
            if(!etOTP.text.toString().isNullOrEmpty() || etOTP.text.length != 6){
                tv_error.visibility = View.GONE
                val jsonObject = JSONObject()
                jsonObject.put("EMAIL", userDataPojo.email)
                jsonObject.put("REGISTRATION_OTP", etOTP.text.toString())
                jsonObject.put("INSTITUTION_ID", 1)
                webRequest.POST_METHOD(ConstantVariables.URL_OTP_VERIFICATION, jsonObject, WebCallType.OTP_VERIFICATION, null, this, true)
            }else{
                tv_error.visibility = View.VISIBLE
            }
        }
        if(v.id == R.id.back_btn){
            onBackPressed()
        }
    }

    fun startNextActivity() {
        val intent = Intent(this, ExamMainDashboardActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    override fun onError(message: String?) {

    }

    override fun onResponse(response: Any?, webCallType: WebCallType?) {
        Log.d("ll",response.toString())
        if(webCallType == WebCallType.OTP_VERIFICATION){
            val jsonObject = JSONObject(response.toString())
            if(jsonObject.getInt("status") == 200){
                tv_error.visibility = View.GONE
                databaseResponse = cacheUserData.updateUserLoginStatus(true)
                if(databaseResponse)
                    startNextActivity()
                else
                    Toast.makeText(this,"Something went wrong!", Toast.LENGTH_LONG).show()
                myPref.setPref(ConstantVariables.IS_VERIFIED, true)
            }else{
                tv_error.visibility = View.VISIBLE
            }
        }
    }
}
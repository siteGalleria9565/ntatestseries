package com.nta.ts2020.Activities

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ExamVideoService {
    @GET("video_list.php?")
    fun getVideosList(
            @Query("subjectname") request: String?,
            @Query("pageno") page: Int
    ): Call<ExamVideos?>?
}
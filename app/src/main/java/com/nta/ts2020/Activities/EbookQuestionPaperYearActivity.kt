package com.nta.ts2020.Activities

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.nta.ts2020.Activities.EbookQuestionPaperListActivity
import com.nta.ts2020.Adapters.EbookCourseListAdapter
import com.nta.ts2020.Adapters.EbookCourseListAdapter.OnClickPaper
import com.nta.ts2020.PojoClasses.QuestionPaperPDF
import com.nta.ts2020.R
import com.nta.ts2020.WebService.WebRequest
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class EbookQuestionPaperYearActivity : AppCompatActivity(), OnClickPaper {
    private var rv_course_name: RecyclerView? = null
    private var llEBookquestionpaperError: LinearLayout? = null
    private var webRequest: WebRequest? = null
    private var gson: Gson? = null
    var object_string: String? = null
    private var linearLayoutManager: LinearLayoutManager? = null
    var mQuestionPDFList: List<QuestionPaperPDF> = ArrayList()
    var mList: List<String> = ArrayList()
    var name: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ebook_question_paper_year)
        if (intent.extras != null) {
            object_string = intent.getStringExtra("PDF_LIST")
            mList = intent.getStringArrayListExtra("YEAR_LIST")
            name = intent.getStringExtra("NAME")
        }
        if (supportActionBar != null) {
            supportActionBar!!.title = name
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        init()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun init() {
        rv_course_name = findViewById(R.id.rv_course_name)
        llEBookquestionpaperError = findViewById(R.id.llEBookquestionpaperError)
        webRequest = WebRequest(this)
        gson = Gson()
        Collections.sort(mList)
        linearLayoutManager = LinearLayoutManager(this)
        val ebookCourseListAdapter = name?.let { EbookCourseListAdapter(this, this, null, mList, it) }
        val linearLayoutManager = LinearLayoutManager(this)
        rv_course_name?.layoutManager = linearLayoutManager
        rv_course_name?.adapter = ebookCourseListAdapter
    }

    override fun onSelectPaper(name: String?, position: Int) {}
    override fun onSelectYear(year: String?, name: String?) {
        val mQuestionPaperList: MutableList<QuestionPaperPDF?> = ArrayList()
        try {
            val jsonObject = JSONObject(object_string)
            val jsonArray = jsonObject.getJSONArray(year)
            for (i in 0 until jsonArray.length()) {
                val questionPaperPDF = gson!!.fromJson(jsonArray[i].toString(), QuestionPaperPDF::class.java)
                mQuestionPaperList.add(questionPaperPDF)
            }
            val i = Intent(this, EbookQuestionPaperListActivity::class.java)
            i.putParcelableArrayListExtra("PDF_LIST", mQuestionPaperList as ArrayList<out Parcelable?>)
            i.putExtra("NAME", name)
            startActivity(i)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

//        for (int i = 0; i < mQuestionPDFList.size(); i++) {
//            if (mQuestionPDFList.get(i).getYear().equals(year) && mQuestionPDFList.get(i).getCourse_name().equals(name)) {
//                mQuestionPaperList.add(mQuestionPDFList.get(i));
//            }
//        }
    }
}
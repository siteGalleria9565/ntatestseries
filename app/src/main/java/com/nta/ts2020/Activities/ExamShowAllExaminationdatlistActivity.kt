package com.nta.ts2020.Activities

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.nta.ts2020.Adapters.ExamSetAllExamListDataRecyclerAdapter
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.LocalStorage.ExamAssetToLocalDatabase
import com.nta.ts2020.LocalStorage.ExamCacheAllExaminationData
import com.nta.ts2020.LocalStorage.ExamCacheSetCurrentSessionData
import com.nta.ts2020.PojoClasses.ExamAllExaminationDataPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.ExamLoadAllExaminationDataFromServerHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import java.util.*

class ExamShowAllExaminationdatlistActivity : AppCompatActivity() {
    //List View Object
    private var recyclerView: RecyclerView? = null

    //for Set Content In recyclerView
    var customList: ExamSetAllExamListDataRecyclerAdapter? = null

    // Collections ....
    var localAllExaminationDataList: ArrayList<ExamAllExaminationDataPojo>? = null

    //Cache
    var examCacheAllExaminationDataInstance: ExamCacheAllExaminationData? = null

    //toolbar
    var setToolbar: Toolbar? = null

    //Log
    private val TAG = "AllExamDataActivity"

    //from dashboard
    var localThreadPoolServiceInstance: ThreadPoolService? = null
    var examLoadAllExaminationDataFromServerHandler: ExamLoadAllExaminationDataFromServerHandler? = null
    private var examCacheSetCurrentSessionDataInstance: ExamCacheSetCurrentSessionData? = null
    private var checkCountStatus = 0
    private var resumeExamId = 0
    private var resumeStatus = 0

    // Transaction Layer ....
    var mExamAssetToLocalDatabase: ExamAssetToLocalDatabase? = null
    var checkStatusResumeTest: String? = ""

    //from dashboard -- end
    private var examStatus: String? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var context: AppCompatActivity? = null
    private var linLayoutNoData: LinearLayout? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var imageViewError: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exam_all_examination_list_activity)
        if (supportActionBar != null) {
            supportActionBar!!.title = "Exam"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //context
        context = this

        //Error Message Layout
        linLayoutNoData = findViewById<View>(R.id.llExamListError) as LinearLayout
        textViewMessage = findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        imageViewError = findViewById<View>(R.id.ivExamResultListError) as ImageView

        //Exam module moved from dashboard
        initialization()
        mExamAssetToLocalDatabase = ExamAssetToLocalDatabase(this@ExamShowAllExaminationdatlistActivity)
        mExamAssetToLocalDatabase!!.writableDatabase
        if (examCacheSetCurrentSessionDataInstance!!.currentExamSession != null && examCacheSetCurrentSessionDataInstance!!.currentExamSession.size != 0) {
            checkStatusResumeTest = examCacheSetCurrentSessionDataInstance!!.currentExamSession[0].examStatus
            checkCountStatus = examCacheSetCurrentSessionDataInstance!!.currentExamSession[0].countResum!!.toInt()
            resumeExamId = examCacheSetCurrentSessionDataInstance!!.currentExamSession[0].submitExamID!!.toInt()
        }
        if (checkStatusResumeTest == "Resume" && checkCountStatus <= 3) {
            resumeStatus = resumeExamId
            val resumeButton = findViewById<View>(R.id.resume_test) as Button
            //Visible Resume Button
            resumeButton.visibility = View.VISIBLE
        } else {
            resumeStatus = 0
        }
        swipeRefreshLayout = findViewById<View>(R.id.srlExamList) as SwipeRefreshLayout
        swipeRefreshLayout!!.setColorSchemeResources(
                R.color.material_blue_700,
                R.color.material_green_700,
                R.color.material_yellow_700,
                R.color.material_red_700)
        if (isNetworkConnected) {
            swipeRefreshLayout!!.isRefreshing = true
            localThreadPoolServiceInstance!!.LoadAllExaminationDataFromServer(context, examLoadAllExaminationDataFromServerHandler)
        } else {
            loadData(SUCCESS)
        }
        swipeRefreshLayout!!.setOnRefreshListener {
            if (isNetworkConnected) {
                swipeRefreshLayout!!.isRefreshing = true
                localThreadPoolServiceInstance!!.LoadAllExaminationDataFromServer(context, examLoadAllExaminationDataFromServerHandler)
            } else {
                loadData(SUCCESS)
            }
        }
    }

    //Added after moving examModule from dashboard
    fun loadData(status: Int) {
        var status = status
        examCacheAllExaminationDataInstance = ExamCacheAllExaminationData(this)
        recyclerView = findViewById<View>(R.id.list_view) as RecyclerView

        //fetch data from cache
        localAllExaminationDataList = examCacheAllExaminationDataInstance!!.allExaminationData
        if (localAllExaminationDataList!!.isEmpty() && status != NETWORK_ERROR) status = NO_DATA
        if (status == SUCCESS) {
            linLayoutNoData!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE
            val examStatusId = resumeStatus
            if (examStatusId != 0) {
                resumeExamId = examStatusId
                examStatus = "Resume"
            } else {
                resumeExamId = 0
                examStatus = "Initial"
            }
            customList = ExamSetAllExamListDataRecyclerAdapter(this, localAllExaminationDataList!!, examStatus!!, resumeExamId)
            recyclerView!!.layoutManager = LinearLayoutManager(this)
            recyclerView!!.adapter = customList
        } else if (status == NO_DATA) {
            linLayoutNoData!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
            textViewMessage!!.setText(R.string.result_list_message_no_data)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_no_data)
            imageViewError!!.setImageResource(R.drawable.ic_no_data)
        } else if (status == NETWORK_ERROR) {
            linLayoutNoData!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
            textViewMessage!!.setText(R.string.result_list_message_network_error)
            textViewSubMessage!!.setText(R.string.result_list_sub_message_network_error)
            imageViewError!!.setImageResource(R.drawable.ic_no_network)
        }
        if (swipeRefreshLayout!!.isRefreshing) swipeRefreshLayout!!.isRefreshing = false
    }

    private val isNetworkConnected: Boolean
        private get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    fun resumeButtonClicked(view: View?) {
        val intent = Intent(this, ExamShowQuestionInstructionActivity::class.java)
        intent.putExtra("Status", examStatus.toString())
        intent.putExtra("ExamID", resumeExamId.toString())
        intent.putExtra("TYPE", "EXM")
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }

    //moved from dashboard
    fun initialization() {
        localThreadPoolServiceInstance = ThreadPoolService()
        examLoadAllExaminationDataFromServerHandler = ExamLoadAllExaminationDataFromServerHandler(this, this@ExamShowAllExaminationdatlistActivity)
        examCacheSetCurrentSessionDataInstance = ExamCacheSetCurrentSessionData(this)
    }
}
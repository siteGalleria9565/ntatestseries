package com.nta.ts2020.Activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.R

class ExamWebViewActivity : AppCompatActivity() {
    var mWebView: WebView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        val url = intent.getStringExtra("url")
        /*  mWebView = (WebView) findViewById(R.id.activity_main_webview);
        mWebView.loadUrl(url);*/finish()
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }
}
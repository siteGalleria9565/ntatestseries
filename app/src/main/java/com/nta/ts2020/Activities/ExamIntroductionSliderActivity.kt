package com.nta.ts2020.Activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.Adapters.ViewPagerIntroAdapter
import com.nta.ts2020.AppConstants.ConstantVariables
import com.nta.ts2020.MyApplication
import com.nta.ts2020.R
import com.nta.ts2020.UtilsHelper.FontsOverride
import com.nta.ts2020.UtilsHelper.MyPref
import com.nta.ts2020.UtilsHelper.SharedPrefManager

class ExamIntroductionSliderActivity : AppCompatActivity(), View.OnClickListener {
    private var context: Context? = null
    private lateinit var layouts: IntArray
    private var dotsLayout: LinearLayout? = null
    private var btnSkip: Button? = null
    private var btnNext: Button? = null
    private var viewPagerIntro: ViewPager? = null
    private var sharedPrefManager: SharedPrefManager? = null
    private lateinit var myPref : MyPref


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Checking for first time launch - before calling setContentView()
        sharedPrefManager = SharedPrefManager(this)
        if (!sharedPrefManager!!.isFirstTimeLaunch) {
            launchHomeScreen()
        }
        myPref = MyPref(this)
        FontsOverride.setDefaultFont(applicationContext, "DEFAULT", "fonts/gilroy_light.otf")
        FontsOverride.setDefaultFont(applicationContext, "MONOSPACE", "fonts/gilroy_bold.otf")
        FontsOverride.setDefaultFont(applicationContext, "SERIF", "fonts/gilroy_light.otf")
        FontsOverride.setDefaultFont(applicationContext, "SANS", "fonts/gilroy_light.otf")

        //Make notification bar transparent
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
        }
        setContentView(R.layout.activity_intro)

        //Context
        context = this

        //Page layouts
        layouts = intArrayOf(R.layout.intro_lauout_1,
                R.layout.intro_lauout_2,
                R.layout.intro_layout_4,
                R.layout.intro_layout_5
        )
        viewPagerIntro = findViewById<View>(R.id.viewPagerIntro) as ViewPager
        dotsLayout = findViewById<View>(R.id.dotsHolder) as LinearLayout
        btnSkip = findViewById<View>(R.id.buttonIntroSkip) as Button
        btnNext = findViewById<View>(R.id.buttonIntroNext) as Button
        btnSkip!!.setOnClickListener(this)
        btnNext!!.setOnClickListener(this)
        addBottomDots(0)
        val viewPagerIntroAdapter = ViewPagerIntroAdapter(context as ExamIntroductionSliderActivity, layouts)
        viewPagerIntro!!.adapter = viewPagerIntroAdapter
        viewPagerIntro!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                addBottomDots(position)
                // changing the next button text 'NEXT' / 'GOT IT'
                if (position == layouts.size - 1) {
                    // last page. make button text to GOT IT
                    btnNext!!.text = "START"
                    btnSkip!!.visibility = View.GONE
                } else {
                    // still pages are left
                    btnNext!!.text = "NEXT"
                    btnSkip!!.visibility = View.VISIBLE
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    private fun addBottomDots(currentPage: Int) {
        val dots = arrayOfNulls<TextView>(layouts.size)
        dotsLayout!!.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(this)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(ContextCompat.getColor(context!!, R.color.dot_in_active))
            dotsLayout!!.addView(dots[i])
        }
        if (dots.isNotEmpty()) dots[currentPage]!!.setTextColor(ContextCompat.getColor(context!!, R.color.dot_active))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.buttonIntroSkip -> launchHomeScreen()
            R.id.buttonIntroNext -> {
                val nextPage = viewPagerIntro!!.currentItem + 1
                if (nextPage < layouts.size) {
                    viewPagerIntro!!.currentItem = nextPage
                } else {
                    launchHomeScreen()
                }
            }
        }
    }

    private fun launchHomeScreen() {
        sharedPrefManager!!.isFirstTimeLaunch = false
            startActivity(Intent(this@ExamIntroductionSliderActivity, ExamLoginScreenActivity::class.java))
            finish()
    }
}
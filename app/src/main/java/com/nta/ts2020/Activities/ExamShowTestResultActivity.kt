package com.nta.ts2020.Activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.db.chart.animation.Animation
import com.db.chart.model.Bar
import com.db.chart.model.BarSet
import com.db.chart.renderer.AxisRenderer
import com.db.chart.renderer.XRenderer
import com.db.chart.renderer.YRenderer
import com.db.chart.view.BarChartView
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.Adapters.ShowTableSubjectWiseDataAdapter
import com.nta.ts2020.LocalStorage.ExamCacheAllQuestionRelatedStuff
import com.nta.ts2020.LocalStorage.ExamCacheFinalSubmission
import com.nta.ts2020.LocalStorage.QuizCacheAllQuestionRelatedStuff
import com.nta.ts2020.PojoClasses.ExamGetSubjectArrayStuffPojo
import com.nta.ts2020.PojoClasses.FinalSubmissionPojo
import com.nta.ts2020.PojoClasses.ShowSubjectWiseDataInTablePojo
import com.nta.ts2020.R
import java.util.*

class ExamShowTestResultActivity : AppCompatActivity() {
    // Collections ....
    var finalSubmissionPojosList: ArrayList<FinalSubmissionPojo>? = null
    private var productList: ArrayList<ShowSubjectWiseDataInTablePojo>? = null
    var questionDataBasedOnExamIdPojoList: ArrayList<ExamGetSubjectArrayStuffPojo>? = null

    //Cache
    var cacheAllQuestionRelatedStuffInstance: ExamCacheAllQuestionRelatedStuff? = null
    var examCacheFinalSubmissionInstance: ExamCacheFinalSubmission? = null
    var quizCacheAllQuestionRelatedStuff: QuizCacheAllQuestionRelatedStuff? = null

    //HashMap
    var subjectBasedOnId: HashMap<String?, String?>? = null
    var subjectWiseMarks = 0f
    var Percentage = 0f

    //TextView
    private var examName: TextView? = null
    private var totalMarksObtain: TextView? = null
    private var totalQuestion: TextView? = null
    private var totalExamTime: TextView? = null
    private var studentRank: TextView? = null
    private val TAG = "SubWiseScreActy"
    private val subjectWiseMaxMarks = 0
    private var examType: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exam_activity_test_result)
        if (supportActionBar != null) {
            supportActionBar!!.title = "ExamResult"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        initialization()
        var id: String? = null
        val extras = intent.extras
        if (extras != null) {
            examType = extras.getString("TYPE")
            id = if ("QUIZ" == examType) {
                "Q" + extras.getString("testID")
            } else {
                extras.getString("testID")
            }
        }

        //Final Data Cache
        finalSubmissionPojosList = examCacheFinalSubmissionInstance!!.getFinalSubmissionData(id)
        when (examType) {
            "EXM" -> {
                questionDataBasedOnExamIdPojoList = cacheAllQuestionRelatedStuffInstance!!.getCacheAllQuestionSubjectRelatedStuff(null)
                examName!!.text = ExamCacheAllQuestionRelatedStuff(this).getCacheAllQuestionRelatedStuff(id)[0].examName
            }
            "QUIZ" -> {
                questionDataBasedOnExamIdPojoList = quizCacheAllQuestionRelatedStuff!!.cacheAllQuestionSubjectRelatedStuff
                examName!!.text = QuizCacheAllQuestionRelatedStuff(this).cacheAllQuestionRelatedStuff[0].examName
            }
        }
        val timeCalculate = finalSubmissionPojosList!![0].overAllTime.toInt()
        for (i in questionDataBasedOnExamIdPojoList!!.indices) {
            subjectBasedOnId!![questionDataBasedOnExamIdPojoList!![i].subjectId] = questionDataBasedOnExamIdPojoList!![i].subjectNames
        }

        //  ArrayList<String> subWiseMarksList = finalSubmissionPojosList.get(0).getSubWiseMarks();

        /*
         *  totalRightAns+" "+totalAttemptQuestion+" "+totalSkipedQuestion+" "+totalRightMarks+" "+totalWrongMarks;
        */
        val hour = timeCalculate / 3600
        val minutes = timeCalculate / 60 % 60
        val second = timeCalculate % 60
        val timeStamp = String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, minutes, second)


        /*
        * Show Table subject wise data
        * */productList = ArrayList()
        val lview = findViewById<View>(R.id.listview) as ListView
        val showTableSubjectWiseDataAdapterInstance = ShowTableSubjectWiseDataAdapter(this, productList!!)
        for (i in 1 until finalSubmissionPojosList!![0].subjectIds!!.size + 1) {
            //Set table Value
            subjectWiseMarks = java.lang.Float.valueOf(finalSubmissionPojosList!![0].subWiseMarks!![i - 1])
            val subjectWiseMaxMarks = finalSubmissionPojosList!![0].subWiseMarks!![i - 1].toInt()
            Percentage = subjectWiseMarks / subjectWiseMaxMarks * 100
            subjectBasedOnId!![finalSubmissionPojosList!![0].subjectIds!![i - 1]]?.let { ShowSubjectWiseDataInTablePojo(it, subjectWiseMarks.toString(), "$Percentage%") }?.let { productList!!.add(it) }
        }
        lview.adapter = showTableSubjectWiseDataAdapterInstance
        showTableSubjectWiseDataAdapterInstance.notifyDataSetChanged()
        totalMarksObtain!!.text = java.lang.String.valueOf(finalSubmissionPojosList!![0].totalObtainedMarks)
        totalQuestion!!.text = java.lang.String.valueOf(finalSubmissionPojosList!![0].totalQues)
        totalExamTime!!.text = timeStamp
        studentRank!!.text = "Updating"
        //
//        chart = (BarChart) findViewById(R.id.chart1);
//
//        AddValuesToBARENTRY();
//
//        Bardataset = new BarDataSet(BARENTRY, "Subjects");
//
////        BARDATA = new BarData(BarEntryLabels, Bardataset);
//
//        YAxis yAxis = chart.getAxisLeft();
//        yAxis.setAxisMaxValue(150f);
//        yAxis.setAxisMinValue(0f);
//        Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
//
//        chart.setData(BARDATA);


        //Show chart
        val mChart = findViewById<View>(R.id.chart1) as BarChartView
        mChart.setBarSpacing(20f)
        val barSet = BarSet()
        val colors = resources.getIntArray(R.array.color_array)
        for (i in finalSubmissionPojosList!![0].subjectIds!!.indices) {
            //Set Questions According to section
            subjectWiseMarks = java.lang.Float.valueOf(finalSubmissionPojosList!![0].subWiseMarks!![i])
            val subjectWiseMaxMarks = finalSubmissionPojosList!![0].subWiseMaxMarks!![i].toInt()
            Percentage = subjectWiseMarks / subjectWiseMaxMarks * 100

            //set Chart Value
            val tempSubjectName = subjectBasedOnId!![finalSubmissionPojosList!![0].subjectIds!![i]]!!.split(" ".toRegex()).toTypedArray()
            val subjectName = tempSubjectName[0]
            val bar = Bar(subjectName, Percentage)
            bar.color = colors[i]
            barSet.addBar(bar)
        }
        mChart.addData(barSet)
        mChart.setXLabels(AxisRenderer.LabelPosition.OUTSIDE)
                .setYLabels(AxisRenderer.LabelPosition.OUTSIDE)
                .show(Animation())
    }

    //    public void AddValuesToBARENTRY() {
    //
    //
    //        for (int i = 1; i < finalSubmissionPojosList.get(0).getSubjectIds().size() + 1; i++) {
    //
    //            //Set Questions According to section
    //            subjectWiseMarks = Float.valueOf(finalSubmissionPojosList.get(0).getSubWiseMarks().get(i - 1));
    //            subjectWiseMaxMarks = Integer.parseInt(finalSubmissionPojosList.get(0).getSubWiseMaxMarks().get(i - 1));
    //
    //
    //            Percentage = (subjectWiseMarks / subjectWiseMaxMarks) * 100;
    //
    //            //Set table Value
    //            productList.add(new ShowSubjectWiseDataInTablePojo(subjectBasedOnId.get(finalSubmissionPojosList.get(0).getSubjectIds().get(i - 1)), String.valueOf(subjectWiseMarks), Percentage + "%"));
    //
    //            //set Chart Value
    //            BarEntryLabels.add(subjectBasedOnId.get(finalSubmissionPojosList.get(0).getSubjectIds().get(i - 1)));
    //            BARENTRY.add(new BarEntry(Percentage, i - 1));
    //        }
    //
    //    }
    fun initialization() {
        examCacheFinalSubmissionInstance = ExamCacheFinalSubmission(this)
        cacheAllQuestionRelatedStuffInstance = ExamCacheAllQuestionRelatedStuff(this)
        quizCacheAllQuestionRelatedStuff = QuizCacheAllQuestionRelatedStuff(this)
        examName = findViewById<View>(R.id.ExamName) as TextView

        //Text view initialize
        totalMarksObtain = findViewById<View>(R.id.totalMarksObtain) as TextView
        totalQuestion = findViewById<View>(R.id.totalExamQuestion) as TextView
        totalExamTime = findViewById<View>(R.id.totalExamTime) as TextView
        studentRank = findViewById<View>(R.id.studentRank) as TextView

        //HashMa
        subjectBasedOnId = HashMap()
    }

    override fun onSupportNavigateUp(): Boolean {
        startActivity(Intent(this@ExamShowTestResultActivity, ExamMainDashboardActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        startActivity(Intent(this@ExamShowTestResultActivity, ExamMainDashboardActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
    }
}
package com.nta.ts2020.Activities

import android.animation.ObjectAnimator
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter
import com.github.aakira.expandablelayout.ExpandableLinearLayout
import com.github.aakira.expandablelayout.Utils
import com.nta.ts2020.AppConstants.ConstantVariables.ACADEMIC_DETAILS_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.OTHER_ACTIVITY
import com.nta.ts2020.AppConstants.ConstantVariables.STUDENT_PERSONAL_DETAILS
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.LoadProfileHandler
import com.nta.ts2020.ThreadingHelper.Handlers.UpdateProfileHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class ExamProfAcademicDetailsActivity : AppCompatActivity(), View.OnClickListener {
    private var ellClassXL1: ExpandableLinearLayout? = null
    private var ellClassXL2: ExpandableLinearLayout? = null
    private var ellClassXL3: ExpandableLinearLayout? = null
    private var classXButtonHolderL1: RelativeLayout? = null
    private var classXButtonHolderL2: RelativeLayout? = null
    private var classXButtonHolderL3: RelativeLayout? = null
    private var etAcademicYearOfPassingL1: EditText? = null
    private var etAcademicStreamL1: EditText? = null
    private var etAcademicBoardL1: EditText? = null
    private var etAcademicMarksL1: EditText? = null
    private var etAcademicYearOfPassingL2: EditText? = null
    private var etAcademicStreamL2: EditText? = null
    private var etAcademicBoardL2: EditText? = null
    private var etAcademicMarksL2: EditText? = null
    private var etAcademicYearOfPassingL3: EditText? = null
    private var etAcademicStreamL3: EditText? = null
    private var etAcademicBoardL3: EditText? = null
    private var etAcademicMarksL3: EditText? = null
    private var context: Context? = null
    private var progressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_academic_details)

        //Set Title
        if (supportActionBar != null) {
            supportActionBar!!.title = "Academic Details"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Context
        context = this

        //Expandable Layout 1
        ellClassXL1 = findViewById<View>(R.id.elAcademicClassXL1) as ExpandableLinearLayout
        val classXHeadingL1 = findViewById<View>(R.id.rlAcademicHeadingL1) as RelativeLayout
        classXButtonHolderL1 = findViewById<View>(R.id.rlAcademicButtonHolderL1) as RelativeLayout
        classXHeadingL1.setOnClickListener(this)
        ellClassXL1!!.setListener(object : ExpandableLayoutListenerAdapter() {
            override fun onPreOpen() {
                super.onPreOpen()
                createRotateAnimator(classXButtonHolderL1, 0f, 180f).start()
            }

            override fun onPreClose() {
                super.onPreClose()
                createRotateAnimator(classXButtonHolderL1, 180f, 0f).start()
            }
        })

        //Expandable Layout 1
        ellClassXL2 = findViewById<View>(R.id.elAcademicClassXL2) as ExpandableLinearLayout
        val classXHeadingL2 = findViewById<View>(R.id.rlAcademicHeadingL2) as RelativeLayout
        classXButtonHolderL2 = findViewById<View>(R.id.rlAcademicButtonHolderL2) as RelativeLayout
        classXHeadingL2.setOnClickListener(this)
        ellClassXL2!!.setListener(object : ExpandableLayoutListenerAdapter() {
            override fun onPreOpen() {
                super.onPreOpen()
                createRotateAnimator(classXButtonHolderL2, 0f, 180f).start()
            }

            override fun onPreClose() {
                super.onPreClose()
                createRotateAnimator(classXButtonHolderL2, 180f, 0f).start()
            }
        })

        //Expandable Layout 1
        ellClassXL3 = findViewById<View>(R.id.elAcademicClassXL3) as ExpandableLinearLayout
        val classXHeadingL3 = findViewById<View>(R.id.rlAcademicHeadingL3) as RelativeLayout
        classXButtonHolderL3 = findViewById<View>(R.id.rlAcademicButtonHolderL3) as RelativeLayout
        classXHeadingL3.setOnClickListener(this)
        ellClassXL3!!.setListener(object : ExpandableLayoutListenerAdapter() {
            override fun onPreOpen() {
                super.onPreOpen()
                createRotateAnimator(classXButtonHolderL3, 0f, 180f).start()
            }

            override fun onPreClose() {
                super.onPreClose()
                createRotateAnimator(classXButtonHolderL3, 180f, 0f).start()
            }
        })

        //Initialize layout variables
        etAcademicYearOfPassingL1 = findViewById<View>(R.id.etAcademicYearOfPassingL1) as EditText
        etAcademicStreamL1 = findViewById<View>(R.id.etAcademicStreamL1) as EditText
        etAcademicBoardL1 = findViewById<View>(R.id.etAcademicBoardL1) as EditText
        etAcademicMarksL1 = findViewById<View>(R.id.etAcademicMarksL1) as EditText
        etAcademicYearOfPassingL2 = findViewById<View>(R.id.etAcademicYearOfPassingL2) as EditText
        etAcademicStreamL2 = findViewById<View>(R.id.etAcademicStreamL2) as EditText
        etAcademicBoardL2 = findViewById<View>(R.id.etAcademicBoardL2) as EditText
        etAcademicMarksL2 = findViewById<View>(R.id.etAcademicMarksL2) as EditText
        etAcademicYearOfPassingL3 = findViewById<View>(R.id.etAcademicYearOfPassingL3) as EditText
        etAcademicStreamL3 = findViewById<View>(R.id.etAcademicStreamL3) as EditText
        etAcademicBoardL3 = findViewById<View>(R.id.etAcademicBoardL3) as EditText
        etAcademicMarksL3 = findViewById<View>(R.id.etAcademicMarksL3) as EditText
        val btSave = findViewById<View>(R.id.btAcademicSave) as Button
        btSave.setOnClickListener(this)

        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true

        //Load profile data
        val json = JSONObject()
        var parameterJson = ""
        try {
            val jsonArray = JSONArray()
            jsonArray.put("T_PASS_YEAR")
            jsonArray.put("SUBJECT_OF_T")
            jsonArray.put("T_BOARD")
            jsonArray.put("T_PERCENTILE")
            jsonArray.put("TW_PASS_YEAR")
            jsonArray.put("TW_STREAM")
            jsonArray.put("TW_BOARD")
            jsonArray.put("TW_PERCENTILE")
            jsonArray.put("UG_PASS_YEAR")
            jsonArray.put("UG_STREAM")
            jsonArray.put("UG_BOARD")
            jsonArray.put("UG_PERCENTILE")
            json.put("TABLE", STUDENT_PERSONAL_DETAILS)
            json.put("STUDENT_ID", CacheUserData(context as ExamProfAcademicDetailsActivity).userDataFromCache.userId)
            json.put("data", jsonArray)
            parameterJson = json.toString()
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
        }
        progressDialog!!.show()
        ThreadPoolService().loadProfileFromServer(context, parameterJson, LoadProfileHandler(this@ExamProfAcademicDetailsActivity, ACADEMIC_DETAILS_ACTIVITY, progressDialog!!))
    }

    fun createRotateAnimator(target: View?, from: Float, to: Float): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(target, "rotation", from, to)
        animator.duration = 300
        animator.interpolator = Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR)
        return animator
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlAcademicHeadingL1 -> ellClassXL1!!.toggle()
            R.id.rlAcademicHeadingL2 -> ellClassXL2!!.toggle()
            R.id.rlAcademicHeadingL3 -> ellClassXL3!!.toggle()
            R.id.btAcademicSave -> if (isValid) {
                //Save to server
                val parameterJson = buildParameter()
                //                    Log.d(TAG, "Address Parameters: " + parameterJson);
                progressDialog!!.show()
                val updateProfileHandler = UpdateProfileHandler(this@ExamProfAcademicDetailsActivity, progressDialog!!)
                ThreadPoolService().saveProfileToServer(applicationContext, parameterJson, OTHER_ACTIVITY, updateProfileHandler)
            }
        }
    }

    private fun buildParameter(): String {
        var parameterJson = ""
        val json = JSONObject()
        try {
            json.put("TABLE", STUDENT_PERSONAL_DETAILS)
            json.put("STUDENT_ID", CacheUserData(context!!).userDataFromCache.userId)

            //L1
            if (etAcademicYearOfPassingL1!!.text.toString() != "") {
                json.put("T_PASS_YEAR", etAcademicYearOfPassingL1!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicStreamL1!!.text.toString() != "") {
                json.put("SUBJECT_OF_T", etAcademicStreamL1!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicBoardL1!!.text.toString() != "") {
                json.put("T_BOARD", etAcademicBoardL1!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicMarksL1!!.text.toString() != "") {
                json.put("T_PERCENTILE", etAcademicMarksL1!!.text.toString().trim { it <= ' ' })
            }

            //L2
            if (etAcademicYearOfPassingL2!!.text.toString() != "") {
                json.put("TW_PASS_YEAR", etAcademicYearOfPassingL2!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicStreamL2!!.text.toString() != "") {
                json.put("TW_STREAM", etAcademicStreamL2!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicBoardL2!!.text.toString() != "") {
                json.put("TW_BOARD", etAcademicBoardL2!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicMarksL2!!.text.toString() != "") {
                json.put("TW_PERCENTILE", etAcademicMarksL2!!.text.toString().trim { it <= ' ' })
            }

            //L3
            if (etAcademicYearOfPassingL3!!.text.toString() != "") {
                json.put("UG_PASS_YEAR", etAcademicYearOfPassingL3!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicStreamL3!!.text.toString() != "") {
                json.put("UG_STREAM", etAcademicStreamL3!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicBoardL3!!.text.toString() != "") {
                json.put("UG_BOARD", etAcademicBoardL3!!.text.toString().trim { it <= ' ' })
            }
            if (etAcademicMarksL3!!.text.toString() != "") {
                json.put("UG_PERCENTILE", etAcademicMarksL3!!.text.toString().trim { it <= ' ' })
            }
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
        }
        parameterJson = json.toString()
        return parameterJson
    }

    private val isValid: Boolean
        get() {
            var status = true
            var countFlag = -1
            if (etAcademicYearOfPassingL1!!.text.toString() != "" ||
                    etAcademicStreamL1!!.text.toString() != "" ||
                    etAcademicBoardL1!!.text.toString() != "" ||
                    etAcademicMarksL1!!.text.toString() != "") {
                if (etAcademicYearOfPassingL1!!.text.toString() == "") {
                    etAcademicYearOfPassingL1!!.error = "Enter year of passing"
                    status = false
                }
                if (etAcademicStreamL1!!.text.toString() == "") {
                    etAcademicStreamL1!!.error = "Enter Stream"
                    status = false
                }
                if (etAcademicBoardL1!!.text.toString() == "") {
                    etAcademicBoardL1!!.error = "Enter Board"
                    status = false
                }
                if (etAcademicMarksL1!!.text.toString() == "") {
                    etAcademicMarksL1!!.error = "Enter Marks"
                    status = false
                }
                countFlag++
            } else {
                etAcademicYearOfPassingL1!!.error = null
                etAcademicStreamL1!!.error = null
                etAcademicBoardL1!!.error = null
                etAcademicMarksL1!!.error = null
            }
            if (etAcademicYearOfPassingL2!!.text.toString() != "" ||
                    etAcademicStreamL2!!.text.toString() != "" ||
                    etAcademicBoardL2!!.text.toString() != "" ||
                    etAcademicMarksL2!!.text.toString() != "") {
                if (etAcademicYearOfPassingL2!!.text.toString() == "") {
                    etAcademicYearOfPassingL2!!.error = "Enter year of passing"
                    status = false
                }
                if (etAcademicStreamL2!!.text.toString() == "") {
                    etAcademicStreamL2!!.error = "Enter Stream"
                    status = false
                }
                if (etAcademicBoardL2!!.text.toString() == "") {
                    etAcademicBoardL2!!.error = "Enter Board"
                    status = false
                }
                if (etAcademicMarksL2!!.text.toString() == "") {
                    etAcademicMarksL2!!.error = "Enter Marks"
                    status = false
                }
                countFlag++
            } else {
                etAcademicYearOfPassingL2!!.error = null
                etAcademicStreamL2!!.error = null
                etAcademicBoardL2!!.error = null
                etAcademicMarksL2!!.error = null
            }
            if (etAcademicYearOfPassingL3!!.text.toString() != "" ||
                    etAcademicStreamL3!!.text.toString() != "" ||
                    etAcademicBoardL3!!.text.toString() != "" ||
                    etAcademicMarksL3!!.text.toString() != "") {
                if (etAcademicYearOfPassingL3!!.text.toString() == "") {
                    etAcademicYearOfPassingL3!!.error = "Enter year of passing"
                    status = false
                }
                if (etAcademicStreamL3!!.text.toString() == "") {
                    etAcademicStreamL3!!.error = "Enter Stream"
                    status = false
                }
                if (etAcademicBoardL3!!.text.toString() == "") {
                    etAcademicBoardL3!!.error = "Enter Board"
                    status = false
                }
                if (etAcademicMarksL3!!.text.toString() == "") {
                    etAcademicMarksL3!!.error = "Enter Marks"
                    status = false
                }
                countFlag++
            } else {
                etAcademicYearOfPassingL3!!.error = null
                etAcademicStreamL3!!.error = null
                etAcademicBoardL3!!.error = null
                etAcademicMarksL3!!.error = null
            }
            if (countFlag < 0) {
                status = false
            }
            return status
        }

    fun loadData(parameterJson: String?) {
        try {
            val json = JSONObject(parameterJson)
            //L1
            etAcademicYearOfPassingL1!!.setText(json.getString("T_PASS_YEAR"))
            etAcademicStreamL1!!.setText(json.getString("SUBJECT_OF_T"))
            etAcademicBoardL1!!.setText(json.getString("T_BOARD"))
            etAcademicMarksL1!!.setText(json.getString("T_PERCENTILE"))

            //L2
            etAcademicYearOfPassingL2!!.setText(json.getString("TW_PASS_YEAR"))
            etAcademicStreamL2!!.setText(json.getString("TW_STREAM"))
            etAcademicBoardL2!!.setText(json.getString("TW_BOARD"))
            etAcademicMarksL2!!.setText(json.getString("TW_PERCENTILE"))

            //L3
            etAcademicYearOfPassingL3!!.setText(json.getString("UG_PASS_YEAR"))
            etAcademicStreamL3!!.setText(json.getString("UG_STREAM"))
            etAcademicBoardL3!!.setText(json.getString("UG_BOARD"))
            etAcademicMarksL3!!.setText(json.getString("UG_PERCENTILE"))
        } catch (e: JSONException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            Toast.makeText(context, "OOPS! Something went wrong", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "AcademicDetailsActivity"
    }
}
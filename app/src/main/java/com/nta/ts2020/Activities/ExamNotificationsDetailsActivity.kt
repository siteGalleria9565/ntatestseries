package com.nta.ts2020.Activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.nta.ts2020.Activities.ExamLoginScreenActivity
import com.nta.ts2020.Activities.ExamMainDashboardActivity
import com.nta.ts2020.Activities.ExamNotificationsDetailsActivity
import com.nta.ts2020.Adapters.NotificationRelatedRecyclerAdapter
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.PojoClasses.ParcelPojoClass.NotificationDetailsPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.NotificationDetailsHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import com.squareup.picasso.Picasso

class ExamNotificationsDetailsActivity : AppCompatActivity() {
    private var context: ExamNotificationsDetailsActivity? = null
    private var recyclerView: RecyclerView? = null
    private var image: ImageView? = null
    private var title: TextView? = null
    private var description: WebView? = null
    private var holder: LinearLayout? = null
    private var errorLayout: LinearLayout? = null
    private var progressDialog: ProgressDialog? = null
    private var bt1: Button? = null
    private var bt2: Button? = null
    private var bt3: Button? = null
    private var heading: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications_details)

        //Context
        context = this

        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true
        progressDialog!!.show()
        var id: String? = ""
        if (intent.extras != null) {
            id = intent.extras!!.getString("notification_id")
            heading = intent.extras!!.getString("notification_heading")

            //Set title
            if (supportActionBar != null) {
                supportActionBar!!.title = heading
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            }
            if (!CacheUserData(context!!).userDataFromCache.isUserLoggedIn) {
                val intent = Intent(this, ExamLoginScreenActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.putExtra("notification_id", id)
                startActivity(intent)
                finish()
            }
            ThreadPoolService().loadNotificationDetailsFromServer(context, id, NotificationDetailsHandler(this))
        }

        //Initialize variables
        title = findViewById<View>(R.id.tvNotifyTitle) as TextView
        description = findViewById<View>(R.id.tvNotifyDescription) as WebView
        image = findViewById<View>(R.id.ivNotifyImage) as ImageView
        recyclerView = findViewById<View>(R.id.rvNotifyRelatedPosts) as RecyclerView
        holder = findViewById<View>(R.id.notifyDetailHolder) as LinearLayout
        errorLayout = findViewById<View>(R.id.notifyDetailError) as LinearLayout
        val errorSubMessage = errorLayout!!.findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        errorSubMessage.setText(R.string.sub_message_internet_error)
        bt1 = findViewById<View>(R.id.notifyDetailsBt1) as Button
        bt2 = findViewById<View>(R.id.notifyDetailsBt2) as Button
        bt3 = findViewById<View>(R.id.notifyDetailsBt3) as Button
    }

    fun loadData(status: Int, notificationDetailsPojo: NotificationDetailsPojo?) {
        if (status == SUCCESS && notificationDetailsPojo != null) {
            errorLayout!!.visibility = View.GONE
            holder!!.visibility = View.VISIBLE
            title!!.text = notificationDetailsPojo.title
            description!!.loadData(Html.fromHtml(notificationDetailsPojo.description).toString(), "text/html", "UTF-8")
            Picasso.with(context).load(notificationDetailsPojo.image).into(image)
            bt1!!.visibility = View.GONE
            bt2!!.visibility = View.GONE
            bt3!!.visibility = View.GONE
            if (notificationDetailsPojo.file1 != "") {
                bt1!!.visibility = View.VISIBLE
                bt1!!.text = notificationDetailsPojo.file1Name
                bt1!!.setOnClickListener {
                    val intent = Intent(this@ExamNotificationsDetailsActivity, ExamNotificationWebViewActivity::class.java)
                    intent.putExtra("url", notificationDetailsPojo.file1)
                    intent.putExtra("name", heading)
                    startActivity(intent)
                }
            }
            if (notificationDetailsPojo.file2 != "") {
                bt2!!.visibility = View.VISIBLE
                bt2!!.text = notificationDetailsPojo.file2Name
                bt2!!.setOnClickListener {
                    val intent = Intent(this@ExamNotificationsDetailsActivity, ExamNotificationWebViewActivity::class.java)
                    intent.putExtra("url", notificationDetailsPojo.file2)
                    intent.putExtra("name", heading)
                    startActivity(intent)
                }
            }
            if (notificationDetailsPojo.file3 != "") {
                bt3!!.visibility = View.VISIBLE
                bt3!!.text = notificationDetailsPojo.file3Name
                bt3!!.setOnClickListener {
                    val intent = Intent(this@ExamNotificationsDetailsActivity, ExamNotificationWebViewActivity::class.java)
                    intent.putExtra("url", notificationDetailsPojo.file3)
                    intent.putExtra("name", heading)
                    startActivity(intent)
                }
            }
            val notificationRelatedAdapter = context?.let { NotificationRelatedRecyclerAdapter(it, notificationDetailsPojo.notificationDetailsPojos) }
            recyclerView!!.adapter = notificationRelatedAdapter
        } else {
            errorLayout!!.visibility = View.VISIBLE
            holder!!.visibility = View.GONE
        }
        progressDialog!!.dismiss()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        startActivity(Intent(this@ExamNotificationsDetailsActivity, ExamMainDashboardActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    companion object {
        private const val TAG = "NotificaitonDtlAtivity"
    }
}
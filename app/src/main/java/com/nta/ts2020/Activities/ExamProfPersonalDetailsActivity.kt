package com.nta.ts2020.Activities

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.nta.ts2020.AppConstants.ConstantVariables.EX_STUDENT_USER
import com.nta.ts2020.AppConstants.ConstantVariables.PERSONAL_DETAILS_ACTIVITY
import com.nta.ts2020.LocalStorage.CacheUserData
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.LoadProfileHandler
import com.nta.ts2020.ThreadingHelper.Handlers.UpdateProfileHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class ExamProfPersonalDetailsActivity : AppCompatActivity(), View.OnClickListener {
    private var context: Context? = null
    private var tvDob: TextView? = null
    private var dialog: Dialog? = null
    private var datePicker: DatePicker? = null
    private var tvFName: TextView? = null
    private var tvLName: TextView? = null
    private var tvMobile: TextView? = null
    private var rgGender: RadioGroup? = null
    private var rbMale: RadioButton? = null
    private var rbFemale: RadioButton? = null
    private var progressDialog: ProgressDialog? = null
    private var contact: String? = null
    var phoneNoCheck: ExamRegisterActivity? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_personal_details)

        //Set Title
        if (supportActionBar != null) {
            supportActionBar!!.title = "Personal Details"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Context
        context = this

        //Initialization
        tvFName = findViewById<View>(R.id.tvPersonFName) as TextView
        tvLName = findViewById<View>(R.id.tvPersonLName) as TextView
        tvMobile = findViewById<View>(R.id.tvPersonMobile) as TextView
        rgGender = findViewById<View>(R.id.rgPersonGender) as RadioGroup
        rbMale = findViewById<View>(R.id.rbPersonMale) as RadioButton
        rbMale!!.setOnClickListener(this)
        rbFemale = findViewById<View>(R.id.rbPersonFemale) as RadioButton
        rbFemale!!.setOnClickListener(this)
        tvDob = findViewById<View>(R.id.tvPersonDob) as TextView
        tvDob!!.setOnClickListener(this)
        val btSave = findViewById<View>(R.id.btPersonSave) as Button
        btSave.setOnClickListener(this)

        //progress dialog
        progressDialog = ProgressDialog(context)
        progressDialog!!.setTitle("Loading ....")
        progressDialog!!.setMessage("Please wait")
        progressDialog!!.setCancelable(false)
        progressDialog!!.isIndeterminate = true

        //Load profile data
        val json = JSONObject()
        var parameterJson = ""
        try {
            val jsonArray = JSONArray()
            jsonArray.put("FIRST_NAME")
            jsonArray.put("LAST_NAME")
            jsonArray.put("MOBILE_NO")
            jsonArray.put("GENDER")
            jsonArray.put("DOB")
            json.put("TABLE", EX_STUDENT_USER)
            json.put("ID", CacheUserData(context as ExamProfPersonalDetailsActivity).userDataFromCache.userId)
            json.put("data", jsonArray)
            parameterJson = json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        progressDialog!!.show()
        ThreadPoolService().loadProfileFromServer(context, parameterJson, LoadProfileHandler(this@ExamProfPersonalDetailsActivity, PERSONAL_DETAILS_ACTIVITY, progressDialog!!))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btPersonSave -> if (isValid) {
                val parameterJson = buildParameter()
                //                    Log.d(TAG, "Personal Prof Parameters: " + parameterJson);
                progressDialog!!.show()
                val updateProfileHandler = UpdateProfileHandler(this@ExamProfPersonalDetailsActivity, progressDialog!!)
                ThreadPoolService().saveProfileToServer(applicationContext, parameterJson, PERSONAL_DETAILS_ACTIVITY, updateProfileHandler)
            }
            R.id.tvPersonDob -> {
                dialog = Dialog(context!!)
                dialog!!.setContentView(R.layout.dialog_pick_date)
                datePicker = dialog!!.findViewById<View>(R.id.dpPersonDob) as DatePicker
                val buttonPickDate = dialog!!.findViewById<View>(R.id.buttonPickDate) as Button
                datePicker!!.maxDate = System.currentTimeMillis()
                dialog!!.show()
                buttonPickDate.setOnClickListener {
                    val dayOfMonth = datePicker!!.dayOfMonth
                    val month = datePicker!!.month
                    val year = datePicker!!.year
                    tvDob!!.text = String.format(Locale.getDefault(), "%02d/%02d/%d", dayOfMonth, month + 1, year)
                    tvDob!!.error = null
                    dialog!!.dismiss()
                }
            }
            R.id.rbPersonMale -> rbFemale!!.error = null
            R.id.rbPersonFemale -> rbFemale!!.error = null
        }
    }

    /**
     * Builds parameter in json format to return it to the server
     *
     * @return json format in the mentioned format in docs
     */
    private fun buildParameter(): String {
        var parameterJson = ""
        val json = JSONObject()
        var gender = ""
        when (rgGender!!.checkedRadioButtonId) {
            R.id.rbPersonMale -> gender = "1"
            R.id.rbPersonFemale -> gender = "2"
        }
        parameterJson = try {
            json.put("TABLE", EX_STUDENT_USER)
            json.put("ID", CacheUserData(context!!).userDataFromCache.userId)
            json.put("FIRST_NAME", tvFName!!.text.toString().trim { it <= ' ' })
            json.put("LAST_NAME", tvLName!!.text.toString().trim { it <= ' ' })
            json.put("DOB", tvDob!!.text.toString().trim { it <= ' ' })
            json.put("GENDER", gender)
            json.put("MOBILE_NO", tvMobile!!.text.toString().trim { it <= ' ' })
            json.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
            return "ERROR"
        }
        return parameterJson
    }

    fun isValidatePhoneNo(phn: String?): Boolean {
        val check: Boolean
        val p: Pattern
        val m: Matcher
        val PHONE_STRING = "^[6789]\\d{9}$"
        p = Pattern.compile(PHONE_STRING)
        m = p.matcher(phn)
        check = m.matches()
        if (!check) {
            tvMobile!!.error = "Please enter a valid phone number!(10 digits only)"
        }
        return check
    }

    private val isValid: Boolean
        private get() {
            var status = true
            if (tvFName!!.text.toString() == "") {
                tvFName!!.error = "Enter first name"
                status = false
            }
            if (tvLName!!.text.toString() == "") {
                tvLName!!.error = "Enter last name"
                status = false
            }
            if (tvMobile!!.text.toString() == "") {
                tvMobile!!.error = "Enter mobile number"
                status = false
            } else {
                contact = tvMobile!!.text.toString().trim { it <= ' ' }
                if (!isValidatePhoneNo(contact)) status = false
            }
            if (rgGender!!.checkedRadioButtonId == -1) {
                rbFemale!!.error = "Select gender"
                status = false
            }
            if (tvDob!!.text.toString() == "") {
                tvDob!!.error = "Select date of birth"
                status = false
            }
            return status
        }

    fun loadData(parameterJson: String?) {
        try {
            val json = JSONObject(parameterJson)
            tvFName!!.text = json.getString("FIRST_NAME")
            tvLName!!.text = json.getString("LAST_NAME")
            tvMobile!!.text = json.getString("MOBILE_NO")
            if (json.getString("DOB") == "") {
                tvDob!!.text = ""
            } else {
                tvDob!!.text = json.getString("DOB")
            }
            when (json.getString("GENDER")) {
                "1" -> {
                    rbMale!!.isChecked = true
                    rbFemale!!.isChecked = false
                }
                "2" -> {
                    rbMale!!.isChecked = false
                    rbFemale!!.isChecked = true
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            Toast.makeText(context, "OOPS! Something went wrong", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) // Press Back Icon
        {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "PersonalDetails"
    }
}
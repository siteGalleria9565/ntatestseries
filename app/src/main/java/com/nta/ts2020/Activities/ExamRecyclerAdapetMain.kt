package com.nta.ts2020.Activities

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.nta.ts2020.Activities.ExamRecyclerAdapetMain.MyHolder
import com.nta.ts2020.R
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ExamRecyclerAdapetMain(var context: Context, dataList: ArrayList<*>) : RecyclerView.Adapter<MyHolder>() {
    var Keys = ArrayList<String>()
    var titleURL: String? = null
    var fortitleURL: String? = null
    var title: String? = null
    var videotitle: String? = null
    var authorname: String? = null
    var Position = 0
    var requestQueue: RequestQueue? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recycleradaptermain, parent, false)
        return MyHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        Position = position
        fortitleURL = "https://www.youtube.com/watch?v=" + Keys[position]
        titleURL = "http://www.youtube.com/oembed?url=$fortitleURL"
        requestQueue = Volley.newRequestQueue(context)
        val stringRequest = StringRequest(Request.Method.GET, titleURL, Response.Listener { response ->
            try {
                val jsonObject = JSONObject(response)
                title = jsonObject.getString("title")
                title = title?.substring(0, 1)?.toUpperCase() + title?.substring(1)?.toLowerCase()
                videotitle = title
                holder.titleTextView.text = title
                title = jsonObject.getString("thumbnail_url")
                Glide.with(context)
                        .load(title)
                        .into(holder.thumnilImageView)
            } catch (e: JSONException) {
                Log.e("json parse pro", "error")
                e.printStackTrace()
            }
        }, Response.ErrorListener { error -> Log.e("erorr::", error.toString()) })
        requestQueue?.add(stringRequest)
        holder.mCardView.setOnClickListener {
            val i = Intent(context, ExamYoutubePlay::class.java)
            i.putExtra("id", Keys[position])
            context.startActivity(i)
        }
    }

    override fun getItemCount(): Int {
        return Keys.size
    }

    inner class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var titleTextView: TextView
        var thumnilImageView: ImageView
        var mCardView: CardView

        init {
            titleTextView = itemView.findViewById(R.id.titlename)
            thumnilImageView = itemView.findViewById(R.id.thumnil)
            mCardView = itemView.findViewById(R.id.RecyclercardView)
        }
    }

    init {
        Keys = dataList as ArrayList<String>
    }
}
package com.nta.ts2020.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.GridView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.internal.ContextUtils.getActivity
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.nta.ts2020.Adapters.EBookListRecyclerAdapter
import com.nta.ts2020.Adapters.EBookListRecyclerAdapter.OnButtonClickListener
import com.nta.ts2020.AppConstants.ConstantVariables.NETWORK_ERROR
import com.nta.ts2020.AppConstants.ConstantVariables.NO_DATA
import com.nta.ts2020.AppConstants.ConstantVariables.SUCCESS
import com.nta.ts2020.BuildConfig
import com.nta.ts2020.LocalStorage.CacheEBook
import com.nta.ts2020.PojoClasses.EBookPojo
import com.nta.ts2020.R
import com.nta.ts2020.ThreadingHelper.Handlers.EBookListHandler
import com.nta.ts2020.ThreadingHelper.ThreadPool.ThreadPoolService
import org.json.JSONObject
import java.util.*

class EBooklistActivity : AppCompatActivity() {
    private var gridView: GridView? = null
    private var linLayoutNoData: LinearLayout? = null
    private var textViewMessage: TextView? = null
    private var textViewSubMessage: TextView? = null
    private var imageViewError: ImageView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var context: EBooklistActivity? = null
    private var subjectId: String? = null
    private var eBookType: String? = null
    private var subjectName: String? = null
    private var isOngoingDownload = false
    private var onComplete: BroadcastReceiver? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ebook)
        context = this
        subjectId = ""
        eBookType = ""
        if (intent.extras != null) {
            eBookType = intent.extras!!.getString("E_BOOK_TYPE")
            subjectName = intent.extras!!.getString("SUBJECT_NAME")
            subjectId = intent.extras!!.getString("SUBJECT_ID")
        }
        var jsonObject: JSONObject
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setTitle(subjectName)
        }
        gridView = findViewById<View>(R.id.gvEBookList) as GridView
        //Message Layout
        linLayoutNoData = findViewById<View>(R.id.llEBookListError) as LinearLayout
        textViewMessage = findViewById<View>(R.id.tvExamResultListMessage) as TextView
        textViewSubMessage = findViewById<View>(R.id.tvExamResultListSubMessage) as TextView
        imageViewError = findViewById<View>(R.id.ivExamResultListError) as ImageView
        swipeRefreshLayout = findViewById<View>(R.id.swRefreshEBookList) as SwipeRefreshLayout
        swipeRefreshLayout!!.setColorSchemeResources(
                R.color.material_blue_700,
                R.color.material_green_700,
                R.color.material_yellow_700,
                R.color.material_red_700)
        val eBookListHandler = EBookListHandler(context!!)
        if (isNetworkConnected) {
            swipeRefreshLayout!!.isRefreshing = true
            ThreadPoolService().saveEBookListFromServerToCache(context, subjectId, eBookListHandler, eBookType)
        }
        val finalSubjectId = subjectId
        swipeRefreshLayout!!.setOnRefreshListener {
            if (isNetworkConnected) {
                ThreadPoolService().saveEBookListFromServerToCache(context, finalSubjectId, eBookListHandler, eBookType)
            } else {
                loadData(SUCCESS)
            }
        }
        if (!isNetworkConnected) loadData(SUCCESS)
    }

    private val isNetworkConnected: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }

    fun loadData(status: Int) {
        //fetch data from cache
        var status = status
        val eBookPojoArrayList = context?.let { CacheEBook(it).getEBookListBySubjectId(subjectId, eBookType) }
        if (eBookPojoArrayList!!.isEmpty() && status != NETWORK_ERROR) status = NO_DATA
        when (status) {
            SUCCESS -> {
                linLayoutNoData!!.visibility = View.GONE
                gridView!!.visibility = View.VISIBLE
                val eBookListAdapter = EBookListRecyclerAdapter(eBookPojoArrayList, object : OnButtonClickListener {
                    override fun onDownloadButtonClick(eBookPojo: EBookPojo?, viewHolder: EBookListRecyclerAdapter.ViewHolder?) {
//                    if (checkAndRequestPermissions()) {
//                        if (isNetworkConnected) {
//                            var url = eBookPojo?.downloadUrl!!.replace(" ".toRegex(), "%20")
//
////                            Intent intent=new Intent(EBooklistActivity.this,ExamPdfOpenActivity.class);
////                            intent.putExtra("url",url);
////                            startActivity(intent);
//                            if (!isOngoingDownload) {
//                                isOngoingDownload = true
//                                url = eBookPojo.downloadUrl!!.replace(" ".toRegex(), "%20")
//                                val request = DownloadManager.Request(Uri.parse(url))
//                                request.setDescription("Pdf downloading")
//                                request.setTitle(eBookPojo.eBookPdfName)
//                                request.allowScanningByMediaScanner()
//                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
//                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOCUMENTS, eBookPojo.eBookPdfName+".pdf/")
//                                val manager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
//                                manager.enqueue(request)
//                                onComplete = object : BroadcastReceiver() {
//                                    override fun onReceive(ctxt: Context, intent: Intent) {
//                                        isOngoingDownload = false
//                                        val pdf = File(Environment.DIRECTORY_DOCUMENTS + eBookPojo.eBookPdfName+".pdf/")
////                                        if (pdf.isFile) {
//                                            viewHolder?.download?.visibility = View.GONE
//                                            viewHolder?.open?.visibility = View.VISIBLE
//                                            Toast.makeText(ctxt, "Download completed. Press open to view.", Toast.LENGTH_LONG).show()
//                                        //} else Toast.makeText(ctxt, "Download failed.", Toast.LENGTH_SHORT).show()
//                                    }
//                                }
//                                registerReceiver(onComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
//                            } else {
//                                Toast.makeText(context, "Downloading in progress already", Toast.LENGTH_SHORT).show()
//                            }
//                        } else {
//                            Toast.makeText(context, "File added to the queue. Please connect to the internet to download.", Toast.LENGTH_SHORT).show()
//                        }
//                    }
                    }

                    override fun onOpenButtonClick(eBookPojo: EBookPojo?) {

                        val builder = CustomTabsIntent.Builder()
                        val customTabsIntent = builder.build()
                        customTabsIntent.launchUrl(this@EBooklistActivity, Uri.parse(eBookPojo?.downloadUrl))
//                    var pdfName: String? = "Pdf View"
//                    if (checkAndRequestPermissions()) pdfName = if (eBookPojo?.eBookName == "null") {
//                        eBookPojo.eBookPdfName
//                    } else {
//                        eBookPojo?.eBookName
//                    }
//                    startActivity(Intent(context, ExamPdfViewActivity::class.java)
//                            .putExtra("PDF_NAME", pdfName)
//                            .putExtra("PDF", pdf.toString()))
                    }

                    override fun onViewInfoButtonClick(eBookPojo: EBookPojo?) {
                        val dialog = Dialog(context!!, R.style.Dialog_NoTitle)
                        dialog.setContentView(R.layout.dialog_e_book_info)
                        val eBookInfoPdfName = dialog.findViewById<View>(R.id.tvEBookInfoPdfName) as TextView
                        val eBookInfoName = dialog.findViewById<View>(R.id.tvEBookInfoBookName) as TextView
                        val eBookInfoAuthorName = dialog.findViewById<View>(R.id.tvEBookInfoAuthor) as TextView
                        val eBookInfoPublication = dialog.findViewById<View>(R.id.tvEBookInfoPublication) as TextView
                        eBookInfoPdfName.text = eBookPojo?.eBookPdfName
                        if (eBookPojo?.eBookName != "null") {
                            eBookInfoName.text = eBookPojo?.eBookName
                        } else {
                            eBookInfoName.text = "Updating"
                        }
                        if (eBookPojo?.eBookAuthor != "null") {
                            eBookInfoAuthorName.text = eBookPojo?.eBookAuthor
                        } else {
                            eBookInfoAuthorName.text = "Updating"
                        }
                        if (eBookPojo?.eBookPublication != "null") {
                            eBookInfoPublication.text = eBookPojo?.eBookPublication
                        } else {
                            eBookInfoPublication.text = "Updating"
                        }
                        dialog.show()
                    }
                })
                //        gridView.addItemDecoration(new QuizDividerItemDecoration(this));
                gridView!!.adapter = eBookListAdapter
            }
            NO_DATA -> {
                linLayoutNoData!!.visibility = View.VISIBLE
                gridView!!.visibility = View.GONE
                textViewMessage!!.setText(R.string.result_list_message_no_data)
                textViewSubMessage!!.setText(R.string.result_list_sub_message_no_data)
                imageViewError!!.setImageResource(R.drawable.ic_no_data)
            }
            NETWORK_ERROR -> {
                linLayoutNoData!!.visibility = View.VISIBLE
                gridView!!.visibility = View.GONE
                textViewMessage!!.setText(R.string.result_list_message_network_error)
                textViewSubMessage!!.setText(R.string.result_list_sub_message_network_error)
                imageViewError!!.setImageResource(R.drawable.ic_no_network)
            }
        }
        if (swipeRefreshLayout!!.isRefreshing) {
            swipeRefreshLayout!!.isRefreshing = false
        }
    }

    private fun checkAndRequestPermissions(): Boolean {
        val writePermission = ContextCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val listPermissionNeeded: MutableList<String> = ArrayList()
        if (writePermission != PackageManager.PERMISSION_GRANTED) listPermissionNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!listPermissionNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context!!, listPermissionNeeded.toTypedArray(), PERMISSION_REQUEST)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST -> //                HashMap<String, Integer> permissionResultPair = new HashMap<>();
                if (grantResults.size > 0) {
                    var isAllPermissionsGranted = true
                    for (grantResult in grantResults) {
//                        permissionResultPair.put(permissions[i],grantResults[i]);
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            isAllPermissionsGranted = false
                        }
                    }
                    if (!isAllPermissionsGranted) {
                        showSnackBar()
                    }
                }
        }
    }

    @SuppressLint("WrongConstant")
    private fun showSnackBar() {
        Snackbar.make(findViewById(R.id.ebookActivity), "Please grant proper permission from settings to proceed", BaseTransientBottomBar.LENGTH_INDEFINITE)
                .setAction("Settings") { // Build intent that displays the App settings screen.
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package",
                            BuildConfig.APPLICATION_ID, null)
                    intent.data = uri
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }.show()
    }

    override fun onStop() {
        super.onStop()
        try {
            if (onComplete != null) unregisterReceiver(onComplete)
        } catch (e: IllegalArgumentException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
        }
    }

    override fun onPause() {
        super.onPause()
        try {
            if (onComplete != null) unregisterReceiver(onComplete)
        } catch (e: IllegalArgumentException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    companion object {
        private const val PERMISSION_REQUEST = 0
        const val EXTERNAL_FOLDER_PATH = "/Android/data/com.nta.ts2020/"
    }
}
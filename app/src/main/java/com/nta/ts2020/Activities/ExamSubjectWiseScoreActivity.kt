package com.nta.ts2020.Activities

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.db.chart.animation.Animation
import com.db.chart.model.Bar
import com.db.chart.model.BarSet
import com.db.chart.renderer.AxisRenderer
import com.db.chart.renderer.XRenderer
import com.db.chart.renderer.YRenderer
import com.db.chart.view.BarChartView
import com.nta.ts2020.Adapters.ShowTableSubjectWiseDataAdapter
import com.nta.ts2020.LocalStorage.CacheResultList
import com.nta.ts2020.LocalStorage.ExamCacheFinalSubmission
import com.nta.ts2020.PojoClasses.FinalSubmissionPojo
import com.nta.ts2020.PojoClasses.ResultListPojo
import com.nta.ts2020.PojoClasses.ShowSubjectWiseDataInTablePojo
import com.nta.ts2020.R
import java.util.*

class ExamSubjectWiseScoreActivity : AppCompatActivity() {
    // Collections ....
    var finalSubmissionPojosList: ArrayList<FinalSubmissionPojo>? = null
    private var productList: ArrayList<ShowSubjectWiseDataInTablePojo>? = null
    var cacheResultList: ArrayList<ResultListPojo>? = null

    //Cache
    var cacheResultListInstance: CacheResultList? = null
    var examCacheFinalSubmissionInstance: ExamCacheFinalSubmission? = null

    //HashMap
    var subjectBasedOnId: HashMap<String, String>? = null
    var subjectWiseMarks: Float? = null
    var Percentage = 0f

    //TextView
    private var examName: TextView? = null
    private var totalMarksObtain: TextView? = null
    private var totalQuestion: TextView? = null
    private var totalExamTime: TextView? = null
    private var studentRank: TextView? = null
    private val TAG = "SubWiseScreActy"
    private var id: String? = null
    private var context: Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exam_activity_test_result)
        if (supportActionBar != null) {
            supportActionBar!!.title = "Subject Wise Score"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        //Context
        context = this
        initialization()
        id = null
        val extras = intent.extras
        if (extras != null) {
            id = extras.getString("ID")
        }


//        //Final Data Cache
        finalSubmissionPojosList = examCacheFinalSubmissionInstance!!.getFinalSubmissionData(id)
        cacheResultList = cacheResultListInstance!!.getResultListFromCache(id)
        val timeCalculate = finalSubmissionPojosList!![0].overAllTime.toInt()
        for (i in cacheResultList?.get(0)?.subjectIds!!.indices) subjectBasedOnId!![cacheResultList?.get(0)?.subjectIds!![i]] = cacheResultList?.get(0)?.subjectNames!![i]
        val subWiseMarksList = finalSubmissionPojosList!![0].subWiseMarks

/*
                String setTotalQuesData=totalRightAns+" "+totalAttemptQuestion+" "+totalSkipedQuestion+" "+totalRightMarks+" "+totalWrongMarks;
*/
        val hour = timeCalculate / 3600
        val minutes = timeCalculate / 60 % 60
        val second = timeCalculate % 60
        val timeStamp = String.format(Locale.getDefault(), "%02d:%02d:%02d", hour, minutes, second)
        val totalQuesData = finalSubmissionPojosList!![0].totalQuesData!!.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        examName!!.text = cacheResultList?.get(0)?.examName

        /*
        * Show Table subject wise data
        * */productList = ArrayList()
        val lview = findViewById<View>(R.id.listview) as ListView
        val showTableSubjectWiseDataAdapterInstance = ShowTableSubjectWiseDataAdapter(this, productList!!)
        for (i in 1 until finalSubmissionPojosList!![0].subjectIds!!.size + 1) {
            //Set table Value
            subjectWiseMarks = java.lang.Float.valueOf(finalSubmissionPojosList!![0].subWiseMarks!![i - 1])
            val subjectWiseMaxMarks = finalSubmissionPojosList!![0].subWiseMaxMarks!![i - 1].toInt()
            Percentage = subjectWiseMarks!! / subjectWiseMaxMarks * 100
            subjectBasedOnId!![finalSubmissionPojosList!![0].subjectIds!![i - 1]]?.let { ShowSubjectWiseDataInTablePojo(it, subjectWiseMarks.toString(), "$Percentage%") }?.let { productList!!.add(it) }
        }
        lview.adapter = showTableSubjectWiseDataAdapterInstance
        showTableSubjectWiseDataAdapterInstance.notifyDataSetChanged()
        totalMarksObtain!!.text = java.lang.String.valueOf(finalSubmissionPojosList!![0].totalObtainedMarks)
        totalQuestion!!.text = java.lang.String.valueOf(finalSubmissionPojosList!![0].totalQues)
        totalExamTime!!.text = timeStamp
        studentRank!!.text = "Updating"


        //Show table
//        BarChart mChart = (BarChart) findViewById(R.id.chart1);
//        mChart.setPinchZoom(false);
//
//        mChart.setDrawBarShadow(false);
//        mChart.setDrawGridBackground(false);
//        Description description = new Description();
//        description.setText("Subject Wise Marks");
//        mChart.setDescription(description);
//
//
//        ArrayList<BarEntry> yVals1 = new ArrayList<>();
//        String[] subjectNames = new String[finalSubmissionPojosList.get(0).getSubjectIds().size()];
//
//        for (int i = 0; i < finalSubmissionPojosList.get(0).getSubjectIds().size(); i++) {
//            //Set Questions According to section
//            subjectWiseMarks = Float.valueOf(finalSubmissionPojosList.get(0).getSubWiseMarks().get(i));
//            int subjectWiseMaxMarks = Integer.parseInt(finalSubmissionPojosList.get(0).getSubWiseMaxMarks().get(i));
//
//
//            Percentage = (subjectWiseMarks / subjectWiseMaxMarks) * 100;
//
//            //set Chart Value
//            subjectNames[i] = subjectBasedOnId.get(finalSubmissionPojosList.get(0).getSubjectIds().get(i));
//            yVals1.add(new BarEntry(i, Percentage));
//        }
//
//
//        XAxis xAxis = mChart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.TOP);
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(subjectNames));
//        xAxis.setDrawGridLines(false);
//
//        mChart.getAxisLeft().setDrawGridLines(false);
//
//        mChart.animateY(2500);
//        mChart.getLegend().setEnabled(true);
//
//
//        BarDataSet set1;
//
//        if (mChart.getData() != null &&
//                mChart.getData().getDataSetCount() > 0) {
//            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
//            set1.setValues(yVals1);
//            mChart.getData().notifyDataChanged();
//            mChart.notifyDataSetChanged();
//        } else {
//            set1 = new BarDataSet(yVals1, "Subjects");
//            set1.setLabel("asdasd");
//            set1.setStackLabels(new String[]{"Births", "Divorces", "Marriages"});
//            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
//            set1.setDrawValues(true);
//
//            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
//            dataSets.add(set1);
//
//            BarData data = new BarData(dataSets);
//            mChart.setData(data);
//            mChart.setFitBars(true);
//        }
        val mChart = findViewById<View>(R.id.chart1) as BarChartView
        mChart.setBarSpacing(20f)
        val barSet = BarSet()
        val colors = resources.getIntArray(R.array.color_array)
        for (i in finalSubmissionPojosList!![0].subjectIds!!.indices) {
            //Set Questions According to section
            subjectWiseMarks = java.lang.Float.valueOf(finalSubmissionPojosList!![0].subWiseMarks!![i])
            val subjectWiseMaxMarks = finalSubmissionPojosList!![0].subWiseMaxMarks!![i].toInt()
            Percentage = subjectWiseMarks!! / subjectWiseMaxMarks * 100

            //set Chart Value
            val tempSubjectName = subjectBasedOnId!![finalSubmissionPojosList!![0].subjectIds!![i]]!!.split(" ".toRegex()).toTypedArray()
            val subjectName = tempSubjectName[0]
            val bar = Bar(subjectName, Percentage)
            bar.color = colors[i]
            barSet.addBar(bar)
        }
        mChart.addData(barSet)
        mChart.setXLabels(AxisRenderer.LabelPosition.OUTSIDE)
                .setYLabels(AxisRenderer.LabelPosition.OUTSIDE)
                .show(Animation())
    }

    //    public void AddValuesToBARENTRY() {
    //
    //
    //        for (int i = 1; i < finalSubmissionPojosList.get(0).getSubjectIds().size() + 1; i++) {
    //
    //            //Set Questions According to section
    //            subjectWiseMarks = Float.valueOf(finalSubmissionPojosList.get(0).getSubWiseMarks().get(i - 1));
    //            int subjectWiseMaxMarks = Integer.parseInt(finalSubmissionPojosList.get(0).getSubWiseMaxMarks().get(i - 1));
    //
    //
    //            Percentage = (subjectWiseMarks / subjectWiseMaxMarks) * 100;
    //
    //            //Set table Value
    //            productList.add(new ShowSubjectWiseDataInTablePojo(subjectBasedOnId.get(finalSubmissionPojosList.get(0).getSubjectIds().get(i - 1)), String.valueOf(subjectWiseMarks), Percentage + "%"));
    //
    //            //set Chart Value
    ////            BarEntryLabels.add(subjectBasedOnId.get(finalSubmissionPojosList.get(0).getSubjectIds().get(i - 1)));
    ////            BARENTRY.add(new BarEntry(Percentage, i - 1));
    //        }
    //
    //    }
    fun initialization() {
        examCacheFinalSubmissionInstance = ExamCacheFinalSubmission(this)
        cacheResultListInstance = CacheResultList(this)
        examName = findViewById<View>(R.id.ExamName) as TextView

        //Text view initialize
        totalMarksObtain = findViewById<View>(R.id.totalMarksObtain) as TextView
        totalQuestion = findViewById<View>(R.id.totalExamQuestion) as TextView
        totalExamTime = findViewById<View>(R.id.totalExamTime) as TextView
        studentRank = findViewById<View>(R.id.studentRank) as TextView

        //HashMa
        subjectBasedOnId = HashMap()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}